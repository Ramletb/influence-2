/*
  ___ _   _ _____ _    _   _ _____ _   _  ____ _____ 
 |_ _| \ | |  ___| |  | | | | ____| \ | |/ ___| ____|
  | ||  \| | |_  | |  | | | |  _| |  \| | |   |  _|  
  | || |\  |  _| | |__| |_| | |___| |\  | |___| |___ 
 |___|_| \_|_|   |_____\___/|_____|_| \_|\____|_____|

*/

/**
 * Image Cellrenderer
 *
 *
 */
qx.Class.define("ic.ui.cellrenderer.Image", {
	extend : qx.ui.table.cellrenderer.AbstractImage,

	/**
	 * Constructor
	 *
	 * 
	 *
	 * @param maxWidth {integer} max width
	 * @param maxHeight {Integer} max height
	 */
	construct: function(maxWidth, maxHeight) {
		this.base(arguments);

		this.__imageWidth  = this.__maxWidth  = maxWidth;
		this.__imageHeight = this.__maxHeight = maxHeight;

	},

	members: {

		__imageHeight : 16,
		__imageWidth : 16,
		__maxHeight : 16,
		__maxWidth : 16,

		// overridden
		_identifyImage : function(cellInfo) {
			var imageHints = {
				imageWidth  : this.__imageWidth,
				imageHeight : this.__imageHeight
			};


			if (cellInfo.value < 1) {
				imageHints.url = null;
			} else {
				imageHints.url = cellInfo.value;
			}

			imageHints.tooltip = cellInfo.tooltip;

			return imageHints;
		},

		// overridden
		_getContentHtml : function(cellInfo) {
			var content = "<div></div>";

			var imageData = this._getImageInfos(cellInfo);


			// set image
			if (imageData.url) {
				//content = qx.bom.element.Decoration.create(imageData.url, "scale", styleMap);
				content = '<div><img src="' + imageData.url + '" style="max-height: ' + this.__imageWidth + 'px; max-width: ' + this.__imageHeight + 'px; padding: 5px" /></div>';
			}

			return content;
		},


		/**
		 * Overridden; called whenever the cell updates.
		 *
		 *  
		 *
		 * @param cellInfo {Map} The information about the cell.
		 * @return {String}
		 */
		_getCellStyle: function(cellInfo) {
			return this.base(arguments, cellInfo) + ' background-color: transparent; ';
		}

	},

	destruct : function() {
	}
});

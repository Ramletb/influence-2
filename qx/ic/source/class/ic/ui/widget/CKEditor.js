/*
  ___ _   _ _____ _    _   _ _____ _   _  ____ _____ 
 |_ _| \ | |  ___| |  | | | | ____| \ | |/ ___| ____|
  | ||  \| | |_  | |  | | | |  _| |  \| | |   |  _|  
  | || |\  |  _| | |__| |_| | |___| |\  | |___| |___ 
 |___|_| \_|_|   |_____\___/|_____|_| \_|\____|_____|

*/

/* ************************************************************************

#ignore(CKEDITOR)

************************************************************************ */

/**
 * CKEditor
 * 
 *
 */
qx.Class.define("ic.ui.widget.CKEditor", {
	extend: qx.ui.form.TextArea,

	/**
	* Constructor
	* 
	* @lint ignoreUndefined(CKEDITOR)
	*/
	construct: function(value) {

		this.base(arguments, value);
		this.addListenerOnce("appear", function(e) {
			var b = this.getBounds();

			var el = this.getContentElement().getDomElement();
			//var hint = this.getSizeHint();

			this.__ckEditor = CKEDITOR.replace(el, {
				//skin              : "qx",
				height            : b.height - 140,
				width             : b.width,
				resize_enabled    : false,
				uiColor           : "#dddddd",
				baseHref          : "http://www.metrolinktrains.com/",
				tabIndex          : this.getTabIndex(),
				toolbar           : [
					['Source','-','Preview','-','Templates'],
					['Cut','Copy','Paste','PasteText','PasteFromWord'],
					['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
					['Image','Flash','Table','HorizontalRule','SpecialChar'],
					'/',
					['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
					['NumberedList','BulletedList','-','Outdent','Indent','Blockquote','CreateDiv'],
					['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
					['Link','Unlink','Anchor'],
					['TextColor','BGColor'],
					['Styles','Format','Font','FontSize']
				]
			});

			this.addListener("resize", function(e) {
				var b = this.getBounds();
				this.__ckEditor.resize(b.width, b.height - 5);
			}, this);

		}, this);


	},

	properties: {

		appearance: {
			refine: true,
			init: "widget"
		}

	},

	members: {

		__ckEditor: null,

		/**
		 * Get Content
		 *
		 * 
		 *
		 */
		getContent: function() {
			if (this.__ckEditor) {
				return this.__ckEditor.getSnapshot();
			}
		},

		/**
		 * Set Content
		 *
		 * 
		 *
		 * @param content {String} html content
		 */
		setContent: function(content) {
			this.__ckEditor.setData(content);
		}

	}

});

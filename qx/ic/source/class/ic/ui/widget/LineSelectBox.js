/*
  ___ _   _ _____ _    _   _ _____ _   _  ____ _____ 
 |_ _| \ | |  ___| |  | | | | ____| \ | |/ ___| ____|
  | ||  \| | |_  | |  | | | |  _| |  \| | |   |  _|  
  | || |\  |  _| | |__| |_| | |___| |\  | |___| |___ 
 |___|_| \_|_|   |_____\___/|_____|_| \_|\____|_____|

*/

/* ************************************************************************

#asset(ic/line_av.png)
#asset(ic/line_bbh.png)
#asset(ic/line_ieoc.png)
#asset(ic/line_oc.png)
#asset(ic/line_r.png)
#asset(ic/line_sb.png)
#asset(ic/line_v.png)
#asset(ic/line_91.png)

************************************************************************ */

/**
 * News Editor Window
 * 
 *
 */
qx.Class.define("ic.ui.widget.LineSelectBox", {
	extend: qx.ui.form.SelectBox,

	construct: function() {
		this.base(arguments);

		this.add(new qx.ui.form.ListItem("Antelope Valley", "ic/line_av.png", "Antelope Valley Line"));
		this.add(new qx.ui.form.ListItem("Burbank-Bob Hope", "ic/line_bbh.png", "Burbank-Bob Hope Line"));
		this.add(new qx.ui.form.ListItem("Inland Empire - Orange County", "ic/line_ieoc.png", "Inland Empire-Orange County Line"));
		this.add(new qx.ui.form.ListItem("Orange County", "ic/line_oc.png", "Orange County Line"));
		this.add(new qx.ui.form.ListItem("Riverside", "ic/line_r.png", "Riverside Line"));
		this.add(new qx.ui.form.ListItem("San Bernardino", "ic/line_sb.png", "San Bernardino Line"));
		this.add(new qx.ui.form.ListItem("Ventura", "ic/line_v.png", "Ventura Line"));
		this.add(new qx.ui.form.ListItem("91", "ic/line_91.png", "91 Line"));

	},

	members: {

	}

});

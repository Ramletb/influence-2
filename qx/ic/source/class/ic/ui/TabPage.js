/*
  ___ _   _ _____ _    _   _ _____ _   _  ____ _____ 
 |_ _| \ | |  ___| |  | | | | ____| \ | |/ ___| ____|
  | ||  \| | |_  | |  | | | |  _| |  \| | |   |  _|  
  | || |\  |  _| | |__| |_| | |___| |\  | |___| |___ 
 |___|_| \_|_|   |_____\___/|_____|_| \_|\____|_____|

*/

/**
 * Container
 * 
 *
 */
qx.Class.define("ic.ui.TabPage", {
	extend: qx.ui.tabview.Page,

	construct: function(title) {
		this.base(arguments, title);

		// layout defaults
		var layout = new qx.ui.layout.Grid();
		layout.setColumnFlex(0, 1);
		layout.setRowFlex(1, 1);

		this.setLayout(layout);
		this.setAllowGrowY(true);

	},

	members: {

	}

});

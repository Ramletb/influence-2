/*
  ___ _   _ _____ _    _   _ _____ _   _  ____ _____ 
 |_ _| \ | |  ___| |  | | | | ____| \ | |/ ___| ____|
  | ||  \| | |_  | |  | | | |  _| |  \| | |   |  _|  
  | || |\  |  _| | |__| |_| | |___| |\  | |___| |___ 
 |___|_| \_|_|   |_____\___/|_____|_| \_|\____|_____|

*/

/**
 * Window
 * 
 *
 */
qx.Class.define("ic.ui.Window", {
	extend: qx.ui.window.Window,
	type: "abstract",

	construct: function(caption, icon) {
		this.base(arguments, caption, icon);

		// window defaults
		//
		this.setShowMinimize(false);
		this.setHeight(400);
		this.setWidth(600);
		this.setContentPadding(0);

		// layout defaults
		var layout = new qx.ui.layout.Grid();
		layout.setRowFlex(1, 1);
		layout.setColumnFlex(0, 1);
		this.setLayout(layout);

	},

	members: {

		/**
		 * open
		 *
		 * 
		 *
		 */
		open: function() {
			arguments.callee.base.apply(this, arguments);
			// add the window to the desktop
			ic.Desktop.getInstance().addWindow(this);
		},

		/**
		 * close
		 *
		 * 
		 *
		 */
		close: function() {
			arguments.callee.base.apply(this, arguments);
			// remove the window from the desktop
			ic.Desktop.getInstance().removeWindow(this);
		}

	}

});

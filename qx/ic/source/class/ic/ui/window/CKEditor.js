/*
  ___ _   _ _____ _    _   _ _____ _   _  ____ _____ 
 |_ _| \ | |  ___| |  | | | | ____| \ | |/ ___| ____|
  | ||  \| | |_  | |  | | | |  _| |  \| | |   |  _|  
  | || |\  |  _| | |__| |_| | |___| |\  | |___| |___ 
 |___|_| \_|_|   |_____\___/|_____|_| \_|\____|_____|

*/

/* ************************************************************************

#asset(ic/save.png)

************************************************************************ */

/**
 * CKEditor Window
 * 
 *
 */
qx.Class.define("ic.ui.window.CKEditor", {
	extend: ic.ui.Window,

	construct: function(caption, icon, content) {
		this.base(arguments, caption, icon);

		this.setHeight(500);
		this.setWidth(650);

		var pane = this.getChildControl("pane");
		pane.setBackgroundColor("#f2f2f2");

		var tb = this.getToolbar();
		this.add(tb, { row: 0, column: 0 });

		this.__editor = new ic.ui.widget.CKEditor(content);
		this.add(this.__editor, { row: 1, column: 0 });
	},

	members: {

		__editor: null,

		__toolbar: null,

		/**
		 * Set Content
		 *
		 * 
		 *
		 * @param content {String} html content
		 */
		setContent: function(content) {
			this.__editor.setContent(content);
		},

		/**
		 * get Toolbar
		 *
		 * 
		 *
		 */
		getToolbar: function() {

			// toolbar
			//
			var mainPart   = new qx.ui.toolbar.Part;
			var saveButton = new qx.ui.toolbar.Button("Save", "ic/save.png");
			saveButton.addListener("execute", function() {
			  //console.log(this.__editor.getContent());
			}, this);

			mainPart.add(saveButton);

			this.__toolbar = new qx.ui.menubar.MenuBar();
			this.__toolbar.add(mainPart);

			return this.__toolbar;
		}

	}

});

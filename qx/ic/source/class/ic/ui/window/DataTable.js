/*
  ___ _   _ _____ _    _   _ _____ _   _  ____ _____ 
 |_ _| \ | |  ___| |  | | | | ____| \ | |/ ___| ____|
  | ||  \| | |_  | |  | | | |  _| |  \| | |   |  _|  
  | || |\  |  _| | |__| |_| | |___| |\  | |___| |___ 
 |___|_| \_|_|   |_____\___/|_____|_| \_|\____|_____|

*/

/* ************************************************************************

#asset(ic/arrow_refresh.png)

************************************************************************ */

/**
 * Data Window
 * 
 *
 */
qx.Class.define("ic.ui.window.DataTable", {
	extend: ic.ui.Window,

	construct: function(caption, icon) {
		this.base(arguments, caption, icon);

		this.add(this.getToolbar(), { row: 0, column: 0 });
		this.add(this.getTable(),   { row: 1, column: 0 });
	},

	members: {

		__toolbar: null,

		__table: null,

		__tableModel: null,

		/**
		 * Get Table
		 *
		 * 
		 *
		 */
		getTable: function() {
			if(!this.__table) {
				this.__table = new qx.ui.table.Table(this.getTableModel(), this.getTableOptions());
			} 

			return this.__table;
		},

		/**
		 * Get Table Model
		 *
		 * 
		 *
		 */
		getTableModel: function() {
			return this.__tableModel;
		},

		/**
		 * Get Table Column Model
		 *
		 * 
		 *
		 */
		getTableColumnModel: function() {
			return this.__table.getTableColumnModel();
		},

		/**
		 * Table Opts
		 *
		 * 
		 *
		 */
		getTableOptions: function() {
			return {
				tableColumnModel : function(obj) {
					return new qx.ui.table.columnmodel.Resize(obj);
				}
			};
		},

		/**
		 * get Toolbar
		 *
		 * 
		 *
		 */
		getToolbar: function() {

			// toolbar
			//
			var mainPart   = new qx.ui.toolbar.Part;
			var refreshButton = new qx.ui.toolbar.Button("Refresh", "ic/arrow_refresh.png");
			refreshButton.addListener("execute", function(e) {
				this.getTable().getTableModel().reloadData();
			}, this);

			mainPart.add(refreshButton);

			this.__toolbar = new qx.ui.menubar.MenuBar();
			this.__toolbar.add(mainPart);

			return this.__toolbar;
		}

	}

});

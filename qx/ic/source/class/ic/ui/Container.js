/*
  ___ _   _ _____ _    _   _ _____ _   _  ____ _____ 
 |_ _| \ | |  ___| |  | | | | ____| \ | |/ ___| ____|
  | ||  \| | |_  | |  | | | |  _| |  \| | |   |  _|  
  | || |\  |  _| | |__| |_| | |___| |\  | |___| |___ 
 |___|_| \_|_|   |_____\___/|_____|_| \_|\____|_____|

*/

/**
 * Container
 * 
 *
 */
qx.Class.define("ic.ui.Container", {
	extend: qx.ui.container.Composite,

	construct: function() {
		this.base(arguments);

		// default row 1 to flex since it seems more often that 
		// row 0 is used for a toolbar and row 1 for content.

		// layout defaults
		var layout = new qx.ui.layout.Grid();
		layout.setRowFlex(1, 1);
		layout.setColumnFlex(0, 1);
		this.setLayout(layout);
		this.setDecorator("main");

	},

	members: {

	}

});

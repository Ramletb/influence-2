/*
  ___ _   _ _____ _    _   _ _____ _   _  ____ _____ 
 |_ _| \ | |  ___| |  | | | | ____| \ | |/ ___| ____|
  | ||  \| | |_  | |  | | | |  _| |  \| | |   |  _|  
  | || |\  |  _| | |__| |_| | |___| |\  | |___| |___ 
 |___|_| \_|_|   |_____\___/|_____|_| \_|\____|_____|

*/

/**
 * Content Table
 * 
 *
 */
qx.Class.define("ic.app.page.widget.ContentTable", {
	extend: qx.ui.table.Table,

	construct: function() {
		this.base(arguments, null, {
			tableColumnModel : function(obj) {
				return new qx.ui.table.columnmodel.Resize(obj);
			}});


		this.setEnabled(false);

		var selectionModel = qx.ui.table.selection.Model;
		this.getSelectionModel().setSelectionMode(selectionModel.MULTIPLE_INTERVAL_SELECTION);

	},

	events: {
		"load": "qx.event.type.Event"
	},

	members: {

		/**
		* Get Table Model
		*/
		populate: function(id) {
			var tm = ic.service.AdminModelFactory.getModel("VwPageContent");
			tm.fwApi({ "where": [ { 'page_id': id } ] });
			tm.sortByColumn(0, true);

			this.setTableModel(tm);
			this.setEnabled(true);
			this.fireEvent("load");

			var tcm = this.getTableColumnModel();
			tcm.setColumnVisible(0, false);

			var resizeBehavior = tcm.getBehavior();
			resizeBehavior.set(1, { width:"100", minWidth:100,  maxWidth:100 });
		}

	}


});

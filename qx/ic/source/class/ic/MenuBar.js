/*
  ___ _   _ _____ _    _   _ _____ _   _  ____ _____ 
 |_ _| \ | |  ___| |  | | | | ____| \ | |/ ___| ____|
  | ||  \| | |_  | |  | | | |  _| |  \| | |   |  _|  
  | || |\  |  _| | |__| |_| | |___| |\  | |___| |___ 
 |___|_| \_|_|   |_____\___/|_____|_| \_|\____|_____|

*/

/* ************************************************************************

#asset(ic/vtunnel_logo.png)
#asset(ic/exclamation.png)
#asset(ic/drive_network.png)
#asset(ic/cog.png)
#asset(ic/page_white_text.png)

************************************************************************ */

/**
 * Desktop
 * 
 *
 */
qx.Class.define("ic.MenuBar", {
	extend: qx.ui.menubar.MenuBar,

	construct: function() {

		this.base(arguments);

		var mainPart = new qx.ui.toolbar.Part;
		this.add(mainPart);

		mainPart.add(this.__getConfigurationMenu());

		this.addSpacer();

		var userPart = new qx.ui.toolbar.Part;
		this.add(userPart);

		userPart.add(this.__getUserMenu());

	},

	members: {

		/**
		 * User Menu
		 *
		 */
		__getUserMenu: function() {
			var menu = new qx.ui.menu.Menu();
			var menuButton = new qx.ui.toolbar.MenuButton("User", "ic/user.png", menu);

			var logoutButton = new qx.ui.menu.Button("Logout", "ic/exclamation.png");
			menu.add(logoutButton);

			logoutButton.addListener("execute", function(e) {
				window.location.href = "/admin/app/logout/1/t/" + (new Date).getTime();
			}, this);

			return menuButton;
		},

		/**
		 * Configuration
		 *
		 */
		__getConfigurationMenu: function() {
			var menu = new qx.ui.menu.Menu();
			var menuButton = new qx.ui.toolbar.MenuButton("Configure", null, menu);

			var pagesButton = new qx.ui.menu.Button("Pages", "ic/page_white_text.png");
			menu.add(pagesButton);

			pagesButton.addListener("execute", function(e) {
				var win = new ic.app.page.window.DataTable();
				win.open();
			}, this);

			return menuButton;
		}

	}

});

/*
  ___ _   _ _____ _    _   _ _____ _   _  ____ _____ 
 |_ _| \ | |  ___| |  | | | | ____| \ | |/ ___| ____|
  | ||  \| | |_  | |  | | | |  _| |  \| | |   |  _|  
  | || |\  |  _| | |__| |_| | |___| |\  | |___| |___ 
 |___|_| \_|_|   |_____\___/|_____|_| \_|\____|_____|

*/

/* ************************************************************************

#asset(ic/*)

************************************************************************ */

/**
 * Admin
 *
 * Version 0.1
 *
 */
qx.Class.define("ic.Application", {
	extend : qx.application.Standalone,

	members : {

		main : function() {
			// Call super class
			this.base(arguments);

			// Enable logging in debug variant
			if (qx.core.Environment.get("qx.debug")) {
				// support native logging capabilities, e.g. Firebug for Firefox
				qx.log.appender.Native;
				// support additional cross-browser console. Press F7 to toggle visibility
				qx.log.appender.Console;
			}

			// Create the desktop
			var desktop = ic.Desktop.getInstance();

			// Document is the application root
			var doc = this.getRoot();

			// Add button to document at fixed coordinates
			doc.add(desktop, { width: "100%", height: "100%" });
		}
	}
});

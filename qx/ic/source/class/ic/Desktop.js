/*
  ___ _   _ _____ _    _   _ _____ _   _  ____ _____ 
 |_ _| \ | |  ___| |  | | | | ____| \ | |/ ___| ____|
  | ||  \| | |_  | |  | | | |  _| |  \| | |   |  _|  
  | || |\  |  _| | |__| |_| | |___| |\  | |___| |___ 
 |___|_| \_|_|   |_____\___/|_____|_| \_|\____|_____|

*/

/* ************************************************************************

#asset(ic/influence_light_20x93.png)

************************************************************************ */

/**
 * Desktop
 * 
 *
 */
qx.Class.define("ic.Desktop", {
	extend: qx.ui.container.Composite,
	type: "singleton",

	construct: function() {

		this.base(arguments);

		// layout
		var layout = new qx.ui.layout.Grid();
		layout.setColumnFlex(0, 1);
		layout.setRowFlex(2, 1);
		this.setLayout(layout);

		// Header
		var layout = new qx.ui.layout.HBox(); 
		var header = new qx.ui.container.Composite(layout);
		header.setAppearance("vt-app-header");
		this.add(header, { row: 0, column: 0 });


		// Logo
		var logo = new qx.ui.basic.Image("ic/influence_light_20x93.png");
		header.add(logo);

		// Version
		var version = new qx.ui.basic.Label(" v0.2").set({
			font: qx.bom.Font.fromString("12px sans-serif"),
			paddingTop: 6,
			paddingRight: 4
		});

		header.add(new qx.ui.core.Spacer, {flex : 1});
		header.add(version);

		this.add(new ic.MenuBar, { row: 1, column: 0 });

		// Desktop
		this.__dm      = new qx.ui.window.Manager();
		this.__desktop = new qx.ui.window.Desktop(this.__dm);
		this.add(this.__desktop, { row: 2, column: 0 });

	},

	members: {

		__dm: null,

		__desktop: null,

		/**
		 * Add Window
		 *
		 * 
		 *
		 * @param win {mt.ui.Window} a window
		 */
		addWindow: function(win) {
			this.__desktop.add(win);
			this.__positionWindow(win);
		},

		/**
		 * Remove Window
		 *
		 * 
		 *
		 * @param win {mt.ui.Window} a window
		 */
		removeWindow: function(win) {
			this.__desktop.remove(win);
		},

		/**
		 * Get Manager
		 *
		 * 
		 *
		 */
		getManager: function() {
			return this.__dm;
		},

		__nextX: 20,

		__nextY: 20,

		/**
		 * Positiion Window
		 *
		 *
		 * @param win {mt.ui.Window} a window
		 */
		__positionWindow: function(win) {
			win.moveTo(this.__nextX,this.__nextY);

			this.__nextX = this.__nextX + 10;
			this.__nextY = this.__nextY + 10;
		}

	}

});

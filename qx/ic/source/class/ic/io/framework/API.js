/*
  ___ _   _ _____ _    _   _ _____ _   _  ____ _____ 
 |_ _| \ | |  ___| |  | | | | ____| \ | |/ ___| ____|
  | ||  \| | |_  | |  | | | |  _| |  \| | |   |  _|  
  | || |\  |  _| | |__| |_| | |___| |\  | |___| |___ 
 |___|_| \_|_|   |_____\___/|_____|_| \_|\____|_____|

*/

/**
 * Framework API
 * 
 *
 */
qx.Class.define("ic.io.framework.API", {
	type: "static",

	statics: {

		/**
		 * Encode
		 *
		 * Encode a Map to a Framework API string (URL style)
		 * If there is something to encode a string will be returned with
		 * a leading trailing slashes.
		 *
		 * @param api {Map} api configuration map
		 * @return {String} url string
		 */
		encode: function(api) {
			var a = '';

			for (var st in api) {
				if(api.hasOwnProperty(st)) {
					a = a + '/' + st + '/';

					if(api[st] instanceof Array) {

						var args = [];
						for (var i = 0; i < api[st].length; i++) {
							for (var at in api[st][i]) {
								args.push(at + '=' + api[st][i][at].replace(/\./g, '%2E'));
							}
						}
						a = a + escape(args.join(','));

					} else {
						a = a + escape(api[st].toString());
					}

				}
			}

			return a;
		}

	}

});

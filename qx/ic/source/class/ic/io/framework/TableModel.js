/*
  ___ _   _ _____ _    _   _ _____ _   _  ____ _____ 
 |_ _| \ | |  ___| |  | | | | ____| \ | |/ ___| ____|
  | ||  \| | |_  | |  | | | |  _| |  \| | |   |  _|  
  | || |\  |  _| | |__| |_| | |___| |\  | |___| |___ 
 |___|_| \_|_|   |_____\___/|_____|_| \_|\____|_____|

*/

/**
 * Framework Table Model
 *
 *
 */
qx.Class.define("ic.io.framework.TableModel", {
	extend: qx.ui.table.model.Remote,

	/**
	 * Constructor
	 *
	 * 
	 *
	 * @param serviceUrl {String}   framework URL
	 * @param modelName  {String}   model name
	 * @param colMap       {Map}    map column name
	 */
	construct : function(serviceUrl, modelName, colMap) {
		this.base(arguments);

		this.__modelName  = modelName;
		this.__serviceUrl = serviceUrl;
		this.__search     = '';
		this.__api        = '';
		this.__cols       = [];

		for(var c in colMap) {
			this.__cols.push(c);
		}

		this.setColumns(this.__cols);
		this.setColumnNamesById(colMap);
	},

	members: {

		/**
		 * Model Name
		 *
		 * 
		 *
		 */
		__modelName: null,

		/**
		 * Service URL
		 *
		 * 
		 *
		 */
		__serviceUrl: null,

		/**
		 * Columns
		 *
		 * 
		 *
		 */
		__cols: null,

		/**
		 * Search
		 *
		 * 
		 *
		 */
		__search: null,

		/**
		 * API Map
		 *
		 * 
		 *
		 */
		__api: null,

		/**
		 * Get Block Size
		 *
		 * Number of records per block.
		 *
		 */
		getBlockSize: function() {
			return 50;
		},

		/**
		 * Max Cached Block Count
		 *
		 * Number of blocks to cache.
		 *
		 */
		maxCachedBlockCount: function() {
			return 10;
		},

		/**
		 * Set Search
		 *
		 * 
		 *
		 * @param type {String} search type
		 * @param col {String} search column
		 * @param val {String|Integer} search value
		 */
		setSearch: function(type, col, val) {
			if(val == '') {
				this.__search = '';
			} else {
				this.__search = '/' + type + '/' + escape(col) + '=' + escape(val);
			} 
		},

		/**
		 * Clear Search
		 *
		 * 
		 *
		 */
		clearSearch: function() {
			this.__search = '';
		},

		/**
		 * Framework API
		 *
		 * Converts an API Map to URL notation
		 *
		 * @param apiConfig {Map} framework api configuration map
		 */
		fwApi: function(apiConfig) {
			var apiString = '';

			for (var st in apiConfig) {
				apiString = apiString + '/' + st + '/';
				var args = [];
				for (var i = 0; i < apiConfig[st].length; i++) {
					for (var a in apiConfig[st][i]) {
						args.push(a + '=' + apiConfig[st][i][a]);
					}
				}
				apiString = apiString + escape(args.join(','));
			}
			this.__api = apiString;
		},

		/**
		 * Load Row Count
		 *
		 * 
		 *
		 */
		_loadRowCount: function() {

			// HACK: Get the row count
			var url = this.__serviceUrl + "getModelData/model/" + this.__modelName + this.__search + this.__api + "/perPage/0/page/0.json";
			var req = new qx.io.remote.Request(url, 'GET', 'application/json');
			req.setTimeout(6000);
			
			req.setRequestHeader('x-framework-json-request', 'PLAINJSON');
			req.addListener('completed', function(r) {
				var data = r.getContent();
				this._onRowCountLoaded(data.totalRecords);
			}, this);

			req.send();
		},

		/**
		 * Load Row Data
		 *
		 * 
		 *
		 * @param firstRow {Integer} first row
		 * @param lastRow {Integer} last row
		 */
		_loadRowData: function(firstRow, lastRow) {
			var pageNum = 1;

			if(firstRow > 1) {
				pageNum = Math.ceil(firstRow / this.getBlockSize()) + 1;
			}

			var sortIndex = this.getSortColumnIndex();
			var sortOrder =  this.isSortAscending() ? "asc" : "desc";

			var sort = "";
			
			if(this.__cols[sortIndex]) {
				var sort = '/order/' + this.__cols[sortIndex] + '=' + sortOrder;
			}

			var url = this.__serviceUrl + "getModelData/model/" + this.__modelName + sort + this.__search + this.__api + "/perPage/" + this.getBlockSize() + "/page/" + pageNum + ".json";
			var req = new qx.io.remote.Request(url, 'GET', 'application/json');
			req.setTimeout(5000);

			req.setRequestHeader('x-framework-json-request', 'PLAINJSON');
			req.addListener('completed', function(r) {
				var rows = r.getContent().pageRecords;

				if(rows.length > 0) {
					this._onRowDataLoaded(rows);
				}
			}, this);

			req.send();
		}
	},

	destruct : function() {
		this._disposeObjects("__store","__search","__modelName","__config","__cols");
	}

});
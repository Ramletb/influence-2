/*
  ___ _   _ _____ _    _   _ _____ _   _  ____ _____ 
 |_ _| \ | |  ___| |  | | | | ____| \ | |/ ___| ____|
  | ||  \| | |_  | |  | | | |  _| |  \| | |   |  _|  
  | || |\  |  _| | |__| |_| | |___| |\  | |___| |___ 
 |___|_| \_|_|   |_____\___/|_____|_| \_|\____|_____|

*/

/**
 * Theme
 */
qx.Theme.define("ic.theme.Theme", {
	meta : {
		color : ic.theme.Color,
		decoration : ic.theme.Decoration,
		font : ic.theme.Font,
		icon : qx.theme.icon.Tango,
		appearance : ic.theme.Appearance
	}
});
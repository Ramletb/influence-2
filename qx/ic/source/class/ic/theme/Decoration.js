/*
  ___ _   _ _____ _    _   _ _____ _   _  ____ _____ 
 |_ _| \ | |  ___| |  | | | | ____| \ | |/ ___| ____|
  | ||  \| | |_  | |  | | | |  _| |  \| | |   |  _|  
  | || |\  |  _| | |__| |_| | |___| |\  | |___| |___ 
 |___|_| \_|_|   |_____\___/|_____|_| \_|\____|_____|

*/

/**
 * Decorations
 */
qx.Theme.define("ic.theme.Decoration", {
	extend : qx.theme.modern.Decoration,

	decorations : {
		"vt-app-header" : {
			decorator : [ qx.ui.decoration.Background ],

			style : {
				backgroundColor : "vt-header-background"
			}
		}
	}
});
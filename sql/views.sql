CREATE OR REPLACE ALGORITHM=MERGE SQL SECURITY INVOKER VIEW `vw_pageContent` AS
SELECT
p.`id`          as page_id,
c.`id`          as content_id,
c.`value`       as content_value

FROM page_content as pc, page as p, content as c
WHERE pc.page_id = p.id AND pc.content_id = c.id
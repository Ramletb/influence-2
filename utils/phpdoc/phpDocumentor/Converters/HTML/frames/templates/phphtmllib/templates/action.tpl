<a name='method_detail'></A>

{section name=actions loop=$actions}

{if !$actions[actions].static && $actions[actions].is_action}

<a id="{$actions[actions].function_name|replace:"__":"uu"}"><!-- --></a>
<div class="{cycle values="evenrow,oddrow"}">
	
	<div class="method-header">
		<span class="method-title">{if $actions[actions].ifunction_call.constructor}Constructor {elseif $actions[actions].ifunction_call.destructor}Destructor {/if}{$actions[actions].function_name}</span> (line <span class="line-number">{if $actions[actions].slink}{$actions[actions].slink}{else}{$actions[actions].line_number}{/if}</span>)
	</div> 
	
	{include file="docblock.tpl" sdesc=$actions[actions].sdesc desc=$actions[actions].desc tags=$actions[actions].tags params=$actions[actions].params function=false}
	
	<div class="method-signature">
		<span class="method-result">{$actions[actions].function_return}</span>
		<span class="method-name">
			{if $actions[actions].ifunction_call.returnsref}&amp;{/if}{$actions[actions].function_name}
		</span>
		{if count($actions[actions].ifunction_call.params)}
			({section name=params loop=$actions[actions].ifunction_call.params}{if $smarty.section.params.iteration != 1}, {/if}{if $actions[actions].ifunction_call.params[params].hasdefault}[{/if}<span class="var-type">{$actions[actions].ifunction_call.params[params].type}</span>&nbsp;<span class="var-name">{$actions[actions].ifunction_call.params[params].name}</span>{if $actions[actions].ifunction_call.params[params].hasdefault} = <span class="var-default">{$actions[actions].ifunction_call.params[params].default}</span>]{/if}{/section})
		{else}
		()
		{/if}
	</div>
	
	{if $actions[actions].params}
		<ul class="parameters">
		{section name=params loop=$actions[actions].params}
			<li>
				<span class="var-type">{$actions[actions].params[params].datatype}</span>
				<span class="var-name">{$actions[actions].params[params].var}</span>{if $actions[actions].params[params].data}<span class="var-description">: {$actions[actions].params[params].data}</span>{/if}
			</li>
		{/section}
		</ul>
	{/if}
	
	{if $actions[actions].method_overrides}
		<hr class="separator" />
		<div class="notes">Redefinition of:</div>
		<dl>
			<dt>{$actions[actions].method_overrides.link}</dt>
			{if $actions[actions].method_overrides.sdesc}
			<dd>{$actions[actions].method_overrides.sdesc}</dd>
			{/if}
		</dl>
	{/if}
	{if $actions[actions].method_implements}
		<hr class="separator" />
		<div class="notes">Implementation of:</div>
	{section name=imp loop=$actions[actions].method_implements}
		<dl>
			<dt>{$actions[actions].method_implements[imp].link}</dt>
			{if $actions[actions].method_implements[imp].sdesc}
			<dd>{$actions[actions].method_implements[imp].sdesc}</dd>
			{/if}
		</dl>
	{/section}
	{/if}
	
	{if $actions[actions].descmethod}
		<hr class="separator" />
		<div class="notes">Redefined in descendants as:</div>
		<ul class="redefinitions">
		{section name=dm loop=$actions[actions].descmethod}
			<li>
				{$actions[actions].descmethod[dm].link}
				{if $actions[actions].descmethod[dm].sdesc}
				: {$actions[actions].descmethod[dm].sdesc}
				{/if}
			</li>
		{/section}
		</ul>
	{/if}
</div>
{/if}
{/section}

#!/usr/bin/php -q
<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * Command Line Intrface Utility
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage utilities
 * @see CliFramework
 */

/**
 * Requires Framework.php
 */
require_once('../core/frameworks/CliFramework.php');
 
$cliFramework = new CliFramework;
$cliFramework->dispatch();

?>
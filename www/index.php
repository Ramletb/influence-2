<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * index.php
 *
 * Calls $webFramework->dispatch();
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @package SP5
 * @subpackage server
 */

/**
 * Require {@link WebFramework.php}
 *
 * Requires the {@link WebFramework} class.
 *
 */
require_once('../core/frameworks/WebFramework.php');
 
$webFramework = new WebFramework;
$webFramework->dispatch();

?>


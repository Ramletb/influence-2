// Global Variables
var etapIsValid = false;
var etapInProgress = false;
var etapCultureValidation = null;
var etapCurrencyValidation = null;

/**
 * RDS: Note to the ecommerce developer. Despite all of the ugly type validation code, you only need
 * to interact with 2 functions to integrate type checking. Sometime before you submit the form
 * (on load is a good spot), you should have something like this... we're wanting to check types
 * on 2 fields, a currency field and a date field, but you don't want to allow the date field to
 * be empty (the amount can be empty), add these 3 lines... that's it!
 *
 *   var typeValidator = registerEcommerceTypeValidator(LOCALES["en_US"]);
 *   typeValidator.addValidator(document.donorForm.amount, "CURRENCY", "Amount must be a valid currency value.", false);
 *   typeValidator.addValidator(document.donorForm.giftDate, "DATE", "Gift Date must contain a mm/dd/yyyy date value.", true);
 *
 *   // USAGE: addValidator(formElement, validationType, errorMessage, isRequiredCantBeEmpty)
 */
var etapEcommerceTypeValidator = false;

function registerEcommerceTypeValidator(localeObj)
{
    return (etapEcommerceTypeValidator = new EcommerceFormTypeValidator(localeObj));
}

var LOCALES = new Array();
LOCALES["en_US"] = getUnitedStatesValidationInstance();
LOCALES["en_AU"] = getAustraliaValidationInstance();
LOCALES["nl_NL"] = getNetherlandsValidationInstance();

function getUnitedStatesValidationInstance()
{
    var localeObj = new Object();

    localeObj.numberRegEx = /^((\d{1,3}(\,\d{3})+(\.\d*)?)|((\d+(\.\d*)?)))$/;
    localeObj.currencyRegEx = /^(\$? ?\d+(\,\d{3})*(\.(\d{1,2})?)?)$/;
    //localeObj.currencyRegEx = /^(\$? ?((\d+|\d{1,3})(\,?\d{3})*)?(\.(\d{1,2})?)?)$/;

    localeObj.currencyFormat = new Object();
    localeObj.currencyFormat.decimalSeparator = ".";
    localeObj.currencyFormat.example = "$125.00";

    localeObj.dateFormat = new Object();
    localeObj.dateFormat.regEx = /^(\d{1}|\d{2})\/(\d{1}|\d{2})\/(\d{2}|\d{4})$/;   // 1 or 2 digit month, dash, 1 or 2 digit day, dash, 2 or 4 digit year
    localeObj.dateFormat.yearlessRegEx = /^\d{1,2}\/\d{1,2}$/;                      // 1 or 2 digit month, forward slash, 1 or 2 digit day
    localeObj.dateFormat.separator = "/";
    localeObj.dateFormat.monthChunk = 0;
    localeObj.dateFormat.dayChunk = 1;
    localeObj.dateFormat.yearChunk = 2;
    localeObj.dateFormat.daysInMonth = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
    
    localeObj.translations = new Array();
    localeObj.translations["label.firstName"] = "first name";
    localeObj.translations["label.lastName"] = "last name";
    localeObj.translations["label.address"] = "address";
    localeObj.translations["label.city"] = "city";
    localeObj.translations["label.state"] = "state";
    localeObj.translations["label.postalCode"] = "postal (zip) code";
    localeObj.translations["label.emailAddress"] = "email address";
    localeObj.translations["label.phoneNumber"] = "phone number";
    localeObj.translations["label.ccNumber"] = "credit card number";
    localeObj.translations["label.ccExpMonth"] = "credit card expiration month";
    localeObj.translations["label.ccExpYear"] = "credit card expiration year";
    localeObj.translations["label.ccSecurityCode"] = "credit card security (CVV2) code";
    localeObj.translations["label.bankRoutingNumber"] = "bank routing number";
    localeObj.translations["label.bankAccountNumber"] = "bank account number";
    localeObj.translations["label.bankAccountType"] = "type of bank account";
    localeObj.translations["label.checkNumber"] = "check number";
    localeObj.translations["label.amount"] = "amount";
    localeObj.translations["label.recurringAmount"] = "recurring amount";
    localeObj.translations["label.recurringFrequency"] = "recurring frequency";
    localeObj.translations["label.sectionQuestion.n"] = "Section {0}, Question {1}.";

    localeObj.translations["msg.pleaseEnter.n"] = "Please enter your {0}.";
    localeObj.translations["msg.pleaseSelect.n"] = "Please select your {0}.";
    localeObj.translations["msg.pleaseEnterAmount"] = "Please enter the amount you wish to donate.";
    localeObj.translations["msg.pleaseAnswerQuestion"] = "Please provide an answer to the following question.";
    localeObj.translations["msg.pleaseEnterName"] = "Please complete the account name or both first and last names.";
    localeObj.translations["msg.pleaseConfirmEmail"] = "Please confirm your email address.";
    localeObj.translations["msg.invalidConfirmEmail"] = "The confirmation email address does not match the initial email address.";
    localeObj.translations["msg.invalidEmail"] = "Please enter a valid email address.";
    localeObj.translations["msg.invalidAmount"] = "Please enter a valid {0} greater than zero.\n\nExample: {1}";
    localeObj.translations["msg.invalidRecurringStartDate"] = "The recurring start date entered is not a valid date of mm/dd/yyyy format.";
    localeObj.translations["msg.invalidCreditCardSecurityCode"] = "The security (CVV2) code entered must be a 3 or 4 digit number.";
    localeObj.translations["msg.invalidCreditCardExpMonth"] = "The credit card expiration month entered is not valid.";
    localeObj.translations["msg.oldCreditCardExpYear"] = "The credit card expiration year entered is in the past.";
    localeObj.translations["msg.invalidCreditCardExpYear"] = "The credit card expiration year entered is not valid.";
    localeObj.translations["msg.shortCreditCardNumber"] = "The credit card number entered must contain at least 14 digits.";
    localeObj.translations["msg.invalidCreditCardNumber"] = "The credit card number entered is not valid.";
    localeObj.translations["msg.shortBankRoutingNumber"] = "The bank routing number entered must contain exactly nine digits.";
    localeObj.translations["msg.invalidBankRoutingNumber"] = "The bank routing number entered is not valid.";
    localeObj.translations["msg.invalidBankAccountNumber"] = "The bank account number entered must contain only digits.";
    localeObj.translations["msg.pleaseWaitTransInProgress"] = "Please wait.  Your transaction is in progress.";

    return localeObj;
}

function getAustraliaValidationInstance()
{
    var localeObj = new Object();

    localeObj.numberRegEx = /^((\d{1,3}(\,\d{3})+(\.\d*)?)|((\d+(\.\d*)?)))$/;
    localeObj.currencyRegEx = /^(\$? ?\d+(\,\d{3})*(\.(\d{1,2})?)?)$/;
    //localeObj.currencyRegEx = /^(\$? ?((\d+|\d{1,3})(\,?\d{3})*)?(\.(\d{1,2})?)?)$/;

    localeObj.currencyFormat = new Object();
    localeObj.currencyFormat.decimalSeparator = ".";
    localeObj.currencyFormat.example = "$125.00";

    localeObj.dateFormat = new Object();
    localeObj.dateFormat.regEx = /^(\d{1}|\d{2})\/(\d{1}|\d{2})\/(\d{2}|\d{4})$/;   // 1 or 2 digit day, dash, 1 or 2 digit month, dash, 2 or 4 digit year
    localeObj.dateFormat.yearlessRegEx = /^\d{1,2}\/\d{1,2}$/;                      // 1 or 2 digit day, forward slash, 1 or 2 digit month
    localeObj.dateFormat.separator = "/";
    localeObj.dateFormat.monthChunk = 1;
    localeObj.dateFormat.dayChunk = 0;
    localeObj.dateFormat.yearChunk = 2;
    localeObj.dateFormat.daysInMonth = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

    localeObj.translations = getUnitedStatesValidationInstance().translations;
    
    localeObj.translations["msg.invalidRecurringStartDate"] = "The recurring start date entered is not a valid date of dd/mm/yyyy format.";

    return localeObj;
}

function getNetherlandsValidationInstance()
{
    var localeObj = new Object();

    localeObj.numberRegEx = /^((\d{1,3}(\.\d{3})+(\,\d*)?)|((\d+(\,\d*)?)))$/;
    localeObj.currencyRegEx = /^(\u20AC? ?\d+(\.\d{3})*(\,(\d{1,2})?)?)$/;

    localeObj.currencyFormat = new Object();
    localeObj.currencyFormat.decimalSeparator = ",";
    localeObj.currencyFormat.example = "\u20AC 125,00";

    localeObj.dateFormat = new Object();
    localeObj.dateFormat.regEx = /^(\d{1}|\d{2})\-(\d{1}|\d{2})\-(\d{2}|\d{4})$/;   // 1 or 2 digit day, dash, 1 or 2 digit month, dash, 2 or 4 digit year
    localeObj.dateFormat.yearlessRegEx = /^\d{1,2}-\d{1,2}$/;                       // 1 or 2 digit day, dash, 1 or 2 digit month
    localeObj.dateFormat.separator = "-";
    localeObj.dateFormat.monthChunk = 1;
    localeObj.dateFormat.dayChunk = 0;
    localeObj.dateFormat.yearChunk = 2;
    localeObj.dateFormat.daysInMonth = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

    localeObj.translations = getUnitedStatesValidationInstance().translations;
    
    localeObj.translations["msg.invalidRecurringStartDate"] = "The recurring start date entered is not a valid date of dd-mm-yyyy format.";

    return localeObj;
}

function getCurrencyValidationInstance()
{
    if (etapCurrencyValidation == null)
    {
        var form = document.donorForm;
        var locale = (form.currencyLocale == null) ? "" : form.currencyLocale.value;
        if (locale == "") locale = (form.locale == null) ? "" : form.locale.value;
        if (locale == "") locale = "en_US";

        etapCurrencyValidation = LOCALES[locale];
        if (etapCurrencyValidation == null) etapCurrencyValidation = LOCALES["en_US"];
    }

    return etapCurrencyValidation;
}

function getCultureValidationInstance()
{
    if (etapCultureValidation == null)
    {
        var form = document.donorForm;
        var locale = (form.cultureLocale == null) ? "" : form.cultureLocale.value;
        if (locale == "") locale = (form.locale == null) ? "" : form.locale.value;
        if (locale == "") locale = "en_US";

        etapCultureValidation = LOCALES[locale];
        if (etapCultureValidation == null) etapCultureValidation = LOCALES["en_US"];
    }

    return etapCultureValidation;
}

function translate(value, args)
{
    var translation = getCultureValidationInstance().translations[value];

    if (args != null)
    {
        if (args.constructor == Array)
        {
            for (var i = 0; i < args.length; i++)
            {
                translation = translation.replace("{" + i + "}", args[i]);
            }
        }
        else
        {
            translation = translation.replace("{0}", args);
        }
    }

    return (translation == null) ? value : translation;
}

function translateAndAlert(translationKey, args)
{
    alert(translate(translationKey, args));
    return (etapIsValid = false);
}

function translateAlertAndFocus(field, translationKey, args)
{
    alert(translate(translationKey, args));
    try { if (!field.readOnly) field.focus(); } catch (err) { }
    return (etapIsValid = false);
}

function translateAlertFocusAndSelect(field, translationKey, args)
{
    alert(translate(translationKey, args));
    try { if (!field.readOnly) { field.focus(); field.select(); } } catch (err) { }
    return (etapIsValid = false);
}

function pleaseEnter(field, label)
{
    return translateAlertAndFocus(field, "msg.pleaseEnter.n", translate(label));
}

function pleaseSelect(field, label)
{
    return translateAlertAndFocus(field, "msg.pleaseSelect.n", translate(label));
}

function pleaseAnswerWalkerQuestion(section, question)
{
    alert(translate("msg.pleaseAnswerQuestion") + "\n\n" + translate("label.sectionQuestion.n", new Array(section, question)));
    return (etapIsValid = false);
}

/**
 * Regular expression support for validating formatted strings
 */
var whitespaceRegEx = /^\s+$/;
var leadingZeroRegEx = /^0\d+$/;

var VALIDATION_MAP = new Array();
VALIDATION_MAP["DATE"] = isValidDate;
VALIDATION_MAP["MONTH_DAY"] = isValidMonthDay;
VALIDATION_MAP["NUMBER"] = isValidNumber;
VALIDATION_MAP["CURRENCY"] = isValidCurrency;
VALIDATION_MAP["TEXT"] = isValidText;

// Grab the proper validator function for the type (defaults to TEXT)
function getValidatorFunction(type) {return VALIDATION_MAP[type] ? VALIDATION_MAP[type] : isValidText;}

// Date tests. Test the basic structure of the string and then validate that the number of days match up with the month
function isValidDate(localeObj, value) {return localeObj.dateFormat.regEx.test(value) ? hasProperMonthDayValues(localeObj.dateFormat, value) : false;}
function isValidMonthDay(localeObj, value) {return localeObj.dateFormat.yearlessRegEx.test(value) ? hasProperMonthDayValues(localeObj.dateFormat, value) : false;}

// Numeric and textual tests. Just match the regular expression
function isValidNumber(localeObj, value) {return localeObj.numberRegEx.test(value);}
function isValidCurrency(localeObj, value) {return localeObj.currencyRegEx.test(value);}
function isValidText(localeObj, value) {return true;}
function checkEmpty(isRequired, v) {return (!isRequired || (v != null && v != "" && !whitespaceRegEx.test(v)));}

// For instance, the 31st of April is invalid... verify this (no leap year support yet). So is month "13"
function hasProperMonthDayValues(dateFormat, dateString)
{
    // Split up the date into its component parts and make sure the days match up given the month
    var chunks = dateString.split(dateFormat.separator);
    var month = parseInt(stripLeadingZeroes(chunks[dateFormat.monthChunk]));
    var day = parseInt(stripLeadingZeroes(chunks[dateFormat.dayChunk]));
    return (month <= 12) && (month >= 1) && (day > 0 && day <= dateFormat.daysInMonth[month-1]);
}

// Turn "002" into "2"
function stripLeadingZeroes(value)
{
    while (leadingZeroRegEx.test(value)) value = value.substring(1);
    return value;
}

/**
 * The form types validator object
 */
function EcommerceFormTypeValidator(localeObj)
{
    this.fieldValidators = new Array();
    this.localeObj = localeObj ? localeObj : getUnitedStatesValidationInstance();
    this.validateFormTypes = _validateFormTypes;
    this.addValidator = _addValidator;
}

function _validateFormTypes()
{
    // Check the validity of all of the registered fields' types. And if at least
    // one of them fails, then the whole form fails to pass.
    for (var i = 0; i < this.fieldValidators.length; i++)
        if (!this.fieldValidators[i].isCurrentlyValid(this.localeObj))
            return false;

    return true;
}

function _addValidator(formField, typeString, errorMsg, isRequired)
{
    var validator = new EcommerceFieldValidator(formField, typeString, errorMsg, isRequired);
    this.fieldValidators[this.fieldValidators.length] = validator;
}

/**
 * The validator object for a single field
 */
function EcommerceFieldValidator(formField, typeString, errorMsg, isRequired)
{
    // member variables
    this.formField = formField;    // for example: document.theForm.firstName  or  document.getElementById("firstName");
    this.type = typeString;        // for example: "NUMBER" or "DATE" or "MONTH_DAY" or "CURRENCY"
    this.errorMsg = errorMsg;      // for example: "Amount: Must be a valid currency value."
    this.isRequired = isRequired;  // for example: true  or  false   (depending on if you want to enfore that it not be empty)
    this.isCurrentlyValid = _isCurrentlyValid;   // function!
}

function _isCurrentlyValid(localeObj)
{
    var validatorFunction = getValidatorFunction(this.type);
    var theValue = this.formField.value;
    var isEmpty = (!theValue || theValue == "" || whitespaceRegEx.test(theValue));

    // Don't validate empty fields if they're not required
    if (isEmpty && !this.isRequired) return true;

    // Check the format of the input
    if (validatorFunction(localeObj, theValue)) return true;

    alert(this.errorMsg);
    this.formField.focus();
    return false;
}

/**
 * Captcha security feature.
 * Assuming your HTML document already has the proper tags/ids set up
 * this will auto-populate those empty tags with the standard fields
 * for captcha security.
 * @param serverContext "prod" or "atl" (i.e. name of environment)
 */
var CAPTCHA_CHALLENGE_ID = "etapCaptchaChallenge";
var CAPTCHA_IMAGE_ID = "etapCaptchaImage";
var CAPTCHA_INPUT_ID = "etapCaptchaInput";
var SHAKE_LENGTH = 32;

function populateCaptchaElements(serverContext)
{
    // Assume prod if no arguments given
    if (arguments.length == 0) serverContext = "prod";

    var handshake = generateCaptchaId();

    // Populate the question
    var element = document.getElementById(CAPTCHA_CHALLENGE_ID);
    element.innerHTML += "<b>Security Code (Upper Case):</b>";
    
    // Populate the input area
    element = document.getElementById(CAPTCHA_INPUT_ID);
    element.innerHTML += "<input type='text' name='captchaResponse'>";
    element.innerHTML += "<input type='hidden' name='captchaResponseId' id='captchaResponseId' value='" + handshake + "'>";

    // Populate the actual image. Make sure to append img element before setting SRC or else
    // firefox has some threading issues and you get a broken image link
    var srcUrl = "https://app.etapestry.com/" + serverContext + "/security/captcha.jpg?captchaResponseId=" + handshake;
    if (serverContext == "atl") srcUrl = "https://atl.etapestry.com/prod/security/captcha.jpg?captchaResponseId=" + handshake;
    else if (serverContext == "sna") srcUrl = "https://sna.etapestry.com/prod/security/captcha.jpg?captchaResponseId=" + handshake;
    else if (serverContext == "bos") srcUrl = "https://bos.etapestry.com/prod/security/captcha.jpg?captchaResponseId=" + handshake;
	else if (serverContext == "seinfeld") srcUrl =  "https://ind.etapestry.com/seinfeld/security/captcha.jpg?captchaResponseId=" + handshake;
	else if (serverContext == "ateam") srcUrl =  "https://ind.etapestry.com/ateam/security/captcha.jpg?captchaResponseId=" + handshake;
	else if (serverContext == "fullhouse") srcUrl =  "https://ind.etapestry.com/fullhouse/security/captcha.jpg?captchaResponseId=" + handshake;
    var createNewImageLink = "<a style='font-size: 11px; font-family: Arial, Helvetica, sans-serif;' href='javascript:createNewCaptchaImage()'>Generate New Security Image</a>";

    element = document.getElementById(CAPTCHA_IMAGE_ID);
    element.innerHTML += "<img id='etapCaptchaImageImg' src='" + srcUrl + "'><br/>" + createNewImageLink;
}

function createNewCaptchaImage()
{
    var handshake = generateCaptchaId();
    var imgSrc = document.getElementById(CAPTCHA_IMAGE_ID + "Img").src;

    imgSrc = imgSrc.substring(0, imgSrc.indexOf("="));

    document.getElementById("captchaResponseId").value = handshake;
    document.getElementById(CAPTCHA_IMAGE_ID + "Img").src = imgSrc + "=" + handshake;
}

/** Generate a 256-bit public key the server will associate the captcha with */
function generateCaptchaId()
{
    var id = "";
    for (var i = 0; i < SHAKE_LENGTH; i++)
    {
        var bits = randomInt(0, 36);
        id += (bits < 26) ? String.fromCharCode(65+bits) : (bits-26);
    }
    return id;
}

/** Generate a random number between from and to (inclusive) */
function randomInt(from, to)
{
    return Math.floor(Math.random() * (to - from + 1)) + from;
}

function prepValidationElements()
{
    var form = document.donorForm;
    for (var i = 0; i < form.elements.length; i++)
    {
        if (form.elements[i].name == null) continue;

        if (form.elements[i].name.indexOf("_SetElement|") >= 0)
        {
            form.elements[i].value = "true";
            if (!form.elements[i].checked) form.elements[i].checked = true;
        }
        else if (form.elements[i].name.indexOf("_TextField|") >= 0)
        {
            if (!form.elements[i].checked) form.elements[i].checked = true;
        }
    }

    gatherCodes("fundName", "validateFundNames");
    gatherCodes("campaignName", "validateCampaignNames");
    gatherCodes("approachName", "validateApproachNames");
    gatherCodes("letterName", "validateLetterNames");
    gatherCodes("cardType", "validateCardTypeNames");
}

function gatherCodes(name, newName)
{
    var form = document.donorForm;
    for (var i = 0; i < form.elements.length; i++)
    {
        if (form.elements[i].name == name)
        {
            if (form.elements[i].length == null)
            {
                createHiddenElement(newName, form.elements[i].value);
                break;
            }
            else
            {
                var element = form.elements[i];
                for (var j = 0; j < element.length; j++) createHiddenElement(newName, element[j].value);
                break;
            }
        }
    }
}

function createHiddenElement(name, value)
{
    var input = document.createElement("input");
    input.setAttribute("type", "hidden");
    input.setAttribute("name", name);
    input.setAttribute("value", value);
    document.donorForm.appendChild(input);
}

/** Validates the form.  Returns true if valid, false otherwise. */
function validate()
{
    etapCultureValidation = null;
    etapCurrencyValidation = null;
    
    setFrequencyOnChangeEventCheck();

    var form = document.donorForm;
    
    if (form.firstName.value == "validatePage")
    {
        prepValidationElements();

        // Added due to Firefox caching
        window.onunload = function() {if (form.submit != null) form.submit.disabled = false;}

        return true;
    }

    // First do a check for the registered field types (Number, Currency, etc.) and if
    // that passes, then delve into the standard validation... assuming we even have such validation
    if (etapEcommerceTypeValidator && !etapEcommerceTypeValidator.validateFormTypes()) return false;

    if (form.transType.value == "check")
    {
        if (form.checkSSN != null && form.checkSSN1 != null && form.checkSSN2 != null && form.checkSSN3 != null)
        {
            form.checkSSN.value = new String(form.checkSSN1.value) +  new String(form.checkSSN2.value) + new String(form.checkSSN3.value);
        }
        
        if (form.checkDOB != null && form.dobMonth != null && form.dobDay != null && form.dobYear != null)
        {
            form.checkDOB.value = new String(form.dobMonth.value) + "/" + new String(form.dobDay.value) + "/" + new String(form.dobYear.value);
        }
    }

    var isValid = validateCoreFields() && validateTransactionFields();

    if (isValid)
    {
        createHiddenElement("eCommercePageName", window.location.href);

        if (form.submit != null)
        {
            form.submit.disabled = true;

            // Added due to Firefox caching
            window.onunload = function() {form.submit.disabled = false;}
        }
        else
        {
            if (!etapInProgress)
            {
                etapInProgress = true;
                isValid = true;
            }
            else
            {
                translateAndAlert("msg.pleaseWaitTransInProgress");
                isValid = false;
            }
        }
    }

    return isValid;
}


/**
 * Validates the required form fields based upon the transType value
 * Returns true if valid, false if not.
 */
function validateCoreFields()
{
    etapIsValid = true;
    var form = document.donorForm;

    // Validate cash fields
    if (form.transType.value == "cash")
    {
        if (form.accountName.value == "" && (form.firstName.value == "" || form.lastName.value == "")) translateAndAlert("msg.pleaseEnterName");
        else if (form.amount.value == "") translateAlertAndFocus(form.amount, "msg.pleaseEnterAmount");
    }
    // Validate check fields
    else if (form.transType.value == "physical_check")
    {
        if (form.accountName.value == "" && (form.firstName.value == "" || form.lastName.value == "")) translateAndAlert("msg.pleaseEnterName");
        else if (form.checkNum.value == "") pleaseEnter(form.checkNum, "label.checkNumber");
        else if (form.amount.value == "") translateAlertAndFocus(form.amount, "msg.pleaseEnterAmount");
    }
    // Validate email field(s) - every type of form not listed above requires an email address
    else if (!validateEmails(form.email, form.confirmEmail))
    {
        etapIsValid = false;
    }
    // Validate registration fields
    else if (form.transType.value == "registration")
    {
        if (form.accountName.value == "" && (form.firstName.value == "" || form.lastName.value == "")) translateAndAlert("msg.pleaseEnterName");
    }
    // Validate walker fields
    else if (form.transType.value == "walker")
    {
        if (form.accountName.value == "" && (form.firstName.value == "" || form.lastName.value == "")) translateAndAlert("msg.pleaseEnterName");
        else if (!isRadioOptionSelected(form.reputation)) pleaseAnswerWalkerQuestion("1", "A");
        else if (!isRadioOptionSelected(form.believeMission)) pleaseAnswerWalkerQuestion("1", "B");
        else if (!isRadioOptionSelected(form.fundsUse)) pleaseAnswerWalkerQuestion("1", "C");
        else if (!isRadioOptionSelected(form.excellentJob)) pleaseAnswerWalkerQuestion("1", "D");
        else if (!isRadioOptionSelected(form.ethical)) pleaseAnswerWalkerQuestion("1", "E");
        else if (!isRadioOptionSelected(form.associated)) pleaseAnswerWalkerQuestion("1", "F");
        else if (!isRadioOptionSelected(form.commitment)) pleaseAnswerWalkerQuestion("1", "G");
        else if (!isRadioOptionSelected(form.personalAttachment)) pleaseAnswerWalkerQuestion("1", "H");
        else if (!isRadioOptionSelected(form.familyFeel)) pleaseAnswerWalkerQuestion("1", "I");
        else if (!isRadioOptionSelected(form.volunteer)) pleaseAnswerWalkerQuestion("2", "A");
        else if (!isRadioOptionSelected(form.financial)) pleaseAnswerWalkerQuestion("2", "B");
        else if (!isRadioOptionSelected(form.specialGift)) pleaseAnswerWalkerQuestion("2", "C");
        else if (form.receivedServices != null && !isRadioOptionSelected(form.receivedServices)) pleaseAnswerWalkerQuestion("2", "D");
    }
    // Validate transaction core fields
    else if (form.transType.value == "donation"
          || form.transType.value == "check"
          || form.transType.value == "pledge"
          || form.transType.value == "rgs")
    {
        if (form.firstName.value == "") pleaseEnter(form.firstName, "label.firstName");
        else if (form.lastName.value == "") pleaseEnter(form.lastName, "label.lastName");
        else if (form.address.value == "") pleaseEnter(form.address, "label.address");
        else if (form.city.value == "") pleaseEnter(form.city, "label.city");
        else if (form.state != null && form.state.selectedIndex == 0) pleaseSelect(form.state, "label.state"); // Netherlands pages may not have state field
        else if (form.postalCode.value == "") pleaseEnter(form.postalCode, "label.postalCode");
        else if (form.phone.value == "") pleaseEnter(form.phone, "label.phoneNumber");
        else if (form.amount.value == "") translateAlertAndFocus(form.amount, "msg.pleaseEnterAmount");

        // Validate credit card fields
        if (etapIsValid && form.transType.value == "donation")
        {
            if (form.cardNumber.value == "") pleaseEnter(form.cardNumber, "label.ccNumber");
            else if (form.cardExpMonth.value == "") pleaseSelect(form.cardExpMonth, "label.ccExpMonth");
            else if (form.cardExpYear.value == "") pleaseSelect(form.cardExpYear, "label.ccExpYear");
            else if (form.cardCVV2 != null && form.cardCVV2.value == "") pleaseEnter(form.cardCVV2, "label.ccSecurityCode");
        }
        // Validate eft fields
        else if (etapIsValid && form.transType.value == "check")
        {
            if (form.checkRoutingNum.value == "") pleaseEnter(form.checkRoutingNum, "label.bankRoutingNumber");
            else if (form.checkAcctNum.value == "") pleaseEnter(form.checkAcctNum, "label.bankAccountNumber");
            else if (form.checkAcctType.value == "") pleaseEnter(form.cardcheckAcctTypeCVV2, "label.bankAccountType");
        }
    }

    return etapIsValid;
}

/**
 * Makes sure a vaild email address was given.
 * Returns true if valid, false if not.
 */
function validateEmails(emailField, confirmEmailField)
{
    if (confirmEmailField != null && emailField.value != "")
    {
        if (confirmEmailField.value == "")
        {
            return translateAlertAndFocus(confirmEmailField, "msg.pleaseConfirmEmail");
        }
        else if (emailField.value != confirmEmailField.value)
        {
            return translateAlertFocusAndSelect(confirmEmailField, "msg.invalidConfirmEmail");
        }
    }

    var emailRegEx = /^[^\@\s,;:]+\@[^\@\s,;:\/]+\.[^\@\s,;:\/]+$/;

    if (emailField.value == "")
    {
        return pleaseEnter(emailField, "label.emailAddress");
    }
    else if (!emailRegEx.test(emailField.value))
    {
        return translateAlertFocusAndSelect(emailField, "msg.invalidEmail");
    }

    return true;
}

/**
 * Validates the form field content.
 * Returns true if valid, false if not.
 */
function validateTransactionFields()
{
    var integerRegEx = /^\d+$/;
    var form = document.donorForm;

    // Validate credit card transaction
    if (form.transType.value == "donation")
    {
        // Validate credit card number
        if (!integerRegEx.test(form.cardNumber.value)) return translateAndAlert("msg.invalidCreditCardNumber");
        else if (form.cardNumber.value.length < 14) return translateAndAlert("msg.shortCreditCardNumber");

        var now = new Date();
        var year = new Number(form.cardExpYear.value);
        var month = new Number(form.cardExpMonth.value);

        // Validate credit card expiration month
        if (isNaN(month) || month <= 0 || month > 12) return translateAndAlert("msg.invalidCreditCardExpMonth");

        // Validate credit card expiration year
        if (year < now.getYear()) return translateAndAlert("msg.oldCreditCardExpYear");
        else if (isNaN(year)) return translateAndAlert("msg.invalidCreditCardExpYear");

        // Validate credit card security code (assuming the field is found on the page)
        if (form.cardCVV2 != null)
        {
            if (!integerRegEx.test(form.cardCVV2.value) || form.cardCVV2.value.length < 3 || form.cardCVV2.value.length > 4)
            {
                return translateAndAlert("msg.invalidCreditCardSecurityCode");
            }
        }
    }
    else if (form.transType.value == "check")
    {
        // Validate bank routing number
        if (!integerRegEx.test(form.checkRoutingNum.value)) return translateAndAlert("msg.invalidBankRoutingNumber");
        else if (form.checkRoutingNum.value.length != 9) return translateAndAlert("msg.shortBankRoutingNumber");

        // Validate bank account number
        if (!integerRegEx.test(form.checkAcctNum.value)) return translateAndAlert("msg.invalidBankAccountNumber");
    }

    // Validate amount and rgs data (if necessary)
    if (form.transType.value == "donation" ||
        form.transType.value == "check" ||
        form.transType.value == "pledge" ||
        form.transType.value == "cash" ||
        form.transType.value == "physical_check" ||
        form.transType.value == "rgs")
    {
        cleanupAmount(form.amount);

        if (!isValidAmount(form.amount.value)) return translateAlertFocusAndSelect(form.amount, "msg.invalidAmount", getInvalidAmountArgs("label.amount"));

        if (form.transType.value == "donation" || form.transType.value == "check" || form.transType.value == "rgs")
        {
            // Validate rgs data
            if (form.transType.value == "rgs" || (form.rgsCreate != null && form.rgsCreate.value == "true"))
            {
                // Validate rgs frequency
                if (form.rgsFrequency == null || form.rgsFrequency.value == "") return translateAndAlert("msg.pleaseEnter.n", translate("label.recurringFrequency"));

                // Validate rgs amount
                if (form.rgsAmount != null && form.rgsAmount.value != "")
                {
                    cleanupAmount(form.rgsAmount);

                    if (!isValidAmount(form.rgsAmount.value)) return translateAlertFocusAndSelect(form.rgsAmount, "msg.invalidAmount", getInvalidAmountArgs("label.recurringAmount"));
                }

                // Validate rgs start date
                if (form.rgsStartDate != null && form.rgsStartDate.value != "")
                {
                    if (!isValidDate(getCultureValidationInstance(), form.rgsStartDate.value)) return translateAndAlert("msg.invalidRecurringStartDate");
                }
            }
        }
    }

    return true;
}

/**
 * This method should only be called after the value
 * has passed the appropriate currency regex and basically
 * ensures the amount isn't zero.
 */
function isPositiveAmount(value)
{
    if (value == null) return 0;

    var currencyValue = "";
    var integerRegEx = /^\d$/;
    
    for (var i = 0; i < value.length; i++)
    {
        var c = value.charAt(i);
        if (integerRegEx.test(c)) currencyValue += c;
    }

    return (currencyValue == "") ? false : parseFloat(currencyValue) > 0;
}

function getInvalidAmountArgs(label)
{
    return new Array(translate(label), getCurrencyValidationInstance().currencyFormat.example);
}

function isValidAmount(value)
{
    return isValidCurrency(getCurrencyValidationInstance(), value) && isPositiveAmount(value) > 0;
}

function cleanupAmount(field)
{
    trimWhitespace(field);
    addLeadingZero(field);
}

function addLeadingZero(field)
{
    if (field.value.charAt(0) == getCurrencyValidationInstance().currencyFormat.decimalSeparator) field.value = "0" + field.value;
}

function trimWhitespace(field)
{
    if (field != null && field.value != null) field.value = field.value.replace(/^\s+|\s+$/g, '');
}

var states = [
    new Array("Please Select", ""),
    new Array("Outside US", "Outside US"),
    new Array("Alabama", "AL"),
    new Array("Alaska", "AK"),
    new Array("Alberta", "AB"),
    new Array("American Samoa", "AS"),
    new Array("Arizona", "AZ"),
    new Array("Arkansas", "AR"),
    new Array("Armed Forces the Americas", "AA"),
    new Array("Armed Forces Europe", "AE"),
    new Array("Armed Forces Pacific", "AP"),
    new Array("British Columbia", "BC"),
    new Array("California", "CA"),
    new Array("Colorado", "CO"),
    new Array("Connecticut", "CT"),
    new Array("Delaware", "DE"),
    new Array("District Of Columbia", "DC"),
    new Array("Federated States Of Micronesia", "FM"),
    new Array("Florida", "FL"),
    new Array("Georgia", "GA"),
    new Array("Guam", "GU"),
    new Array("Hawaii", "HI"),
    new Array("Idaho", "ID"),
    new Array("Illinois", "IL"),
    new Array("Indiana", "IN"),
    new Array("Iowa", "IA"),
    new Array("Kansas", "KS"),
    new Array("Kentucky", "KY"),
    new Array("Louisiana", "LA"),
    new Array("Maine", "ME"),
    new Array("Manitoba", "MB"),
    new Array("Marshall Islands", "MH"),
    new Array("Maryland", "MD"),
    new Array("Massachusetts", "MA"),
    new Array("Michigan", "MI"),
    new Array("Minnesota", "MN"),
    new Array("Mississippi", "MS"),
    new Array("Missouri", "MO"),
    new Array("Montana", "MT"),
    new Array("Nebraska", "NE"),
    new Array("Nevada", "NV"),
    new Array("New Brunswick", "NB"),
    new Array("New Hampshire", "NH"),
    new Array("New Jersey", "NJ"),
    new Array("New Mexico", "NM"),
    new Array("New York", "NY"),
    new Array("Newfoundland", "NF"),
    new Array("North Carolina", "NC"),
    new Array("North Dakota", "ND"),
    new Array("Northern Mariana Islands", "MP"),
    new Array("Northwest Territories", "NT"),
    new Array("Nova Scotia", "NS"),
    new Array("Nunavut", "NU"),
    new Array("Ohio", "OH"),
    new Array("Oklahoma", "OK"),
    new Array("Ontario", "ON"),
    new Array("Oregon", "OR"),
    new Array("Palau", "PW"),
    new Array("Pennsylvania", "PA"),
    new Array("Prince Edward Island", "PE"),
    new Array("Puerto Rico", "PR"),
    new Array("Quebec", "PQ"),
    new Array("Rhode Island", "RI"),
    new Array("Saskatchewan", "SK"),
    new Array("South Carolina", "SC"),
    new Array("South Dakota", "SD"),
    new Array("Tennessee", "TN"),
    new Array("Texas", "TX"),
    new Array("Utah", "UT"),
    new Array("Vermont", "VT"),
    new Array("Virgin Islands", "VI"),
    new Array("Virginia", "VA"),
    new Array("Washington", "WA"),
    new Array("West Virginia", "WV"),
    new Array("Wisconsin", "WI"),
    new Array("Wyoming", "WY"),
    new Array("Yukon", "YT")];

var countries = [
    new Array("Please Select", ""),
    new Array("United States", "US"),
    new Array("Australia", "AU"),
    new Array("Canada", "CA"),
    new Array("France", "FR"),
    new Array("Germany", "DE"),
    new Array("Israel", "IL"),
    new Array("Italy", "IT"),
    new Array("Japan", "JP"),
    new Array("Mexico", "MX"),
    new Array("New Zealand", "NZ"),
    new Array("Spain", "ES"),
    new Array("United Kingdom", "UK")];

var allCountries = [
    new Array("Please Select", ""),
    new Array("United States", "US"),
    new Array("Afghanistan", "AF"),
    new Array("Albania", "AL"),
    new Array("Algeria", "DZ"),
    new Array("American Samoa", "AS"),
    new Array("Andorra", "AD"),
    new Array("Angola", "AO"),
    new Array("Anguilla", "AI"),
    new Array("Antarctica", "AQ"),
    new Array("Antigua And Barbuda", "AG"),
    new Array("Argentina", "AR"),
    new Array("Armenia", "AM"),
    new Array("Aruba", "AW"),
    new Array("Australia", "AU"),
    new Array("Austria", "AT"),
    new Array("Azerbaijan", "AZ"),
    new Array("Bahamas", "BS"),
    new Array("Bahrain", "BH"),
    new Array("Bangladesh", "BD"),
    new Array("Barbados", "BB"),
    new Array("Belarus", "BY"),
    new Array("Belgium", "BE"),
    new Array("Belize", "BZ"),
    new Array("Benin", "BJ"),
    new Array("Bermuda", "BM"),
    new Array("Bhutan", "BT"),
    new Array("Bolivia", "BO"),
    new Array("Bosnia-Herzegovina", "BA"),
    new Array("Botswana", "BW"),
    new Array("Bouvet Island", "BV"),
    new Array("Brazil", "BR"),
    new Array("British Indian Ocean Territory", "IO"),
    new Array("Brunei Darussalam", "BN"),
    new Array("Bulgaria", "BG"),
    new Array("Burkina Faso", "BF"),
    new Array("Burundi", "BI"),
    new Array("Cambodia", "KH"),
    new Array("Cameroon", "CM"),
    new Array("Canada", "CA"),
    new Array("Cape Verde", "CV"),
    new Array("Cayman Islands", "KY"),
    new Array("Central African Republic", "CF"),
    new Array("Chad", "TD"),
    new Array("Chile", "CL"),
    new Array("China", "CN"),
    new Array("Christmas Island", "CX"),
    new Array("Cocos (Keeling) Islands", "CC"),
    new Array("Colombia", "CO"),
    new Array("Comoros", "KM"),
    new Array("Congo", "CG"),
    new Array("Cook Islands", "CK"),
    new Array("Costa Rica", "CR"),
    new Array("Cote D'ivoire", "CI"),
    new Array("Croatia (Local Name: Hrvatska)", "HR"),
    new Array("Cuba", "CU"),
    new Array("Cyprus", "CY"),
    new Array("Czech Republic", "CZ"),
    new Array("Denmark", "DK"),
    new Array("Djibouti", "DJ"),
    new Array("Dominica", "DM"),
    new Array("Dominican Republic", "DO"),
    new Array("East Timor", "TP"),
    new Array("Ecuador", "EC"),
    new Array("Egypt", "EG"),
    new Array("El Salvador", "SV"),
    new Array("Equatorial Guinea", "GQ"),
    new Array("Eritrea", "ER"),
    new Array("Estonia", "EE"),
    new Array("Ethiopia", "ET"),
    new Array("Falkland Islands (Malvinas)", "FK"),
    new Array("Faroe Islands", "FO"),
    new Array("Fiji", "FJ"),
    new Array("Finland", "FI"),
    new Array("France", "FR"),
    new Array("France, Metropolitan", "FX"),
    new Array("French Guiana", "GF"),
    new Array("French Polynesia", "PF"),
    new Array("French Southern Territories", "TF"),
    new Array("Gabon", "GA"),
    new Array("Gambia", "GM"),
    new Array("Georgia", "GE"),
    new Array("Germany", "DE"),
    new Array("Ghana", "GH"),
    new Array("Gibraltar", "GI"),
    new Array("Greece", "GR"),
    new Array("Greenland", "GL"),
    new Array("Grenada", "GD"),
    new Array("Guadeloupe", "GP"),
    new Array("Guam", "GU"),
    new Array("Guatemala", "GT"),
    new Array("Guinea", "GN"),
    new Array("Guinea-bissau", "GW"),
    new Array("Guyana", "GY"),
    new Array("Haiti", "HT"),
    new Array("Heard And Mc Donald Islands", "HM"),
    new Array("Honduras", "HN"),
    new Array("Hong Kong", "HK"),
    new Array("Hungary", "HU"),
    new Array("Iceland", "IS"),
    new Array("India", "IN"),
    new Array("Indonesia", "ID"),
    new Array("Iran (Islamic Republic Of)", "IR"),
    new Array("Iraq", "IQ"),
    new Array("Ireland", "IE"),
    new Array("Israel", "IL"),
    new Array("Italy", "IT"),
    new Array("Jamaica", "JM"),
    new Array("Japan", "JP"),
    new Array("Jordan", "JO"),
    new Array("Kazakhstan", "KZ"),
    new Array("Kenya", "KE"),
    new Array("Kiribati", "KI"),
    new Array("Korea, Democratic People's Republic Of", "KP"),
    new Array("Korea, Republic Of", "KR"),
    new Array("Kuwait", "KW"),
    new Array("Kyrgyzstan", "KG"),
    new Array("Lao People's Democratic Republic", "LA"),
    new Array("Latvia", "LV"),
    new Array("Lebanon", "LB"),
    new Array("Lesotho", "LS"),
    new Array("Liberia", "LR"),
    new Array("Libyan Arab Jamahiriya", "LY"),
    new Array("Liechtenstein", "LI"),
    new Array("Lithuania", "LT"),
    new Array("Luxembourg", "LU"),
    new Array("Macau", "MO"),
    new Array("Macedonia, The Former Yugoslav Republic Of", "MK"),
    new Array("Madagascar", "MG"),
    new Array("Malawi", "MW"),
    new Array("Malaysia", "MY"),
    new Array("Maldives", "MV"),
    new Array("Mali", "ML"),
    new Array("Malta", "MT"),
    new Array("Marshall Islands", "MH"),
    new Array("Martinique", "MQ"),
    new Array("Mauritania", "MR"),
    new Array("Mauritius", "MU"),
    new Array("Mayotte", "YT"),
    new Array("Mexico", "MX"),
    new Array("Micronesia, Federated States Of", "FM"),
    new Array("Moldova, Republic Of", "MD"),
    new Array("Monaco", "MC"),
    new Array("Mongolia", "MN"),
    new Array("Montserrat", "MS"),
    new Array("Morocco", "MA"),
    new Array("Mozambique", "MZ"),
    new Array("Myanmar", "MM"),
    new Array("Namibia", "NA"),
    new Array("Nauru", "NR"),
    new Array("Nepal", "NP"),
    new Array("Netherlands", "NL"),
    new Array("Netherlands Antilles", "AN"),
    new Array("New Caledonia", "NC"),
    new Array("New Zealand", "NZ"),
    new Array("Nicaragua", "NI"),
    new Array("Niger", "NE"),
    new Array("Nigeria", "NG"),
    new Array("Niue", "NU"),
    new Array("Norfolk Island", "NF"),
    new Array("Northern Mariana Islands", "MP"),
    new Array("Norway", "NO"),
    new Array("Oman", "OM"),
    new Array("Pakistan", "PK"),
    new Array("Palau", "PW"),
    new Array("Panama", "PA"),
    new Array("Papua New Guinea", "PG"),
    new Array("Paraguay", "PY"),
    new Array("Peru", "PE"),
    new Array("Philippines", "PH"),
    new Array("Pitcairn", "PN"),
    new Array("Poland", "PL"),
    new Array("Portugal", "PT"),
    new Array("Puerto Rico", "PR"),
    new Array("Qatar", "QA"),
    new Array("Reunion", "RE"),
    new Array("Romania", "RO"),
    new Array("Russian Federation", "RU"),
    new Array("Rwanda", "RW"),
    new Array("Saint Kitts And Nevis", "KN"),
    new Array("Saint Lucia", "LC"),
    new Array("Saint Vincent And The Grenadines", "VC"),
    new Array("Samoa", "WS"),
    new Array("San Marino", "SM"),
    new Array("Sao Tome And Principe", "ST"),
    new Array("Saudi Arabia", "SA"),
    new Array("Senegal", "SN"),
    new Array("Serbia", "RS"),
    new Array("Seychelles", "SC"),
    new Array("Sierra Leone", "SL"),
    new Array("Singapore", "SG"),
    new Array("Slovakia (Slovak Republic)", "SK"),
    new Array("Slovenia", "SI"),
    new Array("Solomon Islands", "SB"),
    new Array("Somalia", "SO"),
    new Array("South Africa", "ZA"),
    new Array("South Georgia And The South Sandwich Islands", "GS"),
    new Array("Spain", "ES"),
    new Array("Sri Lanka", "LK"),
    new Array("St. Helena", "SH"),
    new Array("St. Pierre And Miquelon", "PM"),
    new Array("Sudan", "SD"),
    new Array("Suriname", "SR"),
    new Array("Svalbard And Jan Mayen Islands", "SJ"),
    new Array("Swaziland", "SZ"),
    new Array("Sweden", "SE"),
    new Array("Switzerland", "CH"),
    new Array("Syrian Arab Republic", "SY"),
    new Array("Taiwan, Republic Of China", "TW"),
    new Array("Tajikistan", "TJ"),
    new Array("Tanzania, United Republic Of", "TZ"),
    new Array("Thailand", "TH"),
    new Array("Togo", "TG"),
    new Array("Tokelau", "TK"),
    new Array("Tonga", "TO"),
    new Array("Trinidad And Tobago", "TT"),
    new Array("Tunisia", "TN"),
    new Array("Turkey", "TR"),
    new Array("Turkmenistan", "TM"),
    new Array("Turks And Caicos Islands", "TC"),
    new Array("Tuvalu", "TV"),
    new Array("Uganda", "UG"),
    new Array("Ukraine", "UA"),
    new Array("United Arab Emirates", "AE"),
    new Array("United Kingdom", "UK"),
    new Array("United States Minor Outlying Islands", "UM"),
    new Array("Uruguay", "UY"),
    new Array("Uzbekistan", "UZ"),
    new Array("Vanuatu", "VU"),
    new Array("Vatican City State (Holy See)", "VA"),
    new Array("Venezuela", "VE"),
    new Array("Viet Nam", "VN"),
    new Array("Virgin Islands (British)", "VG"),
    new Array("Virgin Islands (U S)", "VI"),
    new Array("Wallis And Futuna Islands", "WF"),
    new Array("Western Sahara", "EH"),
    new Array("Yemen", "YE"),
    new Array("Zaire", "ZR"),
    new Array("Zambia", "ZM"),
    new Array("Zimbabwe", "ZW")];

function writeStates(defaultValue) { writeSelectOptions(states, defaultValue); }
function writeCountries(defaultValue) { writeSelectOptions(countries, defaultValue); }
function writeAllCountries(defaultValue) { writeSelectOptions(allCountries, defaultValue); }

function writeSelectOptions(selectData, defaultValue)
{
    for (var i = 0; i < selectData.length; i++)
    {
        document.write("<option value=\"" + selectData[i][1] + "\"");
        if (defaultValue == selectData[i][1] || selectData[i][2] == "true")
        {
            document.write(" selected");
        }
        document.writeln(">" + selectData[i][0] + "</option>");
    }
}

function writeDays()
{
    for (var i = 1; i <= 31; i++)
    {
        var s = new Number(i).toString();
        if (s.length == 1) s = new String("0") + s;
        document.writeln("<option value=\"" + s + "\">" + s + "</option>");
    }
}

function writeMonths(defaultValue) {
		var selected = null;

    for (var i = 1; i <= 12; i++) {
        var s = new Number(i).toString();
        if (s.length == 1) s = new String("0") + s;
		if (defaultValue == i || defaultValue == s) {
			selected = " selected";
		} else {
			selected = null;
		}
        document.writeln("<option value=\"" + s + "\" "+ selected +">" + s + "</option>");
    }
}

function writeYears(start, num, defaultValue) {
	var selected = null;
    var startYear;
    if (Number(start) == -1)    // start at current year
    {
        startYear = getCurrentYear();
    }
    else
    {
        startYear = start;
    }

    for (var i = startYear; i <= startYear + num; i++) {
		if (defaultValue == i) {
			selected = " selected";
		} else {
			selected = null;
		}
        document.writeln("<option value=\"" + i + "\" " + selected + ">" + i + "</option>");
    }
}

function getCurrentYear() 
{    
  var d = new Date();   
  var year = new Number(d.getFullYear());      

  return year; 
}

function getStringDate()
{
    var d = new Date();
    return (d.getMonth() + 1) + "/" + d.getDate() + "/" + getCurrentYear();
}

function updatePaymentType()
{
    if (document.donorForm.paymentType[0].checked)
    {
        document.donorForm.transType.value = "donation";
    }
    else if (document.donorForm.paymentType[1].checked)
    {
        document.donorForm.transType.value = "check";
    }
}

function updateTransType(type)
{
    document.donorForm.transType.value = type;
}

function setFrequencyOnChangeEventCheck()
{
    var rgsFrequency = document.donorForm.rgsFrequency;
    var onChangeEvent = (rgsFrequency) ? rgsFrequency.onchange : null;
    if (onChangeEvent && onChangeEvent.toString().indexOf("setFrequency()") >= 0) setFrequency();
}

function setFrequency()
{
    var rgsCreate = document.donorForm.rgsCreate;
    var rgsFrequency = document.donorForm.rgsFrequency;
    if (rgsCreate && rgsFrequency) rgsCreate.value = (rgsFrequency.value == "") ? "" : "true";
}

function getElementByName(eName)
{
    for (var i = 0; i < document.donorForm.elements.length; i++)
    {
        if (document.donorForm.elements[i].name == eName)
        {
            return document.donorForm.elements[i];
        }
    }

    return null;
}

function getElementStartsWithName(eName)
{
    for (var i = 0; i < document.donorForm.elements.length; i++)
    {
        if (document.donorForm.elements[i].name.indexOf(eName) >= 0)
        {
            return document.donorForm.elements[i];
        }
    }

    return null;
}

// page author must write a matrix variable "costs" as such
//
// var costs = new Array();
// costs[i] = new Array(amount, eventName, variableName, UDFname);
//
// e.g.
// var costs = new Array();
// costs[0] = new Array(25, "Event 1", "Activities", "Entity_SetElement|Activities|Event 1");
// costs[1] = new Array(50, "Event 2", "Activities", "Entity_SetElement|Activities|Event 2");
//
// in the page call writeCosts(costs, start, end, inputType)
// e.g. <script>writeCosts(costs, 0, 1, "radio")</script>
//
// in validation call getCosts(costs, baseFee)
function writeCosts(costs, start, end, inputType)
{
    for (var i = start; i < (end + 1); i++)
    {
        document.write("<input type= \"" + inputType + "\" name=\"" + costs[i][2] + "\" value=\"" + costs[i][0] + "\">" + costs[i][1] + " - $" + costs[i][0] + "<br>");
        document.write("<input type=\"hidden\" name=\"" + costs[i][3] + "\">");
    }
}

function getCosts(costs, baseFee)
{
    var form = document.donorForm;
    var total = parseFloat(baseFee);
    
    for (var i = 0; i < costs.length; i++)
    {
        for (var c = 0; c < (form.elements.length - 1); c++)
        {
            if (form.elements[c+1].name == costs[i][3])
            {
                if (form.elements[c].checked)
                {
                    form.elements[c+1].value = "true";
                    total = total + parseFloat(form.elements[c].value);
                }
                else if (form.elements[c].checked != true)
                {
                    form.elements[c+1].value = "false";
                }
            }
        }
    }
    form.amount.value = total;
}

// this function changes fields from the donorForm into UDF format
// this will most likely be use when a UDF can only take one value
// 
// form requirements: hidden fields for the UDF's which follow the UDF naming conventions
//
// Example: if a company has a UDF named Grades with values A, B, C, D, and F
// <input type="radio" name="Grades" value="A">A
// <input type="radio" name="Grades" value="B">B
// <input type="radio" name="Grades" value="C">C
// <input type="radio" name="Grades" value="D">D
// <input type="radio" name="Grades" value="F">F
// <input type="hidden" name="Entity_SetElement|Grades|A">
// <input type="hidden" name="Entity_SetElement|Grades|B">
// <input type="hidden" name="Entity_SetElement|Grades|C">
// <input type="hidden" name="Entity_SetElement|Grades|D">
// <input type="hidden" name="Entity_SetElement|Grades|F">
//
// possible input types are radio and select box
//
// <script>setUDFValue(document.donorForm.Grades, "Grades");</script>

function setUDFValue(fieldName, eventName)
{
    if (fieldName == null) return;
    
    for (var i = 0; i < fieldName.length; i++)
    {
        if (fieldName[i].checked || fieldName[i].selected)
        {
            var form = document.donorForm;
            for (var c = 0; c < form.elements.length; c++)
            {
                if (form.elements[c].name == null) continue;
                if (form.elements[c].name.indexOf("_SetElement|" + eventName + "|" + fieldName[i].value) >= 0)
                {
                    form.elements[c].value = "true";
                }
            }
        }
        else if (fieldName[i].checked != true && fieldName[i].selected != true)
        {
            var form = document.donorForm;
            for (var c = 0; c < form.elements.length; c++)
            {
                if (form.elements[c].name == null) continue;
                if (form.elements[c].name.indexOf("_SetElement|" + eventName + "|" + fieldName[i].value) >= 0)
                {
                    form.elements[c].value = "false";
                }
            }
        }
    }
}

// this function changes fields from the donorForm into UDF format.
// this should only be used when a UDF can only take one value.
// 
// form requirements: hidden div for the udf
// Acceptable third parameters: Entity, Persona, JournalEntry
//
// Example: if a org has a UDF named Grades with values A, B, C, D, and F
// <input type="radio" name="Grades" value="A">A
// <input type="radio" name="Grades" value="B">B
// <input type="radio" name="Grades" value="C">C
// <input type="radio" name="Grades" value="D">D
// <input type="radio" name="Grades" value="F">F
//
// possible input types are radio and select box
//
// <script>createUDFValue(document.donorForm.Grades, "Grades", "Entity", "gradesDiv");</script>
//
// The fourth parameter should be an unused element name.  This name will be used to dynamically
// create a div that will be appended to the form if the UDF has a selected value.

function createUDFValue(fieldName, udfName, appliesTo, divName)
{
    if (fieldName == null) return;
    
    // Locate and clear div if found
    var udfDiv = document.getElementById(divName);
    if (udfDiv != null)
    {
        var copy = new Array();
        for (var i = 0; i < udfDiv.childNodes.length; i++) copy.push(udfDiv.childNodes[i]);
        for (var i = 0; i < copy.length; i++) udfDiv.removeChild(copy[i]);

        document.donorForm.appendChild(udfDiv);
    }

    var validating = document.donorForm.firstName.value == "validatePage";

    for (var i = 0; i < fieldName.length; i++)
    {
        if (validating || fieldName[i].checked || fieldName[i].selected)
        {
            if (fieldName[i].value != "")
            {
                // Create div & append to form if not found
                if (udfDiv == null)
                {
                    udfDiv = document.createElement("div");
                    udfDiv.setAttribute("id", divName);
                    document.donorForm.appendChild(udfDiv);
                }

                var name = appliesTo + "_SetElement|" + udfName + "|" + fieldName[i].value;
                
                // Append element to div
                var input = document.createElement("input");
                input.setAttribute("type", "hidden");
                input.setAttribute("name", name);
                input.setAttribute("value", "true");
                udfDiv.appendChild(input);
            }

            if (!validating) break;
        }
    }
}

function createUDFValueAPI(fieldName, udfName, appliesTo, divName)
{
    // Locate and clear div if found
    var udfDiv = document.getElementById(divName);
    if (udfDiv != null)
    {
        var copy = new Array();
        for (var i = 0; i < udfDiv.childNodes.length; i++) copy.push(udfDiv.childNodes[i]);
        for (var i = 0; i < copy.length; i++) udfDiv.removeChild(copy[i]);

        document.donorForm.appendChild(udfDiv);
    }

    for (var i = 0; i < fieldName.length; i++)
    {
        if (fieldName[i].checked || fieldName[i].selected)
        {
            if (fieldName[i].value != "")
            {
                // Create div & append to form if not found
                if (udfDiv == null)
                {
                    udfDiv = document.createElement("div");
                    udfDiv.setAttribute("id", divName);
                    document.donorForm.appendChild(udfDiv);
                }

                var name = appliesTo + "_SetElement[" + udfName + "|" + fieldName[i].value + "]";
                
                // Append element to div
                var input = document.createElement("input");
                input.setAttribute("type", "hidden");
                input.setAttribute("name", name);
                input.setAttribute("value", "true");
                udfDiv.appendChild(input);
            }
            break;
        }
    }
}

function isRadioOptionSelected(radioField)
{
    for (var i = 0; i < radioField.length; i++)
    {
        if (radioField[i].checked) return true;
    }

    return false;
}

// Removes all characters which appear in string bag from string s.
function stripCharsInBag(s, bag)
{
    if (s == null) return "";

    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    var returnString = "";
    for (var i = 0; i < s.length; i++)
    {
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }

    return returnString;
}

//This function writes the ssl certificate image to the page
function getSSL()
{
    document.write('<table width="135" border="0" cellpadding="2" cellspacing="0" title="Click to Verify - This site chose VeriSign SSL for secure e-commerce and confidential communications."><tr><td width="135" align="center" valign="top"><script type="text/javascript" src="https://seal.verisign.com/getseal?host_name=app.etapestry.com&amp;size=M&amp;use_flash=YES&amp;use_transparent=YES&amp;lang=en"></script><br /><a href="http://www.verisign.com/ssl-certificate/" target="_blank"  style="color:#000000; text-decoration:none; font:bold 7px verdana,sans-serif; letter-spacing:.5px; text-align:center; margin:0px; padding:0px;">ABOUT SSL CERTIFICATES</a></td></tr></table>');
    //document.write('<script type="text/javascript" src="https://sealserver.trustwave.com/seal.js?style=invert"></script>');
}

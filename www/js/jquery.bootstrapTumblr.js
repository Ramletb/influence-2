// Bootstrap Tumblr
// v1.0
// cjimti@gmail.com
//
// Example:
//
// var g = $("#gridContainer").gridTool(34,40, function(boxName, boxMap) { console.log(boxMap[boxName]); } );
//
;(function( $, window, document, undefined ){
	$.fn.bootstrapTumblr = function( posts, options, context ) {

		context = context ? context : this;

		var settings = $.extend( {
			displayRows: true,
			header1: "h1",
			postLink: "#"
		}, options);

		var container = $(document.createElement('div'));

		var renderImage = function(content, post) {
			content.html('<a href="' + settings.postLink + post.id + '/title/' + post.slug + '">' + '<' + settings.header1 + ' style="line-height: 30px">Image</' + settings.header1 + '></a><p><ul class="thumbnails"><li><div class="thumbnail"><img src="' + post["photo-url-1280"] + '" /><div class="caption">' + post["photo-caption"] + '</div></li></ul></div></p>');
		};

		var renderQuote = function(content, post) {
			content.html('<a href="' + settings.postLink + post.id + '/title/' + post.slug + '">' + "<" + settings.header1 + " style=\"line-height: 30px\">Quote</" + settings.header1 + "></a><p><div class=\"hero-unit\"><p>" + post["quote-text"] + "</p><p><small>" + post["quote-source"] + "</small></p></div></p>");
		};

		var renderRegular = function(content, post) {
			content.html('<a href="' + settings.postLink + post.id + '/title/' + post.slug + '">' + "<" + settings.header1 + " style=\"line-height: 30px\">" + post["regular-title"] + "</" + settings.header1 + "></a>" + post["regular-body"]);
		};

		var renderConversation = function(content, post) {
			var html = '<a href="' + settings.postLink + post.id + '/title/' + post.slug + '">' + "<" + settings.header1 + " style=\"line-height: 30px\">" + post["conversation-title"] + "</" + settings.header1 + "></a><dl class=\"dl-horizontal\">";
			for(var i = 0; i < post.conversation.length; i++ ) {
				html += "<dt>" + post.conversation[i].name + "</dt>";
				html += "<dd>" + post.conversation[i].phrase + "</dd>";
			}
			html += "</dl>";
			content.html(html);
		};

		var renderLink = function(content, post) {
			var html = '<a href="' + settings.postLink + post.id + '/title/' + post.slug + '">' + "<" + settings.header1 + " style=\"line-height: 30px\">" + post["link-text"] + "</" + settings.header1 + "></a>";
			if(post["regular-description"]) {
				html += "<p><a href=\"" + post["link-url"] + "\">" + post["link-description"] + "</a></p>"
			} else {
				html += "<p><a href=\"" + post["link-url"] + "\">" + post["link-url"] + "</a></p>"
			}
			content.html(html);
		};

		var renderAudio = function(content, post) {
			var html = '<a href="' + settings.postLink + post.id + '/title/' + post.slug + '">' + "<" + settings.header1 + " style=\"line-height: 30px\">" + post["id3-title"] + "</a> <small>" + post["audio-caption"] + "</small><" + settings.header1 + ">";
			html += "<p>" + post["audio-player"] + "</p>";
			content.html(html);
		};

		var renderVideo = function(content, post) {
			var html = '<a href="' + settings.postLink + post.id + '/title/' + post.slug + '">' + "<" + settings.header1 + " style=\"line-height: 30px\">Watch</a> <small>" + post["video-caption"] + "</small><" + settings.header1 + ">";
			html += "<p class=\"visible-phone\">" + post["video-player-250"] + "</p>";
			html += "<p class=\"hidden-phone\">" + post["video-player-500"] + "</p>";
			content.html(html);
		};

		container.each(function(index) {
			for(var r = 0; r < posts.length; r++) {

				var dateContent = $(document.createElement('div'));
				dateContent.html('<a href="' + settings.postLink + posts[r].id + '/title/' + posts[r].slug + '"><h6>' + posts[r].date.substring(0, posts[r].date.length - 8) + '</h6></a></p><p>');

				var postContent = $(document.createElement('div'));

				if(settings.displayRows) {
					var row = $(document.createElement('div')).addClass("row");
					dateContent.addClass("span2");
					postContent.addClass("span10");
				}


				switch (posts[r].type) {
					case "photo":
						renderImage(postContent, posts[r]);
						break;

					case "quote":
						renderQuote(postContent, posts[r]);
						break;

					case "regular":
						renderRegular(postContent, posts[r]);
						break;

					case "link":
						renderLink(postContent, posts[r]);
						break;

					case "conversation":
						renderConversation(postContent, posts[r]);
						break;

					case "audio":
						renderAudio(postContent, posts[r]);
						break;

					case "video":
						renderVideo(postContent, posts[r]);
						break;

					default:
						postContent.html("<p></p>");
				}

				if(settings.displayRows) {
					row.append(dateContent);
					row.append(postContent);

					container.append(row);
				} else {
					container.append(dateContent);
					container.append(postContent);
				}

			}
		});

		this.append(container);

		return this;
  };
})( jQuery, window, document  );

$('#donationForm').validate({

	errorElement: "p",

	rules: {
		giftAmount: {
			digits: true,
			required: true,
			min: 1
		},
		salutation: "required",
		firstName: "required",
		lastName: "required",
		address: "required",
		city: "required",
		state: "required",
		postalCode: "required",
		country: "required",
		email: {
			required: true,
			email: true
		},
		phone: {
			required: true,
			phoneUS: true
		},
		cardType: "required",
		cardNumber: {
			required: true,
			creditcard: true
		},
		cvv2: {
			required: true,
			minlength: {
				param: function() { 
					return $('#cardType').val() == "American Express" ? 4 : 3;
				}
			},
			maxlength: {
				param: function() { 
					return $('#cardType').val() == "American Express" ? 4 : 3;
				}
			},
			digits: true
		},
		cardExpMonth: "required",
		cardExpYear: "required"
	},

	submitHandler: function(form) {
		$('#submitDonationBtn').click(function(event) { 
			event.preventDefault();
			event.stopPropagation();
		});

		$('#submitDonationBtn').html('<i class="icon-time icon-white"></i> Processing...');
		$('#submitDonationBtn').addClass("disabled");
		form.submit();
	},

	highlight: function(element, errorClass, validClass) {
		$(element).closest(".control-group").addClass("error");
	},

	unhighlight: function(element, errorClass, validClass) {
		$(element).closest(".control-group").removeClass("error");
	}

});


function initialize() {
  var image = '/images/i_map.png';
  var imageKids = '/images/i_map_kids.png';
  var imageOffice = '/images/i_map_service2.png';
  
  var mapStyles = [
    {
      "stylers": [
        { "visibility": "on" },
        { "saturation": -100 }
      ]
    }
  ];

  var styledMap = new google.maps.StyledMapType(mapStyles, { name: "Styled Map" });

  var mapOptions = {
    zoom: 15,
    center: new google.maps.LatLng(33.865615,-117.74402),
    mapTypeControlOptions: {
      mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
    },
    disableDefaultUI: false
  };

  var map = new google.maps.Map(document.getElementById('service_map_canvas'), mapOptions);
  map.mapTypes.set('map_style', styledMap);
  map.setMapTypeId('map_style');

  var marker = new google.maps.Marker({
    position: new google.maps.LatLng(33.8658522,-117.7441266),
    map: map,
    icon: image
  });

}

function loadScript() {
  var script = document.createElement("script");
  script.type = "text/javascript";
  script.src = "http://maps.googleapis.com/maps/api/js?key=&sensor=false&callback=initialize";
  document.body.appendChild(script);
}

window.onload = loadScript;

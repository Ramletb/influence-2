<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * ActionService.php
 *
 * Contains the {@link ActionService} class.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage core
 */


/**
 * The ActionService Class
 *
 * @package SP5
 * @subpackage core
 */
class ActionService extends Service {

  /**
   * Constructor
   * 
   * ActionService constructor is called when a 
   * service is instantiated by a serviceFactory.
   * 
   * @ignore
   * @see Database
   * @see DatabaseFactory
   * @access public
   */  
  public function __construct() {
    parent::__construct();
  }

  /**
   * get
   * 
   * Perform a GET to a controller/action.
   *
   * @access protected
   * @param array $args_name_value_pairs
   * @returns string $connection
   */
   public function get($model, $url, $args) {
     $qs = '';
     if (is_array($args)) {
       $args = $model->prepare($args);

       foreach ($args as $key => $value) {
         $qs .= $qs ? '&' : '';
         $qs .= "$key=$value";
       }
     }

     $url .= '.' . $model->getFormat();
     $url .= $qs ? '?' . $qs : '';;

     $ret = file_get_contents($url);
 
     return $model->getUnserialize() ? unserialize($ret) : $ret;
   }

    /**
   * post
   *
   * Perform a POST to a controller/action.
   *
   * @access protected
   * @param ActionModel $model
   * @param string $url
   * @param array $getArgs
   * @param array $postArgs
   * @param array $optionalHeaders
   * @returns string $connection
   */
   public function post($model, $url, $getArgs, $postArgs, $optionalHeaders = null) {

     $url .= '.' . $model->getFormat();

     $getQS = '';
     if ($getArgs && is_array($getArgs)) {
       $getArgs = $model->prepare($getArgs);

       foreach ($getArgs as $key => $value) {
         $getQS .= $getQS ? '&' : '';
         $getQS .= "$key=$value";
       }

       $url .= '?' . $getQS;
     }


     $postQS = '';
     if ($postArgs && is_array($postArgs)) {
       $args = $model->prepare($postArgs);

       foreach ($postArgs as $key => $value) {
         $postQS .= $postQS ? '&' : '';
         $postQS .= "$key=$value";
       }
     }

     $params = array('http' => array( 'method' => 'POST',
                                      'content' => $postQS)
                    );

     if ($optional_headers !== null) {
        $params['http']['header'] = $optionalHeaders;
     }

     $ctx = stream_context_create($params);
     $fp = @fopen($url, 'rb', false, $ctx);
     if (!$fp) {
        throw new Exception("Problem with $url, $php_errormsg");
     }
     $response = @stream_get_contents($fp);

     if ($response === false) {
        throw new Exception("Problem reading data from $url, $php_errormsg");
     }

     return $model->getUnserialize() ? unserialize($response) : $response;
  }

    /**
   * invoke
   *
   * execute a remote controller/action.
   *
   * @access protected
   * @param ActionModel $model
   * @param string $url
   * @param array $getArgs
   * @param array $postArgs
   * @param array $optionalHeaders
   * @returns string $connection
   */
   public function invoke($model, $url, $getArgs, $postArgs, $optionalHeaders = null) {
     if (!$postArgs) {
       return $this->get($model, $url, $getArgs);
     } else {
       return $this->post($model, $url, $getArgs, $postArgs, $optionalHeaders = null);
     }
   }

}


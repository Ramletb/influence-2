<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * Framework.php 
 * 
 * Contains the abstract {@link Framework} class.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage core
 */

/**
 * Require default_ini.php
 *
 * or require custom_ini.php if it exists.
 *
 */
if(file_exists('../config/custom_ini.php')) {
  require_once('../config/custom_ini.php');
 } else if (file_exists('../config/default_ini.php')) {
  require_once('../config/default_ini.php');
 }

/**
 * Require Constants.php
 *
 * Requires constatns contained 
 * in {@link Constants.php}
 *
 */
require_once('Constants.php');

/**
 * Require Core.php
 *
 * Requires the {@link Core} class 
 * contained in {@link Core.php}.
 */
require_once('Core.php');

/**
 * Require Payload.php
 *
 * Requires the {@link Payload} class
 * contained in {@link Payload.php}.
 */
require_once('Payload.php');

/**
 * Require App.php
 * 
 * Requires the {@link App} class
 * contained in {@link App.php}.
 */
require_once('App.php');

/**
 * Require Renderer.php
 * 
 * Requires the {@link Renderer} class
 * contained in {@link Renderer.php}.
 */
require_once('Renderer.php');

/**
 * Require ClientData.php
 * 
 * Requires the {@link ClientData} class
 * contained in {@link ClientData.php}.
 */
require_once('ClientData.php');

/**
 * The Abstract Framework Class
 * 
 * See the Quikstart Guide on {@link local:docs/r.php?t=quickstart&s=helloworld.keyconcepts.frameworks Frameworks}
 * for a brief overview.
 *
 * @package SP5
 * @subpackage core
 */
abstract class Framework extends Core {

  /**
   * Controller Name
   *
   * Holds the name of the current {@link Controller}
   *
   * @access protected
   * @var string $controllerName
   */
  protected $controllerName;

  /**
   * Action Name
   *
   * Holds the name of the current action.
   *
   * @access protected
   * @var string $actionName
   */
  protected $actionName;

  /**
   * Constructor
   *
   * @ignore
   * @access public
   */  
  public function __construct() {
    parent::__construct();
    @set_exception_handler(array('Framework', 'exceptionHandler'));

    date_default_timezone_set($this->configuration->application['TIME']['zone']);


    $this->logger->trace("----------------------- BEGIN FRAMEWORK PROCESS [".getmypid()."]-------------------------");
    $this->logger->trace("REQUEST       : [". $this->getRequest() ."]");
    $this->logger->trace("REQUEST METHOD: [". $this->getRequestMethod() ."]");
    $this->logger->trace("REQUEST   FROM: [". $this->getRequester() ."]");
  }

  /**
   * Execption Handler
   *
   * The Framework exception handler
   *
   * @access public
   * @static
   */  
  public static function exceptionHandler($e) {
       print "Uncaught Framework Exception: ". $e->getMessage() ."\n";
   }
  
  /**
   * Clone
   *
   * Prevent Cloneing
   *
   * @access private
   */  
  private function __clone() {
  }

  /**
   * Get Request
   *
   * Get the request (REQUEST_URI for web)
   *
   * @access public
   * @return string call
   */  
  public function getRequester() {
    return $_SERVER['REMOTE_ADDR'];
  }

  /**
   * Get Request
   *
   * Get the request (REQUEST_URI for web)
   *
   * @access public
   * @return string call
   */  
  public function getRequest() {
    return $_SERVER['REQUEST_URI'];
  }

  /**
   * Get Request Method
   *
   * Get the request method (REQUEST_METHOD for web)
   *
   * @access public
   * @return string call
   */  
  public function getRequestMethod() {
    return $_SERVER['REQUEST_METHOD'];
  }

  /**
   * Get Controller Name
   *
   * Provides the Framework dispatcher with the
   * name of the Controller to dispatch and action request to.
   *
   * @access public
   * @return string Name of controller
   */  
  public function getControllerName() {
    return ucfirst($this->configuration->application['FRAMEWORK']['default']['controller']);
  }

  /**
   * Get Action Name
   *
   * Provides the Framework dispatcher with the
   * name of the action (prefixed method within a controller) to 
   * call.
   *
   * @access public
   * @return string Name of action (prefixed method with controller)
   */
  public function getActionName() {
    return $this->configuration->application['FRAMEWORK']['default']['action'];
  }

  /**
   * Get Renderer Name
   *
   * Provides the Framework dispatcher with the
   * name of the renderer to use for views yet 
   * may also be set by the controller action.
   *
   * @see Renderer
   * @access protected
   * @return string Name of Renderer
   */
  protected function getRendererName() {
    return $this->getRendererMapping();
  }

  /**
   * Get Default Renderer Name
   *
   * Provides the Framework dispatcher with the
   * name of the default renderer to use for views.
   *
   * @see Renderer
   * @access protected
   * @return string Name of Default Renderer
   */
  protected function getDefaultRendererName() {
    return $this->configuration->application['FRAMEWORK']['default']['renderer'];
  }

  /**
   * Get Renderer Mapping
   *
   * Renderer Mapping to format
   *
   * @access protected
   * @return string Name of action (prefixed method with controller)
   */
  protected function getRendererMapping() {
    $defaultRenderer = $this->getDefaultRendererName();
    $renderer = '';

    if(strlen($this->getFormatName()) > 0 && array_key_exists($this->getFormatName(), $this->configuration->application['FRAMEWORK']['format'])) {
      $renderer = $this->configuration->application['FRAMEWORK']['format'][$this->getFormatName()];
    }

    if(strlen($renderer) > 0) {
      return $renderer;
    } else {
      return $defaultRenderer;
    }
  }

  /**
   * Get Action Prefix
   *
   * Provides the Framework dispatcher with the
   * name of prefix used for a method to make it an action.
   * An example of prefixed methods would be web_index or
   * cli_dosomething. Prefixed Controller methods are considered
   * actions.
   *
   * @access public
   * @return string Name of prefix (web, cli, etc..)
   */
  public function getActionPrefix() {
    return "core";
  }

  /**
   * Get Format Name
   *
   * Provides the Framework dispatcher with the
   * name of the format (used for mapping Parsers and Renderers)
   *
   * @access protected
   * @return string Name of format (php, txt, xhtml, xml, json, etc...)
   */
  protected function getFormatName() {
    return '';
  }

  /**
   * Get Payload
   *
   * Provides the Framework dispatcher with a
   * pointer to the Payload object
   *
   * @access protected
   * @uses Payload::getPayload()
   * @return Payload Payload refrence
   */
  protected function getPayload() {
    return Payload::getPayload();
  }

  /**
   * Get ClientData
   *
   * Provides the Framework dispatcher with a
   * pointer to the ClientData object
   *
   * @access protected
   * @uses ClientData::getClientData()
   * @param Payload $payload (optional)
   * @return ClientData ClientData refrence
   */
  protected function getClientData($payload=null) {
    return ClientData::getClientData();
  }

  /**
   * Handle Pre Render
   *
   * Handle pre render callback
   *
   * @access public
   * @param Controller $controller
   */
  public function handlePreRender($controller) {
  }

  /**
   * Pre Dispatch
   *
   * called pre dispatch
   *
   * @access protected
   */
  protected function preDispatch() {
  }

  /**
   * Post Dispatch
   *
   * called post dispatch
   *
   * @access protected
   */
  protected function postDispatch() {
  }

  /**
   * Dispatch
   *
   * The beginning of the framework process.
   * 
   * Example: <br />
   * <pre>
   * require_once('../core/frameworks/WebFramework.php');
   *
   * $webFramework = new WebFramework;
   * $webFramework->dispatch();
   * </pre>
   *
   * @access public
   * @uses Factory::getFactory()
   * @uses Framework::getActionPrefix()
   * @uses Framework::getActionName()
   * @uses Framework::getFormatName()
   * @uses Framework::getPayload()
   * @uses Framework::getClientData()
   * @uses Framework::getRendererName()
   * @uses Framework::preDispatch()
   * @uses Framework::postDispatch()
   * @final
   */  
  final public function dispatch() {
    
    try {

      // Get a controllerFactory
      //
      $controllerFactory = Factory::getFactory('Controller', array(C_PATH_CONTROLLERS,C_PATH_CORE_CONTROLLERS));

      // Ask to controllerFactory to produce a controller
      //
      $controller = $controllerFactory->load($this->getControllerName());

      if ($controller != false) {

	$actionPrefix = $this->getActionPrefix();
	$actionName   = $this->getActionName();
	$formatName   = $this->getFormatName();
	$payload      = $this->getPayload();
	$clientData   = $this->getClientData($payload);
	$rendererName = $this->getRendererName();

	// pre Dispatch
	$this->preDispatch();

	$this->logger->trace("DISPATCHING [" . $this->getControllerName() . "Controller] [" . $this->getActionPrefix() . "_" . $this->getActionName() . "]");
	
	// Ask controller to dispatch to an action
	// 
	$controller->dispatch($this,
			      $actionPrefix,
			      $actionName,
			      $formatName,
			      $payload,
			      $clientData,
			      $rendererName);
	
	// post Dispatch
	$this->postDispatch();


      } else {
        throw new Exception("Exception: Was not able to get a controller of type: [" . $this->getControllerName() . "]");
      }
            
    } catch (Exception $e) {

      $this->logger->error("TRACE: Message: " . $e->getMessage());
      $this->logger->error("TRACE:    Code: " . $e->getCode());

      foreach($e->getTrace() as $t) {
	$this->logger->error(sprintf("TRACE: [%3s] %s", $t['line'], $t['file']));
      }

      header("HTTP/1.0 400 Bad Request");
      echo "An error occurred during processing.";
    }

  }
  
}

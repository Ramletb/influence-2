<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * Core.php
 *
 * Contains the abstract {@link Core} class.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage core
 */

/**
 * Require Configuration.php
 *
 * Requires the {@link Configuration} class
 * contained in {@link Configuration.php}.
 */
require_once('Configuration.php');

/**
 * Requires Logger.php
 *
 * Requires the {@link Logger} class
 * contained in {@link Logger.php}.
 */
require_once('Logger.php');

/**
 * The Abstract Core Class
 *
 * @package SP5
 * @subpackage core
 */
abstract class Core {

  /**
   * Logger
   *
   * The protected instance variable $logger.
   *
   * Holds a pointer to a singleton {@link Logger} object used throughout 
   * various layers of the framework. This instance provides 
   * Controller subclasses access to the core Logger object.
   *
   * Example:
   * <pre>
   * $this->logger->debug('Got Here');
   * </pre>
   *
   * @var Logger $logger
   * @uses Logger::getLogger()
   * @access protected
   */  
  protected $logger;

  /**
   * Configuration
   *
   * Holds a pointer to a {@link Configuration} object.
   *
   * See the {@link local:docs/r.php?t=&s=configuration Configuration} section 
   * of the framework manual. 
   *
   * @var Configuration $configuration
   * @uses Configuration::getConfiguration()
   * @access protected
   */  
  protected $configuration;

  /**
   * Constructor
   *
   * Provide Logger and Configuration to all
   * core objects.
   *
   * @ignore
   * @access protected
   */  
  protected function __construct() {
    $this->logger = Logger::getLogger($this);
    $this->configuration = Configuration::getConfiguration();
  }

}


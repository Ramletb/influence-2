<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * Controller.php
 * 
 * Contains the abstract {@link Controller} class.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage core
 */

/**
 * Require Constants.php
 *
 * Requires constants needed for paths from {@link Constants.php}.
 */
require_once('Constants.php');

/**
 * Require Bundle.php
 *
 * Requires the base bundle class
 */
require_once('Bundle.php');

/**
 * Require DatabaseFactory.php 
 *
 * Requires the {@link DatabaseFactory} class 
 * contained in {@link DatabaseFactory.php}.
 */
require_once('DatabaseFactory.php');

/**
 * The Abstract Controller Class
 *
 * @package SP5
 * @subpackage core
 */
abstract class Controller extends App {

  /**
   * Renderer Name
   *
   * Holds the name of the current {@link Renderer}
   *
   * @var string $rendererName
   * @access protected
   */  
  protected $rendererName;

  /**
   * Renderer
   *
   * Holds a {@link Renderer} object.
   * This var is instanciated by the Framework 
   * through the the Controller's dispatch method.
   *
   * @var Renderer $renderer
   * @access protected
   */  
  protected $renderer;

  /**
   * Handled Pre Render
   *
   * Was handlePreRender already called?
   *
   * @var bool $handledPreRender
   * @access protected
   */  
  protected $handledPreRender;

  /**
   * Controller Name
   *
   * Holds the current controller name
   *
   * @var string $controllerName
   * @access protected
   */  
  protected $controllerName;

  /**
   * Action Prefix
   *
   * Holds the current action prefix. 
   * This var is instanciated by the Framework 
   * through the the Controller's dispatch method.
   *
   * @var string $actionPrefix
   * @access protected
   */  
  protected $actionPrefix;

  /**
   * Action Name
   *
   * Holds the current action name. 
   * This var is instanciated by the Framework 
   * through the the Controller's dispatch method.
   *
   * @var string $actionName
   * @access protected
   */  
  protected $actionName;

  /**
   * Format Name
   *
   * Holds the current format name.
   * This var is instanciated by the Framework 
   * through the the Controller's dispatch method.
   *
   * @var string $formatName
   * @access protected
   */  
  protected $formatName;

  /**
   * Payload
   *
   * Holds a refrence to Payload.
   * This var is instanciated by the Framework 
   * through the the Controller's dispatch method.
   *
   * @var Payload $payload
   * @access public
   */  
  private $payload;

  /**
   * Client Data
   *
   * Holds a refrence to ClientData.
   * This var is instanciated by the Framework 
   * through the the Controller's dispatch method.
   *
   * @var ClientData $clientData
   * @access public
   */  
  private $clientData;

  /**
   * Framework
   *
   * Holds a refrence to the Framework.
   *
   * @var Framework $framework
   * @access protected
   */  
  protected $framework;

  /**
   * Web Discover
   *
   * Web Action discover action.
   *
   * @access public
   */  
  public function web_discover() {

    $controllerName = $this->getControllerName();

    if($this->configuration->environment['FRAMEWORK']['development']['mode'] ||
       $this->configuration->application['DISCOVERY']['controller'][strtolower($controllerName)]) {

      $this->addMetaData('title', $controllerName . ' Action Discovery');
      $this->addMetaData('header', $controllerName . ' Actions');
      $this->addMetaData('prefix', $this->prefix);
      $this->addMetaData('webPath', $this->configuration->environment['FRAMEWORK']['web']['path']);
      $this->addMetaData('discovery', true);

      $discoverPlugin = $this->pluginFactory->load('Discover');      
      $this->setData(array(strtolower($controllerName) => $discoverPlugin->discoverActions($this, 'web')));
      $this->render('core/discover/controller');

    } else {
      $this->addMetaData('title','Discovery Error');
      $this->setData(array('error_message' => 'Discovery forbidden on ' . $controllerName . ' controller.',
			   'error'         => 'Forbidden'));
      $this->render('core/discover/error');
    }
  }

  /**
   * Cli Discover
   *
   * Cli Action discover action.
   *
   * @access public
   */  
  public function cli_discover() {
    $discoverPlugin = $this->pluginFactory->load('Discover');      
    $this->setData(array(strtolower($this->getControllerName()) => 
			 $discoverPlugin->discoverActions($this, 'cli')));
    $this->render('core/discover/controller');
  }

  /**
   * Web Unit Test
   *
   * Controller unit test for the web framework.
   *
   * @access public
   * @param PayloadPkg $message GET string optional test message.
   */  
  public function web_unitTest(PayloadPkg $message) {
    $this->addMetaData('controllerName', $this->prefix);
    $this->setData(array('controllerStatus' => 1,
			 'payloadMessage' => $message->getData()));
    $this->render();
  }

  /**
   * CLI Unit Test
   *
   * Controller unit test for the cli framework.
   *
   * @access public
   * @payload ALL string message An optional message to test payload.
   */  
  public function cli_unitTest() {
    $this->addMetaData('controllerName', $this->prefix);
    $this->setData(array('controllerStatus' => 1));
    $this->render();
  }
  
  /**
   * Dispatch
   *
   * Called by Framework
   *
   * @ignore
   * @access public
   * @param Framework $framework a pointer to the framework object.
   * @param string $actionPrefix Prefix for methods to be called as actions (web, cli, etc...)
   * @param string $actionName Name of method to be called as an action
   * @param string $formatName Format used for Parsers and Renderers (aka file extension)
   * @param Payload $payload Server data
   * @param ClientData $clientData Client data
   * @param string $rendererName Name of renderer to create
   */
  public function dispatch(Framework $framework,
			   $actionPrefix,
			   $actionName, 
			   $formatName, 
			   Payload $payload, 
			   ClientData $clientData,
			   $rendererName) {

    $this->framework      = $framework;
    $this->actionPrefix   = $actionPrefix;
    $this->actionName     = $actionName;
    $this->formatName     = $formatName;
    $this->payload        = $payload;
    $this->clientData     = $clientData;
    $this->controllerName = $this->prefix;

    // set a renderer on this controller
    //
    $this->setRenderer($rendererName);

    // call pre action / determin action
    //
    $preActionReturnedMethod = $this->makeArgs($this, "preAction", $payload, $clientData, true);

    $action = $preActionReturnedMethod ? $preActionReturnedMethod : $actionPrefix . '_' . $actionName;

    // make Arguments and invoke the Action
    //
    $this->makeArgs($this, $action, $payload, $clientData);

    // call postAction
    //
    $this->postAction();
  }


  /**
   * Make Args
   *
   * @access private 
   * @param object $o object
   * @param string $m method name
   * @param Payload $payload payload object
   * @param ClientData $clientData clientData object
   * @returns array (method =>, args=> ) an array of objects used as arguments for the class
   */
  private function makeArgs($o, $m, $payload, $clientData, $ret=null) {
    $args = array();

    if($m == 'setBundle') {
      $o->controller = $this;
    }

    // discover action
    //
    $method = new ReflectionMethod($o,$m);
    $methodParams = $method->getParameters();

    
    foreach($methodParams as $methodParam) {

      $bundle = $this->getBundle($methodParam);

      if($bundle) {
	$ma = $this->makeArgs($bundle, 'setBundle', $payload, $clientData);
	array_push($args, $bundle);

      } else {
	$pkgClassVar     = $methodParam->getClass()->getName();
	$pkgClassVars    = get_class_vars($pkgClassVar);
	$pkgFrom         = $pkgClassVars['packageFrom'];
	$pkgIsCollection = $pkgClassVars['isCollection'];

	if($pkgIsCollection) {
	  array_push($args, $$pkgFrom->getCollectionPackage($methodParam->getName()));
	} else {
	  array_push($args, $$pkgFrom->getPackage($methodParam->getName()));
	}
      }

    }

    if($o === $this) {
      if($this->configuration->application['FRAMEWORK']['memcache']['plugin']) {
	if(!$this->cacheControl($m,$method,$args)) {
	  $r = $method->invokeArgs($o,$args);
	}
      } else {
	$r = $method->invokeArgs($o,$args);
      }
    } else {
	$r = $method->invokeArgs($o,$args);
    }

    if($ret) {
      return $r;
    } else {
      return array('method' => $method,
		   'args'   => $args);
    }
  }

  /**
   * Get Bundle
   *
   * If a parameter in the reflected method
   * id type of bundle, create and return it.
   *
   * @see Bundle
   * @access private 
   * @param ReflectionParameter $methodParam
   */
  private function getBundle(ReflectionParameter $methodParam) {
      // Hack alert / Bundle Functionality
      //
      // Unfortunatly we can not use Refection to get the Class name.
      // The script containing the class has not yet been loaded and
      // Refection have the class loaded before it can give us the name.
      //
      // So we end up using the message returned from exception
      try {
	$methodParam->getClass();
      } catch (ReflectionException $e) {
	preg_match("/(Class)\s((.*)(Bundle))\sdoes\snot\sexist/", $e->getMessage(), $m);	

	if($m[1] == "Class" && $m[4] == "Bundle") {

	  // Get a bundleFactory
	  //
	  $bundleFactory = Factory::getFactory('Bundle', array(C_PATH_BUNDLES,C_PATH_CORE_BUNDLES));
	  $bundle = $bundleFactory->load($m[3]);
	  
	  return $bundle;
	} else {
	  return false;
	}
      }
  }

  /**
   * Set Renderer
   *
   * Can be called by and action to set the Renderer
   *
   * @see Renderer
   * @access public 
   * @param string $rendererName
   */
  public function setRenderer($rendererName) {
    $this->rendererName = $rendererName;
    $this->renderer = $this->rendererFactory->load($rendererName);
    
    $this->renderer->setViewSubdirectory(strtolower($this->controllerName));
    $this->renderer->setFramework($this->framework);
  }

  /**
   * Render
   *
   * Calls the methed render on a Renderer
   *
   * @see Renderer
   * @param string $view (optional)
   * @access public 
   */
  public function render($view=null) {
    $this->handledPreRender = true;
    $this->framework->handlePreRender($this);
    
    $contentType = $this->renderer->contentType();
    if($contentType) {
      header($contentType);
    }

    $headers = $this->renderer->headers();
    if(count($headers) > 0) {
      foreach($headers as $header);
      header($header);
    }

    $this->renderer->render($view);
  }

  /**
   * Render Return
   *
   * Calls the methed renderReturn on a Renderer
   *
   * @see Renderer
   * @param string $view (optional)
   * @access public 
   */
  public function renderReturn($view=null) {
    $this->renderer->renderReturn($view);
  }

  /**
   * Render Data
   *
   * used for streaming output
   *
   * @see Renderer
   * @param mixed $data
   * @param string $view (optional)
   * @access public 
   */
  public function renderData($data, $view=null) {
    if(!$this->handledPreRender) {
      $this->framework->handlePreRender($this);
    } else {
      $this->handledPreRender = true;
    }
    
    $this->renderer->setData($data);
    $this->renderer->render($view);
  }

  /**
   * Set Data
   *
   * Calls the setData method on a Renderer
   *
   * @see Renderer
   * @access public 
   * @param mixed $data
   */
  public function setData($data) {
    $this->renderer->setData($data);
  }

  /**
   * Add Meta Data
   *
   * Adds meta data to a renderer
   *
   * @access public
   * @param string $name variable name
   * @param mixed $val variable value
   */
  public function addMetaData($name,$val) {
    $this->renderer->addMetaData($name, $val);
  }

  /**
   * Pre Action
   *
   * preAction is called by the Controller before
   * dispatching to an action.
   *
   * @access public
   */
  public function preAction() {
  }

  /**
   * Post Action
   *
   * postAction is called by the Controller after
   * dispatching to an action
   *
   * @access public
   */
  public function postAction() {
  }

  /**
   * Get Controller Name
   *
   * Get this controllers name.
   *
   * @access public
   * @returns string The controller's name.
   */
  public function getControllerName() {
    return $this->controllerName;
  }

  /**
   * Get Client Data Token
   *
   * Get the client data token.
   *
   * @access public
   * @returns string client data token
   */
  public function getClientDataToken() {
    return $this->clientData->getToken();
  }

  /**
   * Set Client Data
   *
   * Set client data.
   *
   * @access public
   * @param $key string
   * @param $value string|int
   */
  public function setClientData($key,$value) {
    return $this->clientData->setData($key,$value);
  }

  /**
   * Cache Control
   *
   * Caching and flushing actions
   *
   * @access private
   * @param string           $action (ie. web_index)
   * @param ReflectionMethod $method
   * @param array            $args
   */
  private function cacheControl($action,$method,$args) {

    // memcache flush
    //
    $memcacheFlushPackage = $this->configuration->application['MEMCACHE']['flush'][strtolower($this->controllerName)][$action];

    // memcache cache
    //
    $memcacheCachePackage = $this->configuration->application['MEMCACHE']['cache'][strtolower($this->controllerName)][$action];
    $connectStatus        = false;

    if($memcacheCachePackage || $memcacheFlushPackage) {
      $memcachePackage = $memcacheCachePackage ? $memcacheCachePackage : $memcacheFlushPackage;

      if($this->configuration->environment[$memcachePackage]['memcache']['enabled']) {

	$this->memcache = $this->pluginFactory->load('Memcache');
	$this->memcache->init($this->configuration->environment[$memcachePackage]['memcache']['host'],
			      $this->configuration->environment[$memcachePackage]['memcache']['port']);
	
	$connectStatus = $this->memcache->connect();
      }
    }

    // if controler/action is configured for flusing cache
    //
    if($connectStatus && $memcacheFlushPackage) {
      $this->memcache->flush();
      $this->logger->trace('Cache FLUSH... ('. $memcacheFlushPackage .')');
    }

    // if controler/action is configured for caching
    //
    if($connectStatus && $memcacheCachePackage) {      
      $key = hash('md4',serialize(array($_SERVER['REQUEST_METHOD'], $this->configuration->environment['FRAMEWORK']['web']['server'],
      					$this->controllerName, $this->actionName, $this->formatName, $args)));

      $contentHeaders = $this->memcache->get($key . "-headers");

      if(is_array($contentHeaders)) {
	if(is_array($contentHeaders)) {
	  foreach($contentHeaders as $h => $v) {
	    header($h . ': ' . $v);
	  }
	}
      }

      $this->logger->trace("HTTP_IF_NONE_MATCH: ". $_SERVER['HTTP_IF_NONE_MATCH']);
      $this->logger->trace("       CACHED ETAG: ". $contentHeaders['ETag']);

      // check Etag
      //
      if(strlen($_SERVER['HTTP_IF_NONE_MATCH']) > 1 && ($_SERVER['HTTP_IF_NONE_MATCH'] == $contentHeaders['ETag'])) {
	header("HTTP/1.0 304 Not Modified");
	header('Content-Length: 0');
	$this->logger->trace('ETAG MATCH NO RETURN NEEDED ("HTTP/1.0 304 Not Modified")');
	exit;
      }      

      $content = $this->memcache->get($key);
      
      if($content) {
	$this->logger->trace('Cache HIT... ('. $memcacheCachePackage .')');
	echo $content;
      } else {
	$this->logger->trace('Cache MISS... ('. $memcacheCachePackage .')');
	ob_start();
	$method->invokeArgs($this,$args);
	
	$headers = headers_list();
	$cacheHeaders = array();

	foreach($headers as $header) {
	  list($headerKey, $headerValue) = explode(':', $header, 2);
	  if(preg_match("/^content-type/i", $headerKey)) {
	    $cacheHeaders[$headerKey] = $headerValue;
	  }
	}

	$content = ob_get_clean();	  

	$lastModified  = date('r');
	$cacheHeaders['Last-Modified'] = $lastModified;
	$cacheHeaders['ETag']          = '"' . $key . md5($lastModified) . '"';

	foreach($cacheHeaders as $h => $v) {
	  header($h . ': ' . $v);
	}

	echo $content;

	$this->memcache->set($key,$content, MEMCACHE_COMPRESSED, 0);
	$this->memcache->set($key . "-headers",$cacheHeaders, MEMCACHE_COMPRESSED, 0);
      }
      
      return true;
    } else {
      return false;
    }
  }

  /**
   * Call
   *
   * Called automatically when a method does not exist in the controller.
   * Used for "Magic Actions"
   * 
   * @ignore
   * @access public
   * @param string $method
   * @param mixed $args
   */  
  public function __call($method, $args) {

    // if we get here, the action was not implemented - assume a simple render.
    //
    list($prefix, $magicAction) = explode('_', $method, 2);

    // are we trying to call a legit action? if not throw an exception
    //
    if($prefix == $this->actionPrefix) {
      $this->logger->trace("NOTE: METHOD [" . $method . "] WAS NOT FOUND. CALLING RENDER WITH VIEW NAMED [" . $magicAction . "]");
      $this->render($magicAction);
    } else {
      throw new Exception("Trying to call a magic action [" . $method . "] without the correct prefix [" . $this->actionPrefix . "].");
    }
    
    if (method_exists($this, $magicAction) && $this->configuration->environment['FRAMEWORK']['development_mode']) {
      $name = get_class($this);
      $msg  = "Attempt to call " . $name . "->" . "$method when " . $name . "->" . "$smethod exists. \n";
      $msg .= "Rename $name" . '->' . "$smethod or make it callable by prefixing it with 'web_'.  \n";
      $msg .= "This is a development mode only error.";
      
      throw new Exception($msg);
    }
  }
  
}

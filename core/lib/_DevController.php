<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * _DevController.php
 *
 * Contains the abstract {@link _DevController} class.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage controllers
 */

/**
 * The Abstract _DevControiller Class
 *
 * The DevController class (Development Only) is an abstract Controller class. 
 * Controllers that inherit the Development Only controller will only be 
 * executed while esp is in development mode.
 *
 * @abstract
 * @package SP5
 * @subpackage controllers
 */
abstract class _DevController extends Controller {

}
?>

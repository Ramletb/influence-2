<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * Encryptor.php
 * 
 * Contains the {@link Encryptor} class.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage core
 */

/**
 * Require Crypt/Blowfish.php
 * 
 * Requires the Crypt_Blowfish class in Crypt/Blowfish.php
 */
require_once('Crypt/Blowfish.php');

/**
 * The Encryptor Class
 *
 * @package SP5
 * @subpackage core
 */
class Encryptor extends Core {

  /**
   * Instance
   *
   * Instance is used to ensure a singleton
   * ClientData object when getClientData is used.
   *
   * @var ClientData $instance
   * @static
   */
  private static $instance;

  /**
   * Constructor
   * 
   * prevents direct instantiation
   * 
   * @ignore
   * @access protected
   */  
  protected function __construct() {
    parent::__construct();

  }

  /**
   * Get Encryptor
   *
   * Get an Encryptor object
   *
   * @access public
   * @returns Encryptor an {@link Encryptor} object.
   * @static
   */  
  public static function getEncryptor() {

    if (self::$instance == null) {
      self::$instance = new Encryptor();    
    }

    return self::$instance;
  }

  /**
   * Encrypt
   *
   * Encrypt a string
   *
   * @access public
   * @param string $rawString
   * @param string $key
   * @returns string an encrypted string
   * @static
   */  
  public static function encrypt($rawString,$key) {
    $key = substr($key, 0, 56);
    $bf = new Crypt_Blowfish($key);
    if (PEAR::isError($bf)) {
      exit;
    }

    $data = base64_encode($bf->encrypt($rawString)); 

    return $data;
  }

  /**
   * Decrypt
   *
   * Decrypt a string
   *
   * @access public
   * @param string $encryptedString
   * @param string $key
   * @returns string a decrypted string
   * @static
   */  
  public static function decrypt($encryptedString,$key) { 
    $key = substr($key, 0, 56);
    $bf = new Crypt_Blowfish($key);
    if (PEAR::isError($bf)) {
      exit;
    }

    $data = $bf->decrypt(base64_decode($encryptedString));

    return $data;
  }

}

<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * ClientData.php
 *
 * Contains the {@link ClientData} class.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage core
 */

/**
 * Require Data.php
 * 
 * Requires the abstract {@link Data} class
 * contained in {@link Data.php}.
 */
require_once('Data.php');

/**
 * Require DataPackage.php
 * 
 * Requires the {@link ClientDataPkg} class
 * contained in {@link DataPackage.php}.
 */
require_once('DataPackage.php');

/**
 * The ClientData Class
 *
 * ClientData is used to store persistent data client side. 
 * The {@link WebFramework} stores client data in a cookie while 
 * the {@link CliFramework} stores client data in a file. ClientData 
 * is stored encrypted.
 * 
 * See the Quikstart Guide's {@link local:docs/r.php?t=quickstart&s=helloworld.clientdata ClientData Example}.
 * 
 * @package SP5
 * @subpackage core
 */
class ClientData extends Data {

  /**
   * Instance
   *
   * Instance is used to ensure a singleton
   * ClientData object when getClientData is used.
   *
   * @var ClientData $instance
   * @static
   */
  private static $instance;

  /**
   * Data
   *
   * Holds the client data
   *
   * @var Array $data
   * @access protected
   */  
  protected $data = array();

  /**
   * Token
   *
   * Holds a serialized encrypted string
   *
   * @var string $token
   * @access protected
   */  
  protected $token;

  /**
   * Constructor
   * 
   * Prevents direct instantiation
   * 
   * @ignore
   * @access protected
   */  
  protected function __construct() {
    parent::__construct();
  }

  /**
   * Get Client Data
   *
   * Get a ClientData object
   *
   * @access public
   * @param string $token (optional)
   */  
  public static function getClientData($token=null) {
    
    if (self::$instance == null) {
      self::$instance = new ClientData();    
    }

    if($token) {
      self::$instance->data = self::$instance->openToken($token);
    }
    self::$instance->makeToken();

    return self::$instance;
  }

  /**
   * Open Token
   *
   * Opens a token
   *
   * @uses Encryptor::decrypt()
   * @access public
   * @param string $token
   * @returns mixed $value
   */  
  public function openToken($token) {
    $decoded   = base64_decode($token);
    $decrypted = $this->encryptor->decrypt($decoded,$this->configuration->environment['CLIENTDATA']['encryption']['key']);
    $unseralized = unserialize($decrypted);
    return $unseralized;
  }

  /**
   * Set Data
   *
   * Set a Name/Value pair in ClientData
   *
   * @access public
   * @param string $name
   * @param mixed $value
   */  
  public function setData($name,$value) {
    $this->data[$name] = new ClientDataPkg($name,$value);
    $this->makeToken();
  }

  /**
   * Get a data
   *
   * @access public
   * @param string $name (optional to get all)
   * @returns array|string returns string if name is used, otherwise an array
   */  
  public function getData($name=null) {
    if($name != null) {
      $dataPackage = $this->data[$name];
      return $dataPackage ? $dataPackage->getData() : null;
    } else {
      $hash = array();
      foreach($this->data as $k => $dataPackage) {
	$hash[$k] = $dataPackage->getData();
      }
      return $hash;
    }
  }

  /**
   * Get package
   *
   * @access public
   * @param string $name
   * @returns ClientDataPkg
   */  
  public function getPackage($name) {
    return $this->data[$name] ? $this->data[$name] : new ClientDataPkg($name,null);
  }

  /**
   * Make Token
   *
   * Make a Client Data token.
   *
   * @access private
   */
  private function makeToken() {    
    $seralized   = serialize($this->data);
    $encrypted   = $this->encryptor->encrypt($seralized, $this->configuration->environment['CLIENTDATA']['encryption']['key']);
    $b64         = base64_encode($encrypted);
    $this->token = $b64;
  }

  /**
   * Get Token
   *
   * Get a Client Data token.
   *
   * @uses Encryptor::encrypt()
   * @access public
   * @return string $token 
   */
  public function getToken() {
    $this->makeToken();
    return $this->token;
  }

  
}

?>

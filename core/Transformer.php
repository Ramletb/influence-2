<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * Transformer.php
 *
 * Contains the abstract {@link Transformer} class.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage core
 */

/**
 * The Abstract Transformer Class
 *
 * Transformer class objects are used by 
 * TransformerRenderers. Any class descending 
 * {@link TransformerRenderer} will have at least 
 * one associated Transformer class.
 *
 * @package SP5
 * @subpackage core
 */
abstract class Transformer extends Renderer {


}

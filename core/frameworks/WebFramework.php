<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * WebFramework.php
 * 
 * Contains the {@link WebFramework} class.
 *
 * @author Brian Hull <brisn.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage frameworks
 */

/**
 * Require {@link Framework.php}
 *
 * Requires the {@link Framework} class.
 *
 */
require_once('../core/Framework.php');

/**
 * The WebFramework Class
 *
 * See the Quikstart Guide on {@link local:docs/r.php?t=quickstart&s=helloworld.keyconcepts.frameworks Frameworks}
 * for a brief overview.
 *
 * @package SP5
 * @subpackage frameworks
 */
class WebFramework extends Framework {

  /**
   * Get Request
   *
   * Get the request (REQUEST_URI for web)
   *
   * @access public
   * @return string call
   */  
  public function getRequest() {
    return $_SERVER['REQUEST_URI'];
  }

  /**
   * Get Request Method
   *
   * Get the request method (REQUEST_METHOD for web)
   *
   * @access public
   * @return string call
   */  
  public function getRequestMethod() {
    return $_SERVER['REQUEST_METHOD'];
  }

  /**
   * Get Format Name
   *
   * Provides the Framework dispatcher with the
   * name of the format (used for mapping Parsers and Renderers)
   *
   * @access protected
   * @return string Name of format (php, txt, xhtml, xml, json, etc...)
   */
  protected function getFormatName() {
    return strtolower($_REQUEST['format']);
  }

  /**
   * Get Action Name
   *
   * Provides the Framework dispatcher with the
   * name of prefix used for a method to make it an action.
   * An example of prefixed methods would be web_index or
   * cli_dosomething. Prefixed Controller methods are considered
   * actions.
   *
   * @access public
   * @return string Name of prefix (web, cli, etc..)
   */
  public function getActionPrefix() {
    return $this->configuration->application['FRAMEWORK']['type']['web']['prefix'];
  }

  /**
   * Get ClientData
   *
   * Provides the Framework dispatcher with a
   * pointer to the ClientData object
   *
   * @access protected
   * @uses ClientData::getClientData()
   * @param Payload $payload (optional)
   * @return ClientData ClientData refrence
   */
  protected function getClientData($payload=null) {
    $token = $payload->getParam($this->configuration->environment['CLIENTDATA']['web']['cookie']['name'],'COOKIE');
    return ClientData::getClientData($token);
  }

  /**
   * Get Payload
   *
   * Provides the Framework dispatcher with a
   * pointer to the Payload object
   *
   * @access protected
   * @uses Payload::getPayload()
   * @return Payload Payload refrence
   */
  protected function getPayload() {
    if(!isset($this->payload) || !$this->payload) {
      if(get_magic_quotes_gpc()) {
	$stripSlashes = true;
      } else {
	$stripSlashes = false;
      }

      $this->payload = Payload::getPayload();
      $this->payload->setData('GET', $_GET, $stripSlashes);
      $this->payload->setData('POST', $_POST, $stripSlashes);
      $this->payload->setData('REQUEST', $_REQUEST, $stripSlashes);
      $this->payload->setData('COOKIE', $_COOKIE, false);
    }
      
    return $this->payload;
  }

  /**
   * Handle Pre Render
   *
   * Handle pre render callback.
   *
   * @access public
   * @param Controller $controller
   */
  public function handlePreRender($controller) {
    $this->setCookie($controller->getClientDataToken());
  }

  /**
   * Set Cookie
   *
   * Set a cookie.
   *
   * @access private
   * @param string $data
   */
  private function setCookie($data) {
    $expire   = false;
    $path     = '/';
    $domain   = false;
    $secure   = false;
    $httponly = true;

    setcookie($this->configuration->environment['CLIENTDATA']['web']['cookie']['name'], 
	      $data, 
	      $expire, 
	      $path, 
	      $domain,
	      $secure,
	      $httponly);
  }
  
  /**
   * Get Controller Name
   *
   * Provides the Framework dispatcher with the
   * name of the Controller to dispatch and action request to.
   *
   * @access public
   * @return string Name of controller
   */  
  public function getControllerName() {
    $this->controllerName = $this->controllerName ? $this->controllerName : $_REQUEST['controller'];

    return $this->controllerName ? ucfirst($this->controllerName) : 
      ucfirst($this->configuration->application['FRAMEWORK']['default']['controller']['web']);
  }

  /**
   * Get Action Name
   *
   * Provides the Framework dispatcher with the
   * name of the action (prefixed method within a controller) to 
   * call.
   *
   * @access public
   * @return string Name of action (prefixed method with controller)
   */
  public function getActionName() {
    if(!isset($this->actionName) || !$this->actionName) {
      list($actionName) = explode(".", $_REQUEST['action'], 1);
      $this->actionName = $actionName;
    }

    // check action for slashes
    //
    $actionSlashes    = explode('/', $this->actionName);
    if(count($actionSlashes) > 1) {
      $this->actionName = array_shift($actionSlashes);

      $payload = $this->getPayload();

      $actionSlashesPairs = array_chunk($actionSlashes, 2);
      $actionSlashesHash  = array();
      foreach($actionSlashesPairs as $k => $v) {
	  $actionSlashesHash[$v[0]] = $v[1];
	
      }

      $this->payload->setData('GET', $actionSlashesHash, true);

    }

    $this->actionName = $this->actionName ? $this->actionName : $this->configuration->application['FRAMEWORK']['default']['action']['web'];
   
    return $this->actionName;
  }

  /**
   * Get Default Renderer Name
   *
   * Provides the Framework dispatcher with the
   * name of the default renderer to use for views.
   *
   * @see Renderer
   * @access protected
   * @return string Name of Default Renderer
   */
  protected function getDefaultRendererName() {
    return $this->configuration->application['FRAMEWORK']['default']['renderer']['web'];
  }


  /**
   * Pre Dispatch
   *
   * called pre dispatch
   *
   * @access protected
   */
  protected function preDispatch() {

    // is compression on
    //
    if($this->configuration->application['FRAMEWORK']['default']['compression']) {
      $this->logger->trace('Output Compression is ON.');
      ob_start('ob_gzhandler');
    }

    if(!$this->configuration->application['FRAMEWORK']['payload']['preserve']['get']) {
      unset($_GET);
    }
    if(!$this->configuration->application['FRAMEWORK']['payload']['preserve']['post']) {
      unset($_POST);
    }
    if(!$this->configuration->application['FRAMEWORK']['payload']['preserve']['request']) {
      unset($_REQUEST);
    }
    if(!$this->configuration->application['FRAMEWORK']['payload']['preserve']['cookie']) {
      unset($_COOKIE);
    }
  }


}

?>

<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * CliFramework.php 
 *
 * Contains the {@link CliFramework} class.
 *
 * @author Brian Hull <brisn.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage frameworks
 */

/**
 * Require Framework.php
 *
 * Requires the abstract {@link Framework} class
 * contained in {@link Framework.php}.
 */
require_once('../core/Framework.php');

/**
 * The CliFramework Class
 *
 * See the Quikstart Guide on {@link local:docs/r.php?t=quickstart&s=helloworld.keyconcepts.frameworks Frameworks}
 * for a brief overview.
 *
 * @package SP5
 * @subpackage frameworks
 */
class CliFramework extends Framework {

  /**
   * Options
   *
   * Holds a private collection of command line
   * options.
   *
   * @var Array $options
   * @access private
   */  
  private $options = array();

  /** 
   * Constructor
   *
   * The Cli Framework constructor
   *
   * @access public
   * @ignore
   */  
  public function __construct() {
    parent::__construct();
    $this->getArgs();
  }

  /**
   * Get Args
   *
   * Get all "--" command line arguments and pass to getopt
   * then sets the private attribute $options.
   *
   * @access private
   */
  private function getArgs() {

    $commandLineArray = explode("--", implode(' ', $_SERVER['argv']));
    $scriptName = array_shift($commandLineArray);

    foreach($commandLineArray as $optionPair) {
      list($option, $value) = explode(" ", trim($optionPair), 2);
      $this->options[$option] = $value;
    }

  }

  /**
   * Get Request
   *
   * Get the request. Full command line statement.
   *
   * @access public
   * @return string call
   */  
  public function getRequest() {
    return implode(" ", $_SERVER['argv']);
  }

  /**
   * Get Request Method.
   *
   * Get the request method
   *
   * @access public
   * @return string call
   */  
  public function getRequestMethod() {
    return 'CLI';
  }

  /**
   * Get ClientData
   *
   * Provides the Framework dispatcher with a
   * pointer to the ClientData object.
   *
   * @access protected
   * @uses ClientData::getClientData()
   * @param Payload $payload (optional)
   * @return ClientData ClientData refrence
   */
  protected function getClientData($payload=null) {
    $file = $_ENV['HOME'] . '/' . $this->configuration->environment['CLIENTDATA']['cli']['file']['name'];
    if(is_file($file)) {
      $token = file_get_contents($file);
    } else {
      $token = null;
    }
    return ClientData::getClientData($token);
  }

  /**
   * Handle Pre Render
   *
   * Handle pre render callback.
   *
   * @access public
   * @param Controller $controller
   */
  public function handlePreRender($controller) {
    $this->writeClientData($controller->getClientDataToken());
  }

  /**
   * Write ClientData
   *
   * Write Client Data to file
   *
   * @access public
   * @param string $token
   */
  private function writeClientData($token) {
    file_put_contents($_ENV['HOME'] . '/' . $this->configuration->environment['CLIENTDATA']['cli']['file']['name'], $token);
  }

  /**
   * Get Format Name
   *
   * Provides the Framework dispatcher with the
   * name of the format (used for mapping Parsers and Renderers)
   *
   * @access protected
   * @return string Name of format (php, txt, xhtml, xml, json, etc...)
   */
  protected function getFormatName() {
    return strtolower($this->options['format']);
  }

  /**
   * Get Action Prefix
   *
   * Provides the Framework dispatcher with the
   * name of prefix used for a method to make it an action.
   * An example of prefixed methods would be web_index or
   * cli_dosomething. Prefixed Controller methods are considered
   * actions.
   *
   * @access public
   * @return string Name of prefix (web, cli, etc..)
   */
  public function getActionPrefix() {
    return $this->configuration->application['FRAMEWORK']['type']['cli']['prefix'];
  }

  /**
   * Get Payload
   *
   * Provides the Framework dispatcher with a
   * pointer to the Payload object
   *
   * @access protected
   * @uses Payload::getPayload()
   * @return Payload Payload refrence
   */
  protected function getPayload() {
    $payload = Payload::getPayload();
    $payload->setData('CLI', $this->options);

    return $payload;
  }

  /**
   * Get Controller Name
   *  
   * Provides the Framework dispatcher with the
   * name of the Controller to dispatch and action request to.
   *
   * @access public
   * @return string Name of controller
   */  
  public function getControllerName() {
    $this->controllerName = $this->controllerName ? $this->controllerName : $this->options['controller'];

    return $this->controllerName ? ucfirst($this->controllerName) : 
      ucfirst($this->configuration->application['FRAMEWORK']['default']['controller']['cli']);
  }

  /**
   * Get Action Name
   *
   * Provides the Framework dispatcher with the
   * name of the action (prefixed method within a controller) to 
   * call.
   *
   * @access public
   * @return string Name of action (prefixed method with controller)
   */
  public function getActionName() {   
    list($actionName, $junk) = explode(".", $this->options['action']);
    $actionName = $actionName ? $actionName : $this->configuration->application['FRAMEWORK']['default']['action']['cli'];
   
    return $actionName;
  }

  /**
   * Get Default Renderer Name
   *
   * Provides the Framework dispatcher with the
   * name of the default renderer to use for views.
   *
   * @see Renderer
   * @access protected
   * @return string Name of Default Renderer
   */
  protected function getDefaultRendererName() {
    return $this->configuration->application['FRAMEWORK']['default']['renderer']['cli'];
  }

  /**
   * Pre Dispatch
   *
   * Called pre dispatch.
   *
   * @access protected
   */
  protected function preDispatch() {
    unset($argv);
  }


}

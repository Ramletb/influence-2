<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * App.php 
 *
 * Contains the abstract {@link App} class.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage core
 */

/**
 * Require Factory.php
 *
 * Requires the {@link Factory} class
 * contained in {@link Factory.php}.
 */
require_once('Factory.php');

/**
 * Require DatabaseFactory.php
 *
 * Requires the {@link DatabaseFactory} class
 * contained in {@link DatabaseFactory.php}.
 */
require_once('DatabaseFactory.php');

/**
 * Require ServiceFactory.php
 *
 * Requires the {@link ServiceFactory} class
 * contained in {@link ServiceFactory.php}.
 */
require_once('ServiceFactory.php');

/**
 * The Abstract App Class
 *
 * @package SP5
 * @subpackage core
 */
abstract class App extends Core {

  /**
   * Plugin Factory
   * 
   * Holds a pluginFactory object.
   *
   * @var Factory $pluginFactory
   * @uses Factory::getFactory()
   * @access protected
   */  
  protected $pluginFactory;

  /**
   * Parser Factory
   *
   * Holds a parserFactory object.
   *
   * @var Factory $parserFactory
   * @uses Factory::getFactory()
   * @access protected
   */  
  protected $parserFactory;

  /**
   * Renderer Factory
   *
   * Holds a rendererFactory object.
   *
   * @var Factory $rendererFactory
   * @uses Factory::getFactory()
   * @access protected
   */  
  protected $rendererFactory;

  /**
   * Model Factory
   *
   * Holds a modelFactory object.
   *
   * @var Factory $modelFactory
   * @uses Factory::getFactory()
   * @access protected
   */  
  protected $modelFactory;

  /**
   * Database Factory
   *
   * Holds a databaseFactory object.
   *
   * @var DatabaseFactory $databaseFactory
   * @uses DatabaseFactory::getDatabaseFactory()
   * @access protected
   */  
  protected $databaseFactory;

  /**
   * Service Factory
   *
   * Holds a serviceFactory object.
   *
   * @var ServiceFactory $serviceFactory
   * @uses ServiceFactory::getServiceFactory()
   * @access protected
   */
  protected $serviceFactory;

  /**
   * Prefix
   *
   * Holds the application componenet types prefix
   *
   * @var string $prefix
   * @access protected
   */  
  protected $prefix;

  /**
   * File Path
   *
   * Holds a full path to the file
   *
   * @var string $filePath
   * @access protected
   */  
  protected $filePath;

  /**
   * Constructor
   * 
   * Constructs application components
   * 
   * @ignore
   * @see Controller
   * @see Factory
   * @access public
   */  
  public function __construct() {
    parent::__construct();
    $this->pluginFactory      = & Factory::getFactory('Plugin',      array(C_PATH_PLUGINS,      C_PATH_CORE_PLUGINS));
    $this->parserFactory      = & Factory::getFactory('Parser',      array(C_PATH_PARSERS,      C_PATH_CORE_PARSERS));
    $this->modelFactory       = & Factory::getFactory('Model',       array(C_PATH_MODELS,       C_PATH_CORE_MODELS));
    $this->rendererFactory    = & Factory::getFactory('Renderer',    array(C_PATH_RENDERERS,    C_PATH_CORE_RENDERERS));
    $this->databaseFactory    = & DatabaseFactory::getDatabaseFactory();
    $this->serviceFactory     = & ServiceFactory::getServiceFactory();
  }

  /**
   * Set Prefix
   *
   * Allows the Factory to set a prefix on
   * Factory built objects.
   * 
   * @see Configuration
   * @see Factory
   * @param string $prefix
   * @access public
   */  
  public function setPrefix($prefix) {
    $this->prefix = $prefix;
  }
	
  /**
   * Set File Path
   *
   * Stores the full path to the file.
   * 
   * @param string $filePath
   * @access public
   */  
  public function setFilePath($filePath) {
    $this->filePath = $filePath;
  }

  /**
   * Get File Path
   *
   * Gets the path to file.
   * 
   * @returns string The file path.
   * @access public
   */  
  public function getFilePath() {
    return $this->filePath;
  }
	
}

?>

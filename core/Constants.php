<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * Constants.php
 *
 * Contains a list of constants used internally by the framework.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage core
 */

/**
 * Relative Path
 *
 * C_PATH_REL defines the path relative to core
 */
define('C_PATH_REL', "../");

/**
 * Configuration Directory
 *
 * C_PATH_CONFIG_DIR defines configuration directory
 */
define('C_PATH_CONFIG_DIR', "config/");

/**
 * Configuration Path
 *
 * C_PATH_CONFIG defines the path to configuration directory
 */
define('C_PATH_CONFIG', C_PATH_REL . C_PATH_CONFIG_DIR);

/**
 * Framework Configuration
 *
 * C_FILE_CONFIG defines framework specific variables
 */
define('C_FILE_CONFIG', C_PATH_CONFIG . "core_framework.ini");

/**
 * Custom Environment Configuration
 *
 * C_FILE_CONFIG_ENV_CUSTOM defines the path to the environment 
 * specific configuration. This is the custom configuration for
 * an application. Overrides C_FILE_CONFIG_ENV_DEFAULT
 */
define('C_FILE_CONFIG_ENV_CUSTOM', C_PATH_CONFIG . "custom_environment.ini");

/**
 * Default Environment Configuration
 *
 * C_FILE_CONFIG_ENV_DEFAULT defines the path to the environment 
 * specific configuration. This is the default configuration for
 * an application. Overridden by C_FILE_CONFIG_ENV_CUSTOM
 */
define('C_FILE_CONFIG_ENV_DEFAULT', C_PATH_CONFIG . "default_environment.ini");

/**
 * Custom Application Configuration
 *
 * C_FILE_CONFIG_APP_CUSTOM defines the path to the application specific configuration.
 * This file overrides C_FILE_CONFIG_APP_DEFAULT.
 */
define('C_FILE_CONFIG_APP_CUSTOM', C_PATH_CONFIG . "custom_application.ini");

/**
 * Default Application Configuration
 *
 * C_FILE_CONFIG_APP_DEFAULT defines the path to the application specific configuration.
 * This file should be overriden by C_FILE_CONFIG_APP_CUSTOM.
 */
define('C_FILE_CONFIG_APP_DEFAULT', C_PATH_CONFIG . "default_application.ini");

/**
 * Documentation Directory
 *
 * C_PATH_DOCS_DIR defines the documentation directory
 */
define('C_PATH_DOCS_DIR', "www/docs/");

/**
 * Documentation Path
 *
 * C_PATH_DOCS defines the path to documentation directory
 */
define('C_PATH_DOCS',  C_PATH_REL . C_PATH_DOCS_DIR);

/**
 * Views Path
 *
 * C_PATH_VIEW defines the path to views
 */
define('C_PATH_VIEWS',       "../app/views/");

/**
 * Core Views Path
 *
 * C_PATH_CORE_VIEWS defines the path to views
 */
define('C_PATH_CORE_VIEWS',       "../core/app/views/");

/**
 * Bundles Path
 *
 * C_PATH_BUNDLE defines the path to bundles
 */
define('C_PATH_BUNDLES',       "../app/bundles/");

/**
 * Core Bundles Path
 *
 * C_PATH_CORE_BUNDLES defines the path to bundles
 */
define('C_PATH_CORE_BUNDLES',       "../core/app/bundles/");

/**
 * Parsers Path
 *
 * C_PATH_PARSERS defines the path to {@link Parser} subclasses.
 */
define('C_PATH_PARSERS',     "../app/parsers/");

/**
 * Core Parsers Path
 *
 * C_PATH_CORE_PARSERS defines the path to {@link Parser} subclasses.
 */
define('C_PATH_CORE_PARSERS',     "../core/app/parsers/");

/**
 * Renderers Path
 *
 * C_PATH_RENDERERS defines the path to {@link Renderer} subclasses.
 */
define('C_PATH_RENDERERS',   "../app/renderers/");

/**
 * Core Renderers Path
 *
 * C_PATH_CORE_RENDERERS defines the path to {@link Renderer} core subclasses.
 */
define('C_PATH_CORE_RENDERERS',   "../core/app/renderers/");

/**
 * Models Path
 *
 * C_PATH_MODELS defines the path to {@link Model} subclasses.
 */
define('C_PATH_MODELS',      "../app/models/");

/**
 * Core Models Path
 *
 * C_PATH_CORE_MODELS defines the path to {@link Model} core subclasses.
 */
define('C_PATH_CORE_MODELS',      "../core/app/models/");

/**
 * Plugins Path
 *
 * C_PATH_PLUGINS defines the path to {@link Plugin} subclasses.
 */
define('C_PATH_PLUGINS',     "../app/plugins/");

/**
 * Core Plugins Path
 *
 * C_PATH_CORE_PLUGINS defines the path to {@link Plugin} core subclasses.
 */
define('C_PATH_CORE_PLUGINS',     "../core/app/plugins/");

/**
 * Controllers Path
 *
 * C_PATH_CONTROLLERS defines the path to {@link Controller} subclasses.
 */
define('C_PATH_CONTROLLERS', "../app/controllers/");

/**
 * Core Controllers Path
 *
 * C_PATH_CORE_CONTROLLERS defines the path to {@link Controller} core subclasses.
 */
define('C_PATH_CORE_CONTROLLERS', "../core/app/controllers/");

/**
 * Core Frameworks Path
 *
 * C_PATH_FRAMEWORKS defines the path to {@link Framework} subclasses.
 */
define('C_PATH_FRAMEWORKS', "../core/frameworks/");

/**
 * Model Subclasses Path
 *
 * C_PATH_MODEL_SUBCLASSES defines the path to the {@link Model} subclasses
 */
define('C_PATH_MODEL_SUBCLASSES', "../core/models/");

/**
 * Service Subclasses Path
 *
 * C_PATH_SERVICE_SUBCLASSES defines the path to the {@link Service} subclasses
 */
define('C_SERVICE_SUBCLASSES', "../core/services/");


<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * TestPlugin.php 
 *
 * Contains the {@link TestPlugin} class.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage plugins
 */

/**
 * The TestPlugin Class
 *
 * See the Quikstart Guide's {@link local:docs/r.php?t=quickstart&s=helloworld.plugin TestPlugin Example}.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage plugins
 */
class TestPlugin extends Plugin {

  /**
   * @access private
   * @var object $outputCallbackObj;
   */
  private $outputCallbackObj;

  /**
   * @access private
   * @var object $outputCallbackMethod;
   */
  private $outputCallbackMethod;

  /**
   * Status method returns the string "Test Plugin Loaded..."
   * This method is used to test the proper functioning of the
   * framework.
   *
   * @return string
   */
  public function status() {
    return "Test Plugin Message...";
  }

  /**
   * Set Output Callback
   *
   * @access public
   * @param object $obj
   * @param string $method
   */
  public function setOutputCallback($obj,$method) {
    $this->outputCallbackObj = $obj;
    $this->outputCallbackMethod = $method;
  }

  /**
   * Output a message
   *
   * @access private
   * @param string $msg
   */
  private function output($msg) {
    $obj = $this->outputCallbackObj;
    $method = $this->outputCallbackMethod;

    $obj->$method($msg);
  }

  /**
   * Incremental output of data.
   *
   */
  public function outputDataTest() {

    for($i=0; $i <= 10; $i++) {
      $this->output(" .. This is a test [" . $i . "]");
      sleep(1);
    }

  }

}

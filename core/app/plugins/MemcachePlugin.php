<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * MemcachedPlugin.php 
 *
 * Contains the {@link MemcachedPlugin} class.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage plugins
 */

/**
 * The MemcachedPlugin Class
 *
 * Uses the {Memcache http://www.php.net/manual/en/book.memcache.php}
 * php class.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage plugins
 */
class MemcachePlugin extends Plugin {

  /**
   * @access private
   * @var string $host
   */
  private $host;

  /**
   * @access private
   * @var string $port
   */
  private $port;

  /**
   * @access private
   * @var Memcache $memcache
   */
  private $memcache = false;

  /**
   * Init
   *
   * @access public
   * @param string $host
   * @param int    $port
   */
  public function init($host,$port=11211) {
    $this->host = $host;
    $this->port = $port;
    $this->memcache = new Memcache;
    $this->logger->trace("Initialized Memcache for " . $this->host . ":" . $this->port);
  }

  /**
   * Connect
   *
   * @access public
   * @param string $host
   * @param int    $port
   * @returns bool connect status
   */
  public function connect() {
    $this->logger->trace("Connecting to: " . $this->host . ":" . $this->port);
    $status = $this->memcache->connect($this->host, $this->port) or $this->logger->fatal("COULD NOT CONNECT TO MEMCACHE");
    //$this->logger->trace("Memcache version: " . $this->memcache->getVersion());
    return $status;
  }

  /**
   * Get
   *
   * @access public
   * @param string $key
   * @returns string
   */
  public function get($key) {
    return $this->memcache->get($key);
  }

  /**
   * Set
   *
   * @access public
   * @param string $key
   * @param mixed $value
   */
  public function set($key,$value) {
    return $this->memcache->set($key,$value);
  }

  /**
   * Flush
   *
   * @access public
   */
  public function flush() {
    return $this->memcache->flush();
  }

  /**
   * Call
   *
   * Called automatically when a method does not exist in the pulgin.
   * Send call to $this->memcache;
   * 
   * @ignore
   * @access public
   * @param string $method
   * @param mixed $args
   */  
  public function __call($method, $args) {
    return $this->memcache->$method($args);
  }



}

<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * ApiPlugin.php
 *
 * Contains the {@link ApiPlugin} class.
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage plugins
 */

/**
 * The ApiPlugin Class
 *
 * For use with database models.
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage plugins
 */
class ApiPlugin extends Plugin {

  /**
   * Page
   *
   * @var int $page
   * @access public
   */
  public $page;

  /**
   * Page Number
   *
   * @var int $pageNumber
   * @access public
   */
  public $pageNumber;

  /**
   * Per Page
   *
   * @var int $perPage
   * @access public
   */
  public $perPage;

  /**
   * Select Statement
   *
   * @var string $selectS
   * @access protected
   */
  protected $selectS = 'SELECT * ';

  /**
   * From Statement
   *
   * @var string $fromS
   * @access protected
   */
  protected $fromS;

  /**
   * Where Statement
   *
   * @var string $whereS
   * @access protected
   */
  protected $whereS;

  /**
   * Or Statement
   *
   * @var string $orS
   * @access protected
   */
  protected $orS;

  /**
   * Having Statement
   *
   * @var string $havingS
   * @access protected
   */
  protected $havingS;

  /**
   * Order Statement
   *
   * @var string $orderS
   * @access protected
   */
  protected $orderS;

  /**
   * Group Statement
   *
   * @var string $groupS
   * @access protected
   */
  protected $groupS;

  /**
   * Limit Statement
   *
   * @var string $limitS
   * @access protected
   */
  protected $limitS;

  /**
   * Model
   *
   * @var Model $model
   * @access protected
   */
  protected $model;

  /**
   * Aggregate
   *
   * @var Boolean $aggregate
   * @access private
   */
  protected $aggregate = false;

  /**
   * Aggregate Aliases
   *
   * @var Boolean $aggregate
   * @access private
   */
  protected $aggregateAliases = array();

  /**
   * Load
   *
   * @access public
   * @param string $modelName
   * @param string $packageName
   */
  public function load($modelName, $packageName) {
    $this->model   = $this->modelFactory->load($modelName, $packageName);
    $this->selectS = $this->getSelectType() . " * ";
    $this->fromS   = 'FROM `' . $this->model->getTableName() . '` ';
  }

  /**
   * Reset Conditions
   *
   * @access public
   */
  public function resetConditions() {
    $this->page       = null;
    $this->pageNumber = null;
    $this->perPage    = null;
    $this->aggregate  = false;
    $this->aggregateAliases = false;
    $this->groupS  = null;
    $this->whereS  = null;
    $this->havingS = null;
    $this->limitS  = null;
    $this->orderS  = null;
    $this->orS     = null;

    $this->select  = $this->getSelectType() . ' * ';
  }

  /**
   * Select Builder
   *
   * @access private
   * @param array $selectHash array of columns to select from
   */
  private function selectBuilder($selectHash) {
    if($this->model) {
      //
      // distinct needed for count, max, min, sum
      //
      $acceptableValues = array('GROUP_CONCAT' => 'GROUP_CONCAT(',
				'AVG'          => 'AVG(',
				'BIT_AND'      => 'BIT_AND(',
				'BIT_OR'       => 'BIT_OR(',
				'BIT_XOR'      => 'BIT_XOR(',
				'COUNT'        => 'COUNT(',
				'MAX'          => 'MAX(',
				'MIN'          => 'MIN(',
				'STD'          => 'STD(',
				'STDDEV'       => 'STDDEV(',
				'STDDEV_POP'   => 'STDDEV_POP(',
				'STDDEV_SAMP'  => 'STDDEV_SAMP(',
				'SUM'          => 'SUM(',
				'VAR_POP'      => 'VAR_POP(',
				'VAR_SAMP'     => 'VAR_SAMP(',
				'VARIANCE'     => 'VARIANCE(');
      

      if(key($selectHash)) {
	$this->selectS = null;
	foreach($selectHash as $col => $v) {
	  $kv = $this->model->prepare(array($col => null));
	  if(key($kv)) {
	    foreach($v as $f) {
	      $this->selectS .= $this->selectS ? ',' : $this->getSelectType();
	      $funcKey = strtoupper($f);
	      $func = $acceptableValues[$funcKey];
	      if(array_key_exists($funcKey, $acceptableValues)) {
		$this->aggregate = true;
		$aggregateAlias = strtolower($funcKey) . '_' . key($kv);
		$this->aggregateAliases[$aggregateAlias] = 1;
		$this->selectS .= ' ' . $func . '`'. key($kv) . '`) as `' . $aggregateAlias . '`';
	      } else {
		$this->selectS .= ' `'. key($kv) . '`';
	      }
	    }
	  }
	}
	$this->selectS .= ' ';
      }
    }
  }

  /**
   * Select
   *
   * @access public
   * @param array $selectHash array of columns to select from
   */
  public function select($selectHash) {
    $this->selectBuilder($selectHash);
  }

  /**
   * Where Builder
   *
   * @access public
   * @param array $whereHash
   * @param string $operator [=] (Equality Operator)
   * @param string $conjunction [AND] (Logical Operator)
   */
  private function whereBuilder($whereHash,$operator='=',$conjunction='AND',$dv=null) {
    if($this->model) {
      foreach($whereHash as $col => $valArray) {
	foreach($valArray as $val) {
	  $kv = $this->model->prepare(array($col => $val));
	  if(key($kv)) {
	    $this->whereS .= $this->whereS ? ' ' . $conjunction : '';
	    $this->whereS .= ' `'. key($kv) . '` ' . $operator . ' ' . ($dv ? $dv : $kv[key($kv)]);
	  }
	}
      }
    }
  }

  /**
   * Or Builder
   *
   * @access public
   * @param array $orHash
   * @param string $operator [=] (Equality Operator)
   * @param string $conjunction [OR] (Logical Operator)
   */
  private function orBuilder($orHash,$operator='=',$conjunction='OR',$dv=null) {
    if($this->model) {
      foreach($orHash as $col => $valArray) {
	foreach($valArray as $val) {
	  $kv = $this->model->prepare(array($col => $val));
	  if(key($kv)) {
	    $this->orS .= $this->orS ? ' ' . $conjunction : '';
	    $this->orS .= ' `'. key($kv) . '` ' . $operator . ' ' . ($dv ? $dv : $kv[key($kv)]);
	  }
	}
      }
    }
  }

  /**
   * Having Builder
   *
   * @access public
   * @param array $havingHash
   * @param string $operator (Equality Operator)
   * @param string $conjunction (Logical Operator)
   */
  private function havingBuilder($havingHash,$operator='=',$conjunction='AND',$dv=null) {
    if($this->model) {
      foreach($havingHash as $col => $valArray) {
	foreach($valArray as $val) {
	  $kv = $this->model->prepare(array($col => $val));
	  if(key($kv)) {
	    $this->havingS .= $this->havingS ? ' ' . $conjunction : 'HAVING';
	    $this->havingS .= ' `'. key($kv) . '` ' . $operator . ' ' . ($dv ? $dv : $kv[key($kv)]);
	  }
	  if(is_array($this->aggregateAliases) && array_key_exists($col, $this->aggregateAliases)) {
	    $this->havingS .= $this->havingS ? ' ' . $conjunction : 'HAVING';
	    $this->havingS .= ' `'. $col . '` ' . $operator . ' ' . ($val + 0);
	  }
	}
      }
    }
  }

  /**
   * Where
   *
   * @access public
   * @param array $whereHash
   */
  public function where($whereHash) {
    $this->whereBuilder($whereHash);
  }

  /**
   * Where Or
   *
   * @access public
   * @param array $orHash
   */
  public function whereOr($orHash) {
    $this->orBuilder($orHash);
  }

  /**
   * Where Is Not Null
   *
   * @access public
   * @param array $isNotNullHash
   */
  public function isNotNull($isNotNullHash) {
    $this->whereBuilder($isNotNullHash,'IS NOT','AND','NULL');
  }

  /**
   * Where Is Null
   *
   * @access public
   * @param array $isNullHash
   */
  public function isNull($isNullHash) {
    $this->whereBuilder($isNullHash,'IS','AND','NULL');
  }

  /**
   * Where Not
   *
   * @access public
   * @param array $notHash
   */
  public function not($notHash) {
    $this->whereBuilder($notHash,'<>');
  }

  /**
   * Where Like
   *
   * @access public
   * @param array $likeHash
   */
  public function like($likeHash) {
    $this->whereBuilder($likeHash,'LIKE');
  }

  /**
   * Where Greater Than
   *
   * @access public
   * @param array $gtHash
   */
  public function gt($gtHash) {
    $this->whereBuilder($gtHash,'>');
  }

  /**
   * Having Greater Than
   *
   * @access public
   * @param array $gtHash
   */
  public function havingGt($gtHash) {
    $this->havingBuilder($gtHash,'>');
  }

  /**
   * Where Less Than
   *
   * @access public
   * @param array $ltHash
   */
  public function lt($ltHash) {
    $this->whereBuilder($ltHash,'<');
  }

  /**
   * Where In
   *
   * @access public
   * @param array $inHash
   */
  public function in($inHash) {
    $this->inBuilder($inHash);
  }

  /**
   * Where Not In
   *
   * @access public
   * @param array $inHash
   */
  public function notIn($inHash) {
    $this->inBuilder($inHash,' NOT');
  }

  /**
   * In Builder
   *
   * @access private
   * @param array $inHash
   */
  private function inBuilder($inHash,$not='') {
    if($this->model) {
      foreach($inHash as $col => $valArray) {
    	$kv = $this->model->prepare(array($col => null));
    	if(key($kv)) {
    	  $this->whereS .= $this->whereS ? ' AND' : '';
    	  $this->whereS .= ' `'. key($kv) . '`' . $not . ' IN (';
	    }

    	$inV = false;
	    foreach($valArray as $val) {
    	  $kv = $this->model->prepare(array($col => $val));
	      $this->whereS .= $inV ? ',' : '';
    	  $this->whereS .= $kv[key($kv)];
	      $inV = true;
    	}

    	if(key($kv)) {
	      $this->whereS .= ')';
	    }

      }
    }
  }

  /**
   * Where Between
   *
   * @access public
   * @param array $betweenHash
   */
  public function between($betweenHash) {
    if($this->model) {
      foreach($betweenHash as $col => $valArray) {
	$kv = $this->model->prepare(array($col => null));
	if(key($kv)) {
	  $this->whereS .= $this->whereS ? ' AND' : '';
	  $this->whereS .= ' `'. key($kv) . '` BETWEEN ';
	}

	$inV = false;
	foreach($valArray as $val) {
	  $kv = $this->model->prepare(array($col => $val));
	  $this->whereS .= $inV ? ' AND ' : '';
	  $this->whereS .= $kv[key($kv)];
	  $inV = true;
	}

      }
    }
  }

  /**
   * Order
   *
   * @access public
   * @param array $orderHash
   */
  public function order($orderHash) {
    if($this->model) {
      $acceptableValues = array('ASC' => 1, 'DESC' => 1);
      foreach($orderHash as $col => $order) {
	$kv = $this->model->prepare(array($col => null));
	if(key($kv)) {
	  $order = array_pop($order);
	  if(array_key_exists(strtoupper($order), $acceptableValues)) {
	    $this->orderS .= $this->orderS ? ',' : ' ORDER BY';
	    $this->orderS .= ' `'. key($kv) . '` ' . $order;
	  }
	}
      }
      $this->orderS .= ' ';
    }
  }

  /**
   * Group
   *
   * @access public
   * @param array $groupHash
   */
  public function group($groupHash) {
    if($this->model) {

      foreach($groupHash as $col => $v) {
	$kv = $this->model->prepare(array($col => null));
	if(key($kv)) {
	  $this->aggregate = true;
	  $this->groupS .= $this->orderS ? ',' : ' GROUP BY';
	  $this->groupS .= ' `'. key($kv) . '` ';
	}
      }
      $this->groupS .= ' ';
    }
  }

  /**
   * Page
   *
   * @access public
   * @param int $perPage
   * @param int $page
   */
  public function page($perPage, $page) {
    $this->page    = $page;
    $this->perPage = $perPage;

    $this->pageNumber  = ($this->page - 1);
    $recordsIn   = ($this->perPage * $this->pageNumber);

    $this->limitS = " LIMIT " . $recordsIn . ',' . $perPage;
  }

  /**
   * Raw Statement
   *
   * @access private
   * @returns string sql statement
   */
  private function rawStatement() {

    $orS    = '';
    $whereS = '';

    if($this->orS) {
      $orS = '(' . $this->orS . ') ';

      if($this->whereS) {
	$orS = $orS . 'AND';
      }
    }

    if($this->whereS || $this->orS) {
      $whereS = 'WHERE ' . $orS . $this->whereS;
    }

    return $this->fromS . $whereS . $this->groupS . $this->havingS . $this->orderS;
  }

  /**
   * Get Statement
   *
   * @access public
   * @returns string sql statement
   */
  public function getStatement() {
    return $this->selectS . $this->rawStatement() . $this->limitS;
  }

  /**
   * Get Count Statement
   *
   * @access public
   * @returns string sql statement
   */
  public function getCountStatement() {

    if($this->aggregate) {
      $countSql = $this->getSelectType() . ' COUNT(1) as total FROM (' . $this->selectS . $this->rawStatement() . ') AS g';
    } else {
      $countSql = $this->getSelectType() . ' COUNT(1) as total ' . $this->rawStatement();
    }

    return $countSql;
  }

  /**
   * Get Data
   *
   * @access public
   * @returns array results
   */
  public function getData() {
    return $this->model->apiSelect($this);
  }

  /**
   * Get Select Type
   *
   * @access private
   * @returns string type
   */
  private function getSelectType() {
    $selectType = "SELECT";

    if($this->model) {
      if($this->model->getTableType() == "VIEW") {
	$selectType = "SELECT STRAIGHT_JOIN";
      }
    }

    return $selectType;
  }


}


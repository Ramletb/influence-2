<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * ModelFinderPlugin.php 
 *
 * Contains the {@link ModelFinderPlugin} class.
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @package SP5
 * @subpackage plugins
 */

/**
 * The ModelFinderPlugin Class
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @package SP5
 * @subpackage plugins
 */
class ModelFinderPlugin extends Plugin {

  /**
   * @access private
   * @var array $modelList
   */
  private $modelList = array();

  /**
   * @access private
   * @var array $modelList
   */
  private $fileList = array();

  /**
   * Get Models
   *
   * @param string package
   * @access public
   */
  public function getModels($package) {

    $r = '/.+_(.+)Model\.php/';

    $this->findFiles(C_PATH_MODELS . $package . '/' , $r, 'addFile');
  
    foreach($this->fileList as $fileName) {
      preg_match($r, $fileName, $matches);
      if($matches[1] != 'Procedure' && $matches[1] !='Function') {
	array_push($this->modelList, array('model' => $matches[1]));
      }
    }

    sort($this->modelList);

    return $this->modelList;
  }

  /**
   * Get Model Types
   *
   * @param string package
   * @access public
   */
  public function getModelTypes($package, $model) {
    $model = $this->modelFactory->load($model, $package);    
    return $model->getTypes();
  }

  /**
   * Get Auto Increment
   *
   * @param string package
   * @access public
   */
  public function getAutoIncrement($package, $model) {
    $model = $this->modelFactory->load($model, $package);
    return $model->getAutoIncrement();
  }

  /**
   * Add file to list of files to process
   *
   * @access private
   * @param string $filename
   */
  private function addFile($filename) {
    array_push($this->fileList,$filename);
  }

  /**
   * Get a file list
   *
   * @access private
   * @param string $path
   * @param string $pattern
   * @param string $callback callback method
   */
  private function findFiles($path, $pattern, $callback) {
    $path = rtrim(str_replace("\\", "/", $path), '/') . '/';
    $matches = Array();
    $entries = Array();

    if(is_dir($path)) {
      $dir = dir($path);
    
      while (false !== ($entry = $dir->read())) {
	$entries[] = $entry;
      }
    
      $dir->close();
    }

    foreach ($entries as $entry) {
      $fullname = $path . $entry;
      if ($entry != '.' && $entry != '..' && is_dir($fullname) && substr($entry,0,1)) {

      } else if (is_file($fullname) && preg_match($pattern, $entry)) {
	$this->$callback($entry);
      }
      
    }
    
  }  

}


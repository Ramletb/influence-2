<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * DatabaseModelBuilderPlugin.php 
 * 
 * Contains the abstract {@link DatabaseModelBuilderPlugin} class.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage plugins
 */

/**
 * Requires ModelBuilderPlugin.php
 *
 * @see ModelBuilderPlugin.php
 */
require_once(C_PATH_CORE_PLUGINS . 'ModelBuilderPlugin.php');

/**
 * The abstract DatabaseModelBuilderPlugin Class
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @abstract
 * @package SP5
 * @subpackage plugins
 */
abstract class DatabaseModelBuilderPlugin extends ModelBuilderPlugin {

  /**
   * Build
   *
   * Builds Models
   *
   * @access public
   * @returns bool status
   */  
  public function build() {
    
    if(!is_file($this->headerFilePath)) {
      $this->output("ERROR: setHeader [" . $this->headerFilePath . "] is not a file.");
      return false;
    }

    if(!$this->packageName) {
      $this->output("ERROR: setPackage was not set.");
      return false;
    }

    // grab header for generated files.
    $this->header = file_get_contents($this->headerFilePath);

    // make our db object
    $this->dbInfo = $this->configuration->environment[$this->packageName]['db'];
    $dbf = DatabaseFactory::getDatabaseFactory();
    $this->db = $dbf->load($this->packageName);

    // get the table list
    $tables = $this->getTables();

    // get columns for each table
    foreach ($tables as $t) {
      //make tidy model name
      $chunks = explode('_', $t);
    
      $model = '';
      foreach ($chunks as $chunk) {
        $chunk = ucfirst($chunk);
        $model .= $chunk;
      }

      $columns = $this->getColumns($t);
    
      $modelPath = $this->isCore ? C_PATH_CORE_MODELS : C_PATH_MODELS;

      $modelFileName = $this->dbInfo['prepend'] ? $modelPath . $this->packageName . "/" . $this->packageName . '_' . $model . "Model.php" :
	$modelPath . $this->packageName . "/" . $model . "Model.php";
                                              
      
      $structFileName = $this->dbInfo['prepend'] ? $modelPath . $this->packageName . "/generated/" . $this->packageName . '_' . $model . "Structure.php" :
	$modelPath . $this->packageName . "/generated/" . $model . "Structure.php";
      
      $this->output("Creating $structFileName...");
      
      if (!file_exists($modelPath . $this->packageName . "/")) {
        mkdir($modelPath . $this->packageName . "/", 0777, true);
      }
      
      if (!file_exists($modelPath . $this->packageName . "/generated")) {
        mkdir($modelPath . $this->packageName . "/generated", 0777, true);
      }

      $fl = fopen($structFileName, 'w');

      // get the views list
      $views = $this->getViews();
    
      $type = "BASE TABLE";

      if(in_array($t, $views)) {
	$type = 'VIEW';
      }

      $this->writeStructure($fl, $t, $columns, $model, $type);
      fclose($fl);
  
      if (!file_exists($modelFileName)) {
        $this->output("Creating $modelFileName...");

        $fl = fopen($modelFileName, "w");
        $this->writeModel($fl, $t, $columns, $model);
        fclose($fl);
      } else {
        $this->output("Skipping $modelFileName (It exists)...");
      }
    }

    // stored procedures
    //
    $procFileName = $modelPath . $this->packageName . "/generated/" . $this->packageName . '_' . "Procedure.php";
    $procModelFileName = $modelPath . $this->packageName . "/" . $this->packageName . '_' . "ProcedureModel.php";

    $procedures = $this->getProcedures();

    $this->output("Creating $procFileName...");
    $fl = fopen($procFileName, 'w');
    $this->writeProcedure($fl, $procedures, $model);
    fclose($fl);

    if (!file_exists($procModelFileName)) {
      $this->output("Creating $procModelFileName...");
      $fl = fopen($procModelFileName, 'w');
      $this->writeProcedureModel($fl);
      fclose($fl);
    } else {
      $this->output("Skipping $procModelFileName (It exists)...");
    }

    // stored functions
    //
    $funcFileName = $modelPath . $this->packageName . "/generated/" . $this->packageName . '_' . "Function.php";
    $funcModelFileName = $modelPath . $this->packageName . "/" . $this->packageName . '_' . "FunctionModel.php";

    $functions = $this->getFunctions();

    $this->output("Creating $funcFileName...");
    $fl = fopen($funcFileName, 'w');
    $this->writeFunction($fl, $functions, $model);
    fclose($fl);

    if (!file_exists($funcModelFileName)) {
      $this->output("Creating $funcModelFileName...");
      $fl = fopen($funcModelFileName, 'w');
      $this->writeFunctionModel($fl);
      fclose($fl);
    } else {
      $this->output("Skipping $funcModelFileName (It exists)...");
    }

  }

  /**
   * Get Procedures
   *
   * @access protected
   * @returns array of procedures
   */
  protected function getProcedures() {
  }

  /**
   * Get Views
   *
   * return any views (supported in mysql > v5)
   *
   * @access protected
   * @returns array $tables
   */
  protected function getViews() {
  }

  /**
   * Get Tables
   *
   * return normalized table list (Mysql by default - override for other databases)
   *
   * @access protected
   * @returns array $tables
   */
  protected function getTables() {
  }

  /**
   * Get Create
   *
   * get sql create statement for a table
   *
   * @access protected
   * @param string $table
   * @returns string sql
   */
  protected function getCreate($table) {
  }

  /**
   * Get Columns
   *
   * return normalized column list (Mysql by default - override for other databases)
   *
   * @param string $table
   * @access protected
   * @returns array $columns
   */
  protected function getColumns($table) {
  }

  /**
   * Get Column Name
   *
   * return normalized column name (Mysql by default - override for other databases)
   *
   * @param array $col
   * @access protected
   * @returns string $name
   */
   protected function getColumnName($col) {
   }

  /**
   * Get Column Type
   *
   * return normalized column type (Mysql by default - override for other databases)
   *
   * @param array $col
   * @access protected
   * @returns string $type
   */
   protected function getColumnType($col) {
   }

  /**
   * Get Column Default
   *
   * return normalized column default value (Mysql by default - override for other databases)
   *
   * @param array $col
   * @access protected
   * @returns string $default
   */
   protected function getColumnDefault($col) {
   }

  /**
   * Is Column Primary Key
   *
   * return normalized determination as to whether this is the primary key (Mysql by default - override for other databases)
   *
   * @param array $col
   * @access protected
   * @returns boolean $isPrimary
   */
   protected function isColumnPrimaryKey($col) {
   }

  /**
   * Is Column Not Null
   *
   * return normalized not null flag (Mysql by default - override for other databases)
   *
   * @param array $col
   * @access protected
   * @returns boolean $isNotNull
   */
   protected function isColumnNotNull($col) {
   }

  /**
   * Get Enum Values
   *
   * return normalized enumeration values (Mysql by default - override for other databases)
   *
   * @param array $col
   * @access protected
   * @returns array $enums
   */
   protected function getEnumValues($col) {
   }

  /**
   * Write Structure
   *
   * Write the generated model file.
   *
   * @param filehandle $fl
   * @param string $table
   * @param array $cols
   * @param string $models
   * @param string $type
   * @access protected
   */
   protected function writeStructure($fl, $table, $cols, $model, $type='BASE TABLE') {
     
     $structName = $this->dbInfo['prepend'] ?  $this->packageName . '_' . $model . "Structure" : $model . "Structure";     
     $revGen     = 'Generated By ModelBuilderPlugin.php: $Rev: 2 $';
     
     
     $scriptRenderer = $this->rendererFactory->load('Script');
     $scriptRenderer->addMetadata('header', $this->header);
     $scriptRenderer->addMetadata('revGen', $revGen);
     $scriptRenderer->addMetadata('requirePath', C_PATH_MODEL_SUBCLASSES);

     $data = new stdClass;
     $data->name          = $structName;
     $data->type          = $type;
     $data->packageName   = $this->packageName;
     $data->tableName     = $table;
     $data->modelName     = $model;
     $data->cols          = $cols;
     $data->primaryKey    = null;
     $data->autoIncrement = null;

     foreach ($cols as $c) {
       if ($c['primary']) { $data->primaryKey = $c['name']; break; }
     }

     foreach ($cols as $c) {
       if (array_key_exists('auto_increment', $c)) { $data->autoIncrement = $c['auto_increment']; break; }
     }

     $scriptRenderer->setData($data);

     $contents = $scriptRenderer->renderReturn('core/build_models/structure');

     fputs($fl, $contents);
   }


  /**
   * Write Model
   *
   * Write the user editable model file.
   *
   * @param filehandle $fl
   * @param string $table
   * @param array $cols
   * @param string $model
   * @access protected
   */
   protected function writeModel($fl, $table, $cols, $model) {

     $modelName = $this->dbInfo['prepend'] ?  $this->packageName . '_' . $model :
                                              $model;

     $revGen     = 'Generated By ModelBuilderPlugin.php: $Rev: 2 $';

     $modelPath = $this->isCore ? C_PATH_CORE_MODELS : C_PATH_MODELS;
     
     
     $scriptRenderer = $this->rendererFactory->load('Script');
     $scriptRenderer->addMetadata('header', $this->header);
     $scriptRenderer->addMetadata('revGen', $revGen);
     $scriptRenderer->addMetadata('requirePath', C_PATH_MODEL_SUBCLASSES);
     $scriptRenderer->addMetadata('requirePath', $modelPath);

     $data = new stdClass;
     $data->packageName = $this->packageName;
     $data->modelName   = $modelName;

     $scriptRenderer->setData($data);

     $contents = $scriptRenderer->renderReturn('core/build_models/model');

     fputs($fl, $contents);
   }

  /**
   * Write Procedure
   *
   * Write the generated procedure file.
   *
   * @param filehandle $fl
   * @param string $table
   * @param array $cols
   * @access protected
   */
   protected function writeProcedure($fl, $procedures, $model) {
     
     $revGen     = 'Generated By ModelBuilderPlugin.php: $Rev: 2 $';
          
     $scriptRenderer = $this->rendererFactory->load('Script');
     $scriptRenderer->addMetadata('header', $this->header);
     $scriptRenderer->addMetadata('revGen', $revGen);
     $scriptRenderer->addMetadata('requirePath', C_PATH_MODEL_SUBCLASSES);

     $data = new stdClass;
     $data->packageName = $this->packageName;
     $data->procedures = $procedures;

     $scriptRenderer->setData($data);
     $contents = $scriptRenderer->renderReturn('core/build_models/procedure');

     fputs($fl, $contents);
   }

  /**
   * Write Procedure Model
   *
   * Write the generated procedure file.
   *
   * @param filehandle $fl
   * @param string $table
   * @param array $cols
   * @access protected
   * @returns array $enums
   */
   protected function writeProcedureModel($fl) {
     
     $revGen     = 'Generated By ModelBuilderPlugin.php: $Rev: 2 $';

     $modelPath = $this->isCore ? C_PATH_CORE_MODELS : C_PATH_MODELS;
          
     $scriptRenderer = $this->rendererFactory->load('Script');
     $scriptRenderer->addMetadata('header', $this->header);
     $scriptRenderer->addMetadata('revGen', $revGen);
     $scriptRenderer->addMetadata('requirePath', $modelPath);

     $data = new stdClass;
     $data->packageName = $this->packageName;
     $scriptRenderer->setData($data);
     $contents = $scriptRenderer->renderReturn('core/build_models/procedureModel');

     fputs($fl, $contents);
   }

  /**
   * Write Function
   *
   * Write the generated function file.
   *
   * @param filehandle $fl
   * @param string $table
   * @param array $cols
   * @access protected
   * @returns array $enums
   */
   protected function writeFunction($fl, $functions, $model) {
     
     $revGen     = 'Generated By ModelBuilderPlugin.php: $Rev: 2 $';
          
     $scriptRenderer = $this->rendererFactory->load('Script');
     $scriptRenderer->addMetadata('header', $this->header);
     $scriptRenderer->addMetadata('revGen', $revGen);
     $scriptRenderer->addMetadata('requirePath', C_PATH_MODEL_SUBCLASSES);

     $data = new stdClass;
     $data->packageName = $this->packageName;
     $data->functions = $functions;

     $scriptRenderer->setData($data);
     $contents = $scriptRenderer->renderReturn('core/build_models/function');

     fputs($fl, $contents);
   }

  /**
   * Write Function Model
   *
   * Write the generated function file.
   *
   * @param filehandle $fl
   * @param string $table
   * @param array $cols
   * @access protected
   * @returns array $enums
   */
   protected function writeFunctionModel($fl) {
     
     $revGen     = 'Generated By ModelBuilderPlugin.php: $Rev: 2 $';

     $modelPath = $this->isCore ? C_PATH_CORE_MODELS : C_PATH_MODELS;
          
     $scriptRenderer = $this->rendererFactory->load('Script');
     $scriptRenderer->addMetadata('header', $this->header);
     $scriptRenderer->addMetadata('revGen', $revGen);
     $scriptRenderer->addMetadata('requirePath', $modelPath);

     $data = new stdClass;
     $data->packageName = $this->packageName;
     $scriptRenderer->setData($data);
     $contents = $scriptRenderer->renderReturn('core/build_models/functionModel');

     fputs($fl, $contents);
   }

  /**
   * Get PHP Type By Column Type
   *
   * @param string $colType
   * @access protected
   * @returns string php type (string or int)
   */
   protected function getPHPTypeByColumnType($colType) {
     $phpType = null;

     if ($colType == "b" || $colType == "i" || $colType == "f") {
       $phpType = "int";
     } else {
       $phpType = "string";
     }

     return $phpType;
   }

}

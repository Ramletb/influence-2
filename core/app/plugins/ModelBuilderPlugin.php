<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * ModelBuilderPlugin.php 
 * 
 * Contains the {@link BuildModelsPlugin} class.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage plugins
 */

/**
 * The ModelBuilderPlugin Class
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage plugins
 */
class ModelBuilderPlugin extends Plugin {

  /**
   * Output Callback Object
   *
   * @access protected
   * @var object $outputCallbackObj;
   */
  protected $outputCallbackObj;

  /**
   * Output Callback Method
   *
   * @access protected
   * @var object $outputCallbackMethod;
   */
  protected $outputCallbackMethod;

  /**
   * Package Name
   *
   * Holds the name of the package from environment 
   * configuration.
   *
   * aka [PACKAGENAME] in environment ini
   *
   * @var string $packageName
   * @access protected
   */  
  protected $packageName;

  /**
   * Heander File Path
   *
   * Holds the path to the header file.
   *
   * @var string $headerFilePath
   * @access protected
   */  
  protected $headerFilePath;

  /**
   * Is Core
   *
   * Is this a core model (0 or 1)
   *
   * @var int $isCore
   * @access protected
   */  
  protected $isCore;

  /**
   * Set Core
   *
   * @access public
   * @param int $core 0= false, 1= true
   */
  public function setCore($core=0) {
    $this->isCore = $core;
  }

  /**
   * Set Package
   *
   * Sets the name of the package from environment 
   * configuration.
   *
   * aka [PACKAGENAME] in environment ini
   *
   * @access public
   * @param string $packageName Name of environment package.
   */
  public function setPackage($packageName) {
    $this->packageName = $packageName;
  }

  /**
   * Set Header
   *
   * Sets the path to the header file.
   *
   * @access public
   * @param string $headerFilePath
   */  
  public function setHeader($headerFilePath) {
    $this->headerFilePath = $headerFilePath;
  }

  /**
   * Build
   *
   * Builds Models
   *
   * @access public
   * @returns bool status
   */  
  public function build() {
  }

  /**
   * Write Structure
   *
   * Write the generated model file.
   *
   * @param filehandle $fl
   * @param string $table
   * @param array $cols
   * @access protected
   * @returns array $enums
   */
   protected function writeStructure($fl, $table, $cols, $model) {
   }

  /**
   * Write Model
   *
   * Write the user editable model file.
   *
   * @param filehandle $fl
   * @param string $table
   * @param array $cols
   * @access protected
   * @returns array $enums
   */
   protected function writeModel($fl, $table, $cols, $model) {
   }

  /**
   * Set Output Callback
   *
   * @access public
   * @param object $obj
   * @param string $method
   */
  public function setOutputCallback($obj,$method) {
    error_reporting(E_ALL ^ E_WARNING);

    $this->outputCallbackObj = $obj;
    $this->outputCallbackMethod = $method;
  }

  /**
   * Output
   *
   * Output a message
   *
   * @access protected
   * @param string $msg
   */
  protected function output($msg) {
    $obj = $this->outputCallbackObj;
    $method = $this->outputCallbackMethod;

    $obj->$method($msg);
  }


}

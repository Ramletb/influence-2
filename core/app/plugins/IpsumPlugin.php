<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * IpsumPlugin.php 
 *
 * Contains the {@link IpsumPlugin} class.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage plugins
 */

/**
 * The IpsumPlugin Class
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage plugins
 */
class IpsumPlugin extends Plugin {

  /**
   * Lorem Ipsum Content Generator
   *
   * Returns xhtml content of Lorem Ipsum.
   *
   * @returns string A some paragraphs of Lorem Ipsum.
   * @access public
   */
  public function getContent() {
    $paras = rand(1, 5);

    $paragraphs = '<div class="ipsum-content">';

    for($i = 1; $i <= $paras; $i++) {
      $paragraphs .= $this->getParagraph();
    }

    $paragraphs .= '</div>' . "\n";

    return $paragraphs;
  }

  /**
   * Lorem Ipsum Paragraph Generator
   *
   * Returns a paragraph of Lorem Ipsum.
   *
   * @returns string A paragraph of Lorem Ipsum.
   * @access public
   */
  public function getParagraph() {
    $sen = rand(10, 15);

    $paragraph = '<p>';

    for($i = 0; $i <= $sen; $i++) {
      $paragraph .= $this->getSentence();
    }

    $paragraph .= '</p>' . "\n";

    return $paragraph;
  }

  /**
   * Lorem Ipsum Sentance Generator
   *
   * Returns a sentance of Lorem Ipsum.
   *
   * @returns string A sentance of Lorem Ipsum.
   * @access public
   */
  public function getSentence() {

    $ipsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. ";
    $ipsum .= "Nunc porttitor, ligula nec auctor cursus, leo tellus accumsan ligula, feugiat egestas velit nibh at turpis. ";
    $ipsum .= "Nulla egestas varius nunc. ";
    $ipsum .= "Vivamus vulputate, sem sed egestas varius, mi justo faucibus eros, ac sagittis sapien purus id lectus. ";
    $ipsum .= "Praesent auctor leo in nisi. Nunc sollicitudin augue sit amet sem. ";
    $ipsum .= "Proin in ligula ut metus convallis euismod. ";
    $ipsum .= "Praesent scelerisque, libero ut rutrum laoreet, dui leo facilisis nunc, id suscipit pede nulla quis neque. ";
    $ipsum .= "Etiam lacinia ornare sapien. ";
    $ipsum .= "Mauris ligula turpis, tristique sed, pharetra nec, lobortis ut, lectus. ";
    $ipsum .= "Duis ante nunc, eleifend et, aliquet eget, semper a, neque. Aliquam magna. ";
    $ipsum .= "Phasellus laoreet viverra libero. ";
    $ipsum .= "Proin a tortor quis ligula egestas convallis. ";
    $ipsum .= "Nulla ligula egestas facilisi. ";
    $ipsum .= "Nulla tincidunt pellentesque mi. ";
    $ipsum .= "Vestibulum orci quam, facilisis vel, congue eu, sagittis egestas, est. ";
    $ipsum .= "Proin lacinia nulla vel ligula. ";
    $ispum .= "Vivamus facilisis vel, faucibus. ";
    $ipsum .= "Curabitur mi metus, accumsan faucibus, eleifend condimentum, porttitor id, erat. ";
    $ipsum .= "Fusce ultrices erat non odio. ";
    $ipsum .= "Ut ac risus at ante dictum condimentum. ";
    $ipsum .= "Suspendisse nec eros. ";
    $ipsum .= "Etiam pretium nunc quis pede. ";
    $ipsum .= "Quisque turpis. Cras adipiscing. ";
    $ipsum .= "Donec lobortis justo nec lectus. ";
    $ipsum .= "Etiam eleifend, metus iaculis rhoncus pellentesque, elit risus bibendum sapien, vel commodo justo diam a lectus. ";
    $ipsum .= "Mauris vitae ipsum. ";
    $ipsum .= "Maecenas placerat sagittis magna. ";
    $ispum .= "Sed velit ipsum, porttitor in, gravida at, auctor ut, quam. ";
    $ipsum .= "Integer euismod mattis arcu. ";
    $ipsum .= "Sed vitae nulla a nisl vestibulum aliquet. ";
    $ipsum .= "Sed eget justo. ";
    $ipsum .= "Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. ";
    $ipsum .= "Maecenas nulla a nisl pretium. ";
    $ipsum .= "Donec velit lacus, mollis vitae, ullamcorper quis, ultrices eget, ligula. ";
    $ipsum .= "Aliquam vitae leo eget dolor semper vehicula. ";
    $ipsum .= "Phasellus vitae eros non purus sodales luctus. ";
    $ipsum .= "Nulla id magna at libero porttitor rutrum. ";
    $ipsum .= "Aliquam sit amet risus sit amet diam consectetur auctor. ";
    $ipsum .= "Etiam turpis magna, facilisis nec, sollicitudin sed, hendrerit sit amet, nibh. ";
    $ipsum .= "Nulla facilisi nunc vulputate. ";

    $ipsum = explode(" ", strtolower($ipsum));

    $ipsumCount = count($ipsum) - 1;

    $wordCount = rand(3, 15);

    $s = "";

    $p = array(' ',
	       ' ',
	       ' ',
	       ' ',
	       '-',
	       ' ',
	       '; ',
	       ' ',
	       ' ',
	       ', ',
	       ' ',
	       ' ',
	       ' ',
	       ' ',
	       ' ',
	       ' ',
	       ' ',
	       ' ',
	       ' ',
	       ' ',
	       ' ',
	       ' ',
	       ' ',
	       ', ',
	       ' & ',
	       ' ',
	       ' ',
	       ' ',
	       ' ',
	       ' ',
	       ' ',
	       ' ',
	       ' ');

    for($i=0; $i < $wordCount; $i++) {
      $word = $ipsum[rand(0, $ipsumCount)];
      $word = preg_replace('/\.+|\,/', '', $word);

      if(rand(0,100) == 1) {
	$word = strtoupper($word);
      }

      if($i < ($wordCount - 1) && rand(0,5) == 2) {
	$word .= $p[rand(0,count($p))];
      } else {
	$r = rand(0,300);
	if($r == 0) {
	  $word = '"' . $word . ' ' . $ipsum[rand(0, $ipsumCount)] . '"';
	}
	if($r == 1) {
	  $word = '(' . $word . ' ' . $ipsum[rand(0, $ipsumCount)] . ')';
	}
	if($r == 2) {
	  $word = $word . '. ' . ucfirst($ipsum[rand(0, $ipsumCount)]);
	}
	if($r == 3) {
	  $word = '@' . $word;
	}
	if($r == 4) {
	  $word = 'RE: ' . $word;
	}
	if($r == 5) {
	  $word = '*' . $word . '*';
	}
	if($r == 6) {
	  $word = ':) ' . $word;
	}
	if($r == 7) {
	  $word = $word . ' :(';
	}
	if($r == 8) {
	  $word = $word . ' WTF? ' . ucfirst($ipsum[rand(0, $ipsumCount)]);
	}
	if($r == 9) {
	  $word = $word . ' LMAO! ' . ucfirst($ipsum[rand(0, $ipsumCount)]);
	}
	if($r == 10) {
	  $word = '_' . $word . '_ ' . $ipsum[rand(0, $ipsumCount)];
	}
	if($r == 11) {
	  $word = $word . '/' . $ipsum[rand(0, $ipsumCount)];
	}
	if($r == 12) {
	  $word = '"' . $word . ' ' . $ipsum[rand(0, $ipsumCount)] . ' ' . $ipsum[rand(0, $ipsumCount)] . '"';
	}
	if($r == 13) {
	  $word = '"' . $word . ' ' . $ipsum[rand(0, $ipsumCount)] . ' ' . $ipsum[rand(0, $ipsumCount)] . ' ' . $ipsum[rand(0, $ipsumCount)] . '"';
	}

	if($r == 14) {
	  $words = $word . ' ' . $ipsum[rand(0, $ipsumCount)] . ' ' . $ipsum[rand(0, $ipsumCount)] . ' ' . $ipsum[rand(0, $ipsumCount)];
	  $word = preg_replace(array('/o/',
				     '/a/',
				     '/e/',
				     '/l/',
				     '/t/'),
			       array('0',
				     '@',
				     '3',
				     '1',
				     '7'),
			       $words);
	}
	if($r == 15) {
	  $word = $word . ' == ' . $ipsum[rand(0, $ipsumCount)];
	}
	if($r == 16) {
	  $word = $word . ' <8 ' . $ipsum[rand(0, $ipsumCount)];
	}
	if($r == 17) {
	  $word = $word . '... ' . ucfirst($ipsum[rand(0, $ipsumCount)]);
	}
	if($r == 18) {
	  $word = $word . '!! ' . ucfirst($ipsum[rand(0, $ipsumCount)]);
	}
	if($r == 19) {
	  $word = $word . '? ' . ucfirst($ipsum[rand(0, $ipsumCount)]);
	}
	if($r == 20) {
	  $word = $word . ' ;) ' . $ipsum[rand(0, $ipsumCount)];
	}
	if($r == 21) {
	  $word = $word . ' ;-) ' . $ipsum[rand(0, $ipsumCount)];
	}
	if($r == 22) {
	  $word = $word . ' <-- ' . $ipsum[rand(0, $ipsumCount)];
	}
	if($r == 23) {
	  $word = $word . ' -- ' . $ipsum[rand(0, $ipsumCount)];
	}


	$word .= ' ';
      }

      $s .= $word;
    }
    

    $e = array('.'.'.'.'.','...','','!','?','.','.','.','.','.','.','.','.','...','.','?','!','???');

    $sen = ucfirst(chop($s)) . $e[rand(0,count($e))];

    if(rand(0,5) > 3) {
      $sen = strtolower($sen);
    }

    if(rand(0,200) == 100) {
      $sen = strtoupper($sen);
    } 

    return $sen;
  }

  /**
   * Lorem Ipsum Title Generator
   *
   * Returns Lorem Ipsum title (1-4 words.)
   *
   * @returns string A Lorem Ipsum title.
   * @access public
   */
  public function getTitle() {
    $wordsCount = rand(2,5);

    $words = explode(' ', str_replace("\s+", " ", preg_replace('/[^a-zA-Z0-9-\s-\n]/', '', $this->getParagraph() )));

    $title = '';
    
    for($i=1; $i < $wordsCount; $i++) {
      $title .= ucfirst($words[$i]) . ' ';
    }

    return $title;
  }

  /**
   * Lorem Ipsum Atricle Generator
   *
   * Returns Lorem Ipsum article.
   *
   * @returns array A hash of title, content.
   * @access public
   */
  public function getArticle() {
    return array('title' => $this->getTitle(), 'content' => $this->getContent());
  }

}


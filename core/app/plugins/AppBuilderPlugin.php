<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * AppBuilderPlugin.php 
 * 
 * Contains the {@link AppBuilderPlugin} class.
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @package SP5
 * @subpackage plugins
 */

/**
 * The AppBuilderPlugin Class
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @package SP5
 * @subpackage plugins
 */
class AppBuilderPlugin extends Plugin {

  /**
   * Output Callback Object
   *
   * @access protected
   * @var object $outputCallbackObj;
   */
  protected $outputCallbackObj;

  /**
   * Output Callback Method
   *
   * @access protected
   * @var object $outputCallbackMethod;
   */
  protected $outputCallbackMethod;

  /**
   * Make
   *
   *
   * @param string $name
   * @param string $type
   * @param string $package
   * @param string $header
   * @param string $path
   * @param string $suffix
   * @access protected
   */
  public function make($name, $package, $header, $type, $path, $suffix="") {

    $scriptRenderer = $this->rendererFactory->load('Script');

    $scriptRenderer->addMetadata('header', file_get_contents($header));

     $data = new stdClass;
     $data->name        = $name;
     $data->packageName = $package;

     $scriptRenderer->setData($data);

     $contents = $scriptRenderer->renderReturn('core/build_app/' . $type);

     $file = $path . $name . $suffix . ".php";

     if(is_file($file)) {

       $this->output("ERROR: " . $file . " was found!");

     } else {

       $this->output("GENERATED: " . $file);
       
       mkdir($path, 0777, true);
       
       $fl = fopen($file, 'w');
       fputs($fl, $contents);
       fclose($fl);

     }

   }

  /**
   * Set Output Callback
   *
   * @access public
   * @param object $obj
   * @param string $method
   */
  public function setOutputCallback($obj,$method) {
    error_reporting(E_ALL ^ E_WARNING);

    $this->outputCallbackObj = $obj;
    $this->outputCallbackMethod = $method;
  }

  /**
   * Output
   *
   * Output a message
   *
   * @access protected
   * @param string $msg
   */
  protected function output($msg) {
    $obj = $this->outputCallbackObj;
    $method = $this->outputCallbackMethod;

    $obj->$method($msg);
  }


}

<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * HeaderPlugin.php 
 *
 * Contains the {@link HeaderPlugin} class.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage plugins
 */

/**
 * Header split string
 *
 */
define("C_SPLITSTRING", "/* ------------------------- END FRAMEWORK HEADER ------------------------- */");

/**
 * The HeaderPlugin Class
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage plugins
 */
class HeaderPlugin extends Plugin {

  /**
   * @access private
   * @var array $fileList
   */
  private $fileList = array();

  /**
   * @access private
   * @var object $outputCallbackObj;
   */
  private $outputCallbackObj;

  /**
   * @access private
   * @var object $outputCallbackMethod;
   */
  private $outputCallbackMethod;

  /**
   * Set Output Callback
   *
   * @access public
   * @param object $obj
   * @param string $method
   */
  public function setOutputCallback($obj,$method) {
    $this->outputCallbackObj = $obj;
    $this->outputCallbackMethod = $method;
  }

  /**
   * Output a message
   *
   * @access private
   * @param string $msg
   */
  private function output($msg) {
    $obj = $this->outputCallbackObj;
    $method = $this->outputCallbackMethod;

    $obj->$method($msg);
  }

  /**
   * Generate Headers
   *
   * @access public
   * @param string $package
   */
  public function generate($package) {

    if(!$package) {
      $this->output('try ./cli.php --controller utils --action build_headers --package PKG');
      die;
    }

    $header = file_get_contents('./doc/' . $package . '.header');

    if(!$header) {
      $this->output('ERROR: ./docs/' . $package . '.header does not exist');
      die;
    }

    $this->findFiles('../', '//', 'addFile');
  
    if(count($this->fileList > 0)) {
      foreach($this->fileList as $file) {
	$this->parseFile($file,$header,$package,$this->scriptCheck($file));
      }
    } else {
      $this->output("ERROR: no files found...");
    }

  }

  /**
   * Parse a file
   *
   * @access private
   * @param string $fileName
   * @param string $headerFile
   * @param string $package
   * @param bool $script
   */
  private function parseFile($fileName,$headerFile,$package,$script) {
    
    $file   = file_get_contents($fileName);    
    $fileParts = explode(C_SPLITSTRING, $file, 2);
    
    if(count($fileParts) == 2) {
      $newContents = '';
      if($script) {
	$newContents .= "#!/usr/bin/php -q\n";
      }
      $newContents .= '<?php '."\n";
      $newContents .= $headerFile . "\n";
      $newContents .= C_SPLITSTRING;
      $newContents .= preg_replace('/(\ \*\ @package\ )\w+/',"$1".$package, $fileParts[1]);
      $this->output("[".$fileName."] done...");
      file_put_contents($fileName, $newContents);
    } else {
      // skip... no split string
    }
  }


  /**
   * Script Check
   *
   * @access private
   * @param string $file
   * @returns bool
   */
  private function scriptCheck($file) {
    $fileContents = file_get_contents($file);
    if(substr($fileContents, 0, 2) == '#!') {
      return true;
    } else {
      return false;
    }
  }

  /**
   * Get a file list
   *
   * @access private
   * @param string $path
   * @param string $pattern
   * @param string $callback callback method
   */
  private function findFiles($path, $pattern, $callback) {
    $path = rtrim(str_replace("\\", "/", $path), '/') . '/';
    $matches = Array();
    $entries = Array();
    $dir = dir($path);
    
    while (false !== ($entry = $dir->read())) {
      $entries[] = $entry;
    }
    
    $dir->close();
    
    foreach ($entries as $entry) {
      $fullname = $path . $entry;
      if ($entry != '.' && $entry != '..' && is_dir($fullname) && substr($entry,0,1) != '.' && $entry != 'logs' && $entry != 'docs') {
	$this->findFiles($fullname, $pattern, $callback);
      } else if (is_file($fullname) && preg_match($pattern, $entry)) {
	$this->$callback($fullname);
      }
      
    }
    
  }


  /**
   * Add file to list of files to process
   *
   * @access private
   * @param string $filename
   */
  private function addFile($filename) {
    array_push($this->fileList,$filename);
  }
  

}

<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * ControllerBuilderPlugin.php 
 * 
 * Contains the {@link ControllerBuilderPlugin} class.
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @package SP5
 * @subpackage plugins
 */

/**
 * The ControllerBuilderPlugin Class
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @package SP5
 * @subpackage plugins
 */
class ControllerBuilderPlugin extends Plugin {

  /**
   * Output Callback Object
   *
   * @access protected
   * @var object $outputCallbackObj;
   */
  protected $outputCallbackObj;

  /**
   * Output Callback Method
   *
   * @access protected
   * @var object $outputCallbackMethod;
   */
  protected $outputCallbackMethod;

  /**
   * Controller Name
   *
   * @var string $controllerName
   * @access protected
   */  
  protected $controllerName;

  /**
   * Package Name
   *
   * Holds the name of the package from environment 
   * configuration.
   *
   * aka [PACKAGENAME] in environment ini
   *
   * @var string $packageName
   * @access protected
   */  
  protected $packageName;

  /**
   * Header File Path
   *
   * Holds the path to the header file.
   *
   * @var string $headerFilePath
   * @access protected
   */  
  protected $headerFilePath;

  /**
   * Set Controller Name
   *
   *
   * @access public
   * @param string $controllerName Name of controller to create
   */
  public function setControllerName($controllerName) {
    $this->controllerName = $controllerName;
  }

  /**
   * Set Package
   *
   * Sets the name of the package from environment 
   * configuration.
   *
   * aka [PACKAGENAME] in environment ini
   *
   * @access public
   * @param string $packageName Name of environment package.
   */
  public function setPackage($packageName) {
    $this->packageName = $packageName;
  }

  /**
   * Set Header
   *
   * Sets the path to the header file.
   *
   * @access public
   * @param string $headerFilePath
   */  
  public function setHeader($headerFilePath) {
    $this->headerFilePath = $headerFilePath;
  }

  /**
   * Make
   *
   *
   * @param string $name
   * @param string $type
   * @access protected
   */
  public function make($name, $type) {
     

    $name = ucfirst($name);

    $scriptRenderer = $this->rendererFactory->load('Script');

    $scriptRenderer->addMetadata('header', file_get_contents($this->headerFilePath));

     $data = new stdClass;
     $data->name          = $name;
     $data->packageName   = $this->packageName;

     $this->output("this is a test...");
     $scriptRenderer->setData($data);

     $contents = $scriptRenderer->renderReturn('core/build_app/' . $type);
     $this->output($contents);

     //fputs($fl, $contents);
   }

  /**
   * Set Output Callback
   *
   * @access public
   * @param object $obj
   * @param string $method
   */
  public function setOutputCallback($obj,$method) {
    error_reporting(E_ALL ^ E_WARNING);

    $this->outputCallbackObj = $obj;
    $this->outputCallbackMethod = $method;
  }

  /**
   * Output
   *
   * Output a message
   *
   * @access protected
   * @param string $msg
   */
  protected function output($msg) {
    $obj = $this->outputCallbackObj;
    $method = $this->outputCallbackMethod;

    $obj->$method($msg);
  }


}

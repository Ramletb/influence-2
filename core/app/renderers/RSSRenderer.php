<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * RSSRenderer.php
 *
 * Contains the {@link RSSRenderer}, {@link RSSTransformer} and
 * {@link RSSTransformerItem} classes.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage core
 */

/**
 * The Abstract RSSTransformer Class
 *
 * Read {@link http://cyber.law.harvard.edu/rss/rss.html RSS 2.0 at Harvard Law} for
 * more detailed descriptions of each element.
 *
 * For channel elements that are not required by the RSS specification simply return false 
 * or do not return.
 *
 * @package SP5
 * @subpackage renderer
 */
abstract class RSSTransformer extends Transformer {

  /**
   * Get Namespace Hash
   *
   * Returns a hash of namespaces and urls
   * 'georss' => 'http://www.georss.org/georss'
   *
   * @see http://validator.w3.org/feed/docs/howto/declare_namespaces.html
   * @access public 
   * @returns array xml namespace array
   */
  public function getNamespaces() {
    return array();
  }

  /**
   * Get Channel Title (REQUIRED)
   *
   * Returns the RSS channel title.
   * Uses $this->metaData['title'] by default.
   *
   * @access public 
   * @returns string RSS channel title
   */
  public function getTitle() {
    return $this->metaData['title'];
  }

  /**
   * Get Channel Description (REQUIRED)
   *
   * Returns the RSS channel description.
   * Uses $this->metaData['description'] by default.
   *
   * @access public 
   * @returns string RSS channel description
   */
  public function getDescription() {
    return $this->metaData['description'];
  }

  /**
   * Get Channel Link (REQUIRED)
   *
   * Returns the RSS channel link.
   *
   * @access public 
   * @returns string RSS channel link (URL.)
   */
  public function getLink() {
  }

  /**
   * Get Channel Language
   *
   * Returns the RSS channel lauguage.
   *
   * Example: en-us
   *
   * @access public 
   * @returns string RSS channel lauguage.
   */
  public function getLanguage() {
  }

  /**
   * Get Channel Copyright
   *
   * Returns the RSS channel copyright.
   *
   * Example: Copyright 2009, John Doe
   *
   * @access public 
   * @returns string RSS channel copyright.
   */
  public function getCopyright() {
  }

  /**
   * Get Channel Managing Editor
   *
   * Returns the RSS channel managing editor.
   *
   * Example: john.doe@example.com (John Doe)
   *
   * @access public 
   * @returns string RSS channel managing editor.
   */
  public function getManagingEditor() {
  }

  /**
   * Get Channel Web Master
   *
   * Returns the RSS channel web master.
   *
   * Example: john.doe@example.com (John Doe)
   *
   * @access public 
   * @returns string RSS channel web master.
   */
  public function getWebMaster() {
  }

  /**
   * Get Channel Publication Date
   *
   * Returns the RSS channel publicationdate.
   *
   * Retunrs the current data time as (@link http://www.faqs.org/rfcs/rfc2822 RFC 2822}
   * by default.
   *
   * Example: Tue, 10 Feb 2009 12:46:12 -0800
   *
   * @access public 
   * @returns string RSS channel publication date.
   */
  public function getPubDate() {
    return date('r');
  }

  /**
   * Get Channel Last Build Date
   *
   * Returns the RSS channel last build date.
   *
   * Retunrs the current data time as (@link http://www.faqs.org/rfcs/rfc2822 RFC 2822}
   * by default.
   *
   * Example: Tue, 10 Feb 2009 12:46:12 -0800
   *
   * @access public 
   * @returns string RSS channel last build date.
   */
  public function getLastBuildDate() {
    return date('r');
  }

  /**
   * Get Channel Category
   *
   * Returns the RSS channel category.
   *
   * Example: News
   *
   * @access public 
   * @returns string RSS channel category
   */
  public function getCategory() {
  }

  /**
   * Get Channel TTL
   *
   * Returns the RSS channel TTL. 
   *
   * Example: 60
   *
   * @access public 
   * @returns int RSS channel TTL in minutes
   */
  public function getTTL() {
  }

  /**
   * Get Channel Image URL
   *
   * Returns the RSS channel image URL, used in the <image> element.
   * The image title and link are taken from the channel title and link.
   *
   * Read more about the 
   * {@link http://cyber.law.harvard.edu/rss/rss.html#ltimagegtSubelementOfLtchannelgt image element}.
   *
   * Example: http://www.example.com/image.png
   *
   * @access public 
   * @returns string RSS channel image url.
   */
   public function getImageURL() {
   }

  /**
   * Get Channel Rating
   *
   * Returns the RSS channel {@link http://www.w3.org/PICS/ PICS} rating. 
   *
   * @access public 
   * @returns string RSS channel rating.
   */
   public function getRating() {
   }

  /**
   * Get Channel Items
   *
   * Returns an array of {@link RSSTransformerItem}s.
   *
   * @uses RSSTransformerItem
   * @access public 
   * @returns array an array of {@link RSSTransformerItem}s.
   */
   public function getItems() {
   }

}

/**
 * The RSSTransformerItem Class
 *
 * Read {@link http://cyber.law.harvard.edu/rss/rss.html RSS 2.0 at Harvard Law} for
 * more detailed descriptions of RSS items.
 *
 * For item elements that are not required by the RSS specification simply return false 
 * or do not return.
 *
 * @package SP5
 * @subpackage renderer
 */
class RSSTransformerItem extends Transformer {

  /**
   * Item Title
   *
   * An RSS channel item title.
   *
   * @access public 
   * @var string $title the RSS channel item title
   */
  public $title;

  /**
   * Item Description
   *
   * An RSS channel item description.
   *
   * @access public 
   * @var string $description the RSS channel item description
   */
  public $description;

  /**
   * Item Link
   *
   * An RSS channel item link.
   *
   * @access public 
   * @var string $link the RSS channel item link (URL.)
   */
  public $link;

  /**
   * Item Category
   *
   * An RSS channel item category.
   *
   * Example: News
   *
   * @access public 
   * @var string $category the RSS channel item category.
   */
  public $category;

  /**
   * Item Author
   *
   * An RSS channel item author.
   *
   * Example: john.doe@example.com (John Doe)
   *
   * @access public 
   * @var string $author the RSS channel item author.
   */
  public $author;

  /**
   * Item Enclosure URL
   *
   * An RSS channel item enclosure URL.
   *
   * @access public 
   * @var string $enclosureURL the RSS channel item enclosure URL.
   */
  public $enclosureURL;

  /**
   * Item Enclosure Length
   *
   * An RSS channel item enclosure length.
   *
   * @access public 
   * @var string $enclosureLength the RSS channel item enclosure length.
   */
  public $enclosureLength;

  /**
   * Item Enclosure Type
   *
   * An RSS channel item enclosure type.
   *
   * @access public 
   * @var string $enclosureLength the RSS channel item enclosure type.
   */
  public $enclosureType;

  /**
   * Item GUID
   *
   * An RSS channel item GUID.
   *
   * @access public 
   * @var string $GUID the RSS channel item GUID.
   */
  public $GUID;

  /**
   * Item GUID IsPermaLink
   *
   * Is the RSS channel item GUID a perma link?
   *
   * @access public 
   * @var bool $GUIDIsPermaLink
   */
  public $GUIDIsPermaLink = false;

  /**
   * Item Publication Date
   *
   * An RSS channel item publication date.
   *
   * @access public 
   * @var string $pubDate the RSS channel item publication date.
   */
  public $pubDate;

  /**
   * Item Source
   *
   * An RSS channel item source.
   *
   * @access public 
   * @var string $source the RSS channel item source.
   */
  public $source;

  /**
   * Item Custom Elements
   *
   * An array of RSS channel item custom elements.
   *
   * @access public 
   * @var array $customElements a custom element.
   */
  public $customElements = array();

}

/**
 * The RSSRenderer Class
 *
 * The RSSRenderer class is a {@link TransformerRenderer}. 
 * This type of {@link Renderer} requires a {@link RSSTransformer} class view.
 *
 * Read {@link http://cyber.law.harvard.edu/rss/rss.html RSS 2.0 at Harvard Law} for
 * more detailed description of the RSS 2.0 spec.
 *
 * @uses RSSTransformer
 * @package SP5
 * @subpackage renderer
 */
class RSSRenderer extends TransformerRenderer {

  /**
   * Content Type
   *
   * @access public 
   */
  public function contentType() {
    return "content-type: application/rss+xml";
  }

  /**
   * Render
   *
   * Render RSS 2.0.
   *
   * @access public 
   */
  public function render($view=null) {

    $this->loadTransformation($view);

    echo '<?xml version="1.0" encoding="utf-8"?>'."\n";
    echo '<rss version="2.0"';

    $ns = array();
    if(is_object($this->transformer)) {
      $ns = $this->transformer->getNamespaces();
    }

    if(count($ns) > 0) {
      foreach($ns as $name => $url) {
	echo ' xmlns:' . $name . '="' . $url . '" ';
      }
    }
    echo '>' . "\n";
    echo '  <channel>' . "\n";

    // required elements
    //
    echo '    <title>' . $this->transformer->getTitle() . '</title>' . "\n";
    echo '    <description>' . $this->transformer->getDescription() . '</description>' . "\n";
    echo '    <link>' . $this->transformer->getLink() . '</link>' . "\n";
    echo '    <pubDate>' . $this->transformer->getPubDate() . '</pubDate>' . "\n";
    echo '    <lastBuildDate>' . $this->transformer->getLastBuildDate() . '</lastBuildDate>' . "\n";

    if($this->transformer->getManagingEditor()) {
      echo '    <managingEditor>$this->transformer->getManagingEditor()</managingEditor>' . "\n";
    }

    if($this->transformer->getWebMaster()) {
      echo '    <webMaster>$this->transformer->getWebMaster()</webMaster>' . "\n";
    }

    if($this->transformer->getTTL()) {
      echo '    <ttl>$this->transformer->getTTL()</ttl>' . "\n";
    }

    if($this->transformer->getRating()) {
      echo '    <rating>$this->transformer->getRating()</rating>' . "\n";
    }

    if($this->transformer->getImageURL()) {
      echo '    <image>' . "\n";
      echo '       <title>' . $this->transformer->getTitle()    . '</title>' . "\n";
      echo '       <url>'   . $this->transformer->getImageURL() . '</url>'   . "\n";
      echo '       <link>'  . $this->transformer->getLink() . '</link>'  . "\n";
      echo '       <description>' . $this->transformer->getDescription() . '</description>' . "\n";
      echo '    </image>' . "\n";
    }

    $items = $this->transformer->getItems();

    if(is_array($items)) {
      foreach($items as $item) {
	echo '    <item>' . "\n";

	if($item->title) {
	  echo '      <title>' . $item->title . '</title>' . "\n";
	}

	if($item->link) {
	  echo '      <link>' . $item->link . '</link>' . "\n";
	}

	if($item->author) {
	  echo '      <author>' . $item->author . '</author>' . "\n";
	}

	if($item->source) {
	  echo '      <source>' . $item->source . '</source>' . "\n";
	}

	if(count($item->customElements) > 0) {
	  foreach($item->customElements as $customElement) {
	    echo '      ' . $customElement . "\n";
	  }
	}

	if($item->enclosureURL) {
	  echo '      <enclosure url="'.$item->enclosureURL.'" ';
	  if($item->enclosureLength) {
	    echo 'length="'. $item->enclosureLength .'" ';
	  }
	  if($item->enclosureType) {
	    echo 'type="'. $item->enclosureType .'" ';
	  }
	  echo '/>' . "\n";
	}

	if($item->GUID) {
	  $GUIDIsPermaLink = $item->GUIDIsPermaLink ? 'true' : 'false';
	  echo '      <guid isPermaLink="' . $GUIDIsPermaLink . '">' . $item->GUID . '</guid>' . "\n";
	}

	$pubDate = $item->pubDate ? $item->pubDate : date('r');

	echo '      <pubDate>' . $pubDate . '</pubDate>' . "\n";

	if($item->description) {
	  echo '      <description>' ."\n";
	  echo $item->description . "\n";
	  echo '      </description>' . "\n";
	}

	echo '    </item>' . "\n";
      }
    }

    echo '  </channel>' . "\n";  
    echo '</rss>' . "\n";  

  }

}


?>

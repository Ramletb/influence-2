<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * AtomRenderer.php
 *
 * Contains the {@link AtomRenderer} class and the
 * {@link AtomTransformer} class.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage core
 */

/**
 * The Abstract AtomTransformer Class
 *
 *
 * @package SP5
 * @subpackage renderer
 */
class AtomTransformer extends Transformer {

  /**
   * Get Feed Title
   *
   * Gets the title for the Atom feed.
   * Uses $this->metaData['title'] by default.
   *
   * @access public 
   */  
  public function getTitle() {
    return $this->metaData['title'];
  }

}

/**
 * The AtomRenderer Class
 *
 *
 * @package SP5
 * @subpackage renderer
 */
class AtomRenderer extends TransformerRenderer {

  /**
   * Render
   *
   * Render Atom 1.0.
   *
   * @param string $view A {@link Transformer} view.
   * @access public 
   */
  public function render($view) {

    $this->loadTransformation($view);
    
    echo '<?xml version="1.0" encoding="utf-8"?>'."\n";
    echo '<feed xmlns="http://www.w3.org/2005/Atom">'."\n";
    
    echo '  <title>' . $this->transformer->getTitle() . '</title>'."\n"; 
    echo '  <subtitle>Insert witty or insightful remark here</subtitle>'."\n"; 
    echo '  <link href="http://example.org/"/>'."\n"; 
    echo '  <updated>2003-12-13T18:30:02Z</updated>'."\n"; 
    echo '  <author>'."\n"; 
    echo '    <name>John Doe</name>'."\n"; 
    echo '    <email>johndoe@example.com</email>'."\n"; 
    echo '  </author>'."\n"; 
    echo '  <id>urn:uuid:60a76c80-d399-11d9-b93C-0003939e0af6</id>'."\n"; 
    
    echo '  <entry>'."\n"; 
    echo '    <title>Atom-Powered Robots Run Amok</title>'."\n"; 
    echo '    <link href="http://example.org/2003/12/13/atom03"/>'."\n"; 
    echo '    <id>urn:uuid:1225c695-cfb8-4ebb-aaaa-80da344efa6a</id>'."\n"; 
    echo '    <updated>2003-12-13T18:30:02Z</updated>'."\n"; 
    echo '    <summary>Some text.</summary>'."\n"; 
    echo '  </entry>'."\n"; 
   
    echo '</feed>'."\n"; 
  }

  /**
   * Content Type
   *
   * @access public 
   */
  public function contentType() {
    return "content-type: application/atom+xml";
  }


}


?>

<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * WADLRenderer.php
 *
 * Contains the {@link WADLRenderer} class.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage core
 */

/**
 * The WADLRenderer Class
 *
 *
 * @package SP5
 * @subpackage renderer
 */
class WADLRenderer extends TransformerRenderer {

  /**
   * Render
   *
   * Render WADL.
   *
   * @access public 
   */
  public function render() {
    echo '<?xml version="1.0" encoding="utf-8"?>'."\n";

    echo '<application xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ';
    echo 'xmlns:xsd="http://www.w3.org/2001/XMLSchema" ';
    echo 'xsi:schemaLocation="http://research.sun.com/wadl/2006/10 wadl.xsd" ';
    echo 'xmlns="http://research.sun.com/wadl/2006/10">'."\n";

    $webServer = $this->configuration->environment['FRAMEWORK']['web']['server'];

    echo '  <resources base="http://'.$webServer. $this->configuration->environment['FRAMEWORK']['web']['path'] . '"> '."\n";

    foreach($this->data as $controllerName => $details) {

      echo '    <resource path="' . strtolower($controllerName) . '">'."\n";
      
      foreach($details['action'] as $actionMethod => $actionStatus) {
	echo '      <resource path="' . $actionMethod . '">'."\n";
	
	if(is_array( $this->data[strtolower($controllerName)]['actionMethod'][$actionMethod])) {
	  foreach($this->data[strtolower($controllerName)]['actionMethod'][$actionMethod] as $proto => $params) {
	    echo '        <method name="' . $proto . '">';
	    
	    foreach($params as $param => $paramDetails) {
	      echo '          <param name="' . $param . '" type="' . $paramDetails['type'] . '">';
	      echo '            <doc title="' . $param . '">' . $paramDetails['desc'] . '</doc>'."\n";
	      echo '          </param>';
	    }
	    
	    echo '        </method>';
	  }
	}
	
	echo '    </resource>'."\n";
      }
      
      echo '    </resource>'."\n";
    }

    echo '  </resources>'."\n";
    echo '</application>'."\n";
  }

  /**
   * Content Type
   *
   * @access public 
   */
  public function contentType() {
    return "content-type: text/xml";
  }

}


?>

<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * DumpRenderer.php
 *
 * Contains the {@link DumpRenderer} class.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage core
 */

/**
 * The DumpRenderer Class
 *
 *
 * @package SP5
 * @subpackage renderer
 */
class DumpRenderer extends PresentationRenderer {

  /**
   * Render
   *
   * Render a dump.
   *
   * @access public 
   */
  public function render($view=null) {
    echo $this->header();

    if($this->message) {
      echo '<h1>Message</h1>';
      echo '<p>' . $this->message . '</p>';
    }

    echo '<h1>Meta Data</h1>';
    echo '<pre>' . print_r($this->metaData,1) . '</pre>';

    echo '<h1>Data</h1>';
    echo '<pre>' . print_r($this->data,1) . '</pre>';

    echo $this->footer();
  }

  /**
   * Display an XHTML header
   *
   * @access private
   * @returns string html footer
   */
  private function header() {

    $yuiBase = 'http://ajax.googleapis.com/ajax/libs/yui/2.6.0/build/';

    $content = '';

    $content .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">'."\n";
    $content .= '<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="\'en\'">'."\n";
    $content .= '<head>'."\n";
    $content .= '<title>Dump</title>'."\n";
    $content .= '  <meta http-equiv="content-type" content="application/xhtml+xml; charset=\'utf-8\'" />'."\n";
    $content .= '</head>'."\n";
    $content .= '<body>'."\n";
    
    return $content;
  }


  /**
   * Display an XHTML footer
   *
   * @access private
   * @returns string footer html
   */
  private function footer() {
    $content = '';
    $content .= '</body>'."\n";
    $content .= '</html>'."\n";

    return $content;
  }

}

?>

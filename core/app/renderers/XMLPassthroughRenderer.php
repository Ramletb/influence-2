<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * XMLPassthroughRenderer.php
 *
 * Contains the {@link XMLPassthroughRenderer} class.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage core
 */

/**
 * The XMLPassthroughRenderer Class
 *
 *
 * @package SP5
 * @subpackage renderer
 */
class XMLPassthroughRenderer extends PresentationRenderer {

  /**
   * Render
   *
   * Render XML (Passthrough)
   *
   * @access public 
   */
  public function render() {
    header("content-type: text/xml");
    echo $this->data;
  }


}


?>

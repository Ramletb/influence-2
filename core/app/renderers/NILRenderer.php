<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * NILRenderer.php
 *
 * Contains the {@link NILRenderer} class.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage core
 */

/**
 * The NILRenderer Class
 *
 *
 * @package SP5
 * @subpackage renderer
 */
class NILRenderer extends PresentationRenderer {

  /**
   * Render 
   *
   * Render nothing.
   *
   * @access public 
   */
  public function render() {
  }

}

?>

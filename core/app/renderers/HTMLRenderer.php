<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * HTMLRenderer.php
 *
 * Contains the {@link HTMLRenderer} class.
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @package SP5
 * @subpackage core
 */

/**
 * The HTMLRenderer Class
 *
 *
 * @package SP5
 * @subpackage renderer
 */
class HTMLRenderer extends PresentationRenderer {

  /**
   * Render
   *
   * Render an HTML webpage.
   *
   * @access public 
   * @param string $view an optional view parameter.
   */
  public function render($view=null) {
    $this->loadView($view);
  }

}

?>

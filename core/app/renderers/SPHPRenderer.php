<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * SPHPRenderer.php
 * 
 * Contains the {@link SPHPRenderer} class.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage core
 */

/**
 * The SPHPRenderer Class
 *
 *
 * @package SP5
 * @subpackage renderer
 */
class SPHPRenderer extends SerializerRenderer {

  /**
   * Render
   *
   * Render serialized PHP.
   *
   * @access public 
   */
  public function render() {
    echo serialize($this->data);
    echo "\n";
  }

  /**
   * Content Type
   *
   * @access public 
   */
  public function contentType() {
    return "content-type: text/plain";
  }


}

?>

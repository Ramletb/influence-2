<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * CoreController.php
 *
 * Contains the {@link CoreController} class.
 *
 * @author Brian Hull <brisn.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage controllers
 */

/**
 * The CoreController Class
 *
 * @package SP5
 * @subpackage controllers
 */
class CoreController extends Controller {

  /**
   * preAction
   *
   */
  public function preAction() {

    $errorCode = array();
    $errorCode['404'] = 'File Not Found';

    if($errorCode[$this->actionName]) {

      $this->logger->error($this->actionName . " ACTION CALLED FOR: [". $_SERVER['REQUEST_URI'] ."]");
      $data = new StdClass;
      $data->errorCode = $this->actionName;
      $data->errorMessage = $errorCode[$this->actionName];
      $this->setData($data);

      return 'error';
    }
  }

  /**
   * error
   *
   */
  public function error() {
    $this->render('core/error');
  }

}

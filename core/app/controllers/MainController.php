<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * MainController.php
 *
 * Contains the {@link MainController} class.
 *
 * @author Brian Hull <brisn.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @package SP5
 * @subpackage controllers
 */

/**
 * The MainController Class
 *
 * @package SP5
 * @subpackage controllers
 */
class MainController extends Controller {

  /**
   * preAction
   *
   * This example administration system should only be used in
   * development mode.
   *
   * @access public
   */
  public function preAction() {
    parent::preAction();

    if(!$this->configuration->environment['FRAMEWORK']['development']['mode']) {
      return 'devOnly';
    }

  }

  /**
   * Development Only
   *
   * @access public
   */
  public function devOnly() {
    $this->addMetaData('webPath', $this->configuration->environment['FRAMEWORK']['web']['path']);
    $this->render('core/default/production');
  }

  /**
   * Index
   *
   * Default View
   *
   * @access public
   */
  public function web_index() {

    $now       = date('l \t\h\e jS \of F Y \a\t h:i:s A T \(O \G\M\T \/ e)');
    $fwPackage = $this->configuration->framework['CORE']['framework']['package'];

    $this->addMetaData('title', $fwPackage . ' Default Controller');
    $this->addMetaData('webPath', $this->configuration->environment['FRAMEWORK']['web']['path']);


    $data = array();
    $this->setData($data);

    $this->render('core/default/index');
  }

}


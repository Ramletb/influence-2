<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * UtilsController.php 
 *
 * Contains the {@link UtilsController} class.
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage controllers
 */

/**
 * The UtilsController Class
 *
 * @package SP5
 * @subpackage controllers
 */
class UtilsController extends Controller {

  /**
   * web default action
   *
   * @access public
   */
  public function web_index() {
  }

  /**
   * cli default action
   *
   * @access public
   */
  public function cli_help() {    

    // TODO: Build this with reflection...
    //

    $help = <<<EOD

The cli.php script wraps the CliFramework class.

   ./cli.php --controller CONTROLLER --action ACTION --arg1 ARG1 --arg2 ARG2 [etc...]

CLI Developer Commands

   If the default CLI controller is not set to Utils, you will need to add the argument 
   "--controller utils" to the following calls.

   Make a Controller
   ./cli.php --action make_controller --name example --package PACKAGE --header ./doc/PACKAGE.txt

   Make a View
   ./cli.php --action make_view --name index --type html --for example --package PACKAGE --header ./doc/PACKAGE.txt

   Make a Plugin
   ./cli.php --action make_plugin --name example --package PACKAGE --header ./doc/PACKAGE.txt

   Make a Bundle
   ./cli.php --action make_bundle --name example --package PACKAGE --header ./doc/PACKAGE.txt

   Build Models
   ./cli.php --action build_models --package PACKAGE --header ./docs/PACKAGE.txt

   Build Documentation
   ./cli.php --action build_docs    

CLI Framework Administration

   Build Headers 
   (WARNING: If you don't know what this is don't use it)
   ./cli.php --action build_headers

EOD;

    $this->setData($help);

    $this->render();
  }

  /**
   * cli build models
   *
   * Example:
   * ./cli.php --controller utils --action build_models --package TEST --header ./doc/SAMPLE.header
   *
   * @access public
   * @param PayloadPkg $package OPT string the configuration package in custom_environment.ini ([EXAMPLE])
   * @param PayloadPkg $header OPT string path to a text header to used for generated files. 
   */
  public function cli_build_models(PayloadPkg $package,
				   PayloadPkg $header,
				   PayloadPkg $core) {

    $pkg = $this->configuration->environment[$package->getString()];

    $this->logger->debug("Looking for [" . $package->getString() . "] Models configuration...");

    $class = $pkg['db']['class'] ? $pkg['db']['class'] : $pkg['service']['class'];
    $class = $class ? $class : $pkg['od']['class'];

    $this->logger->debug("Building [" . $class . "] Models...");

    $class .= 'ModelBuilder';
    
    $this->modelBuilder = $this->pluginFactory->load($class);

    $this->modelBuilder->setPackage($package->getString());
    $this->modelBuilder->setHeader($header->getString());
    $this->modelBuilder->setCore($core->getInt(0));
    $this->modelBuilder->setOutputCallback($this,'renderData');
    
    $status = $this->modelBuilder->build();

    $this->setData($status);

    $this->render();
  }

  /**
   * cli make bundle
   *
   * Example:
   * ./cli.php --controller utils --action make_bundle --name Example --package PACKAGE --header ./doc/SAMPLE.header
   *
   * @access public
   * @param PayloadPkg $name OPT string controller name
   * @param PayloadPkg $package OPT string the configuration package in custom_environment.ini ([EXAMPLE])
   * @param PayloadPkg $header OPT string path to a text header to used for generated files. 
   */
  public function cli_make_bundle(PayloadPkg $name,
				  PayloadPkg $package,
				  PayloadPkg $header) {
    

    $this->appBuilder = $this->pluginFactory->load('AppBuilder');
    $this->appBuilder->setOutputCallback($this,'renderData');
    
    $status = $this->appBuilder->make(ucfirst($name), $package, $header, 'bundle', C_PATH_BUNDLES, "Bundle");

    $this->setData($status);
    
  }

  /**
   * cli make controller
   *
   * Example:
   * ./cli.php --controller utils --action make_controller --name Example --package PACKAGE --header ./doc/SAMPLE.header
   *
   * @access public
   * @param PayloadPkg $name OPT string controller name
   * @param PayloadPkg $package OPT string the configuration package in custom_environment.ini ([EXAMPLE])
   * @param PayloadPkg $header OPT string path to a text header to used for generated files. 
   */
  public function cli_make_controller(PayloadPkg $name,
				      PayloadPkg $package,
				      PayloadPkg $header) {
    

    $this->appBuilder = $this->pluginFactory->load('AppBuilder');
    $this->appBuilder->setOutputCallback($this,'renderData');
    
    $status = $this->appBuilder->make(ucfirst($name), $package, $header, 'controller', C_PATH_CONTROLLERS, "Controller");

    $this->setData($status);
    
  }

  /**
   * cli make view
   *
   * Example:
   * ./cli.php --controller utils --action make_view --type html --for example --name index --package PACKAGE --header ./doc/SAMPLE.header
   *
   * @access public
   * @param PayloadPkg $name OPT string controller name
   * @param PayloadPkg $type OPT string view type
   * @param PayloadPkg $for OPT string controller name
   * @param PayloadPkg $package OPT string the configuration package in custom_environment.ini ([EXAMPLE])
   * @param PayloadPkg $header OPT string path to a text header to used for generated files. 
   */
  public function cli_make_view(PayloadPkg $name,
				PayloadPkg $type,
				PayloadPkg $for,
				PayloadPkg $package,
				PayloadPkg $header) {
    

    $this->appBuilder = $this->pluginFactory->load('AppBuilder');
    $this->appBuilder->setOutputCallback($this,'renderData');
    

    $for = strtolower($for);
    $name = strtolower($name);
    $type = strtolower($type);

    $path = C_PATH_VIEWS . $for . '/' . $type . '/';

    $status = $this->appBuilder->make($name, $package, $header, 'view_'. $type, $path);

    $this->setData($status);
  }

  /**
   * cli make plugin
   *
   * Example:
   * ./cli.php --controller utils --action make_plugin --name Example --package PACKAGE --header ./doc/SAMPLE.header
   *
   * @access public
   * @param PayloadPkg $name OPT string plugin name
   * @param PayloadPkg $package OPT string used for documentation
   * @param PayloadPkg $header OPT string path to a text header to used for generated files. 
   */
  public function cli_make_plugin(PayloadPkg $name,
				  PayloadPkg $package,
				  PayloadPkg $header) {
    

    $this->appBuilder = $this->pluginFactory->load('AppBuilder');
    $this->appBuilder->setOutputCallback($this,'renderData');
    
    $status = $this->appBuilder->make(ucfirst($name), $package, $header, 'plugin', C_PATH_PLUGINS, "Plugin");

    $this->setData($status);
  }

  /**
   * cli build documentation
   *
   * Example:
   * ./cli.php --controller utils --action build_docs
   *
   * @access public
   * @param PayloadPkg $package OPT string the default documentation package
   */
  public function cli_build_docs(PayloadPkg $package) {
    $this->fdocPlugin = $this->pluginFactory->load('DocBuilder');
    $this->fdocPlugin->setOutputCallback($this,'renderData');
    $this->fdocPlugin->generate($package->getString($this->configuration->framework['CORE']['framework']['package']));
  }

  /**
   * cli build headers
   *
   * Example:
   * ./cli.php --controller utils --action build_headers
   *
   * @access public
   * @param PayloadPkg $package OPT string the default documentation package
   */
  public function cli_build_headers(PayloadPkg $package) {
    $this->headerPlugin = $this->pluginFactory->load('Header');
    $this->headerPlugin->setOutputCallback($this,'renderData');
    $this->headerPlugin->generate($package->getString($this->configuration->framework['CORE']['framework']['package']));
  }

}


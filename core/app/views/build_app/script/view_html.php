<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * view_html.php
 *
 * View for rendering a Controller.
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @package SP5
 * @subpackage views
 */

echo "<" . "?php\n";
echo $header;
?>

/**
 * <?php echo $data->name; ?>.php
 *
 *
 * @package <?php echo $data->packageName . "\n"; ?>
 * @subpackage views
 */
<?php echo "?" . ">\n"; ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <title>Framework</title>
  <link href='http://fonts.googleapis.com/css?family=Terminal+Dosis+Light' rel='stylesheet' type='text/css'>
  <style>body { font-family: 'Terminal Dosis Light', arial, sans-serif; } h2 { margin: 0 }</style>
</head>
<body>
  <h2>HTML View</h2>
</body>
</html>


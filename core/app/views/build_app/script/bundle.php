<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * bundle.php
 *
 * View for rendering a Bundle.
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @package SP5
 * @subpackage views
 */

echo "<" . "?php\n";
echo $header;
?>

/** 
 * <?php echo $data->name; ?>Bundle.php
 * 
 * Contains the {@link <?php echo $data->name; ?>Bundle} class.
 * 
 * @package <?php echo $data->packageName; ?> 
 * @subpackage bundles
 */ 

/**
 * <?php echo $data->name; ?>Plugin class
 *
 * @package <?php echo $data->packageName; ?> 
 * @subpackage bundles
 */
class <?php echo $data->name; ?>Bundle extends Bundle {

  /**
   * Set Bundle
   *
   * @access public
   * @param PayloadPkg $arg1 GET string arg1
   * @param PayloadPkg $arg2 GET string arg2
   */
  public function setBundle(PayloadPkg $arg1,
			    PayloadPkg $arg2) {

  }
  
}

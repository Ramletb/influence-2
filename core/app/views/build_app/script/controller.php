<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * controller.php
 *
 * View for rendering a Controller.
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @package SP5
 * @subpackage views
 */

echo "<" . "?php\n";
echo $header;
?>

/** 
 * <?php echo $data->name; ?>Controller.php
 * 
 * Contains the {@link <?php echo $data->name; ?>Controller} class.
 * 
 * @package <?php echo $data->packageName; ?> 
 * @subpackage controllers
 */ 

/**
 * <?php echo $data->name; ?>Controller class
 *
 * @package <?php echo $data->packageName; ?> 
 * @subpackage controllers
 */
class <?php echo $data->name; ?>Controller extends Controller {

  /**
   * Pre Action
   *
   * preAction is called by the Controller before
   * dispatching to an action.
   *
   * @access public
   */
  public function preAction() {
    parent::preAction();
  }

  /**
   * Index
   *
   */
  public function web_index() {

    $this->render();
  }

  
}

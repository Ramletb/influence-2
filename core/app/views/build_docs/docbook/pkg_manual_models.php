<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * Models Documentation
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage views
 */
?>
<refentry id="{@id models}">
  <refnamediv>
    <refname>Models</refname>
  </refnamediv>
  <refsynopsisdiv>
    <author>
      Craig Johnston
      <authorblurb>
      {@link mailto:craig.johnston@sudjam.com craig.johnston@sudjam.com}
      </authorblurb>
    </author>
    <author>
      Brian Hull
      <authorblurb>
      {@link mailto:brian.hull@sudjam.com brian.hull@sudjam.com}
      </authorblurb>
    </author>
  </refsynopsisdiv>
  {@toc}

  <refsect1 id="{@id models_overview}">
    <title>Overview</title>


<para>
  Models encapsulate external data and data sources and provide a repository for code enforcing data integrity.   Models are loaded by a ModelFactory, and at load time, instantiate a raw service object of the class specified in the configuration and the given model subclass.  Models currently are subclassed into DatabaseModels, RestModels and ActionModels.  A DatabaseModel abstracts a database table and wraps a raw service which is a Database object.  A RestModel abstracts service calls to an external REST service and wraps a raw service of class Service. An ActionModel abstracts web service calls to other <?php echo $package; ?> frameworks.  All subclasses extend a Structure class which defines various properties of the data in the model such as type and enumeration values.  Models may be automatically generated using a cli.php call if the targeted external service is a database or supports discovery. The remainder of this section will focus exclusively on DatabaseModels.
</para>

<para>
To generate a set of DatabaseModels, one must first define the database connection information in a configuration file (typically custom_environment.ini, since database connection info almost always changes based on the environment.)  This is also the configuration information consumed and required by the Database object embedded in the DatabaseModel.
</para>

<para>
Below is an example configuration:
</para>

<programlisting role="ini">
<![CDATA[
[TEST]

db.class           = "MySQL"
db.username        = "test_user"
db.password        = "test_pass"
db.database        = "test_database"
db.host            = "localhost"
db.format.charset  = "utf-8"
db.format.datetime = "Y-m-d H:i:s"
]]>                                                                                                                                                   
</programlisting> 

<para>
In the above configuration, "[TEST]" is referred to as the package, 
which is one of the arguments required to generate the models.  
The db.class specifies what subclass of Database the model should instantiate and db.username, db.password, db.database and db.host contain the connection information.  The information in db.format specifies the character set and default datetime format for the database.  If you use the values shown above, db.format is optional.
</para>

<para>
To generate the models, cd to the utils directory.  
If you type "ls doc" you will see a .header file.  
The path to this file is the second argument required to generate models.
</para>

<para>
A sample generation is shown below:
</para>

<screen>
./cli.php --action build_models --package TEST --header ./doc/EXAMPLE.header
</screen>

<para>
You will see a Model and Structure generated for every table in the targeted database.  Note that this command can be run every time the database is changed, and the Model files will not be generated again if they already exist, but the Structures will always be generated.  Models are intended to be fleshed out and modified, but Structures should never be hand edited.
</para>

<para>
If you cd to app/models, you will now see a directory named TEST, which contains the models and a subdirectory named generated which contains the structures.  The models are named TableNameModel.php  (or TEST_TableNameModel.php if db.prepend = true) and the structures are named TableNameStructure.php  (or TEST_TableNameStructure.php if db.prepend = true). 
</para>

<para>
The following example returns a configured model for the package TEST and database table TestName (and can be used in any subclass of App, such as a Controller, Model, or Plugin):
</para>

<programlisting role="php">
<![CDATA[
$test_model = $this->modelFactory->load("ModelName", "TEST");
]]>
</programlisting> 

<para>
The model is now ready to be used.  By convention, the database object encapsulated by the model provides two calls to support raw SQL: query and execute. The database object is protected, to force all raw SQL to be specified within the model itself.

Below are examples as to how to use these methods (within a model method):
</para>

<programlisting role="php">
<![CDATA[

  /**
   * A sample DatabaseModel method
   *
   * Demonstrates the raw db execute and query calls.
   *
   * @access public
   */
  public function exampleInsert() {

    $e_query = "INSERT INTO " . $this->TABLE_NAME . " (id, name) VALUES (1, 'foo');";

    $new_id = $this->db->execute($e_query);

    $a_query = "UPDATE " . $this->TABLE_NAME . " SET name='bar' WHERE id=1;";

    $rows_affected = $this->db->execute($a_query);

    $rows = $this->db->query("SELECT * FROM " . $this->TABLE_NAME . ";");
  }

]]>
</programlisting> 

<para>
Of course, you will be inserting data using variables more typically, and to prevent SQL injection, you will need to prepare your data, using $this->db->prepareNumber($var) and $this->db->prepareString($var) for each variable you use in your SQL statement.  You should always do this since data coming into the controller via the web is considered 'dirty' even if magic quotes are on.  Note that the prepareString and prepareNumber calls are to the embedded Database object and not to the model.  This is because the Model does not know how to prepare the data for the database embedded within it.
</para>

<programlisting role="php">
<![CDATA[

  /**
   * A sample DatabaseModel method
   *
   * Demonstrates the raw db execute and query calls.
   *
   * @access public
   * @param integer $id
   * @param string $name
   * @returns string GUID
 
   */
  public function exampleRawQuery($id, $name) {

    $db_id = $this->db->prepareNumber($id);

    $db_name = $this->db->prepareString($name);

    $query = "SELECT * FROM " . $this->TABLE_NAME . " WHERE id=$db_id AND name=$db_name;";

    $rows = $this->db->query($query);
  }

]]>
</programlisting> 

<para>
To relieve the tedium of preparing variables for simple queries, the Model provides a set of methods: smartInsert,  smartSelect, smartSelectAll, smartSelectOne, smartUpdate and smartDelete.  To use these methods, simply pass in a hash of arguments (some require more than one hash) where the keys match the column names.  The convenience function will prepare the variables, generate the SQL statement and execute the statement, returning the data, new id, or rows affected as appropriate.
</para>

<programlisting role="php">
<![CDATA[

  /**
   * A sample DatabaseModel method
   *
   * Demonstrates the smart helper methods.
   *
   * @access public
   * @param array $argArray
   * @param string $name
   * @param string $address
   * @access public
   * @returns string GUID
   */
  public function exampleDbCalls($argArray, $name, $address) {

    // for piecemeal fields.
    $user = array('name' => $name, 'address' => $address);

    $new_id = $this->smartInsert($user); 

    // when a suitable array of name-value pairs is passed in.
    $new_id = $this->smartInsert($argArray); 

    // select all columns where name=$name
    $rows = $this->smartSelect(null, array('name' => $name)); 

    // select only address column where name=$name
    $rows = $this->smartSelect(array('address'), array('name' => $name)); 

    // select a single row of all columns where name=$name
    $row = $this->smartSelectOne(null, array('name' => $name)); 

    // select all rows with all columns where name=$name
    $row = $this->smartSelectAll(null, array('name' => $name)); 

    // update address where name=$name
    $args = array('address' => '123 Elm'), array('name' => $name);
    $affected_rows = $this->smartUpdate($args); 

    // delete where name=$name
    $affected_rows = $this->smartDelete(array('name' => $name)); 

  }

]]>
</programlisting>

<para>
By and large, it is good practice to move as much code involving 
the database data into the model as possible to keep the action 
as clean and understandable as possible.  As mentioned above, raw SQL calls <important>must</important> be invoked from within a method in a model.  Code that does not "fit" 
in a model would be moved to a plugin.  Models have the same factories 
as a controller, and can load other models or even plugins, as necessary.  
</para>

  </refsect1>

</refentry>

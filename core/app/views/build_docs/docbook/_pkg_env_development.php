<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * Development Environment Documentation
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage views
 */
?>
<refentry id="{@id developmentenv}">
  <refnamediv>
    <refname>Development</refname>
  </refnamediv>
  <refsynopsisdiv>
    <author>
      Craig Johnston
      <authorblurb>
      {@link mailto:craig.johnston@sudjam.com craig.johnston@sudjam.com}
      </authorblurb>
    </author>
  </refsynopsisdiv>
  {@toc}

  <refsect1 id="{@id developmentenv_overview}">
    <title>Overview</title>
<para>
</para>

<para>
  The following diagram describes:
</para>
<para>
  <graphic src="../../images/core/development_env.png"></graphic>
</para>

</refsect1>

</refentry>
<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * Client Side Data Tutorial
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage views
 */
?>
<refentry id="{@id tutorial_client_side_data}">
  <refnamediv>
    <refname>Client Side Data</refname>
    <refpurpose></refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <author>
      Craig Johnston
      <authorblurb>
      {@link mailto:craig.johnston@sudjam.com craig.johnston@sudjam.com}
      </authorblurb>
    </author>
    <author>
      Brian Hull
      <authorblurb>
      {@link mailto:brian.hull@sudjam.com brian.hull@sudjam.com}
      </authorblurb>
    </author>
  </refsynopsisdiv>

  {@toc}

    <refsect1 id="{@id tutorial_client_side_data_keyconcepts}">
      <title>Key Concepts</title>
      <para>
Traditionally Web Application store Semi-persistent client side data in Cookies 
or Sessions. Due to the problematic nature of scaling and replication of
sessions, the WebFramework favors cookies. However, while it is possible to configure 
the Framework to utilize php sessions, the topic is beyond the scope of this tutorial.
    </para>
    <para>
The CLIFramework does not have the advantage of Cookie storage, 
yet this problem is solved with a "dot file" in the user's home directory. 
In either the case of the WebFramework or CLIFramework, the storage of 
Client Data has been abstracted to the ClientData object. 
    </para>
    <para>
    The ClientData object, like Payload, stores it<?php // ?>s data in Packages with the data 
type ClientDataPkg. Both ClientDataPkg and PayloadPkg inherit from DataPackage 
and provide polymorphic, transparent access to their data.
    </para>
    </refsect1>
    
    <refsect1 id="{@id tutorial_client_side_data_set_cliendata}">
      <title>Set Client Data</title>
      <para>
In this example we will store the current time and our incoming Payload 
parameter "user" in the ClientData object.
      </para>
      <para>
    Below we add code (lines 9-16) to our "web_world" action:
      </para>
      <para>
      <programlisting role="php">
	  <![CDATA[
/**
 * Web world action
 *
 * @access public
 * @param PayloadPkg $user GET string user the username
 */
public function web_world(PayloadPkg $user) {

  // get the current date and time
  //
  $now = date('l jS \of F Y h:i:s A');

  // set ClientData
  //
  $this->setClientData('then', $now);
  $this->setClientData('user_data', $user->getString());

  $this->addMetaData("title", "Hello User Page");

  $data = new stdClass();
  $data->user = $user->getString();
  $data->content = "Hello " . $data->user;

  $this->setData($data);

  $this->render();
}
]]>
    </programlisting>
    </para> 
    <para>
The above code will store two key/value pairs in the ClientData 
    object: "then" the current time and "user_data", the "user" 
parameter passed in from Payload. The ClientData object is then 
serialized, encrypted and stored automatically. ClientData can 
be considered secure. By default the Framework uses Blowfish 
encryption to secure ClientData.
    </para>
    </refsect1>
    <refsect1 id="{@id tutorial_client_side_data_get_cliendata}">
    <title>Get Client Data</title>
    <para>
In order to get Client Data from the ClientData object we need to 
tell the Framework what data we are expecting. For this we add two 
more parameters to our "web_world" Action: ClientDataPkg $then and 
    ClientDataPkg $user_data (on lines 10 and 11).
    </para>
    <para>
    <programlisting role="php">
    <![CDATA[
/**
 * Web world action
 *
 * @access public
 * @param PayloadPkg    $user      GET string username
 * @param ClientDataPkg $then      GET string previous value of payload user
 * @param ClientDataPkg $user_data GET string previous "now"
 */
public function web_world(PayloadPkg $user, 
			  ClientDataPkg $then, 
			  ClientDataPkg $user_data) {

  // get the current date and time
  //
  $now = date('l jS \of F Y h:i:s A');
    
  // set ClientData
  //
  $this->setClientData('then', $now);
  $this->setClientData('user_data', $user->getString());
  
  $this->addMetaData("title", "Hello User Page");
  
  $data = new stdClass();
  $data->user      = $user->getString();
  $data->content   = "Hello " . $data->user;
  $data->user_data = $user_data->getString();
  $data->then      = $then->getString();
  $data->now       = $now;
  
  $this->setData($data);
  
  $this->render();
}

]]>
    </programlisting>
    </para> 
    <para>
To see the result of our new ClientDataPkg parameters we added 
three new attributes to our data object before passing it to 
$this->setData (see lines 27-29 in the example above).
    </para>
    <para>
We use the DumpRenderer to view the data below:
    </para>
    <para>
    <browserurl>
	http://<?php echo $webserver; ?>/hello/world/user/Brian.dump
    </browserurl>
    </para>
    <para>
    <browserscreen>
    <graphic src="../../images/core/client_data_dump.png"></graphic>
    </browserscreen>
    </para>
    </refsect1>
    <refsect1 id="{@id tutorial_client_side_data_cli}">
    <title>Command Line Interface</title>
    <para>
If we attempt to call our command line "cli_world" we will end up with 
an incorrect number of arguments supplied to "web_world". Adjust your 
    Action "cli_world" to reflect the following:
    </para>
    <para>
    <programlisting role="php">
    <![CDATA[
/**
 * CLI world action
 *
 * @access public
 * @param PayloadPkg    $user      CLI    string username
 * @param ClientDataPkg $then      CLIENT string previous value of payload user
 * @param ClientDataPkg $user_data CLIENT string previous "now"
 */
public function cli_world(PayloadPkg $user,
			  ClientDataPkg $then, 
			  ClientDataPkg $user_data) {

  $this->web_world($user,$then,$user_data);
}
]]>
    </programlisting>
    </para> 
    <para>
Executing the command shown below will now produce an acceptable result.
    </para>
    <para>
    <screen>
    $ cd ./utils/; ./cli.php --controller hello --action world --user Craig --format txt
    </screen>
    </para>
    <para>
    <screen>
stdClass Object
(
    [user] => Craig
    [content] => Hello Craig
    [user_data] => Brian
    [then] => Saturday 20th of June 2009 10:29:44 PM
    [now] => Saturday 20th of June 2009 10:29:54 PM
)
    </screen>
    </para>

    </refsect1>
    
</refentry>


<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * Controllers Documentation
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage views
 */
?>
<refentry id="{@id controllers}">
  <refnamediv>
    <refname>Controllers</refname>
  </refnamediv>
  <refsynopsisdiv>
    <author>
      Craig Johnston
      <authorblurb>
      {@link mailto:craig.johnston@sudjam.com craig.johnston@sudjam.com}
      </authorblurb>
    </author>
    <author>
      Brian Hull
      <authorblurb>
      {@link mailto:brian.hull@sudjam.com brian.hull@sudjam.com}
      </authorblurb>
    </author>
  </refsynopsisdiv>
  {@toc}

  <refsect1 id="{@id controllers_overview}">
    <title>Overview</title>
<para>
Controllers are classes that hold special methods known as Actions. Actions are the entry points into application logic. The framework instantiates a Controller and invokes an Action method. All outside interaction with an application logic begins with the Action. 
</para>
<para>
The first responsibility for an Action is to provide an entry point to the application. The second responsibility to collect and set data and meta data used for rendering a response. 
</para>
<para>
It is important to note that Controllers and their Actions are not intended to contain application logic. The majority of application logic should exist in Plugins and Models. An Action is intended to be implemented as a simple thin layer between the core framework, Plugins and Renderers. A typical interaction with the framework would result in an Action loading an appropriate Plugin, passing required parameters from the Payload or ClientData and setting any returned data to be used by a Renderer.
</para>
<para>
Controllers are pre-loaded with a Renderer class object. The four most important methods inherited from the core Controller are addData(), addMetaData(), addClientData() and render(). All three methods invoke the same method on the loaded Renderer object. The action does not need to be aware of what type of Renderer is currently loaded.
</para>
<para>
Building Controllers for the framework involves writing a php class to extend the base class Controller found in core. The Controller class must be contained in a file ending with Controller.php (SomethingController.php). Controllers begin with an uppercase character and contain no other upper case charters. Controllers are typically named with a noun UserController, AdminController, NewsController, etc...
</para>
<para>
The core Controller inherits the Core class. The Core class provides Controllers with the protected attributes $this->configuration, (a Configuration class object) and $this->logger (a Logger class object.)
</para>
<para>
The core Controller also inherits from the App class, providing it with the protected attributes $this->pluginFactory (for loading Plugin class object,) and $this->modelFactory (for loading Model class objects.)
</para>
<para>
The core Controller class contains the protected methods preAction(), called before an Action and postAction() called immediately after. Custom Controllers may override preAction() and postAction() methods when necessary. 
</para>

  <refsect2 id="{@id controllers.example}">
    <title>Example Controller</title>
<para>
An example Controller (<important>app/controllers/AccountController.php</important>):
</para>
<programlisting role="php">
<![CDATA[

 /*
 / Example Project
 /
 / $LastChangedBy: CraigJohnston $
 / $LastChangedDate: 2010-07-14 12:20:21 -0700 (Wed, 14 Jul 2010) $
 / $Rev: 2 $
 /
*/

/**
 * AccountController.php
 *
 * Contains the {@link AccountController} class.
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage controllers
 */

/**
 * The AccountController Class
 *
 * @package SP5
 * @subpackage controllers
 */
class AccountController extends Controller {

  /**
   * Get User
   *
   * @access public
   * @param PayloadPkg $where   GET string where hash
   * @param PayloadPkg $order   GET string order hash
   * @param PayloadPkg $page    GET int    page number
   * @param PayloadPkg $perPage GET int    results per page
   */
  public function web_getUser(PayloadPkg $where,
			      PayloadPkg $order,
			      PayloadPkg $page,
			      PayloadPkg $perPage) {

    $userModel = $this->modelFactory->load("User", "EXAMPLE");

    $userData = $userModel->getAllPaginated($page->getInt(1),
                                            $perPage->getInt(1),
                                            $where->getHash(),
                                            $order->getHash());     

    $this->setData($userData);

    $this->render('account/user');
  }
}
]]>
</programlisting> 
  </refsect2>


  </refsect1>

</refentry>
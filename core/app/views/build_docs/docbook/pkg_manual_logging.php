<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * Logging Documentation
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage views
 */
?>
<refentry id="{@id logging}">
  <refnamediv>
    <refname>Logging</refname>
  </refnamediv>
  <refsynopsisdiv>
    <author>
      Craig Johnston
      <authorblurb>
      {@link mailto:craig.johnston@sudjam.com craig.johnston@sudjam.com}
      </authorblurb>
    </author>
    <author>
      Brian Hull
      <authorblurb>
      {@link mailto:brian.hull@sudjam.com brian.hull@sudjam.com}
      </authorblurb>
    </author>
  </refsynopsisdiv>
  {@toc}

  <refsect1 id="{@id logging_overview}">
    <title>Overview</title>
<para>
Logging is an essential function of development debugging and live system monitoring. 
The framework's {@link Logger Logger Class} is instantiated in the {@link Core Core Class}, making the instance variable $this->logger available to all derived classes.
</para>

<para>
There are five levels of logging provided: 0 Trace, 1 Debug, 2 Info, 3 Error, 4 Fatal. Development systems are typically configured to produce log output for level 0 and up. Live systems are typically configured to product log output for levels 2 and up.
</para>

<para>
The Trace level is heavily used by the framework itself and useful to a developer as an ongoing live stack trace.
</para>

<para>
Developers use the Debug to output message and state information useful during development.
</para>

<para>
Info and Error levels should be used sparingly as they are often set to output in live mode. The Info level is indented for information useful to system administrators and others monitoring a live system. Long Query executions or warnings are appropriate for the Info level.
</para>

<para>
The Error level is intended for notification of non fatal yet important errors. Error level (3 and up) is nearly always configured to log out on live systems and should be of particular interest to system administrators. For example, application logic may require that a file be written out to the file system, yet permission may not be set correctly. The application would notify the user that an error has occurred as well as output more relevant information about the error in the error log, then proceed with an alternative scenario.
</para>

<para>
The final logging level is Fatal. The Fatal level should be used before a die statement. Fatal logging is useful for applications that encounter a state when the only appropriate action is to cease execution.
</para>

<para>
Runtime/compiletime PHP fatal errors are not able to be caught by logger since they may have already put the system in an unrecoverable state. PHP errors are directed to a separate log file.
</para>

<para>
The following php code give various examples using the logger:
</para>

<programlisting role="php">                                                                                                                               
<![CDATA[
$this->logger->trace("Loaded some method...");

$this->logger->debug("GOT HERE");

$this->logger->debug("Some data: ".print_r($data,true));

$this->logger->info("WARNING: This took too long");

$this->logger->error("ERROR: Got to a bad state!");

$this->logger->fatal("FATAL: Got to a very bad state and going to die!");

]]>                                                                                                                                                   
</programlisting> 

<para>
Log file are stored in the ./logs/ directory. A useful technique for viewing logs in real time is to issue the following unix command.
</para>
<screen>
tail -f  ./logs/*
</screen>
<para>
The following diagram describes the various columns of log output:
</para>
    <para>
    <graphic src="../../images/core/log.png"></graphic>
    </para>

  </refsect1>



</refentry>
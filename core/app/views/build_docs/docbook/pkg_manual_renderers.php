<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * Renderers Documentation
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage views
 */
?>
<refentry id="{@id renderers}">
  <refnamediv>
    <refname>Renderers</refname>
  </refnamediv>
  <refsynopsisdiv>
    <author>
      Craig Johnston
      <authorblurb>
      {@link mailto:craig.johnston@sudjam.com craig.johnston@sudjam.com}
      </authorblurb>
    </author>
    <author>
      Brian Hull
      <authorblurb>
      {@link mailto:brian.hull@sudjam.com brian.hull@sudjam.com}
      </authorblurb>
    </author>
  </refsynopsisdiv>
  {@toc}

  <refsect1 id="{@id renderer_overview}">
    <title>Overview</title>
<para>
Renderers are responsible for output from the framework.
</para>
<para>
The framework maps Renderers to formats specified in the Configuration. A format is represented as a file extension on the query string for the web framework. The cli framework gets the format from the "--format" command line option. A default format is mapped for both cli and web frameworks to use when no format is specified.
</para>
<para>
After the framework instantiates a Controller it calls the method setRenderer() and passes the name of the Renderer to be used by the controller. It should never be necessary to call setRenderer() explicitly from within a Controller, the framework handles this in a polymorphic manner through the use of specified format and Configuration mappings. After the Renderer is set, the Controller can then call setData(), setMetaData(), setClientData() and render() on the Renderer.
</para>
    <para>
    <graphic src="../../images/core/renderMapping.png"></graphic>
    </para>
<para>
Renderers may optionally use Views to serialize their data and meta data. By default a renderer is set to look in a directory named after the Controller making the render call. 
</para>
<para>
The base directory for storing views is ./app/views. From the base directory views are typically organized by Controller name, Renderer name and default view name (index). An example XHTML Renderer view for the Controller Test would be located in ./app/views/test/xhtml/index.php. The Controller directory and View name can be overridden with an optional parameter in the render method. The call $this->render('admin/welcome') would suggest that the XHMLRenderer should use the view ./views/admin/xhtml/welcome.php. Renderers that do not use views would ignore this directive.
</para>
<para>
A Renderers output can be buffered and assigned to a variable using renderReturn(). It is not uncommon to use the output of one renderer as data for another, storage in a database or ClientData. Controllers pass $this->renderReturn() directly to the loaded Renderer. 
</para>
<para>
There are three types of Renderers. Presentation, Transformer and Serializer.
</para>

  <refsect2 id="{@id renderers.presentation}">
    <title>Presentation</title>

    <para>
    <graphic src="../../images/core/presentation.png"></graphic>
    </para>

  </refsect2>

  <refsect2 id="{@id renderers.transformer}">
    <title>Transformer</title>

    <para>
    <graphic src="../../images/core/transformer.png"></graphic>
    </para>

  </refsect2>

  <refsect2 id="{@id renderers.serializer}">
    <title>Serializer</title>

    <para>
Serializer...
    </para>

  </refsect2>

  </refsect1>

</refentry>
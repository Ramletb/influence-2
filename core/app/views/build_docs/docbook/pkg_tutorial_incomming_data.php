<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * Incomming Data Tutorial
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage views
 */
?>
<refentry id="{@id tutorial_incomming_data}">
  <refnamediv>
    <refname>Incoming Data</refname>
    <refpurpose></refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <author>
      Craig Johnston
      <authorblurb>
      {@link mailto:craig.johnston@sudjam.com craig.johnston@sudjam.com}
      </authorblurb>
    </author>
    <author>
      Brian Hull
      <authorblurb>
      {@link mailto:brian.hull@sudjam.com brian.hull@sudjam.com}
      </authorblurb>
    </author>
  </refsynopsisdiv>

  {@toc}

    <refsect1 id="{@id tutorial_incomming_data_keyconcepts}">
      <title>Key Concepts</title>
      <para>
The Framework encapsulates incoming data into a Payload object. 
All incoming data is considered Payload data. Since data can come 
from both trusted and untrusted sources, Payload data is 
considered "dirty".
    </para>
    <para>
Since actions are the entry point into your application, they are 
responsible for specifying what Payload data they are interested in. 
The Framework delivers Payload data to Actions in the form of Packages.
    </para>
    </refsect1>
    
    <refsect1 id="{@id tutorial_incomming_data_payload_packages}">
      <title>Payload Packages</title>
      <para>
Actions can specify both Payload Packages and Client Data Packages. 
We will discuss Client Data in a later tutorial.
      </para>
      <para>
Open up your HelloController and add a new Action called web_world. 
The web_world action expects one parameter from Payload named "user". 
The Payload Package data type is PayloadPgk. Add the following code:
      </para>
      <para>
      <programlisting role="php">
	  <![CDATA[
/**
 * Web world action
 *
 * @access public
 * @param PayloadPkg $user GET string the username
 */
public function web_world(PayloadPkg $user) {

  $this->addMetaData("title", "Hello User Page");

  $data = new stdClass();
  $data->user = $user->getString();
  $data->content = "Hello " . $data->user;

  $this->setData($data);

  $this->render();
}
]]>
    </programlisting>
    </para> 
    <para>
The Framework attempts to find Payload data named "user" and deliver 
the data to the Action in a PayloadPkg. If no data is found the 
Framework will deliver an empty PayloadPkg to the Action.
    </para> 
    <para>
Lets send data to our new action via Query String as follows:
    </para>
    <para>
    <browserurl>
	http://<?php echo $webserver; ?>/hello/world.xhtml?user=Craig
    </browserurl>
    </para>
    <para>
    <browserscreen>
      Hello Craig
    </browserscreen>
    </para>
    <para>
NOTE: You may also notice that the XHTML Renderer is still using the index.php 
View. If we do not pass the name of a view to render, the Renderer will 
use the default view. The default view is set in configuration and for 
this example it is set to "index".
    </para>
    <para>
Next we will look at various ways to send data to the Framework. 
    </para>
    <refsect2 id="{@id tutorial_incomming_data_query_string}">
      <title>Query String</title>
      <para>
The Framework provides two options for sending data via query string. 
Query string data may also be combined with "POST" data. In the above 
example we set a key/value pair by using the "key=value" convention. 
However, the Framework can also parse key/value pairs from the path. 
See the following examples below.
      </para>
    <para>
    <browserurl>
	http://<?php echo $webserver; ?>/hello/world.xhtml?user=Craig
    </browserurl>
    </para>
    <para>
The example above is equivalent to the one below:
    </para>
    <para>
    <browserurl>
	http://<?php echo $webserver; ?>/hello/world/user/Craig.xhtml
    </browserurl>
    </para>
    </refsect2>
    <refsect2 id="{@id tutorial_incomming_data_cli}">
      <title>Command Line Interface</title>
      <para>
The Command Line Interface is handled by the CLIFramework and parses 
Payload from command line options. See the example below:     
      </para>
      <para>
      <screen>
      $ cd ./utils; ./cli.php --controller hello --action world --format xhtml --user Craig
      </screen>
      </para>
      <para>
Unfortunately the example above will fail since we do not have a 
"cli_world" action. Rather than duplicating functionality we can 
write a wrapper Action like the one below:      
      </para>
      <para>
      <programlisting role="php">
	  <![CDATA[
/**
 * CLI world action
 *
 * This action wraps the web_world action.
 *
 * @access public
 * @param PayloadPkg $user the username
 */
public function cli_world(PayloadPkg $user) {
   $this->web_world($user);
}

]]>
      </programlisting>
      </para> 
      <para>
In the next tutorial we will look at storing and retrieving data from the client.
      </para>
      </refsect2>
 
    </refsect1>
    
</refentry>


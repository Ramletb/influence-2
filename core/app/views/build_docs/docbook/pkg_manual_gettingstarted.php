<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * Getting Started Documentation
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage views
 */
?>
<refentry id="{@id gettingstarted}">
  <refnamediv>
    <refname>Getting Started</refname>
  </refnamediv>
  <refsynopsisdiv>
    <author>
      Craig Johnston
      <authorblurb>
      {@link mailto:craig.johnston@sudjam.com craig.johnston@sudjam.com}
      </authorblurb>
    </author>
    <author>
      Brian Hull
      <authorblurb>
      {@link mailto:brian.hull@sudjam.com brian.hull@sudjam.com}
      </authorblurb>
    </author>
  </refsynopsisdiv>
  {@toc}
    <refsect1 id="{@id gettingstarted_wheretostart}">
      <title>Where to start</title>
      <para>
      </para>
    </refsect1>

    <refsect1 id="{@id gettingstarted_documentation}">
      <title>Documentation</title>
      <para>
This documentation is generated using a wrapped and extended version of phpDocumentor. 
The documentation may be generated in a variety of formats however the online version 
is recommended.
      </para>
      <para>
To ensure that you reading the most up-to-date version of
this documentation run the following command.
      </para>
      <screen>
    $ cd ./utils; ./cli.php --action build_docs --package <?php echo $package; ?>
      </screen>
    </refsect1>


</refentry>
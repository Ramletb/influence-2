<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * Command Line Interface Tutorial
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage views
 */
?>
<refentry id="{@id tutorial_plugin}">
  <refnamediv>
    <refname>Writing Plugins</refname>
    <refpurpose></refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <author>
      Craig Johnston
      <authorblurb>
      {@link mailto:craig.johnston@sudjam.com craig.johnston@sudjam.com}
      </authorblurb>
    </author>
    <author>
      Brian Hull
      <authorblurb>
      {@link mailto:brian.hull@sudjam.com brian.hull@sudjam.com}
      </authorblurb>
    </author>
  </refsynopsisdiv>

  {@toc}

    <refsect1 id="{@id tutorial_plugin_key_concepts}">
      <title>Key Concepts</title>
      <para>
Until till now we have focused heavily on Controllers, yet
Controllers are only intended to coordinate application flow. 
Plugins, however, are intended to encapsulate most or all of 
your business logic. Applications should be designed to segment 
easily into a series of re-usable Plugins. Consider this early 
in your application design.
      </para>
      <para>
A Plugin is simply a class which (eventually) extends the core 
abstract Plugin class. Plugins may extend other Plugins. The 
core abstract Plugin class extends the core App class, giving 
access to Logger, Configuration and major Factory objects, 
modelFactory, parserFactory, pluginFactory, rendererFactory, 
databaseFactory and serviceFactory. This gives Plugins the 
ability to not only extend other Plugins but also load Plugins, 
Renderers, Models, etc.
      </para>
      <para>
In the following examples we will develop a very simple Plugin 
and access it<?php // ?>s functionality from an Action.
      </para>
    </refsect1>
    
    <refsect1 id="{@id tutorial_plugin_simple}">
      <title>Simple Plugin</title>
      <para>
Change your current working directory to your application<?php // ?>'s plugins directory.
      </para>
      <para>
      <screen>
      $ cd ./app/plugins/
      </screen>
      </para>
      <para>
Copy the Example Controller into your app<?php // ?>'s controller directory.
      </para>
      <para>
      <screen>
      $ cp ../../core/app/plugins/ExamplePlugin.php .
      </screen>
      </para>
      <para>
Rename the ExamplePlugin.php to SimplePlugin.php.
      </para>
      <para>
      <screen>
      $ mv ExamplePlugin.php SimplePlugin.php
      </screen>
      </para>
      <para>
Open the new SimplePlugin.php file in an editor.
      </para>
      <para>
      <screen>
      $ vi SimplePlugin.php
      </screen>
      </para>

     </refsect1>
      <refsect1 id="{@id tutorial_plugin_document_plugin}">
      <title>Documenting a Plugin</title>
      <para>
Documenting a Plugin is exactly the same as documenting a Controller. 
There are three blocks for documentation in every framework file containing 
a class. The structure of documentation is important to note since generated 
documentation is dependent on this structure. 
      </para>
      <para>
The first documentation block refers to a project in general and all files 
related to this application should have the same basic header.
      </para>
      <para>
The second block describes the document/file and the third block describes 
the class. The format of these two blocks are particularly important to the 
structure of generated documentation. In this case we will change the 
"@package EXAMPLE" of the file and class to "@package TUTORIAL" and leave 
the "@subpackage plugins". 
      </para>
      </refsect1>
      <refsect1 id="{@id tutorial_plugin_document_plugin}">
      <title>The Plugin Class</title>
      <para>
We will also change the class itself from ExamplePlugin to SimplePlugin.
      </para>
      <para>
    You should end up with the following code:
      </para>
      <para>
      <programlisting role="php">
	  <![CDATA[
<?php echo "<?php\n"; ?>
 /*
 / Framework Tutorial Project
*/

/**
 * SimplePlugin.php 
 *
 * Contains the {@link SimplePlugin} class.
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage plugins
 */

/**
 * The SimplePlugin Class
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage plugins
 */
class SimplePlugin extends Plugin {

  /**
   * Get Message
   *
   * @access public
   */
  public function getMessage() {
    return 'SimplePlugin';
  }
}

<?php echo "?>\n"; ?>
]]>
    </programlisting>
    </para>
    </refsect1>
    <refsect1 id="{@id tutorial_plugin_externallibs}">
    <title>External Libraries</title>
    <para>
External libraries used by a Plugin should reside in a 
directory with the same name as a Plugin in the 
application<?php // ?>'s plugin directory.    
    </para>
    </refsect1>
    <refsect1 id="{@id tutorial_plugin_using}">
    <title>Using a Plugin</title>
    <para>
Open your HelloController and add the following Action:
    </para>
      <para>
      <programlisting role="php">
	  <![CDATA[
/**
 * Web plugin action
 *
 * @access public
 */
public function web_plugin() {

  $this->addMetaData("title", "Using a Plugin");
  
  // load the SimplePlugin
  $simplePlugin = $this->pluginFactory->load('Simple');
  
  $data = new stdClass();
  $data->content = "Hello " . $simplePlugin->getMessage();
  
  $this->setData($data);
  
  $this->render();
}

]]>
    </programlisting>
    </para>
    <para>
On line 11 we load our new Simple Plugin. On line 14 
we append the returned message to our data object<?php // ?>'s 
content attribute.
    </para>
    <para>
To observe the result of our new action we go to the following url:
    </para>
    <para>
    <browserurl>
    http://<?php echo $webserver; ?>/hello/plugin
    </browserurl>
    </para>
    <para>
A web browser should display the following result:
    </para>
    <para>
    <browserscreen>
Hello SimplePlugin...
    </browserscreen>
    </para>
    
    </refsect1>
    
</refentry>


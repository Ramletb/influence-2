<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * Web Applications Tutorial
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage views
 */
?>
<refentry id="{@id tutorial_web_applications}">
  <refnamediv>
    <refname>Web Applications</refname>
<refpurpose>Developing Web Applications with <?php echo $package; ?></refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <author>
      Craig Johnston
      <authorblurb>
      {@link mailto:craig.johnston@sudjam.com craig.johnston@sudjam.com}
      </authorblurb>
    </author>
    <author>
      Brian Hull
      <authorblurb>
      {@link mailto:brian.hull@sudjam.com brian.hull@sudjam.com}
      </authorblurb>
    </author>
  </refsynopsisdiv>

  {@toc}

    <refsect1 id="{@id tutorial_web_applications_keyconcepts}">
      <title>Key Concepts</title>
      <para>
The term Web Application refers to a website providing server side functionality. 
Rather than just displaying static information, a Web Application has the ability 
to dynamically display, accept and manipulate data at the request of a user. 
      </para>
      <para>
We will begin with using the framework to output a simple static web page. 
Although this framework is overkill for the task, we will slowly begin to 
add functionality, eventually developing a much more sophisticated Web Application.
      </para>
      <para>
At a minimum we will need to start with creating a Controller, Action and View. 
A Controller is the first object beyond the UI layer that coordinates a system 
operation. Controllers accept input events into our system.
      </para>
    </refsect1>
    
    <refsect1 id="{@id tutorial_web_applications_create_controller}">
      <title>Create a Controller</title>
      <para>
Change your current working directory to your application's controllers directory.
      </para>
      <para>

      <screen>
      $ cd ./app/controllers/
      </screen>

      </para>
      <para>
Copy the Example Controller into your app's controller directory.
      </para>
      <para>

      <screen>
      $ cp ../../core/app/controllers/ExampleController.php .
      </screen>
      </para>

      <para>
Rename the ExampleController.php to HelloController.php.
      </para>
      <para>

      <screen>
      $ mv ExampleController.php HelloController.php
      </screen>
      </para>
      <para>
Open the new HelloController.php file in an editor.
      </para>
      <para>

      <screen>
      $ vi HelloController.php
      </screen>

      </para>
      </refsect1>
      <refsect1 id="{@id tutorial_web_applications_document_controller}">
      <title>Document Controller</title>
      <para>
There are three blocks for documentation in every framework file containing a class. The structure of documentation is important to note since both generated documentation and discovery services are dependent on this structure. 
      </para>
      <para>
The first documentation block refers to the project in general, and all files related to this application should have the same basic header.
      </para>
      <para>
The second block describes the document/file and the third block describes the class. The format of these two blocks is particularly important to the structure of generated documentation. In this case we will change the "@package EXAMPLE" of the file and class to "@package TUTORIAL" and leave the "@subpackage controllers". 
      </para>
      </refsect1>
      <refsect1 id="{@id tutorial_web_applications_document_controller}">
      <title>The Controller Class</title>
      <para>
      We will also change the class itself from ExampleController to HelloController and adjust the above documentation accordingly.
      </para>
      <para>
    You should end up with the following code:
      </para>
      <para>

      <programlisting role="php">
	  <![CDATA[
<?php echo "<?php\n"; ?>
 /*
 / Framework Tutorial Project
*/

/**
 * HelloController.php
 *
 * Contains the {@}link HelloController} class.
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage controllers
 */

/**
 * The HelloController Class
 *
 * @package SP5
 * @subpackage controllers
 */
class HelloController extends Controller {

  /**
   * Web index action
   *
   * @access public
   */
  public function web_index() {
    $this->render();
  }
}
<?php echo "?>\n"; ?>

]]>
      </programlisting>

      </para>
      <para>
The controller class must end with the suffix "Controller" and exist 
in a file with the same name. The Controller must (eventually) 
inherit from the core class "Controller". Controllers may be extended by
other controllers.
      </para>


      </refsect1>
      

      <refsect1 id="{@id tutorial_web_applications_action}">
      <title>Actions</title>
      <para>
The HelloController class currently has one method named "web_index". 
Within the context of the framework this method is known as an Action. 
Actions are methods prefixed with the type of framework permitted to 
call them. The Web based framework may only invoke methods prefixed 
with "web_". The Command Line Interface (CLI) Framework may only invoke 
methods prefixed with "cli_". We will discuss the CLIFramework in a 
later tutorial.
      </para>
      <para>
Controllers may contain regular methods. It is a best practice to 
assign private or protected access to non-Action methods.
      </para>
      <para>
It is highly recommended that Action methods are all lowercase, 
underscore delimited. A typical methods named "getUserInfo" would be 
named "web_get_user_info" as a web Action. This convention becomes 
especially important for Service Models (discussed in later tutorials.)
      </para>
      </refsect1>

     <refsect1 id="{@id tutorial_web_applications_rendering_a_view}">
      <title>Rendering a View</title>
      <para>
Now that we have a Controller and Action we can see the results of the 
Framework invoking our Action with the following URL.
      </para>
      <para>
      <browserurl>
	http://<?php echo $webserver; ?>/hello/index.xhtml
      </browserurl>
      </para>
      <para>
      Since we are running in development mode we get the following message; 
if we were not running in development mode we would get an error 
message and prompted to check logs.
      </para>
      <para>
      <browserscreen>
      <graphic src="../../images/core/no_view.png"></graphic>
      </browserscreen>
      </para>
      <para>
The Framework can not find a View necessary to render the XHTML format. 
In our URL we specified .xhtml. The file extension specified in the URL is 
known to the Framework as a Format. Formats are mapped to Renderers.
      </para>
      <para>
There are three types of Framework Renderers: Presentation, Transformation 
    and Serialization (we will discuss all three in a later tutorial).
The XHTML Format is mapped to a Presentation Renderer. Presentation renderers 
utilize simple php scripts as output templates. Presentation Renderers are 
the simplest form of rendering output.
      </para>
      <para>
Presentation Renderers require Views. A View is a file located in your 
application<?php // ?>'s view directory "./app/views/". Views are organized by Renderer
and Controller; the XHTMLRenderer when called by HelloController looks for 
views in "./app/views/hello/xhtml/".
      </para>
      <para>
To make an XHTML view for our "index" action we can start by copying 
an example view from core.      
      </para>
      <para>
Change your current working directory to your controller's xhtml 
view directory. Make the directory if it does not yet exist.
      </para>
      <para>
      <screen>
      $ cd ./app/views/hello/xhtml/
      </screen>
      </para>
      <para>
Copy an example view from core to your xhtml directory.
      </para>
      <para>
      <screen>
      $ cp ../../../../core/app/views/default/xhtml/example.php .
      </screen>
      </para>
      <para>
Rename the view to index.php.
      </para>
      <para>
      <screen>
      $ mv example.php index.php
      </screen>
      </para>
      <para>
Views only contain two documentation blocks: the project block at the top of the file and document block directly after. Change the package to "@package TUTORIAL" and the leave the @subpackage as "@subpackage views". 
      </para>
      <para>
After the closing php tag add the text "Hello World". Your code should resemble the following:
      </para>
      <para>
      <programlisting role="php">
	  <![CDATA[
<?php echo "<?php\n"; ?>
 /*
 / Framework Tutorial Project
*/

/**
 * hello index view
 *
 * index.php is the index view for
 * the HelloController. Used to display
 * some interesting Hello World text.
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage views
 */
<?php echo "?>\n"; ?>

Hello World
]]>
      </programlisting>
      </para>
      <para>
Reload the URL to your HelloController index action.
      </para>
      <para>
      <browserurl>
	http://<?php echo $webserver; ?>/hello/index.xhtml
      </browserurl>
      </para>
      <para>
      You should get the following result:
      </para>
      <browserscreen>
      Hello World
      </browserscreen>
      <para>
The view we just created is not really XHTML. Presentation 
renderers simply output the results of a php script. It is 
our responsibility to write valid XHTML.
      </para>
      <para>
Replace the text "Hello World" with the following XHTML in
index.php.
      </para>
      <para>
      <programlisting role="php">
	  <![CDATA[
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
  <head>
  <title>Hello World</title>
  </head>
  <body>
Hello World
  </body>
</html>
]]>
      </programlisting>
      </para>
      <para>
Now we have a valid XHTML view. This could have simply been accomplished 
by placing the above XHMTL on a standard web server. So it<?php // ?>'s 
about time we begin utilizing the framework.
      </para>
      <para>
The text "Hello World" is considered "Data" while the title of the page is 
"Meta Data" describing the page. The Controller is capable of delivering 
both Data and Meta Data to a view. 
      </para>
      <para>
By extending the core Controller class we inherit the methods "addMetaData" 
and "setData". These methods wrap and call methods in a pre-loaded Renderer; 
in this case, the XHTML Renderer. 
      </para>
      <para>
We will set "data" and "meta data" in our HelloController. Add lines 10-15 
to your HelloController so it matches the following code:
      </para>
      <para>
      <programlisting role="php">
<![CDATA[
class HelloController extends Controller {

  /**
   * Web index action
   *
   * @access public
   */
  public function web_index() {

    $this->addMetaData("title", "Hello Controller Page");

    $data = new stdClass();
    $data->content = "Hello Controller";

    $this->setData($data);

    $this->render();
  }
}
]]>
      </programlisting>
      </para>
      <para>
Now that "Data" and "Meta Data" are being passed to our Renderer we 
can now access them in our view. Adjust your "index.php" view with 
the following code.
      </para>
      <para>
      <programlisting role="php">
<![CDATA[
<head>
  <title><?php echo "<?php "; ?>echo $title;<?php echo " ?>"; ?></title>
</head>
<body>
  <?php echo "<?php "; ?>echo $data->content;<?php echo " ?>"; ?>

</body>
]]>
      </programlisting>
      </para>
      <para>
Reload the URL to your HelloController index action.
      </para>
      <para>
      <browserurl>
	http://<?php echo $webserver; ?>/hello/index.xhtml
      </browserurl>
      </para>
      <para>
      You should get the following result:
      </para>
      <para>
      <browserscreen>
      Hello Controller
      </browserscreen>
      </para>
      <para>
You have now written a simple Web Application using the Framework. 
Data and MetaData are delivered from the Controller to the view. 
In our next tutorial we demonstrate how different Renderers can use 
the same Data and MetaData to render a variety of output formats.      
      </para>
      </refsect1>

    
</refentry>


<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * Web Services Tutorial
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage views
 */
?>
<refentry id="{@id tutorial_web_services}">
  <refnamediv>
    <refname>Web Services</refname>
<refpurpose>Developing Web Services with <?php echo $package; ?></refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <author>
      Craig Johnston
      <authorblurb>
      {@link mailto:craig.johnston@sudjam.com craig.johnston@sudjam.com}
      </authorblurb>
    </author>
    <author>
      Brian Hull
      <authorblurb>
      {@link mailto:brian.hull@sudjam.com brian.hull@sudjam.com}
      </authorblurb>
    </author>
  </refsynopsisdiv>

  {@toc}

    <refsect1 id="{@id tutorial_web_services_keyconcepts}">
      <title>Key Concepts</title>
      <para>
The Framework was initially designed to provide polymorphic access 
to data via REST based Web Services.
Communicating with different systems requires the flexibility to 
deliver complex data structures in various formats.
      </para>
    </refsect1>
    
    <refsect1 id="{@id tutorial_web_services_xxx}">
      <title>Format Mappings</title>
      <para>
Utilizing the HelloController built in the first tutorial we will 
rely on the the default "Format-to-Renderer" mappings in order to 
retrieve the data via JSON and serialized PHP. 
     </para>
     <para>
In the diagram below you can see how various formats are mapped to 
Renderers. This is the FRAMEWORK package in the default_application.ini.
     </para>
     <para>
      <graphic src="../../images/core/renderer_mappings.png"></graphic>
     </para>
    </refsect1>

    <refsect1 id="{@id tutorial_web_services_xxx}">
      <title>Data Serializaton</title>
      <para>
There are three default serialization formats: JSON, SPHP 
(Serialized PHP) and XML. We will take a look at the output of each.
      </para>
      <para>
First we will look at the output of the useful Dump Renderer. The Dump 
Renderer creates a human readable webpage showing both data and metadata. 
The Dump Renderer is only configured to work in development mode.
      </para>
      <para>
      <browserurl>
	http://<?php echo $webserver; ?>/hello/index.dump
      </browserurl>
      </para>
      <para>
      <browserscreen>
      <graphic src="../../images/core/example_dump.png"></graphic>
      </browserscreen>
      </para>
      <para>
Serialization renderers will only serialize data and not metadata.
      </para>
      <refsect2 id="{@id tutorial_web_services_json}">
        <title>JSON</title>
        <para>
JSON (JavaScript Object Notation) is an extremely useful serialized output 
format. JSON client libraries are available in nearly every major programming 
      language. See {@link http://www.json.org/}
        </para>
	<para>
JSON is automatically returned commented (//) from the Framework if the request 
is made via "GET". This is to prevent XSS (Cross-Domain) security problems. 
JSON requested via "POST" will be be returned plain.         
        </para>
        <para>
        <browserurl>
  	http://<?php echo $webserver; ?>/hello/index.json
        </browserurl>
        </para>
        <para>
        <browserscreen>
        <graphic src="../../images/core/example_json.png"></graphic>
        </browserscreen>
        </para>
      </refsect2>
      <refsect2 id="{@id tutorial_web_services_sphp}">
        <title>SPHP</title>
        <para>
SPHP (serialized PHP) is useful for communicating with other PHP based systems. 
The SPHP Renderer is used internally to store and retrieve ClientData 
and cross-framework communication.        
        </para>
        <para>
        <browserurl>
  	http://<?php echo $webserver; ?>/hello/index.sphp
        </browserurl>
        </para>
        <para>
        <browserscreen>
        <graphic src="../../images/core/example_sphp.png"></graphic>
        </browserscreen>
        </para>
      </refsect2>
      <refsect2 id="{@id tutorial_web_services_xml}">
        <title>XML</title>
        <para>
The Framework provides a very basic XML Renderer for XML serialization. 
More complex XML requires the use of a custom Transformation Renderer. 
We will discuss Transformation renderers in a later tutorial. 
        </para>
        <para>
        <browserurl>
  	http://<?php echo $webserver; ?>/hello/index.xml
        </browserurl>
        </para>
        <para>
        <browserscreen>
        <graphic src="../../images/core/example_xml.png"></graphic>
        </browserscreen>
        </para>
        <para>
Dynamic and flexible data output is only half the solution to building 
Web Applications and Web Services. In the next tutorial we discuss the 
handling of incoming data.
        </para>
      </refsect2>

    </refsect1>

    
</refentry>


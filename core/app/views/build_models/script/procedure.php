<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * procedure.php
 *
 * View for rendering a Procedure.
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage views
 */

echo "<" . "?php\n";
echo $header;
?>

 /* 
 / <?php echo $revGen; ?> 
 /
 / THIS FILE MAY BE OVERWRITTEN
 / THIS IS A GENERATED SCRIPT
 / DO NOT EDIT THIS FILE
*/ 

/** 
 * <?php echo $data->packageName ?>_Procedure.php (Generated)
 * 
 * Contains the {<?php echo $data->packageName ?>_Procedure} class.
 * 
 * @version <?php echo $revGen; ?> 
 * @package <?php echo $data->packageName; ?> 
 * @subpackage models
 */ 

/**
 * Requires appropriate model subclass
 */
require_once('<?php echo $requirePath; ?>DatabaseModel.php');

/** 
 * <?php echo $data->packageName ?>_Procedure Class
 * 
 * @version GENERATED by ModelBuilderPlugin.php 
 * @package <?php echo $data->packageName; ?> 
 * @subpackage models
 */ 
abstract class <?php echo $data->packageName ?>_Procedure extends DatabaseModel { 

  /**
   * Specify package name.
   *
   * @var string $PACKAGE
   * @access protected
   */
  protected $PACKAGE='<?php echo $data->packageName; ?>';

  <?php foreach($data->procedures as $procedureName => $procedureInfo) { ?>

  /**
   * <?php echo $procedureName; ?> 
   *
   * Calls the <?php echo $procedureName; ?> stored procedure found in the
   * <?php echo $procedureInfo['meta']['Db']; ?> database defined in the <?php echo $data->packageName ?> package.
   *
   * <?php  echo $procedureInfo['meta']['Comment']; ?> 
   *
<?php 
foreach($procedureInfo['args'] as $arg => $argInfo) {
echo '   * @param ' . $argInfo['phpType'] . ' $' . $arg . "\n";
}
?>
   * @access public
   * @returns int
   */
   public function <?php echo $procedureName; ?>(<?php 
						 $hasArg = false;
						 foreach($procedureInfo['args'] as $arg => $argInfo) {
						   echo $hasArg ? ',' : '';
						   $hasArg = true;
						   echo '$' . $arg;
						 }
						 ?>) {

     $call  = 'CALL <?php echo $procedureName; ?>';
     $call .= '(' . <?php
       $hasArg = false;
     foreach($procedureInfo['args'] as $arg => $argInfo) {
       echo $hasArg ? ' . \',\' . ' : ''; $hasArg = true;
       $hasArg = true;
       echo '$this->db->' . $argInfo['prep'] . '($' . $arg . ') ';
     }
     echo $hasArg ? ' . ' : '';
     ?> ');';

     return $this->db->execute($call);
   }
   <?php } ?>

}

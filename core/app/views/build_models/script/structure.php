<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * structure.php
 *
 * View for rendering a Structure.
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage views
 */

echo "<" . "?php\n";
echo $header;
?>
 /* 
 / <?php echo $revGen; ?> 
 /
 / THIS FILE MAY BE OVERWRITTEN
 / THIS IS A GENERATED SCRIPT
 / DO NOT EDIT THIS FILE
*/ 

/** 
 * <?php echo $data->name; ?>.php (Generated)
 * 
 * Contains the {<?php echo $data->name; ?>} class.
 * 
 * @version <?php echo $revGen; ?> 
 * @package <?php echo $data->packageName; ?> 
 * @subpackage models
 */ 

/**
 * Requires appropriate model subclass
 */
require_once('<?php echo $requirePath; ?>DatabaseModel.php');

/** 
 * <?php echo $data->name; ?> Class
 * 
 * @version GENERATED by ModelBuilderPlugin.php 
 * @package <?php echo $data->packageName; ?> 
 * @subpackage models
 */ 
abstract class <?php echo $data->name; ?> extends DatabaseModel { 
  
  /**
   * Create Statement
   *
   * @var string $CREATE
   * @access protected
   */
  protected $CREATE = "";

  /**
   * Table Type
   *
   * @var string $TABLE_TYPE
   * @access protected
   */
  protected $TABLE_TYPE = '<?php echo $data->type; ?>';
  
  /**
   * Specify column types.
   *
   * @var array $TYPES
   * @access protected
   */
  protected $TYPES = array(
<?php
$types = false;
foreach ($data->cols as $c) {
  if ($types) { echo ",\n"; }
  $types = true;
  echo "\t\t\t\t'" . $c['name'] . "' => " . "'" . $c['type'] . "'";
}
?> 
			   );
  
  /**
   * Specify column defaults.
   *
   * @var array $DEFAULTS
   * @access protected
   */
  protected $DEFAULTS = array(
<?php
$defaults = false;
foreach ($data->cols as $c) {
  if ($defaults) { echo ",\n"; }
  $defaults = true;
  echo "\t\t\t\t'" . $c['name'] . "' => " . "'" . $c['default'] . "'";
}
?> 
			      );
  
  /**
   * Specify column enumeration values.
   *
   * @var array $ENUMS
   * @access protected
   */
  protected $ENUMS = array(
<?php
foreach ($data->cols as $c) {
  if ($c['type'] != 'e') { continue; }
  if ($enums) { $enums=''; $enums .= ",\n"; }
  $enums .= "\t\t\t\t'" . $c['name'] . "' => array(";
  
  $values = '';
  foreach ($c['enums'] as $e) {
    if ($values) { $values .= ","; }
    $values .= "'" . $e . "'";
  }
  
  echo $enums . $values . ")";
}
?> 
                           );
  
  /**
   * Specify primary key.
   *
   * @var string $PRIMARY_KEY
   * @access protected
   */
  protected $PRIMARY_KEY='<?php echo $data->primaryKey; ?>';
  
  /**
   * Auto Increment
   *
   * @var string $AUTO_INCREMENT
   * @access protected
   */
  protected $AUTO_INCREMENT='<?php echo $data->autoIncrement; ?>';
  
  /**
   * Specify table name.
   *
   * @var string $TABLE
   * @access protected
   */
  protected $TABLE_NAME='<?php echo $data->tableName; ?>';
  
  /**
   * Specify model name.
   *
   * @var string $MODEL_NAME
   * @access protected
   */
  protected $MODEL_NAME='<?php echo $data->modelName; ?>';
  
  /**
   * Specify package name.
   *
   * @var string $PACKAGE
   * @access protected
   */
  protected $PACKAGE='<?php echo $data->packageName; ?>';

}

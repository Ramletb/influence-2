<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * procedureModel.php
 *
 * View for rendering a ProcedureModel.
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage views
 */

echo "<" . "?php\n";
echo $header;
?>

/** 
 * <?php echo $data->packageName ?>_ProcedureModel.php
 * 
 * Contains the {<?php echo $data->packageName ?>_ProcedureModel} class.
 * (this file may be modified safely)
 * 
 * @version <?php echo '$R' . 'ev: $'; ?> 
 * @package <?php echo $data->packageName; ?> 
 * @subpackage models
 */ 

/**
 * Requires <?php echo $data->packageName ?>_Procedure
 */
require_once('<?php echo $requirePath  .  $data->packageName; ?>/generated/<?php echo $data->packageName ?>_Procedure.php');

/**
 * <?php echo $data->packageName ?>_ProcedureModel class
 *
 * @package <?php echo $data->packageName; ?> 
 * @subpackage models
 */
class <?php echo $data->packageName ?>_ProcedureModel extends <?php echo $data->packageName ?>_Procedure {
  
}

<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * controller.php
 *
 * Contains xhtml for rendering controller discovery data.
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @package SP5
 * @subpackage views
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title><?php echo $title; ?></title>
    <link href='http://fonts.googleapis.com/css?family=Cousine' rel='stylesheet' type='text/css'>
<style>

body 
{ 
  font-family: 'Cousine', arial, sans-serif; 
} 

h1
{
  margin: 0;
}

p
{
  font-size: 10px;
}

table
{
  border-spacing: 0 0;
  border-top: 1px solid;
  border-left: 1px solid;
  border-collapse: separate;
  width: 580px;
}

table td, table th
{
  padding: 5px;
  border: 0;
  border-bottom: 1px solid;
  border-right: 1px solid;
  text-align: left;
  font-size: 12px;
}

.action-details
{
  background-color: #C2DBB4;
  border: 1px solid #999999;
  padding: 0 10px 10px 10px;
}

.action-details table, .action-link, .formats
{
  margin-left: 20px;
}

#content
{
  padding: 10px;
  padding-top: 0px;
}

#header, #footer
{
  padding: 10px;
}

</style>
</head>

<body>

<div id="header">
    <h1><?php echo $title; ?></h1>
</div>


<div id="content">
<?php 

    if(is_array($data)) {
      foreach($data as $controllerName => $discoverData) {
	echo '<h2>' . ucfirst($controllerName) . '\'s Actions</h2>'."\n";
	
	echo '<div class="action-details">'."\n";

	if(is_array($discoverData['action'])) {
	  foreach($discoverData['action'] as $actionName => $actionStatus) {
	    $link = 'http://' . $configuration->environment['FRAMEWORK']['web']['server'] . $webPath . $controllerName . '/' . $actionName;
	  
	    echo '<h3>' . $actionName . '</h3>';
	    
	    echo '<p>';
	    echo '<a class="action-link" href="' . $link . '">' . $link . '</a><br /><span class="formats">'."\n";
	    if(isset($renderers) && is_array($renderers)) {
	      foreach($renderers as $format => $renderer) {
		echo ' [<a href="' . $link . '.' . $format .'">'. $format .'</a>]'."\n";
	      }
	    }
	    echo '</span></p>';
	    
	    if(array_key_exists($actionName, $discoverData['actionInterface']) && is_array($discoverData['actionInterface'][$actionName])) {
	      
	      echo '<table>' . "\n";
	      
	      echo '<thead>' . "\n"; 
	      echo '    <tr>' . "\n";
	      echo '      <th>Param</th>' . "\n";
	      echo '      <th>Method</th>' . "\n";
	      echo '      <th>Type</th>' . "\n";
	      echo '      <th>Description</th>' . "\n";
	      echo '    </tr>' . "\n";
	      echo '</thead>' . "\n";
	    
	      echo '<tbody>' . "\n";
	      
	      foreach($discoverData['actionInterface'][$actionName] as $paramName => $paramDetails) {
		echo ' <tr>';
		echo '  <td>' . $paramName . '</td>';
		echo '  <td>' . $paramDetails['proto'] . '</td>';
		echo '  <td>' . $paramDetails['type'] . '</td>';
		echo '  <td>' . $paramDetails['desc'] . '</td>';
		echo ' </tr>'."\n";
	      }
	      
	      echo '</tbody>' . "\n";
	      echo '</table>' . "\n";
	    }
	  }
	}
	
      echo '</div>'."\n";
    }

}

?>
</div>

</body>
</html>




<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * error.php
 *
 * Contains xhtml for rendering a discovery error message.
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage views
 */
?>

<?php 
/**
 * Page Header
 *
 */
include C_PATH_CORE_VIEWS . 'parts/xhtml/header.php'; 
?>

  <script type="text/javascript" src="/js/core/discover/default.js"></script>
</head>

<body class="yui-skin-sam">

<div id="header">
    <h1>Discovery Error</h1>
</div>


<div id="content">
    <h2><?php echo $data['error']; ?></h2>
<p>
    <?php echo $data['error_message']; ?>
</p>
</div>

<div id="footer">
    <span id="version">Framework v<?php echo $configuration->getFrameworkVersion() .' r'. $configuration->getFrameworkRevision() .' ('. $configuration->getFrameworkRevisionDate() .')'; ?></span>
</div>


<?php 
/**
 * Page Footer
 *
 */
include C_PATH_CORE_VIEWS . 'parts/xhtml/footer.php'; 
?>




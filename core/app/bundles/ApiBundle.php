<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * ApiBundle.php 
 *
 * Contains the {@link ApiBundle} class.
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage bundles
 */

/**
 * The ApiBundle Class
 *
 * @package SP5
 * @uses ApiPlugin
 * @subpackage bundles
 */
class ApiBundle extends Bundle {

  /**
   * API Plugin
   *
   * @var ApiPlugin $apiPlugin
   * @access public
   */  
  public $apiPlugin;

  /**
   * Set Bundle
   *
   * @access public
   * @param PayloadPkg $select    GET string select (columns) array
   * @param PayloadPkg $where     GET string where hash
   * @param PayloadPkg $order     GET string order hash
   * @param PayloadPkg $group     GET string group hash
   * @param PayloadPkg $not       GET string not hash
   * @param PayloadPkg $isNotNull GET string is not null hash/array
   * @param PayloadPkg $isNull    GET string is null hash/array
   * @param PayloadPkg $between   GET string between hash
   * @param PayloadPkg $like      GET string like hash
   * @param PayloadPkg $gt        GET string greater than hash
   * @param PayloadPkg $lt        GET string less than hash
   * @param PayloadPkg $in        GET string in hash
   * @param PayloadPkg $or        GET string or hash
   * @param PayloadPkg $notIn     GET string not in hash
   * @param PayloadPkg $page      GET int page number
   * @param PayloadPkg $perPage   GET int results per page
   */
  public function setBundle(PayloadPkg $select,
			    PayloadPkg $where,
			    PayloadPkg $order,
			    PayloadPkg $group,
			    PayloadPkg $not,
			    PayloadPkg $isNotNull,
			    PayloadPkg $isNull,
			    PayloadPkg $between,
			    PayloadPkg $like,
			    PayloadPkg $gt,
			    PayloadPkg $havingGt,
			    PayloadPkg $lt,
			    PayloadPkg $in,
			    PayloadPkg $or,
			    PayloadPkg $notIn,
			    PayloadPkg $page,
			    PayloadPkg $perPage) {

    $this->apiPlugin  = $this->pluginFactory->load("Api");    

    $this->select    = $select->getHashArray();
    $this->where     = $where->getHashArray();
    $this->order     = $order->getHashArray();
    $this->group     = $group->getHashArray();
    $this->not       = $not->getHashArray();
    $this->isNotNull = $isNotNull->getHashArray();
    $this->isNull    = $isNull->getHashArray();
    $this->between   = $between->getHashArray();
    $this->like      = $like->getHashArray();
    $this->gt        = $gt->getHashArray();
    $this->havingGt  = $havingGt->getHashArray();
    $this->lt        = $lt->getHashArray();
    $this->in        = $in->getHashArray();
    $this->or        = $or->getHashArray();
    $this->notIn     = $notIn->getHashArray();
    $this->page      = $page->getInt(1);
    $this->perPage   = $perPage->getInt(1);
  }

  /**
   * Load (Model);
   *
   * @see ApiPlugin
   * @access public
   * @param string $modelName name of the Model
   * @param string $packageName name of the package the model belongs to
   */
  public function load($modelName, $packageName) {
    $this->apiPlugin->load($modelName, $packageName);
    //$this->setConditions();
  }

  /**
   * Reset
   *
   * @see ApiPlugin
   * @access public
   */
  public function resetConditions() {
    return $this->apiPlugin->resetConditions();
  }

  /**
   * Get Data
   *
   * @see ApiPlugin
   * @access public
   * @returns mixed data
   */
  public function getData() {
    $this->setConditions();
    return $this->apiPlugin->getData();
  }

  /**
   * Get Statement
   *
   * @see ApiPlugin
   * @access public
   * @returns string sql
   */
  public function getStatement() {
    return $this->apiPlugin->getStatement();
  }

  /**
   * Set Conditions
   *
   * @see ApiPlugin
   * @access private
   */
  private function setConditions() {

    $this->apiPlugin->isNotNull($this->isNotNull);
    $this->isNotNull = array();

    $this->apiPlugin->between($this->between);
    $this->between = array();

    $this->apiPlugin->isNull($this->isNull);
    $this->isNull = array();

    $this->apiPlugin->select($this->select);
    $this->select = array();

    $this->apiPlugin->where($this->where);
    $this->where = array();

    $this->apiPlugin->whereOr($this->or);
    $this->or = array();

    $this->apiPlugin->group($this->group);
    $this->group = array();

    $this->apiPlugin->order($this->order);
    $this->order = array();

    $this->apiPlugin->notIn($this->notIn);
    $this->notIn = array();

    $this->apiPlugin->like($this->like);
    $this->like = array();

    $this->apiPlugin->not($this->not);
    $this->not = array();

    $this->apiPlugin->gt($this->gt);
    $this->gt = array();

    $this->apiPlugin->havingGt($this->havingGt);
    $this->havingGt = array();

    $this->apiPlugin->lt($this->lt);
    $this->lt = array();

    $this->apiPlugin->in($this->in);
    $this->in = array();

    $this->apiPlugin->page($this->perPage,$this->page);
  }


}

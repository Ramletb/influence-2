<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * Bundle.php 
 *
 * Contains the abstract {@link Bundle} class.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage core
 */

/**
 * The Abstract Bundle Class
 *
 * @package SP5
 * @subpackage core
 */
abstract class Bundle extends App {

  /**
   * Controller
   *
   * Holds a refrence to the controller that
   * created this Bundle.
   *
   * @access public
   * @var Controller $controller
   */
  public $controller;

  /**
   * Error
   *
   * @var array $error
   * @access public
   */  
  private $error;

  /**
   * Set Data
   *
   * Override this method
   *
   * @access public
   */
  public function setBundle() { }

  /**
   * Set Error
   *
   * @access public
   * @param string $field
   * @param string $name
   * @param string $message
   */
  public function addError($field, $name, $message) {
    $e = array('field'   => $field,
	       'name'    => $name,
	       'message' => $message);

    $this->error[] = $e;
  }

  /**
   * Get Error
   *
   * @access public
   * @returns array error
   */
  public function getError() {
    if($this->error) {
      return array('bundleError' => $this->error);
    }

    return null;
  }



}

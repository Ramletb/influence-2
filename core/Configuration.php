<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * Configuration.php 
 *
 * Contains the {@link Configuration} class.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage core
 */

/**
 * The Configuration Class
 *
 * See the {@link local:docs/r.php?t=&s=configuration Configuration} section of the framework manual. 
 *
 * @package SP5
 * @subpackage core
 */
final class Configuration {

  /**
   * Instance
   *
   * A static inststance of Configuration
   *
   * @var Configuration $instance
   * @access private
   * @static
   */  
  private static $instance;

  /**
   * Framework
   *
   * A hash of Framework configuration items
   * built from core_framework.ini files.
   *
   * @var Array $framework
   * @access private
   */  
  public $framework;

  /**
   * Application
   *
   * A hash of application configuration items
   * built from *_application.ini files.
   *
   * @var Array $application
   * @access public
   */  
  public $application;

  /**
   * Environment
   *
   * A hash of environment configuration.
   * built from *_anvironment.ini
   *
   * @var Array $environment
   * @access public
   */  
  public $environment;
  
  /** 
   * Constructor
   *
   * Prevent direct instantiation
   * 
   * @ignore
   * @access private
   */  
  private function __construct() {
  }
  
  /**
   * Clone
   *   
   * Prevent Cloneing
   * 
   * @ignore
   * @access private
   */  
  private function __clone() {
  }


  /**
   * get Configuration
   *
   * Get a singleton instance of the Configuration class.
   * 
   * Example:
   * <pre>
   * $configuration = Configuration::getConfiguration();
   * </pre>
   *
   * @access public
   * @returns Configuration a configuration class object.
   */  
  public static function getConfiguration() {
    
    if (self::$instance == null) {

      self::$instance = new Configuration();

      if (file_exists(C_FILE_CONFIG)) {
      	self::$instance->framework = self::$instance->parseIni(C_FILE_CONFIG);
      }

      // read the default environment configuration
      //
      self::$instance->environment = self::$instance->parseIni(C_FILE_CONFIG_ENV_DEFAULT);

      // override the default environment configuration
      //
      if (file_exists(C_FILE_CONFIG_ENV_CUSTOM)) {
	$custom_environment = self::$instance->parseIni(C_FILE_CONFIG_ENV_CUSTOM);

	foreach($custom_environment as $package => $kva) {
	  foreach($kva as $key => $val) {
	    self::$instance->environment[$package][$key] = $val;
	  }
	}

      }

      // read the default application configuration
      //
      self::$instance->application = self::$instance->parseIni(C_FILE_CONFIG_APP_DEFAULT);

      // override the default application configuration
      //
      if (file_exists(C_FILE_CONFIG_APP_CUSTOM)) {
	$custom_application = self::$instance->parseIni(C_FILE_CONFIG_APP_CUSTOM);

	foreach($custom_application as $package => $kva) {
	  foreach($kva as $key => $val) {
	    self::$instance->application[$package][$key] = $val;
	  }
	}

      }
    }
    
    return self::$instance;
  }

  /**
   * Parse .ini
   *
   * Parse an ini file
   * 
   * @access public
   * @param string $configIni ini file name
   * @returns array configuration hash
   */  
  public function parseIni($configIni) {
    $configuration = array();
    
    if (file_exists($configIni)) {
      $ini = parse_ini_file($configIni, true);
    }

    foreach($ini as $top_level => $second_level) {
      foreach($second_level as $key => $val) {
	$levels   = array_reverse(explode('.', $key));
	$keystack = $val;
	for($i=1;$i <= count($levels);$i++) {
	  $keystack = array($levels[$i - 1] => $keystack);
	} 
	$keystack = array($top_level => $keystack);
	$configuration = array_merge_recursive($configuration,$keystack);
      }
    }
    
    return $configuration;
  }
  
  /**
   * Get Framework Version
   *
   * Get the current version number of the framework.
   * 
   * @access public
   * @returns int framework version number
   */  
  public function getFrameworkVersion() {
    return $this->framework['CORE']['framework']['version'];
  }

  /**
   * Get Framework Minor
   *
   * Get the current minor number of the framework.
   * 
   * @access public
   * @returns int frameworkminor number
   */  
  public function getFrameworkMinor() {
    return $this->framework['CORE']['framework']['minor'];
  }

  /**
   * Get Framework Build
   *
   * Get the current build number of the framework.
   * 
   * @access public   
   * @returns int framework build number 
   */
  public function getFrameworkBuild() {
    return $this->framework['CORE']['framework']['build'];
  }

  /**
   * Get Framework Full Version
   *
   * Get the current full version number (n.n.n.n).
   * 
   * @access public
   * @returns string Framework revision date
   */  
  public function getFrameworkFullVersion() {
    return $this->getFrameworkVersion() . '.' .
      $this->getFrameworkMinor() . '.' .
      $this->getFrameworkBuild();
  }

}

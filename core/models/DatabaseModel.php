<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * DatabaseModel.php 
 *
 * Contains the abstract {@link DatabaseModel} class.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage core
 */

/**
 * The Abstract DatabaseModel Class
 *
 * DatabaseModels encapsulate database tables and provide a repository for code enforcing 
 * data integrity.
 *
 * See the {@link local:docs/r.php?t=&s=models Models} section of the framework manual. 
 *
 * @package SP5
 * @subpackage core
 */
abstract class DatabaseModel extends Model {

  /**
   * Database Object
   *
   * Holds a Database object
   *
   * @var Database $db
   * @access protected
   */  
  protected $db;

  /**
   * Database Information
   *
   * Hols a database configuration array
   *
   * @var Array $dbInfo
   * @access protected
   */  
  protected $dbInfo;

  /**
   * Where
   *
   * Holds a hash of key/value pairs used
   * to construct a where clause.
   *
   * @var array $where
   * @access private
   */  
  private $where = array();

  /**
   * Order
   *
   * Holds a hash of key/value pairs used
   * to construct an order clause.
   *
   * @var array $order
   * @access private
   */  
  private $order = array();

  /**
   * Constructor
   *
   * Model constructor is called when a
   * model is instantiated by a Factory.
   *
   * Example:
   * <pre>
   * $dbModel = new DatabaseModel();
   * </pre>
   *
   * @ignore
   * @access public
   */
  public function __construct() {
    parent::__construct();

    // make our db object
    $this->dbInfo = $this->configuration->environment[$this->PACKAGE]['db'];
    $this->db = $this->databaseFactory->load($this->PACKAGE);
  }

  /**
   * Get Types
   *
   * Return argument types.
   *
   * Example:
   * <pre>
   * $types = $this->getTypes();
   * foreach($types as $type) {...}
   * </pre>
   *
   * @access public
   * @return array array of argument types
   */
  public function getTypes() {
    return $this->TYPES;
  }

  /**
   * Get Table Name
   *
   * Return table name.
   *
   * Example:
   * <pre>
   * $tableName = $this->getTableName();
   * </pre>
   *
   * @access public
   * @return string string table name
   */
  public function getTableName() {
    return $this->TABLE_NAME;
  }

  /**
   * Get Table Type
   *
   * Return table type.
   *
   * 'BASRE TABLE' or 'VIEW'
   *
   * @access public
   * @return string string table type
   */
  public function getTableType() {
    return $this->TABLE_TYPE;
  }

  /**
   * Get Model Name
   *
   * Return Model name.
   *
   * Example:
   * <pre>
   * $modelName = $this->getModelName();
   * </pre>
   *
   * @access public
   * @return string string model name
   */
  public function getModelName() {
    return $this->MODEL_NAME;
  }

  /**
   * Get Auto Increment
   *
   * @access public
   * @return string string model name
   */
  public function getAutoIncrement() {
    return $this->AUTO_INCREMENT;
  }

  /**
   * Get Defaults
   *
   * Return column defaults.
   * Example:
   * <pre>
   * $defaults = $this->getDefaults();
   * foreach($defaults as $default) {...}
   * </pre>
   *
   * @access public
   * @return array array of column defaults
   */
  public function getDefaults() {
    return $this->DEFAULTS;
  }

  /**
   * Get ENUMs
   *
   * Return column enumeration values.
   *
   * Example:
   * <pre>
   * $enums = $this->getEnums();
   * foreach($enums as $enum) {...}
   * </pre>
   *
   * @access public
   * @return array array of enumeration values
   */
  public function getEnums() {
    return $this->ENUMS;
  }

  /**
   * Get Primary Key
   *
   * Return primary key column name.
   *
   * Example:
   * <pre>
   * $primaryKey = $this->getPrimaryKey();
   * </pre>
   *
   * @access public
   * @return string string primary key column name
   */
  public function getPrimaryKey() {
    return $this->PRIMARY_KEY;
  }

  /**
   * Get Package
   *
   * Return model package
   *
   * Example:
   * <pre>
   * $package = $this->getPackage();
   * </pre>
   *
   * @access public
   * @return string string model package
   */
  public function getPackage() {
    return $this->PACKAGE;
  }

  /**
   * Get All 
   *
   * Returns all records
   *
   * @access public
   * @returns array results
   */
  public function getAll($whereHash=null,$orderHash=null) {
    return $this->db->smartSelectConditions($this, $whereHash,$orderHash);
  }

  /**
   * Get All Paginated
   *
   * Returns all records paginated
   *
   * @param array $whereHash
   * @param array $orderHash
   * @param int $page
   * @param int $perPage
   * @access public
   * @returns array results
   */
  public function getAllPaginated($whereHash, $orderHash, $page, $perPage) {
    if(count($whereHash) < 1) {
      $whereHash = null;
    }
    if(count($orderHash) < 1) {
      $orderHash = null;
    }
    return $this->smartSelectPaginated($whereHash, $orderHash, $page, $perPage);
  }

  /**
   * Set Where
   *
   * Pass a hash of key/values representing database
   * data base colums and values.
   *
   * @access public
   * @param array $whereHash
   */
  public function setWhere(array $whereHash) {
    $this->where = $whereHash;
  }

  /**
   * Add Where
   *
   * Add an entry to the where clause.
   *
   * @access public
   * @param string $column
   * @param string $value
   */
  public function addWhere($column, $value) {
    $this->where[$column] = $value;
  }

  /**
   * Set Order
   *
   * Pass a hash of key/values representing database
   * colums and order values (ASC/DESC).
   *
   * @access public
   * @param array $orderHash
   */
  public function setOrder(array $orderHash) {
    $this->order = $orderHash;
  }

  /**
   * Add Order
   *
   * Add an entry to the order clause.
   *
   * @access public
   * @param string $column
   * @param string $order ASC/DESC
   */
  public function addOrder($column, $order) {
    $this->order[$column] = $order;
  }

  /**
   * Prepare Arguments
   * 
   * Run through an arg list and prepare them for a SQL operation.
   *
   * Example:
   * <pre>
   * $data = array('id' => 23, 'name' => 'bill', address' => '123 Elm.');
   * $cleanedArgs = $this->prepare($data);
   * </pre>
   *
   * @param array $args a hash key value pairs
   * @access public
   * @return array array of prepared values
   */
  public function prepare($args) {
    if (!is_array($args)) { return array(); }

    $preparedArgs = array();
    
    foreach ($args as $column => $val) {
      $foundCol = false;

      foreach ($this->TYPES as $col => $t) {
	if($val == $col) {
	  $preparedArgs[$column] = $val;
	  $foundCol = true;
	}
      }

      if(!$foundCol && array_key_exists($column, $this->TYPES)) {
	if ($this->TYPES[$column] == 's'  || $this->TYPES[$column] == 'd'  ||
	    $this->TYPES[$column] == 'dt' || $this->TYPES[$column] == 'tm' ||
	    $this->TYPES[$column] == 'e'  || $this->TYPES[$column] == 't') {
	  $preparedArgs[$column] = $this->db->prepareString($args[$column]);
	} else if ($this->TYPES[$column] == 'g' ) {
	  $preparedArgs[$column] = $this->db->prepareGeometric($args[$column]);
	} else {
	  $preparedArgs[$column] = $this->db->prepareNumber($args[$column]);
	}
      }
    }

    return $preparedArgs;
  }

  /**
   * Prepare Value
   * 
   * @param string $type
   * @param mixed  $value
   * @access public
   * @returns mixed value
   */
  public function prepareValue($type,$value) {

    if ($type == 's'  || $type == 'd'  ||
	$type == 'dt' || $type == 'tm' ||
	$type == 'e'  || $type == 't') {
      return $this->db->prepareString($value);
    } else if ($type == 'g' ) {
      $type = $this->db->prepareGeometric($value);
    } else {
      $type = $this->db->prepareNumber($value);
    }
  }

  /**
   * Prepare Method
   * 
   * @param string $type
   * @param mixed  $value
   * @access public
   * @returns string method used to prepare
   */
  public function prepareMethod($type) {

    if ($type == 's'  || $type == 'd'  ||
	$type == 'dt' || $type == 'tm' ||
	$type == 'e'  || $type == 't') {
      return "prepareString";
    } else if ($type == 'g' ) {
      return "prepareGeometric";
    } else {
      return "prepareNumber";
    }
  }

  /**
   * Prepare Order Arguments
   * 
   * Run through an arg list and prepare them for a SQL operation.
   *
   * Example:
   * <pre>
   * $data = array('id' => 'ASC', 'name' => 'DESC', address' => 'DESC');
   * $cleanedArgs = $this->prepare($data);
   * </pre>
   *
   * @param array $args an array or key value pairs
   * @access public
   * @return array array of prepared values
   */
  public function prepareOrder($args) {
    $preparedArgs = array();
    if(!is_array($args)) { return $preparedArgs; };

    $acceptableValues = array('ASC' => 1, 'DESC' => 1);

    foreach ($args as $column => $val) {
      if(array_key_exists($column, $this->TYPES)) {
	if(array_key_exists(strtoupper($val), $acceptableValues)) {
	  $preparedArgs[$column] = $val;
	}
      }
    }

    return $preparedArgs;
  }

  /**
   * Where String
   *
   * @param array $whereHash
   * @param string $prefix
   * @access public
   * @return string
   */
  public function whereString($whereHash,$prefix) {
    $whereHash   = $this->prepare($whereHash);
    $whereArray  = array();    

    $i = 0;
    foreach($whereHash as $col => $val) {
      if(substr($val, 1, 1) == '~') {
	array_push($whereArray, '`' . $col . '` LIKE \'' . substr($val, 2));
      } else {
	array_push($whereArray, '`' . $col . '` = ' . $val);
      }
      $i++;
    }
    
    if(strlen($prefix) < 1 || $i < 1) {
      $prefix = '';
    }

    return $prefix . ' ' . implode(' AND ', $whereArray);
  }

  /**
   * Order String
   *
   * @param array $orderHash
   * @param string $prefix
   * @access public
   * @return string
   */
  public function orderString($orderHash,$prefix) {
    $orderHash   = $this->prepareOrder($orderHash);
    $orderArray  = array();

    $i = 0;
    foreach($orderHash as $col => $val) {
      array_push($orderArray, $col . ' ' . $val);
      $i++;
    }
    
    if(strlen($prefix) < 1 || $i < 1) {
      $prefix = '';
    }

    return $prefix . ' ' . implode(', ', $orderArray);
  }

  /**
   * Smart Insert
   *
   *
   * @param array $hash
   * @access public
   * @returns integer $result
   */
  public function smartInsert($hash) {
    return $this->db->smartInsert($this, $hash);
  }

  /**
   * Smart Update
   *
   * Update with clean data, returning rows affected, if any.
   *
   * Example:
   * <pre>
   * $data          = array('id' => 23, 'name' => 'bill', address' => '123 Elm.');
   * $where         = array('id' => $id);
   * $rows_affected = $this->smartUpdate($data, $where);
   * </pre>
   *
   * @param array $setHash an array or key value pairs of     
   * @param array $whereHash
   * @param string $addendum
   * @access public
   * @returns integer $result
   */
  public function smartUpdate($setHash, $whereHash, $addendum='') {
    return $this->db->smartUpdate($this, $setHash, $whereHash, $addendum);
  }

  /**
   * Smart Delete
   *
   * Delete based on clean data, returning rows affected, if any.
   *
   * Example:
   * <pre>
   * $rows_affected = $this->smartDelete(array('id' => 23));
   * </pre>
   *
   * @param array $whereHash
   * @access public
   * @returns integer $result
   */
  public function smartDelete($whereHash) {
    return $this->db->smartDelete($this, $whereHash);
  }

  /**
   * Smart Select
   *
   * Select based on clean data, returning an array of rows (each row being an array of columns).
   *
   * Examples:
   * <pre>
   * $rows = $this->smartSelect(null, array('id' => 23)); // select all columns.
   * 
   * // select id, name and address columns.
   * $rows = $this->smartSelect(array('id', 'name', 'address'), , array('id' => 23));
   * </pre>
   *
   * @param array $whatHash
   * @param array $whereHash
   * @param string $addendum
   * @access public
   * @returns array $rows
   */
  public function smartSelect($whatHash, $whereHash, $addendum=null) {
    return $this->db->smartSelect($this, $whatHash, $whereHash, $addendum);
  }

  /**
   * Smart Select Paginated
   *
   * Execute a paginated select operation on the DB based on supplied args.
   *
   * @param array $whereHash
   * @param array $orderHash
   * @param int $page
   * @param int $perPage
   * @access public
   * @returns array $result
   */
  public function smartSelectPaginated($whereHash, $orderHash, $page, $perPage) {
    return $this->db->smartSelectPaginated($this, $whereHash, $orderHash, $page, $perPage);
  }

  /**
   * Smart Select One
   *
   * Select based on clean data, returning a single row (where the row is an array of columns).
   *
   * Example:
   * <pre>
   * $row = $this->smartSelectOne(null, array('id' => 23));
   * </pre>
   *
   * @param array $whatHash
   * @param array $whereHash
   * @access public
   * @returns array $oneResultRow
   */
  public function smartSelectOne($whatHash, $whereHash) {
    $result = $this->db->smartSelect($this, $whatHash, $whereHash, ' LIMIT 1');
    return $result[0];
  }

  /**
   * Smart Select All
   * 
   * Select all rows, returning the rows in an array of rows (each row being an array of columns).
   *
   * Example:
   * <pre>
   * $rows = $this->smartSelectAll());</pre>
   * </pre>
   *
   * @access public
   * @returns array $allRowsOfData
   */
  public function smartSelectAll() {
    return $this->db->smartSelect($this, null, null, null);
  }

  /**
   * API Select
   *
   * Used by the API plugin.
   *
   * @param ApiPlugin $api
   * @access public
   * @returns array select result
   */
  public function apiSelect($api) {

    $countResult = $this->db->query($api->getCountStatement());
    $count = $countResult[0]['total'];

    if($count > 0 && $api->perPage > 0) {
      $totalPages = ceil($count / $api->perPage);
    } else {
      $totalPages = 0;
    }

    //$this->logger->debug($api->getCountStatement();

    return array('totalRecords' => $count,
		 'totalPages'   => $totalPages,
		 'page'         => ($api->pageNumber + 1),
		 'perPage'      => $api->perPage,
		 'pageRecords'  => $this->db->query($api->getStatement()));
  }

}


<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * RestModel.php
 *
 * Contains the abstract {@link RestModel} class.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage core
 */

/**
 * The Abstract RestModel Class
 *
 * RestModel encapsulate database tables and provide a repository for code enforcing 
 * data integrity.
 *
 * See the {@link local:docs/r.php?t=&s=models Models} section of the framework manual. 
 *
 * @package SP5
 * @subpackage core
 */
abstract class RestModel extends Model {

  /**
   * Service
   *
   * Service Object
   *
   * Holds a Service object
   *
   * @var Service $service
   * @access protected
   */  
  protected $service;

  /**
   * Service Information
   *
   * Holds a database configuration array
   *
   * @var Array $serviceInfo
   * @access protected
   */  
  protected $serviceInfo;

  /**
   * Constructor
   *
   * RestModel constructor is called when a
   * model is instantiated by a Factory.
   *
   * @ignore
   * @access public
   */
  public function __construct() {
    parent::__construct();
    $this->logger->trace("Instantiated " . get_class($this) . "...");

    // make our db object
    $this->serviceInfo = $this->configuration->environment[$this->PACKAGE]['service'];
    $this->service = $this->serviceFactory->load($this->PACKAGE);
  }

  /**
   * Get Types
   *
   * Return argument types.
   *
   * @access protected
   * @returns array $types
   */
  public function getTypes() {
    return $this->TYPES;
  }

  /**
   * Get Name
   *
   * Return name of service call.
   *
   * @access protected
   * @returns string $name
   */
  public function getName() {
    return $this->NAME;
  }

  /**
   * Get Model Name
   *
   * Return Model name.
   *
   * @access protected
   * @returns array $modelName
   */
  public function getModelName() {
    return $this->MODEL_NAME;
  }

  /**
   * Get Defaults
   *
   * Return defaults.
   *
   * @access protected
   * @returns array $fieldDefaults
   */
  public function getDefaults() {
    return $this->DEFAULTS;
  }

  /**
   * Get ENUMs
   *
   * Return field enumeration values.
   *
   * @access protected
   * @returns array $enumValues
   */
  public function getEnums() {
    return $this->ENUMS;
  }

  /**
   * Get Primary Key
   *
   * Return primary id field name.
   *
   * @access protected
   * @returns array $primaryKey
   */
  public function getPrimaryId() {
    return $this->PRIMARY_ID;
  }

  /**
   * Get Package
   *
   * Return model package
   *
   * @access protected
   * @returns array $package
   */
  public function getPackage() {
    return $this->PACKAGE;
  }

  /**
   * Prepare
   * 
   * Run through an arg list and prepare them for a REST operation.
   *
   * Example:
   * <pre>
   * $data = array('id' => 23, 'name' => 'bill', address' => '123 Elm.');
   * $cleanedArgs = $this->prepare($data);
   * </pre>
   *
   * @param array $args an array or key value pairs
   * @access public
   * @return array array of prepared values
   */
  public function prepare($args) {
    if (!$args) { return array(); }

    foreach ($args as $field => $val) {
      if ($this->TYPES[$field] == 's'  || $this->TYPES[$field] == 'd'  ||
          $this->TYPES[$field] == 'dt' || $this->TYPES[$field] == 'tm' ||
          $this->TYPES[$field] == 'e'  || $this->TYPES[$field] == 't') {
        $args[$field] = $this->service->prepareString($args[$field]);
      } else {
        $args[$field] = $this->service->prepareNumber($args[$field]);
      }
    }

    return $args;
  }

  /**
   * Smart Write
   *
   * Write new data, returning the newly created id, if any.
   *
   * Example:
   * <pre>
   * $data   = array('id' => 23, 'name' => 'bill', address' => '123 Elm.');
   * $new_id = $this->smartNew($data);
   * </pre>
   *
   * @param array $valuesHash
   * @access public
   * @returns integer $result
   */
  function smartWrite($valuesHash) {
    return $this->service->smartWrite($this, $valuesHash);
  }

  /**
   * Smart Update
   *
   * Update with clean data, returning datasets affected or status, if any.
   *
   * Example:
   * <pre>
   * $data          = array('id' => 23, 'name' => 'bill', address' => '123 Elm.');
   * $datasets_affected = $this->smartUpdate($data);
   * </pre>
   *
   * @param array $setHash
   * @param array $whereHash
   * @access public
   * @returns integer $result
   */
  function smartUpdate($setHash, $whereHash) {
    return $this->db->smartUpdate($this, $setHash, $whereHash);
  }

  /**
   * Smart Delete
   *
   * Delete based on clean data, returning datasets affected or status, if any.
   *
   * Example:
   * <pre>
   * $datasets_affected = $this->smartDelete(array('id' => 23));
   * </pre>
   *
   * @param array $whereHash
   * @access public
   * @returns integer $result
   */
  function smartDelete($whereHash) {
    return $this->db->smartDelete($this, $whereHash);
  }

  /**
   * Smart Read
   *
   * Read based on clean data, returning an array of datasets (each dataset being an array of fields).
   *
   * Examples:
   * <pre>
   * $data = $this->smartRead(null, array('id' => 23)); // read all fields.
   * 
   * // read id, name and address fields.
   * $data = $this->smartRead(array('id', 'name', 'address'), , array('id' => 23));
   * </pre>
   *
   * @param array $whatHash
   * @param array $whereHash
   * @param string $addendum
   * @access public
   * @returns array $data
   */
  function smartRead($whatHash, $whereHash, $addendum=null) {
    return $this->db->smartRead($this, $whatHash, $whereHash, $addendum);
  }

  /**
   * Smart Read One
   *
   * Read based on clean data, returning a single data set (where the data set is an array of fields).
   *
   * Example:
   * <pre>
   * $dataset = $this->smartReadOne(null, array('id' => 23));
   * </pre>
   *
   * @param array $whatHash
   * @param array $whereHash
   * @access public
   * @returns array $oneResultRow
   */
  function smartReadOne($whatHash, $whereHash) {
    $result = $this->db->smartRead($this, $whatHash, $whereHash, ' LIMIT 1');
    return $result[0];
  }

  /**
   * Smart Read All
   * 
   * Read all datasets, returning the datasets in an array of datasets (each dataset being an array of fields).
   *
   * Example:
   * <pre>
   * $datasets = $this->smartReadAll());</pre>
   * </pre>
   *
   * @access public
   * @returns array $allDatasets
   */
  function smartReadAll() {
    return $this->db->smartRead($this, null, null, null);
  }

}

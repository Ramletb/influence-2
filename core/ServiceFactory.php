<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * ServiceFactory.php
 *
 * Contains the {@link ServiceFactory} class.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage core
 */

/**
 * The ServiceFactory Class
 *
 * @package SP5
 * @subpackage core
 */
final class ServiceFactory extends Core {

  /**
   * Constructor
   * 
   * prevents direct instantiation.
   * 
   * @ignore
   * @access protected
   */  
  protected function __construct() {
    parent::__construct();
  }

  /**
   * Instance
   *
   * Holds a ServiceFactory instance
   * the Service Class
   *
   * @var ServiceFactory $instance
   * @access private
   * @static
   */  
  private static $instance;

  /**
   * Get Service Factory
   *
   * Get a singleton instance of the ServiceFactory class.
   * 
   * Example:
   * <pre>
   * $serviceFactory = ServiceFactory::getServiceFactory();
   * </pre>
   *
   * @access public
   * @returns ServiceFactory a {@link ServiceFactory} object.
   */  
  public static function getServiceFactory() {
    if (!isset(self::$instance)) {
      $c = __CLASS__;
      self::$instance = new $c;
    }

    return self::$instance;
  }

  /**
   * Load
   *
   * Load is a static method used to return a configured 
   * instances of the ServiceFactory class.
   *
   * Example: <br />
   * <pre>$testService = $serviceFactory->load("TEST"); // loads service from [TEST]</pre>
   *
   * @see Service
   * @param string $package Package name from environment ini
   * @access public
   */
  final public function load($package) {
    try {
      $prefix    = $this->configuration->environment[$package]['service']['class'];
      $suffix    = "Service";
      $classFile = $prefix . $suffix . '.php';
      $className = $prefix . $suffix;

      $baseClassFile = $suffix . '.php';

      require_once($baseClassFile);
      require_once($classFile);
      $serviceObject = new $className;
      $serviceObject->type   = $prefix;
      $serviceObject->service = $this->configuration->environment[$package]['service'];
      
      return $serviceObject;
    } catch (Exception $e) {
      $this->logger->error("[" . $package . "]" . " ServiceFactory EXCEPTION - Exception: ".print_r($e,1));
      return false;
    }
  }
  
}

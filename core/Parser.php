<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * Parser.php
 * 
 * Contains the abstract {@link Parser} class.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage core
 */

/**
 * The Abstract Parser Class
 *
 * @package SP5
 * @subpackage core
 */
abstract class Parser extends App {

  /**
   * Parse
   *
   * Abstract method parse.
   *
   * @param mixed $data
   * @access public
   */
  abstract public function parse($data);

}

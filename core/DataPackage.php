<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * DataPackage.php 
 *
 * Contains the {@link PayloadPkg} class.
 * Contains the {@link ClientDataPkg} class.
 * Contains the abstract {@link DataPackage} class.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage core
 */

/**
 * The PayloadPkg Class
 *
 * @package SP5
 * @subpackage core
 */
class PayloadPkg extends DataPackage { 

  /**
   * package from
   *
   * package from payload
   * 
   * @access public
   * @var string $packageFrom
   */  
  public $packageFrom = "payload";

  /**
   * Is Collection
   *
   * Is this a collection of packages?
   * 
   * @access public
   * @var string $isCollection
   */  
  public $isCollection = false;

}

/**
 * The PayloadCollectionPkg Class
 *
 * @package SP5
 * @subpackage core
 */
class PayloadCollectionPkg extends DataPackage { 

  /**
   * package from
   *
   * package from payload
   * 
   * @access public
   * @var string $packageFrom
   */  
  public $packageFrom = "payload";

  /**
   * Is Collection
   *
   * Is this a collection of packages?
   * 
   * @access public
   * @var string $isCollection
   */  
  public $isCollection = true;

  /**
   * Collection
   *
   * The collection
   * 
   * @access public
   * @var string $collection
   */  
  public $collection = array();

  /**
   * Get Collection
   *
   * @access public
   * @returns array
   */  
  public function getCollection() {
    return $this->collection;
  }  

  /**
   * Add Package
   *
   * Add a package to the collection.
   * 
   * @param PayloadPkg $payloadPkg
   * @access public
   */  
  public function addPackage(PayloadPkg $payloadPkg) {
    array_push($this->collection, $payloadPkg);
  }  

  /**
   * Get Collection Data
   *
   * @param string $delimiter
   * @access public
   * @returns array
   */
  public function getCollectionData($delimiter='___') {

    $data = array();
    
    foreach($this->collection as $pkg) {
      $levels   = array_reverse(explode($delimiter, $pkg->getName()));
      $param    = array_pop($levels);
      $keystack = $pkg;
      for($i=1;$i <= count($levels);$i++) {
	$keystack = array('_' . $levels[$i - 1] => $keystack);
      } 
      $keystack = array($param => $keystack);
      $data = array_merge_recursive($data,$keystack);
    }

    
    return $this->fixCollectionKeys($data[$param]);
  }

  /**
   * Fix Collection Keys
   *
   * @param array $arrayData
   * @access private
   */  
  private function fixCollectionKeys($arrayData) {
    $data = array();
    if(is_array($arrayData)) {
      foreach($arrayData as $k => $v) {
	if(is_array($v)) {
	  $data[substr($k,1)] = $this->fixCollectionKeys($v);
	} else {
	  $data[substr($k,1)] = $v;
	}
      }
    } else {
      $data = $arrayData;
    }
    return $data;
  }

}

/**
 * The ClientDataPkg Class
 *
 * @package SP5
 * @subpackage core
 */
class ClientDataPkg extends DataPackage { 

  /**
   * package from
   *
   * package from clientData
   * 
   * @access public
   * @var string $packageFrom
   */  
  public $packageFrom = "clientData";

  /**
   * Is Collection
   *
   * Is this a collection of packages?
   * 
   * @access public
   * @var string $isCollection
   */  
  public $isCollection = false;

}

/**
 * The Abstract DataPackage Class
 *
 * @package SP5
 * @subpackage core
 */
abstract class DataPackage extends Data {

  /**
   * name
   * 
   * @access protected
   * @var string $name
   */  
  protected $name;

  /**
   * data
   * 
   * @access protected
   * @var string $data
   */  
  protected $data;

  /**
   * orgin
   * 
   * @access protected
   * @var string $orgin
   */  
  protected $orgin;

  /**
   * package from
   *
   * @access public
   * @var string $packageFrom
   */
  Public $packageFrom = null;

  /**
   * Constructor
   * 
   * @ignore
   * @access public
   */  
  public function __construct($name=null,$data=null) {
    $this->name  = $name;
    $this->data  = $data;
    $this->orgin = get_class($this);
    parent::__construct();
  }

  /**
   * Get Orgin
   *
   * The Orgin Class
   * 
   * @access public
   * @returns string
   */  
  public function getOrgin() {
    return $this->orgin;
  }

  /**
   * Get Name
   *
   * The ogiginal key/parameter name.
   * 
   * @access public
   * @returns string
   */  
  public function getName() {
    return $this->name;
  }

  /**
   * Get Data
   *
   * The the data
   * 
   * @access public
   * @param string $default default value to return
   * @returns string
   */  
  public function getData($default=null) {
    return $this->data ? $this->data : $default;
  }

  /**
   * Get String
   *
   * The the data as a string
   * 
   * @access public
   * @param string $default default value to return
   * @returns string
   */  
  public function getString($default="") {    
    return (string) $this->data ? $this->data : $default;
  }

  /**
   * Get Int
   *
   * The the data as an int
   * 
   * @access public
   * @param int $default default value to return
   * @returns string
   */  
  public function getInt($default=0) {
    $data = ($this->data || $this->data == "0") ? ($this->data + 0) : ($default + 0);
    return $data;
  }

  /**
   * Get Array
   *
   * Returns the data as an array.
   * 
   * @access public
   * @param string $delimiter the delimiter. Defaults to ",".
   * @returns Array
   */
  public function getArray($delimiter=",") {
    return explode($delimiter, $this->getString());
  }

  /**
   * Get Hash
   *
   * Returns the data as a hash.
   * 
   * @access public
   * @param string $delimiter the delimiter. Defaults to ",".
   * @param string $kvDelimiter the key=value delimiter. Defaults to "-".
   * @returns Array
   */
  public function getHash($delimiter=",", $kvDelimiter="=") {
    $array = $this->getArray($delimiter);
    $hash  = array();

    foreach($array as $item) {
      list($k, $v) = explode($kvDelimiter, $item);
      $hash[$k] = $v;
    }

    return $hash;
  }

  /**
   * Get Hash Array
   *
   * Returns the data as a hash of arrays.
   * 
   * @access public
   * @param string $delimiter the delimiter. Defaults to ",".
   * @param string $kvDelimiter the key=value delimiter. Defaults to "-".
   * @returns Array
   */
  public function getHashArray($delimiter=",", $kvDelimiter="=") {
    $array = $this->getArray($delimiter);
    $hash  = array();

    foreach($array as $item) {
      list($k, $v) = explode($kvDelimiter, $item);

      !is_array($hash[$k]) ? $hash[$k] = array() : false;
      array_push($hash[$k], $v);

    }

    return $hash;
  }

  /**
   * Get Prepared String
   *
   * Get a prepared a string.
   *
   * @param string $default
   * @access public
   * @returns string
   */
  public function getPreparedString($default="") {
    return $this->prepareString($this->getString($default));
  }


  /**
   * Sleep
   *
   * Called by PHP when a DataPackage class obj
   * is seralized
   *
   * @ignore
   * @access public
   */  
  public function __sleep() {
    return array('name','data','orgin','packageFrom');
  }

  /**
   * to String
   *
   * Called by PHP when a DataPackage class object
   * used in the context of a string (echo, print, concatination, etc).
   *
   * @ignore
   * @access public
   */  
  public function __toString() {
    return $this->data ? $this->data : '';
  }
  
}


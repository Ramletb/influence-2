<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * Model.php 
 * 
 * Contains the abstract {@link Model} class.
 *
 * @author Brian Hull <brian.hull@sudjam.com>
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage core
 */

/**
 * The Abstract Model Class
 *
 * Models encapsulate external data and data sources, typically a 
 * database table, and provide a repository for code enforcing data integrity.
 *
 * See the {@link local:docs/r.php?t=&s=models Models} section of the framework manual. 
 *
 * @package SP5
 * @subpackage core
 */
abstract class Model extends App {

  /**
   * Model Constructor
   *
   * Model constructor is called when a
   * model is instantiated by a Factory.
   *
   * @ignore
   * @access public
   */
  public function __construct() {
    parent::__construct();
    $this->logger->trace("Instantiated " . get_class($this) . "...");
  }

  /**
   * Generate Soft GUID
   *
   * Create and return a random string of characters, which if long enough is effectively unique (hence the "soft".)
   *
   * Example:
   * <pre>
   * $guid = $this->generateSoftGUID(10);
   * </pre>
   *
   * @param integer $length
   * @access public
   * @returns string GUID
   */
  public function generateSoftGUID($length) {
    $secret = null;

    for ($i=0; $i<$length; $i++) {
      $r = mt_rand(0,2);
      $secret .= $r == 0 ? chr(mt_rand(97, 122)) : null;
      $secret .= $r == 1 ? chr(mt_rand(65, 90)) : null;
      $secret .= $r == 2 ? mt_rand(1, 9) : null;
    }

    return $secret;
  }
}

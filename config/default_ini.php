<?php 
 /*
  ____  ____  ____       _____ 
 / ___||  _ \| ___|_   _|___ / 
 \___ \| |_) |___ \ \ / / |_ \ 
  ___) |  __/ ___) \ V / ___) |
 |____/|_|   |____/ \_/ |____/

 Service Application Framework (SP5v3)

*/
/* ------------------------- END FRAMEWORK HEADER ------------------------- */

/**
 * default_ini.php 
 * 
 * Contains the default ini directives. Override this
 * entire file with custom_ini.php
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @version $Rev: 2 $
 * @package SP5
 * @subpackage core
 */

/**
 * PHP Set additional paths.
 */
ini_set('include_path', 
	'../core/frameworks' .
	':../core/databases' .
	':../core/renderers' .
	':../core/lib' .
	':../core/models' .
	':../core/services' .
	':../pear/php' .
	':../app/plugins/lib');

/**
 * PHP Set display errors. 
 */
ini_set('display_errors', false);

/**
 * PHP Set error logging.
 */
ini_set('log_errors', true);

/**
 * PHP Set error log.
 */
ini_set('error_log', '../logs/phperrors.log');

?>
<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/

/** 
 * RecapBundle.php
 * 
 * Contains the {@link RecapBundle} class.
 * 
 * @package RECAP 
 * @subpackage bundles
 */ 

require_once(C_PATH_PLUGINS . "RECAP/recaptchalib.php");

/**
 * RecapPlugin class
 *
 * @package RECAP 
 * @subpackage bundles
 */
class RecapBundle extends Bundle {

	/**
	* Set Bundle
	*
	* @access public
	* @param PayloadPkg $recaptcha_challenge_field GET string 
	* @param PayloadPkg $recaptcha_challenge_field GET string
	*/
	public function setBundle(PayloadPkg $recaptcha_challenge_field,
							  PayloadPkg $recaptcha_response_field) {

		$this->props = array();

		$this->props["recaptcha_challenge_field"] = $recaptcha_challenge_field->getString();
		$this->recaptcha_challenge_field = $this->props["recaptcha_challenge_field"];

		$this->props["recaptcha_response_field"] = $recaptcha_response_field->getString();
		$this->recaptcha_response_field = $this->props["recaptcha_response_field"];

	}

	/**
	* Check
	*
	* @access public
	*/
	public function check() {
		$pk = $this->configuration->environment["API"]["key"]["recap"]["private"];

		$pk = "6LeDPdISAAAAAP8267e6rhVMTrRMsF7gTQ_Iofdf";
		return recaptcha_check_answer($pk, $_SERVER["REMOTE_ADDR"], $this->recaptcha_challenge_field, $this->recaptcha_response_field);
	}


}

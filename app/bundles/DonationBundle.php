<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/

/** 
 * DonationBundle.php
 * 
 * Contains the {@link DonationBundle} class.
 * 
 * @package INFLUENCE 
 * @subpackage bundles
 */ 

/**
 * DonationPlugin class
 *
 * @package INFLUENCE 
 * @subpackage bundles
 */
class DonationBundle extends Bundle {

	/**
	* Set Bundle
	*
	* @access public
	*/
	public function setBundle(PayloadPkg $giftAmount,
							  PayloadPkg $recurring,
							  PayloadPkg $salutation,
							  PayloadPkg $firstName,
							  PayloadPkg $lastName,
							  PayloadPkg $address,
							  PayloadPkg $city,
							  PayloadPkg $state,
							  PayloadPkg $postalCode,
							  PayloadPkg $country,
							  PayloadPkg $email,
							  PayloadPkg $phone,
							  PayloadPkg $phoneType,
							  PayloadPkg $cardType,
							  PayloadPkg $cardNumber,
							  PayloadPkg $cvv2,
							  PayloadPkg $cardExpMonth,
							  PayloadPkg $cardExpYear,
							  PayloadPkg $recaptcha_challenge_field) {

		$this->props = array();

		$this->props["giftAmount"] = $giftAmount->getInt(0);
		$this->giftAmount = $this->props["giftAmount"];

		$this->props["recurring"] = $recurring->getInt(0);
		$this->recurring = $this->props["recurring"];

		$this->props["salutation"] = $salutation->getString();
		$this->salutation = $this->props["salutation"];

		$this->props["firstName"] = $firstName->getString();
		$this->firstName = $this->props["firstName"];

		$this->props["lastName"] = $lastName->getString();
		$this->lastName = $this->props["lastName"];

		$this->props["address"] = $address->getString();
		$this->address = $this->props["address"];

		$this->props["city"] = $city->getString();
		$this->city = $this->props["city"];

		$this->props["state"] = $state->getString();
		$this->state = $this->props["state"];

		$this->props["postalCode"] = $postalCode->getString();
		$this->postalCode = $this->props["postalCode"];

		$this->props["country"] = $country->getString();
		$this->country = $this->props["country"];

		$this->props["email"] = $email->getString();
		$this->email = $this->props["email"];

		$this->props["phone"] = $phone->getString();
		$this->phone = $this->props["phone"];

		$this->props["phoneType"] = $phoneType->getString();
		$this->phoneType = $this->props["phoneType"];

		$this->props["cardType"] = $cardType->getString();
		$this->cardType = $this->props["cardType"];

		$this->props["cardNumber"] = $cardNumber->getInt(0);
		$this->cardNumber = $this->props["cardNumber"];

		$this->props["cvv2"] = $cvv2->getInt(0);
		$this->cvv2 = $this->props["cvv2"];

		$this->props["cardExpMonth"] = $cardExpMonth->getInt(0);
		$this->cardExpMonth = $this->props["cardExpMonth"];

		$this->props["cardExpYear"] = $cardExpYear->getInt(0);
		$this->cardExpYear = $this->props["cardExpYear"];

		$this->props["recaptcha_challenge_field"] = $recaptcha_challenge_field->getString();
		$this->recaptcha_challenge_field = $this->props["recaptcha_challenge_field"];

	}
}

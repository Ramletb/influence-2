<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/
/**
 * app.php
 *
 *
 * @package INFLUENCE
 * @subpackage views
 */
?>
<!DOCTYPE html>
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
	<head>
	  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	  <title>Influence Web Administration</title>
	  <script type="text/javascript" src="/script/ic.js?t=<?php echo time(); ?>"></script>
	</head>
	<body></body>
</html>
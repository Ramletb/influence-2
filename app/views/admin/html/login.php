<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/
/**
 * login.php
 *
 *
 * @package INFLUENCE
 * @subpackage views
 */
?>
	<!DOCTYPE html>
		<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<title>Login</title>

			<style>
			body
			{
				background-color: #dfdfdf;
				text-align: center;
				font-family: ariel, helvetica, sans-serif;
				font-size: 12px;
				color: #777777;
			}

			#login
			{
				background-color: #ffffff;
				margin-top: 100px;
				border: 1px solid #777777;
				padding: 20px;
				width: 200px;
				margin-left: auto;
				margin-right: auto;
				text-align: left;
				border-radius: 10px;
				-moz-border-radius: 10px;
				-webkit-border-radius: 10px;
			}
			</style>

		</head>
	<body onload="document.login.username.focus();">
	<form name="login" method="post" action="/admin/app/">
	<div id="login">
	<table>
	<tr><td>Username</td><td><input name="username" type="text" /></td></tr>
	<tr><td>Password</td><td><input name="password" type="password" /></td></tr>
	<tr><td></td><td><input type="submit" /></td></tr>
	</table>
	</div>
	</form>
	</body>
</html>

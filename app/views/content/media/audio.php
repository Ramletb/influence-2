<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/

/**
 * audio.php
 *
 *
 * @package INFLUENCE
 * @subpackage views
 */


?>
<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>

<rss version="2.0" xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" xmlns:atom="http://www.w3.org/2005/Atom">
  <channel>
    <atom:link rel="self" type="application/rss+xml" href="http://www.influencechurch.org/content/podcast.rss"></atom:link>
    <title>Influence Church</title>
    <link>http://influencechurch.org</link>
    <pubDate><?php echo $this->scMakeReleaseDate($data["track"][0]); ?></pubDate>
    <lastBuildDate><?php echo date('D, d M Y H:i:s O'); ?></lastBuildDate>
    <language>en</language>
    <copyright>Copyright <?php echo date('Y'); ?> Influence Church</copyright>
    <webMaster>admin+podcast@influencechurch.org (Influence Church)</webMaster>
    <description>Influence Church is a non-denominational Christian church located in Anaheim Hills, California. Our mission is "To influence the world and spread God's fame, that many may believe that He is the Christ, the Son of the Living God."</description>
    <image>
      <url>http://influencechurch.org/assets/audio_podcast.jpg</url>
      <title>Influence Church</title>
      <link>http://influencechurch.org</link>
    </image>
    <itunes:explicit>no</itunes:explicit>
    <itunes:image href="http://influencechurch.org/assets/audio_podcast.jpg"/>
    <itunes:category text="Religion &amp; Spirituality"><itunes:category text="Christianity" /></itunes:category><itunes:category text="Religion &amp; Spirituality" /><itunes:category text="Society &amp; Culture" />
    <itunes:owner>
      <itunes:name>influencechurch</itunes:name>
      <itunes:email>admin+podcast@influencechurch.org</itunes:email>
    </itunes:owner>
    <itunes:author>influencechurch.org</itunes:author>
    <itunes:subtitle>Influence Church is a non-denominational Christian church located in Anaheim Hills, California. Our mission is "To influence the world and spread God's fame, that many may believe that He is the Christ, the Son of the Living God."</itunes:subtitle>

<?php foreach($data["track"] as $track) { ?>

    <item>
      <guid isPermaLink="true"><?php echo $track->purchase_url; ?></guid>
      <title><![CDATA[<?php echo $track->title; ?>]]></title>
      <pubDate><?php echo $this->scMakeReleaseDate($track); ?></pubDate>
      <link><?php echo $track->purchase_url; ?></link>
      <itunes:image href="<?php echo str_replace("https", "http", $track->artwork_url); ?>"></itunes:image>
      <enclosure type="audio/mpeg" url="http://feeds.soundcloud.com/stream/<?php echo $track->id; ?>-influencechurch-<?php echo $track->permalink; ?>.mp3" length="<?php echo $track->original_content_size; ?>"/>
      <itunes:duration><?php echo $this->millsToHHMMSS($track->duration); ?></itunes:duration>
      <itunes:author>influencechurch.org</itunes:author>
      <itunes:summary><![CDATA[<?php echo $track->description; ?>]]></itunes:summary>
      <description><![CDATA[<?php echo $track->description; ?>]]></description>
    </item>

<?php } ?>

  </channel>
</rss>

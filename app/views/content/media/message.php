<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/

/**
 * message.php
 *
 *
 * @package INFLUENCE
 * @subpackage views
 */
?>
<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>

<rss xmlns:atom="http://www.w3.org/2005/Atom" xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:media="http://search.yahoo.com/mrss/" version="2.0">

<channel>
	<title>Influence Church</title>
	<description>Sermons from Influence Church</description>
	<link>http://www.influencechurch.org</link>
	<language>en-us</language>
	<copyright>Copyright <?php echo date('Y'); ?> Influence Church</copyright>
	<lastBuildDate><?php echo date('D, d M Y H:i O'); ?></lastBuildDate>
	<pubDate><?php echo date('D, d M Y H:i O', strtotime($this->data->media->sermon[0]["media_date"])); ?></pubDate>
	<docs>http://blogs.law.harvard.edu/tech/rss</docs>
	<webMaster>iconnect@influencechurch.org (Influence Church)</webMaster>
	<ttl>10</ttl>

	<atom:link href="<?php echo $this->data->url; ?>.rss" rel="self" type="application/rss+xml" />

	<itunes:author>Influence Church</itunes:author>
	
	<itunes:category text="Religion &amp; Spirituality">
		<itunes:category text="Christianity" />
	</itunes:category>
	<media:keywords>God,Jesus,Holy,Spirit,Influence,Church,Phil,Hotsenpiller,Sermons,Sermon,Teaching</media:keywords>


	<itunes:subtitle>Sermons</itunes:subtitle>
	<itunes:summary>
	Sermons from Influence Church. Visit influencechurch.org for more information. 
	</itunes:summary>

	<itunes:owner>
		<itunes:name>Influence Church</itunes:name>
		<itunes:email>iconnect@influencechurch.org</itunes:email>
	</itunes:owner>
	<dc:creator>iconnect@influencechurch.org (Influence Church)</dc:creator>

	<itunes:explicit>No</itunes:explicit>

	<itunes:image href="http://media.influencechurch.net/logo_1400x1400.jpg" />


<?php foreach($this->data->media->sermon as $item) { ?>
	<item>
		<title><?php echo $item["media_title"] . ($item["media_sub_title"] ? " - " . $item["media_sub_title"] : false); ?></title>
		<link><?php echo $item["media_web"]; ?></link>
		<guid><?php echo $item["media_mp3"]; ?></guid>
		<description><?php echo $item["media_description"]; ?></description>
		<enclosure url="<?php echo $item["media_mp3"]; ?>" length="<?php echo $item["media_mp3_size"]; ?>" type="audio/mpeg"/>
		<category>Sermon</category>
		<pubDate><?php echo date('D, d M Y H:i O', strtotime($item["media_date"])); ?></pubDate>

		<itunes:author><?php echo $item["media_author"]; ?></itunes:author>
		<itunes:explicit>No</itunes:explicit>
		<itunes:subtitle><?php echo $item["media_description"]; ?></itunes:subtitle>
		<itunes:summary><?php echo $item["media_notes"]; ?></itunes:summary>
		<itunes:duration><?php echo $item["media_duration"]; ?></itunes:duration>
		<itunes:keywords><?php echo $item["media_tags"]; ?></itunes:keywords>
		<itunes:image href="<?php echo $item["media_thumb_large"]; ?>" />

	</item>
<?php } ?>


</channel>
</rss>
<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/

require_once('header.php');

/**
 * events.php
 *
 *
 * @package INFLUENCE
 * @subpackage views
 */
?>
<header class="jumbotron">
	<h1>Calendar</h1>
	<br />
	<?php echo $this->nl2p($data->content->calendar_blurb); ?>
	<br />
</header>

<div class="row">
	<div class="span12">
		<iframe width="100%" height="600" src="https://www.google.com/calendar/embed?showTitle=0&src=sfg5qvur2jovhu93a16974ije8%40group.calendar.google.com&ctz=America/Los_Angeles" style="border: 0"frameborder="0" scrolling="no"></iframe>
	</div>
</div>


<?php require_once("footer.php"); ?>
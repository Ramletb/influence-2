<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/


$contentTitle       = "iMOMs";
$contentDescription = $this->nl2p($data->content->moms_description);
$contentAbout       = $this->nl2p($data->content->moms_about);
$contentCalendar    = "";
$contentFeed      = $data->appConfig["feed"]["moms"];

$ogTitle       = "iMOMs - Influence Church";
$ogImage       = "http://25.media.tumblr.com/tumblr_mb1wq8MgEG1rhiijlo1_r3_1280.jpg";
$ogDescription = $data->content->moms_description;

require_once('header.php');

/**
 * moms.php
 *
 *
 * @package INFLUENCE
 * @subpackage views
 */

$page = "moms";
?>

<?php 
require_once("content-header.php"); 
require_once("content-tabs.php"); 
?>

<?php require_once("footer.php"); ?>
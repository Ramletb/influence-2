<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/

function ogTruncate($string, $limit, $break=".", $pad="...") {
	$string = strip_tags($string);

	if(strlen($string) <= $limit) {
		return $string;
	}

	if(false !== ($breakpoint = strpos($string, $break, $limit))) {
		if($breakpoint < strlen($string) - 1) {
			$string = substr($string, 0, $breakpoint) . $pad;
		}
	}

	return $string;
}

$post    = $data->ref->content->response->posts[0];
$newsTitle  = "";
$content    = "";
$ogImage    = null;

if($post->type == "photo") {
	$newsTitle   = "";
	$content = '<img src="' . $post->photos[0]->original_size->url . '" /><p></p><br />' . $post->caption;
	$ogImage = $post->photos[0]->original_size->url;
}

if($post->type == "quote") {
	$newsTitle   = "Quote";
	$content = '<div class="hero-unit"><p>' . $post->text . '</p><p><small>' . $post->source . '</small></p></div>';
}

if($post->type == "text") {
	$newsTitle   = $post->title;
	$content = $post->body;
}

if($post->type == "link") {
	$newsTitle   = "Link";
	$content = '<h2><a href="' . $post->url . '">' . $post->title . '</a></h2><p>[<a href="' . $post->url . '">' . $post->url . '</a>]</p>';
}

if($post->type == "chat") {
	$newsTitle   = $post->title;
	$content = '<dl class="dl-horizontal lead">';

	foreach($post->dialogue as $d) {
		$content .= '<dt style="line-height: 50px;">' . $d->name . '</dt>';
		$content .= '<dd style="line-height: 50px;">' . $d->phrase . '</dd>';
	}

	$content .= '</dl>';

}

if($post->type == "video") {
	$newsTitle   = "Watch";
	$content = $post->player[2]->embed_code . '<p>' . $post->caption . '</p>';
	$ogImage = $post->thumbnail_url;
}

if($post->type == "audio") {
	$newsTitle   = "Listen";
	$content = $post->player . '<p>' . $post->caption . '</p>';
	$ogImage = $post->album_art;
}

$url = "http://" . $webServer . "/content/site/section/" . $section . "/page/post/ref/" . $data->ref->id . "/title/" . $data->ref->content->response->posts[0]->slug;

$pageTitle     = $newsTitle;
$ogTitle       = $newsTitle;
$ogDescription = ogTruncate($content, 100);

require_once('header.php');

/**
 * post.php
 *
 *
 * @package INFLUENCE
 * @subpackage views
 */
?>
<script>
(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=<?php echo $facebookAppId; ?>";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>


<header class="jumbotron">
	<h1><?php echo $newsTitle; ?></h1>
	<br />
</header>

<div class="row">
	<div class="span12">

<?php 

if($content) {
	echo $content; 
} else {
	echo "<b>We're sorry. Service is temporarily unavailable.</b><br /> We are working quickly to resolve the issue. Learn more <a href=\"http://www.influencechurch.org/content/site/section/about\">about Influence Church</a>, or enjoy a <a href=\"http://www.influencechurch.org/content/site/section/message\">message online</a>.";
}

?>

<p></p><br />
<p>
	<!-- AddThis Button BEGIN -->
	<div class="addthis_toolbox addthis_default_style ">
	<a class="addthis_button_preferred_1"></a>
	<a class="addthis_button_preferred_2"></a>
	<a class="addthis_button_preferred_3"></a>
	<a class="addthis_button_preferred_4"></a>
	<a class="addthis_button_compact"></a>
	<a class="addthis_counter addthis_bubble_style"></a>
	</div>
	<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
	<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=ra-501b8a153e544944"></script>	<!-- AddThis Button END -->
</p>
	</div>
</div>


<?php require_once("footer.php"); ?>

<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/

require_once('header.php');

/**
 * live.php
 *
 *
 * @package INFLUENCE
 * @subpackage views
 */
?>

<header class="jumbotron">
	<h1>Live</h1>
	<br />
</header>

<div class="row">
	<div class="span12" id="live">
		<iframe width="480" height="386" src="http://www.ustream.tv/embed/10450696" scrolling="no" frameborder="0" style="border: 0px none transparent;"></iframe>
	</div>
</div>


<?php require_once("footer.php"); ?>

<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/

require_once('parser/TumblrParser.php');

/**
 * content-tabs.php
 *
 * DEFINE:
 *
 * $contentAbout
 * $contentFeed
 * $contentCalendar
 *
 * @package INFLUENCE
 * @subpackage views
 */

//$contentCalendar  = $data->appConfig["pageCfg"][$page]["gcal"];
$contentFeed      = $data->appConfig["pageCfg"][$page]["feed"];

?>

<ul class="nav nav-tabs" id="groupsTab">

<?php if($contentFeed && $contentFeed != "") { ?>
	<li class="<?php if(!$tabActive) { echo "active"; $tabActive = true; } ?>"><a href="#news" data-toggle="tab">News</a></li>
<?php } ?>

<?php if($data->vimeo && $data->vimeo->video) { ?>
	<li class="<?php if(!$tabActive) { echo "active"; $tabActive = true; } ?>"><a href="#video" data-toggle="tab">Video</a></li>
<?php } ?>

<?php if($contentCalendar && $contentCalendar != "") { ?>
	<li class="<?php if(!$tabActive) { echo "active"; $tabActive = true; } ?>"><a href="#calendar" data-toggle="tab">Calendar</a></li>
<?php } ?>

<?php if($contentAbout) { ?>
	<li class="<?php if(!$tabActive) { echo "active"; $tabActive = true; } ?>"><a href="#about" data-toggle="tab">About</a></li>
<?php } ?>

</ul>

<div class="tab-content">


<?php if($contentFeed && $contentFeed != "") { ?>

	<style>
		h6.newsDate {
			color: #999; 
			text-transform: uppercase; 
			padding-top: 0;
		}

		h2.newsTitle {
			line-height: 30px;
		}

	</style>

	<div class="tab-pane <?php if(!$tabContentActive) { echo "active"; $tabContentActive = true; } ?>" id="news">
	<div class="row">
		<div class="span12" id="newsBlog">

			<?php
			$tp = new TumblrParser();

			if($data->feed && $data->feed->posts) {
				foreach($data->feed->posts as $post) {
					$article = $tp->parsePost($post);

					echo '<div class="row">';
					echo '<div class="span1"><a href="/' . $page . '/' . $article["id"] . '/' . $post->slug . '"><h6 class="newsDate">' . date('M j Y', $article["timestamp"]) . '</a></h6></div>';
					echo '<div class="span11">';

					if($post->type == "photo") {
						echo '<a href="/' . $page . '/' . $article["id"] . '/' . $article["slug"] . '">';
					} else {
						echo '<a href="/' . $page . '/' . $article["id"] . '/' . $article["slug"] . '">';
						echo '<h2 class="newsTitle">' . $article["title"] . '</h2></a>';
					}

					echo $article["body"];
					echo '</div>';
					echo '</div>';
				}
			} else {
					echo "<b>We're sorry. Service is temporarily unavailable.</b><br /> We are working quickly to resolve the issue. Learn more <a href=\"http://www.influencechurch.org/content/site/section/about\">about Influence Church</a>, or enjoy a <a href=\"http://www.influencechurch.org/message\">message online</a>.";
			}
			?>

		</div>
	</div>
	</div>
<?php } ?>


<?php if($data->vimeo && $data->vimeo->video) { ?>

	<div class="tab-pane <?php if(!$tabContentActive) { echo "active"; $tabContentActive = true; } ?>" id="video">
	<div class="row">
		<div class="span12" id="newsBlog">

			<table class="table table-striped table-bordered ">
				<thead>
					<tr>
						<th>Title</th>
						<th>Video</th>
					</tr>  
				</thead>
			<?php foreach($data->vimeo->video as $video)  { ?>
			<tr>
					<td>
						<h4><?php echo $video->title; ?></h4>
			<?php
				$desc = explode("\n", $video->description);

				echo "<h5>" . $desc[0] . "<br />";
				echo $desc[1] . "</h5>\n";
				echo "<h6>" . $desc[2] . "</h6>\n";
				echo "<p>" . $desc[3] . "</p>\n";
			?>
					</td>

					<td><div style="background-image: url(<?php echo $video->thumbnails->thumbnail[2]->_content; ?>); background-repeat: no-repeat; background-position: center top; background-size: 100%;"><a href="/<?php echo $page; ?>/watch/<?php echo $video->id; ?>/<?php echo preg_replace("/\s+/", "+", ereg_replace("[^A-Za-z0-9]", " ", $video->title)); ?>"><center><img src="/images/play-button-overlay-8.png" alt="<?php echo $video->title; ?>" /></center></a></div></td>


			</tr>

			<?php } ?>
			</table>

		</div>
	</div>
	</div>
<?php } ?>



<?php if($contentCalendar && $contentCalendar != "") { ?>
	<div class="tab-pane <?php if(!$tabContentActive) { echo "active"; $tabContentActive = true; } ?>" id="calendar">
	<div class="row">
		<div class="span12">

<iframe src="https://www.google.com/calendar/embed?showTitle=0&amp;showDate=0&amp;showCalendars=0&amp;mode=AGENDA&amp;height=600&amp;wkst=1&amp;bgcolor=%23ffffff&amp;src=<?php echo $contentCalendar; ?>%40group.calendar.google.com&amp;color=%23B1365F&amp;ctz=America%2FLos_Angeles" style=" border-width:0 " width="100%" height="600" frameborder="0" scrolling="no"></iframe>

		</div>
	</div>
	</div>
<?php } ?>


<?php if($contentAbout) { ?>
	<div class="tab-pane  <?php if(!$tabContentActive) { echo "active"; $tabContentActive = true; } ?>" id="about">
	<div class="row">
		<div class="span12">
		<?php echo $contentAbout; ?>
	</div>
	</div>
<?php } ?>


</div>
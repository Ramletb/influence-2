<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/

/**
 * blocks.php
 *
 *
 * @package INFLUENCE
 * @subpackage views
 */
?>

<?php 
$row = 0;
$col = 0;
$openRow = false;
$i = 0;
foreach($data->blocks as $bid => $block) { 
	$i % 4 > 0 ? $col++ : $row++;
	$i++;

	if($col == 0) {
		$openRow = true;
		echo '<div class="row understate thumbnails">' . "\n";
	}

?>

	<div class="span3">
		<div class="thumbnail">
			<a href="<?php echo $block["url"]; ?>"><img src="<?php echo $block["photo"]; ?>" alt="<?php echo $block["title"]; ?>"></a>
			<div class="caption">
				<h2><a href="<?php echo $block["url"]; ?>"><?php echo $block["title"]; ?></a></h2>
				<?php echo $this->nl2p($block["blurb"]); ?>
			</div>
		</div>
	</div>

<?php 
	if($col == 3) {
		$col = 0;
		$openRow = false;
		
		?>
</div>
<div class="row">
	<div class="span12 icListPad">
	</div>
</div>
<?php
	}
}
?>

<?php 
	if($openRow) {
		$openRow = false;
		?>
</div>
<div class="row">
	<div class="span12 icListPad">
	</div>
</div>
<?php
	}
?>
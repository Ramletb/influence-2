<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/

require_once('header.php');

/**
 * audio.php
 *
 *
 * @package INFLUENCE
 * @subpackage views
 */
?>

<header class="jumbotron">
	<h1>Audio</h1>
	<br />
</header>

<div class="row">
	<div class="span4">
		<h2>Recent Sermons</h2>
		<h4>Listen or Download</h4>
		<p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh.</p>

		<h2>All Sermons</h2>
		<h4>iTunes Podcast</h4>
		<p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, <a href="#">tortor mauris condimentum nibh.</a></p>

	</div>
	<div class="span8">
		<iframe width="100%" height="550" scrolling="no" frameborder="no" src="http://w.soundcloud.com/player/?url=http%3A%2F%2Fapi.soundcloud.com%2Fusers%2F12394667&amp;auto_play=false&amp;show_artwork=true&amp;color=0088CC"></iframe>
	</div>
</div>



<?php require_once("footer.php"); ?>
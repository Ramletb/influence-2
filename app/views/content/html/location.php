<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/

require_once('header.php');

/**
 * location.php
 *
 *
 * @package INFLUENCE
 * @subpackage views
 */
?>
<h1>Locations</h1>

</div>

<div id="service_map_canvas" class="hidden-phone" style="width: auto; height: 400px; margin-top: 25px; margin-bottom: 25px"></div>

<?php $scripts = array("location.js"); ?>

<div class="container">

<div class="row">
	<div class="span4">
		<h2>Service Times</h2>		
		<h4><img src="/images/i_map.png" /> <?php echo $data->content->service_times; ?></h4>
		<p><?php echo $this->nl2p($data->content->service_address); ?></p>

		<h2>Online</h2>
		<p><a href="http://new.livestream.com/influencechurchoc">Join us live online every Sunday at 10am Pacific time on Livestream.</a></p>
	</div>

	<div class="span4">
		<h2>Satellite Locations</h2>
		<p><b>Big Bear</b>, California</p>
		<p><b>Abu Dhabi</b>, United Arab Emirates</p>
		<p>Email <a href="mailto://iconnect@influencechurch.org">iconnect@influencechurch.org</a> for more information.

		<br />
		<h2>Office</h2>
		<p>100 Chaparral Court, Suite 120<br />Anaheim Hills, CA 92808</p>
	</div>

	<div class="span4">
		<h2>Contact</h2>
		<p><a href="/content/site/section/about/page/contact">Contact Form</a></p>
		<?php echo $this->nl2p($data->content->contact); ?>
		<b>Mailing Address</b>
		<?php echo $this->nl2p($data->content->mailing_address); ?>
	</div>

</div>



<?php require_once("footer.php"); ?>
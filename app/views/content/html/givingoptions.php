<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/

$pageTitle = "Giving Options";
$navTitle = $pageTitle;

$ogTitle   = "Giving Options - Influence Church";
$ogImage   = "http://25.media.tumblr.com/tumblr_me9hv1jX581rilyr5o1_r1_1280.jpg";

require_once('header.php');

/**
 * givingoptions.php
 *
 *
 * @package INFLUENCE
 * @subpackage views
 */
?>

<header class="jumbotron">
	<h1>Giving Options</h1>
	<br />
</header>

<div class="row">

<div class="span12">
	<h3>Since we launched Influence Church on 2.12.12 we have remained faithful to our mission:</h3>
	<br />
	<div class="hero-unit"><p>
	"To influence the world and spread God's fame, that many may believe that He is the
	Christ, the Son of the Living God."
	</p></div>
	<p>
  Your faithfulness, prayers and dedication to the vision of Influence has permitted us to purchase and renovate our new worship center in 22 months! We continue to expand our Kingdom reach with campuses in Abu Dhabi, U.A.E. and Big Bear, California. 
</p><p>
Your generosity changes lives and makes a difference. Joy comes through giving and serving others. Influence is a great movement that was birthed by great faith. We have seen God do things that can only be explained by His divine intervention. We will continue to aggressively pursue opportunities to reach more people and plant more campuses in 2014. We will risk big because we serve a big God.
	</p>
	<p>
	Giving is a matter of the heart and when we give from a "willing heart" (Exodus 25:1-2) we demonstrate our faith in the True God. No one has given more than God and never are we more like God than when we give.
	</p>
	<br />

	<h4>Here are a few year-end giving options you might want to consider:</h4>
	<hr />
	<h4>ONLINE</h4>
	<p>
	Generosity is a kingdom value that reflects one’s attitude and commitment to Jesus. It is our goal to empower every person to become a faithful steward of God’s resources.
	</p>
	<div class="details-area">
	<div class="details">
	<p>
	On the <a href="https://www.influencechurch.org/content/site/section/donate">Online Giving page</a> select "recurring" and then the "weekly" option to ensure that Influence Church fulfills its God-called mission: "To influence the world and spread God's fame, that many may believe that He is the Christ, the Son of the Living God." You will also be able to establish your tithe as a "first" (Proverbs 3:9-10) priority in your life. Consistent giving is a reminder that God is the supplier of everything we have now and in the future. When you honor God with your tithe (10%) you open the windows of heaven (Malachi 3:10). Remember, you simply cannot out give God.
	</p>
	<p>
	Simply choose "recurring" on the <a href="https://www.influencechurch.org/content/site/section/donate">Online Giving page</a> and choose the "weekly" option to ensure that Influence Church fulfills its God-called mission to: "To influence the world and spread God's fame, that many may believe that He is the Christ, the Son of the Living God."
	</p>
	<p>
	For more information, please contact the financial office at <a href="mailto:igive@influencechurch.org">igive@influencechurch.org</a>
	Please consult with your attorney or tax consultant for legal and tax advice for specific to your situation.
	</p>
	</div>
	<a href="#" class="more-info"><span class="label">more +</span></a><br />
	</div>

<hr />

	<h4>REAL ESTATE</h4>
	<p>
	Real estate is a wonderful charitable gift that may be sold or used for ministry purposes beyond what you might envision.
	</p>
	<div class="details-area">
	<div class="details">
	<p>
	A donation of real estate can help you can avoid the hassle of selling a piece of property and the worry about getting a fair price while simultaneously realizing valuable income and estate tax deductions.
	</p>
	<ul>
		<li>Your deduction is equal to the property's full fair market value.</li>
		<li>You eliminate capital gains tax on the property's appreciation.</li>
		<li>The transfer isn't subject to the gift tax and the gift reduces your future taxable estate.</li>
	</ul>
	<p>
		For more information, please contact the financial office at <a href="mailto:igive@influencechurch.org">igive@influencechurch.org</a>
		Please consult with your attorney or tax consultant for legal and tax advice for specific to your situation.
	</p>
	</div>
	<a href="#" class="more-info"><span class="label">more +</span></a><br />
	</div>

<hr />

	<h4>LIFE INSURANCE</h4>
	<p>
	A life insurance policy with an accumulated cash value is an asset that can be redirected to enhance the ministry of Influence Church.
	</p>
	<div class="details-area">
	<div class="details">
	<p>
	There are two options for donating an insurance policy:
	</p>
	<ul>
		<li>Name Influence Church as the primary beneficiary on your policy.  Note, with this option you retain ownership of the policy and therefore do not qualify for an income tax deduction.</li>
		<li>Name Influence Church as the beneficiary and also assign us ownership of the policy as a current charitable gift. This option does provide you tax benefits.</li>
	</ul>
	<p>
		For more information, please contact the financial office at <a href="mailto:igive@influencechurch.org">igive@influencechurch.org</a>
		Please consult with your attorney or tax consultant for legal and tax advice for specific to your situation.
	</p>
	</div>
	<a href="#" class="more-info"><span class="label">more +</span></a><br />
	</div>

<hr />

	<h4>SECURITIES</h4>
	<p>
	A stock portfolio is a valuable asset that can carry substantial capital gain or appreciation in value. With careful planning, you can reduce or even eliminate federal capital gains tax while supporting Influence Church.
	</p>
	<div class="details-area">
		<div class="details">
	<p>
	As stock prices increase, so do the taxes you owe on the long-term capital gain. These taxes are charged at a rate of 15 percent for most tax payers (can be as low as 0 percent or as high as 23.8 percent for some). But, when you donate publicly traded stock you've owned for more than one year to a qualified charitable organization such as Influence Church, you enjoy two major tax benefits:
	<ul>
		<li>You will be exempt from paying capital gains taxes on any increase in value—taxes you would pay if you had otherwise sold the securities.</li>
		<li>You are entitled to a federal income tax deduction based on the current fair market value of the securities, regardless of their original cost.</li>
	</ul>
	<p>
	The income tax deduction for long-term capital gain property is limited to 30 percent of your adjusted gross income in the year you make the gift, but your excess deduction is deductible for up to five additional years.
	</p>
	<p>
		For more information, please contact the financial office at <a href="mailto:igive@influencechurch.org">igive@influencechurch.org</a>
		Please consult with your attorney or tax consultant for legal and tax advice for specific to your situation.
	</p>
	</div>
	<a href="#" class="more-info"><span class="label">more +</span></a><br />
	</div>

<hr />

	<h4>WILLS AND TRUSTS</h4>
	<p>
	Your will or trust is a wonderful way to leave a legacy that will advance the ministry of Influence Church.
	</p>
	<div class="details-area">
		<div class="details">
	<p>
	This method of giving allows for a great deal of flexibility since it is not completed until after your lifetime. Your attorney can assist you in the process of naming Influence Church in your will or irrevocable living trust. This type of gift entitles your estate to a federal estate tax charitable deduction for the full amount of your gift, thus reducing any estate tax that may be due.
	</p>
	<p><b>The official language for Influence Church is:</b></p>
	<blockquote>
	"I, [name], of [city, state, zip], give, devise and bequeath to the Influence Church in Anaheim Hills, California, whose location on the date of the drafting of this will is 5753 E. Santa Ana Canyon Road, Anaheim Hills, California 92808, [written amount or percentage of the estate or description of property] for its unrestricted use and purpose."
	</blockquote>
	<p>
		For more information, please contact the financial office at <a href="mailto:igive@influencechurch.org">igive@influencechurch.org</a>
		Please consult with your attorney or tax consultant for legal and tax advice for specific to your situation.
	</p>
		</div>
		<a href="#" class="more-info"><span class="label">more +</span></a><br />
	</div>

</div>
</div>

<script type="text/javascript">
	$(".details").hide();
	$(".less-info").hide();

	$(".more-info").click(function(e) {
		$(e.currentTarget).prev().show();
		$(e.currentTarget).hide();
		return false;
	});

</script>

<?php require_once("footer.php"); ?>
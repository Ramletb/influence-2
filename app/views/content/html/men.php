<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/


$contentTitle       = "Men's Bible Study";
$contentDescription = $this->nl2p($data->content->men_description);
$contentAbout       = $this->nl2p($data->content->men_about);
//$contentCalendar    = $data->appConfig["gcal"]["groups"];
$contentFeed      = $data->appConfig["feed"]["men"];

$ogTitle       = "Men - Influence Church";
$ogImage       = "http://24.media.tumblr.com/tumblr_mb1wofcpc31rhiijlo1_1280.jpg";
$ogDescription = $data->content->men_description;

require_once('header.php');

/**
 * men.php
 *
 *
 * @package INFLUENCE
 * @subpackage views
 */

$page = "men";
?>

<?php 
require_once("content-header.php"); 
require_once("content-tabs.php"); 
?>

<?php require_once("footer.php"); ?>
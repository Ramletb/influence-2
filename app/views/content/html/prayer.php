<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/


$contentTitle       = "Prayer";
$contentDescription = $this->nl2p($data->content->prayer_description);
$contentAbout       = $this->nl2p($data->content->prayer_about);
//$contentCalendar    = $data->appConfig["gcal"]["groups"];
//$contentFeed      = $data->appConfig["feed"]["fitness"];

$ogTitle       = "Prayer - Influence Church";
$ogImage       = "http://25.media.tumblr.com/tumblr_mb1wrnUwGc1rhiijlo1_1280.jpg";
$ogDescription = $data->content->prayer_description;


require_once('header.php');

/**
 * fitness.php
 *
 *
 * @package INFLUENCE
 * @subpackage views
 */

$page = "prayer";
?>

<?php 
require_once("content-header.php"); 
require_once("content-tabs.php"); 
?>

<?php require_once("footer.php"); ?>
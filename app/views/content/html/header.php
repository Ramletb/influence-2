<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/

if($pageTitle) {
	$title = $pageTitle;
} else {
	$title = $page ? ucfirst($page) : ucfirst($section);
}

/**
 * header.php
 *
 *
 * @package INFLUENCE
 * @subpackage views
 */
?>
<!DOCTYPE html>
<html lang="en" xmlns="https://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml">
	<head>
		<meta charset="utf-8">

		<?php if($page || $section) { ?>
			<title><?php echo $title; ?> - Influence Church</title>
		<?php } ?>

		<?php if(!$page && !$section) { ?>
			<title>Influence Church</title>
		<?php } ?>

		<meta name="google-site-verification" content="H39Vx22nZYewZ6OrkOKvPmgwGK0AGD1aWm27eGH4SvM" />

		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="Influence Church, Anaheim Hills, CA">
		<meta name="keywords" content="<?php echo $keywords; ?>" >
		<meta name="zipcode" content="92808" >

		<meta name="geo.placename" content="Influence Church at <?php echo str_replace(array("\r", "\r\n", "\n"), ', ', $data->content->service_address); ?>" />
		<meta name="geo.position" content="33.865860, -117.744126" />
		<meta name="geo.region" content="US-CA" />
		<meta name="ICBM" content="33.865860, -117.744126" />

		<meta property="og:type" content="non_profit">
		<meta property="og:site_name" content="Influence Church">

		<?php if($ogTitle) { ?><meta property="og:title" content="<?php echo $ogTitle; ?>"><?php } ?>

		<?php if($ogImage) { ?><meta property="og:image" content="<?php echo $ogImage; ?>"><?php } ?>

		<?php if($ogDescription) { ?><meta property="og:description" content="<?php echo $ogDescription; ?>"><?php } ?>

<?php 

$addr = explode("\n", $data->content->service_address); 
$loc  = explode(",", $addr[2]);

?>

		<meta property="og:street-address" content="<?php echo $addr[1]; ?>">
		<meta property="og:locality" content="<?php echo $loc[0]; ?>">
		<meta property="og:region" content="<?php echo $loc[1]; ?>">
		<meta property="og:postal-code" content="<?php echo $loc[2]; ?>">
		<meta property="og:country-name" content="United States">
		<meta property="og:email" content="iconnect@influencecurch.org">
		<meta property="og:phone_number" content="+1-714-455-3910">
		<meta property="og:latitude" content="33.865860">
		<meta property="og:longitude" content="-117.744126">

		<meta property="fb:app_id" content="<?php echo $facebookAppId; ?>" />

		<script>var googleKey = "<?php echo $google; ?>";</script>
		<script src="/js/jquery-1.8.3.min.js"></script>

		<link href="/bootstrap/css/bootstrap.css" rel="stylesheet">
		<style type="text/css">
			body {
				padding-top: 60px;
				padding-bottom: 40px;
			}
		</style>

		<!-- link rel="alternate" type="application/rss+xml" title="Influence Church Sermons" href="/content/site/section/message.rss" / -->

                <link href="https://plus.google.com/+InfluenceChurchAnaheim" rel="publisher" />

<script type="text/javascript">
  (function() {
    var po = document.createElement("script"); po.type = "text/javascript"; po.async = true;
    po.src = "https://apis.google.com/js/plusone.js?publisherid=109740753686675257767";
    var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(po, s);
  })();
</script>

		<link href="/bootstrap/css/bootstrap.css" rel="stylesheet">
		<link href="/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
		<link href="/css/ReferenceTagging.css" rel="stylesheet">
		<link href="/css/docs.css" rel="stylesheet">
		<link href="/css/main.css" rel="stylesheet">

		<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
			<script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

		<!-- fav and touch icons -->
		<link rel="shortcut icon" href="/images/influence_favicon.png">
		<link rel="apple-touch-icon" sizes="144x144" href="/images/apple-touch-icon-144.png">
		<link rel="apple-touch-icon" sizes="114x114" href="/images/apple-touch-icon-114.png">
		<link rel="apple-touch-icon" sizes="72x72" href="/images/apple-touch-icon-72.png">
		<link rel="apple-touch-icon" href="/images/apple-touch-icon-57.png">

<?php

	if($section == 'donate' || $page == 'contact') {
		echo '<script>if (window.location.protocol != "https:") { window.location.href = "https:" + window.location.href.substring(window.location.protocol.length); } </script>';
	} else {
		echo '<script>if (window.location.protocol != "http:") { window.location.href = "http:" + window.location.href.substring(window.location.protocol.length); } </script>';
	}

?>

	</head>

	<body data-spy="scroll" data-target=".subnav" data-offset="50">

		<div class="navbar navbar-fixed-top navbar-inverse">
			<div class="navbar-inner">
				<div class="container">
					<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</a>
					<a class="brand" href="/"><img src="/images/influence_light_20x93.png" alt="Influence Church" /></a>
					<div class="nav-collapse">
						<ul class="nav">

							<li class="<?php if($section == "home"  || !$section) { echo "active"; } ?>"><a href="/">Home</a></li>

							<li class="dropdown <?php if($section == "about") { echo "active"; } ?>" id="aboutMenu">
								<a class="dropdown-toggle" data-toggle="dropdown" href="#aboutMenu">About Us <b class="caret"></b></a>
								<ul class="dropdown-menu">
									<li><a href="/about">Vision</a></li>
									<li><a href="/about#StatementOfFaith">Statement of Faith</a></li>
									<li><a href="/location">Location</a></li>
									<li><a href="/calendar">Calendar</a></li>
									<li><a href="/leadership">Leadership Team</a></li>
									<li><a href="/contact">Contact Us</a></li>
									<li class="divider"></li>
									<li class="nav-header">Social Links</li>
									<li><a href="/facebook" target="_blank">Facebook</a></li>
									<li><a href="/twitter" target="_blank">Twitter</a></li>
									<li><a href="/pinterest" target="_blank">Pinterest</a></li>
									<li><a href="/google" rel="publisher" target="_blank">Google+</a></li>
									<li class="divider"></li>
									<li class="nav-header">Content</li>
									<li><a href="/vimeo" target="_blank">Vimeo</a></li>
									<li><a href="/youtube" target="_blank">Youtube</a></li>
									<li><a href="/live" target="_blank">Livestream</a></li>
									<li><a href="/soundcloud" target="_blank">Soundcloud</a></li>
									<li><a href="/podcast" target="_blank">iTunes</a></li>
								</ul>
							</li>

							<li class="<?php if($section == "calendar") { echo "active"; } ?>"><a href="/calendar">Calendar</a></li>

							<li class="<?php if($section == "message") { echo "active"; } ?>"><a href="/message">Message</a></li>
							
							<li class="dropdown <?php if($section == "ministries") { echo "active"; } ?>" id="ministriesMenu">
								<a class="dropdown-toggle" data-toggle="dropdown" href="#ministriesMenu">Ministries <b class="caret"></b></a>
								<ul class="dropdown-menu">
									<li><a href="/content/site/section/ministries">Overview</a></li>
									<li class="divider"></li>
									<li class="nav-header">Categories</li>
									<li><a href="/children">Children</a></li>
									<li><a href="/community">Community</a></li>
									<li><a href="/events">Events</a></li>
									<li><a href="/fitness">Fitness</a></li>
									<li><a href="/groups">Groups</a></li>
									<li><a href="/men">Men</a></li>
									<li><a href="/moms">Moms</a></li>
									<li><a href="/prayer">Prayer</a></li>
									<li><a href="/students">Students</a></li>
									<li><a href="/women">Women</a></li>
									<li><a href="/worship">Worship</a></li>
								</ul>
							</li>

							<li><a href="https://shop.influencechurch.org">Shop</a></li>


							<li class="<?php if($section == "donate") { echo "active"; } ?>"><a href="https://www.influencechurch.org/donate">Donate</a></li>


						</ul>
					</div><!--/.nav-collapse -->
				</div>
			</div>
		</div>
<?php if($noContainer) { } else { ?>
		<div class="container">
<?php } ?>

<?php if($page) { ?>
		<ul class="breadcrumb">
			<li>
				<a href="/">Home</a> <span class="divider">/</span>
			</li>
			<li>
			<?php 
				$navSectionTitle = $navSectionTitle ? $navSectionTitle : ucfirst($section);
				if($navSectionTitle == "Youngadults") {
					$navSectionTitle = "Young Adults";
				}
			?>
				<a href="/<?php echo $section; ?>"><?php echo $navSectionTitle; ?></a> <span class="divider">/</span>
			</li>
			<?php 
				$navTitle = $navTitle ? $navTitle : ucfirst($page);
				if($navTitle == "Youngadults") {
					$navTitle = "Young Adults";
				}
			?>
				<li class="active"><?php $navTitle; ?></li>
		</ul>
<?php } ?>
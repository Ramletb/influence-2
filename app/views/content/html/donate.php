<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/

require_once('header.php');

/**
 * donate.php
 *
 *
 * @package INFLUENCE
 * @subpackage views
 */
?>
<script src="/js/OnlineRegistration.js?<?php time(); ?>"></script>

<header class="jumbotron">
	<h1>Donate</h1>
	<br />
</header>

<div class="row">

<div class="span5">
		<h3>Giving to God</h3>
		<?php echo $this->nl2p($data->content->giving_to_god); ?>
		<hr>
		<h3>Questions</h3>
		<hr>
		<h4>How will my contribution appear on my statement?</h4>
		<p>It may take a day or so for the donation to appear on your statement. 
		You can locate your donation with this type of entry: BB *INFLUENCE CHURCH. NOTE: The donation may initially apear as *BB NONPROFIT 
		in online statements.</p>
		<hr>
		<h4>What if I enter the wrong amount?</h4>
		<p>If you submit the wrong amount and would like to correct it you may contact
		<a href="mailto:igive@influencechurch.org">igive@influencechurch.org</a> and we will be happy to help you resolve the error.</p>
		<hr>
		<h4>Can I mail check?</h4>
		<p>Yes, you can make checks payable to “Influence Church”. Our mailing address is:</p>
		<?php echo $this->nl2p($data->content->mailing_address); ?>
</div>


<div class="span7">
<?php 
	if($processed) {
?>
		<h3>Thank You!</h3>
		<p>
			Please contact <a href="mailto:igive@influencechurch.org">igive@influencechurch.org</a> with any questions regarding your donation.
		</p>
<?php
	} else {
		require_once("donateForm.php");
	}

?>
</div>


</div>

<?php $scripts = array("jquery.validate.min.js","additional-methods.min.js","donate.js"); ?>

<?php require_once("footer.php"); ?>
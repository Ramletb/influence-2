<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/

/**
 * footer.php
 *
 *
 * @package INFLUENCE
 * @subpackage views
 */
?>

<hr>

<footer>
	<p>&copy; Influence Church 2012-<?php echo date("o"); ?></p>
</footer>

	</div> <!-- /container -->

 	<script src="/bootstrap/js/bootstrap.min.js"></script>


	<script src="/js/main.js"></script>
<?php

if(is_array($scripts)) {
	foreach($scripts as $script) {
		echo "        <script src=\"/js/" . $script  . "\"></script>\n";
	}
}
?>


	<script src="https://bible.logos.com/jsapi/referencetagging.js" type="text/javascript"> </script> 
	<script type="text/javascript">
		Logos.ReferenceTagging.lbsBibleVersion = "NKJV";
		Logos.ReferenceTagging.lbsLinksOpenNewWindow = true;
		Logos.ReferenceTagging.lbsLogosLinkIcon = "dark";
		Logos.ReferenceTagging.lbsNoSearchTagNames = [ "h2", "h3", "h3" ];
		Logos.ReferenceTagging.lbsTargetSite = "biblia";
		Logos.ReferenceTagging.lbsCssOverride = true;
		Logos.ReferenceTagging.tag();
	</script>

	<script type="text/javascript">
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', '<?php echo $googleAnalytics; ?>']);
	  _gaq.push(['_trackPageview']);

	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	</script>

	<script>
	  // hack for bootstrap 2.2.1 doropdown menu bug on touch devices
	$('.dropdown-menu').on('touchstart.dropdown.data-api', function(e) { e.stopPropagation() });
        </script>

  </body>
</html>
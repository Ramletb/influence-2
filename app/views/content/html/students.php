<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/


$contentTitle       = "Students";
$contentDescription = $this->nl2p($data->content->students_description);
$contentAbout       = $this->nl2p($data->content->students_about);
//$contentCalendar    = $data->appConfig["gcal"]["groups"];
$contentFeed      = $data->appConfig["feed"]["students"];

$ogTitle       = "Students - Influence Church";
$ogImage       = "http://24.media.tumblr.com/tumblr_mb1wt8veBR1rhiijlo1_1280.jpg";
$ogDescription = $data->content->students_description;

require_once('header.php');

/**
 * students.php
 *
 *
 * @package INFLUENCE
 * @subpackage views
 */

$page = "students";
?>

<?php 
require_once("content-header.php"); 
require_once("content-tabs.php"); 
?>

<?php require_once("footer.php"); ?>
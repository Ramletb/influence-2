<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/


$contentTitle       = "iConnect";
$contentDescription = $this->nl2p($data->content->connect_description);
$contentAbout       = $this->nl2p($data->content->connect_about);
//$contentCalendar    = $data->appConfig["gcal"]["groups"];
//$contentFeed      = $data->appConfig["feed"]["connect"];

require_once('header.php');

/**
 * community.php
 *
 *
 * @package INFLUENCE
 * @subpackage views
 */

$page = "connect";
?>

<?php 
require_once("content-header.php"); 
require_once("content-tabs.php"); 
?>

<?php require_once("footer.php"); ?>
<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/


$contentTitle       = "Worship";
$contentDescription = $this->nl2p($data->content->worship_description);
$contentAbout       = $this->nl2p($data->content->worship_about);
//$contentCalendar    = $data->appConfig["gcal"]["groups"];
$contentFeed      = $data->appConfig["feed"]["worship"];

$ogTitle       = "Worship - Influence Church";
$ogImage       = "http://25.media.tumblr.com/tumblr_mb1wgvsrjz1rhiijlo1_r1_1280.jpg";
$ogDescription = $data->content->worship_description;

require_once('header.php');

/**
 * moms.php
 *
 *
 * @package INFLUENCE
 * @subpackage views
 */

$page = "worship";
?>

<?php 
require_once("content-header.php"); 
require_once("content-tabs.php"); 
?>

<?php require_once("footer.php"); ?>
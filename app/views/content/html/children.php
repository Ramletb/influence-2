<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/


$contentTitle       = "Children";
$contentDescription = $this->nl2p($data->content->children_description);
$contentAbout       = $this->nl2p($data->content->children_about);
//$contentCalendar    = $data->appConfig["gcal"]["groups"];
$contentFeed      = $data->appConfig["feed"]["children"];

$ogTitle       = "Influence Church - Children";
$ogImage       = "http://25.media.tumblr.com/tumblr_mb1vx88EnK1rhiijlo1_1280.jpg";
$ogDescription = $data->content->children_description;

require_once('header.php');

/**
 * children.php
 *
 *
 * @package INFLUENCE
 * @subpackage views
 */

$page = "children";
?>

<?php 
require_once("content-header.php"); 
require_once("content-tabs.php"); 
?>

<?php require_once("footer.php"); ?>
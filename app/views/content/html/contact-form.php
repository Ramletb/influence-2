<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/

/**
 * contact-form.php
 *
 * DEFINE:
 *
 * $contactType
 *
 * @package INFLUENCE
 * @subpackage views
 */

?>

<div id="contactForm">
	<div class="row">
		<div class="span9" id="contactFormContent">

			<form class="form-horizontal" action="https://docs.google.com/spreadsheet/formResponse" method="GET" id="ss-form">

			<div class="control-group">
				<label class="control-label" for="entry_0">First Name</label>
				<div class="controls">
						<input size="16" type="text" name="entry.0.single" value="" id="entry_0" />
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="entry_1">Last Name</label>
				<div class="controls">
						<input size="16" type="text" name="entry.1.single" value="" id="entry_1" />
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="entry_2">Email</label>
				<div class="controls">
						<div class="input-prepend">
							<span class="add-on"><i class="icon-envelope"></i></span><input size="16" type="text" name="entry.2.single" value="" id="entry_2" />
						</div>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="entry_3">Phone Number</label>
				<div class="controls">
					<div class="input-prepend">
						<span class="add-on"><i class="icon-signal"></i></span><input size="16" type="text" name="entry.3.single" value="" id="entry_3" />
					</div>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="entry_9">Mailing Address</label>
				<div class="controls">
						<textarea name="entry.9.single" rows="4" cols="75" class="ss-q-long" id="entry_9"></textarea>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="entry_6">Leave a Message</label>
				<div class="controls">
						<textarea name="entry.6.single" rows="8" cols="75" id="entry_6"></textarea>
				</div>
			</div>

			<input type="hidden" name="formkey" value="dFlBeGhzc19NbWdFcHFMUHdSWXZ1V2c6MQ" id="formkey" />
			<input type="hidden" name="entry.7.single" value="<?php echo $contactType; ?>" id="entry_7" />
			<br />
			<input type="submit" name="submit" value="Submit">

			</form>
		</div>
	</div>
</div>

<script type="text/javascript">

	var frm = $('#ss-form');
	frm.submit(function () {
		$.ajax({
			type: frm.attr('method'),
			url: frm.attr('action'),
			data: frm.serialize()
		});
		$("#contactFormContent").fadeOut(500, function() {
			$("#contactFormContent").html('<p>Thank you.</p>').fadeIn(500);
		});


		return false;
	});

</script>
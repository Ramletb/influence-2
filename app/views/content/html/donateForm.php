<form id="donationForm" class="form-horizontal" method="post" action="/content/donate/section/donate/">
<fieldset>
	<h3>Gift Information</h3>

	<div class="control-group">
		<label class="control-label" for="appendedPrependedInput">Gift Amount</label>
		<div class="controls">
			<div class="input-prepend">
				<span class="add-on">$</span></span><input class="span1" id="giftAmount" name="giftAmount" size="16" type="text" value="<?php echo $giftAmount; ?>">
			</div>
			<p class="help-block error hidden"></p>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="recurring">Recurring</label>
		<div class="controls">
			<select id="recurring" name="recurring">
				<option value="0"  <?php echo ($recurring == 0)  ? " selected" : "" ?>>One Time</option>
				<option value="1"  <?php echo ($recurring == 1)  ? " selected" : "" ?>>Annually</option>
				<option value="2"  <?php echo ($recurring == 2)  ? " selected" : "" ?>>Semi-Annually</option>
				<option value="4"  <?php echo ($recurring == 4)  ? " selected" : "" ?>>Quarterly</option>
				<option value="6"  <?php echo ($recurring == 6)  ? " selected" : "" ?>>Bi-Monthly</option>
				<option value="12" <?php echo ($recurring == 12) ? " selected" : "" ?>>Monthly</option>
				<option value="24" <?php echo ($recurring == 24) ? " selected" : "" ?>>Semi-Monthly</option>
				<option value="26" <?php echo ($recurring == 26) ? " selected" : "" ?>>Bi-Weekly</option>
				<option value="52" <?php echo ($recurring == 52) ? " selected" : "" ?>>Weekly</option>
			</select>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="appendedPrependedInput"></label>
		<div class="controls">
			<a href="/content/site/section/donate/page/givingoptions" target="_blank">more giving options here.</a>
		</div>
	</div>
<!--
	<div class="control-group">
		<label class="control-label" for="designation">Designation</label>
		<div class="controls">
			<select id="designation" name="designation">
				<option value="general" <?php echo ($designation == "general" || !$designation)  ? " selected" : "" ?>>General</option>
				<option value="building" <?php echo ($designation == "building")  ? " selected" : "" ?>>Building</option>
			</select>
		</div>
	</div>
-->

	<h3>Contact Information</h3>

	<div class="control-group">
		<label class="control-label" for="salutation">Title</label>
		<div class="controls">
		<select name="salutation" id="salutation">
			<option value=""></option>
			<option value="Mr."<?php  echo ($salutation == "Mr.")   ? " selected" : "" ?>>Mr.</option>
			<option value="Mrs."<?php echo ($salutation == "Mrs.")  ? " selected" : "" ?>>Mrs.</option>
			<option value="Miss"<?php echo ($salutation == "Miss.") ? " selected" : "" ?>>Miss</option>
			<option value="Ms."<?php  echo ($salutation == "Ms.")   ? " selected" : "" ?>>Ms.</option>
			<option value="Dr."<?php  echo ($salutation == "Dr.")   ? " selected" : "" ?>>Dr.</option>
		</select>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="firstName">First Name</label>
		<div class="controls">
				<input class="span2" id="firstName" name="firstName" size="16" type="text" value="<?php echo $firstName; ?>">
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="lastName">Last Name</label>
		<div class="controls">
				<input class="span2" id="lastName" name="lastName" size="16" type="text" value="<?php echo $lastName; ?>">
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="address">Address</label>
		<div class="controls">
				<input class="span3" id="address" name="address" size="16" type="text" value="<?php echo $address; ?>">
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="city">City</label>
		<div class="controls">
				<input class="span3" id="city" name="city" size="16" type="text" value="<?php echo $city; ?>">
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="state">State</label>
		<div class="controls">
		<select name="state" id="state" name="state">
		<script type="text/javascript">
		//<![CDATA[
			writeStates("<?php echo $state; ?>");
		//]]>
		</script>
		</select>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="postalCode">Postal Code</label>
		<div class="controls">
				<input class="span3" id="postalCode" name="postalCode" size="16" type="text" value="<?php echo $postalCode; ?>">
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="country">Country</label>
		<div class="controls">
		<select name="country" id="country" name="country">
		<script type="text/javascript">
		//<![CDATA[
			writeCountries("<?php echo $country; ?>");
		//]]>
		</script>
		</select>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="email">Email Address</label>
		<div class="controls">
			<div class="input-prepend">
				<span class="add-on"><i class="icon-envelope"></i></span><input class="span2" id="email" name="email" size="16" type="text" value="<?php echo $email; ?>">
			</div>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="phone">Phone</label>
		<div class="controls">
			<div class="input-prepend">
				<span class="add-on"><i class="icon-signal"></i></span><input class="span2" id="phone" name="phone" size="16" type="text" value="<?php echo $phone; ?>">
			</div>
		</div>
	</div>


	<div class="control-group">
		<div class="controls">
		<select id="phoneType" name="phoneType">
			<option label="Mobile"   value="Mobile"   <?php echo ($phoneType == "Mobile")   ? " selected" : "" ?>>Mobile</option>
			<option label="Home"     value="Home"     <?php echo ($phoneType == "Home")     ? " selected" : "" ?>>Home</option>
			<option label="Business" value="Business" <?php echo ($phoneType == "Business") ? " selected" : "" ?>>Business</option>

		</select>
		</div>
	</div>

	<h3>Payment Information</h3>

	<div class="control-group">
		<label class="control-label" for="cardType">Card Type</label>
		<div class="controls">
		<select name="cardType" id="cardType" name="cardType">
			<option></option>
			<option label="Visa"             value="Visa"             <?php echo ($cardType == "Visa")             ? " selected" : "" ?>>Visa</option>
			<option label="MasterCard"       value="MasterCard"       <?php echo ($cardType == "MasterCard")       ? " selected" : "" ?>>MasterCard</option>
			<option label="Discover"         value="Discover"         <?php echo ($cardType == "Discover")         ? " selected" : "" ?>>Discover</option>
			<option label="American Express" value="American Express" <?php echo ($cardType == "American Express") ? " selected" : "" ?>>American Express</option>
		</select>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="cardNumber">Card Number</label>
		<div class="controls">
				<input class="span3" id="cardNumber" name="cardNumber" size="16" type="text" value="<?php echo $cardNumber; ?>">
		</div>
	</div>

	<div class="modal hide fade" id="cvv2Help">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>CVV2</h3>
		</div>
		<div class="modal-body understate">
			<p>The 3 or 4 digit Security Code (CVV2) can be found on the back of your credit card.</p>
			<img src="/images/cvv2_image.gif" />
			<p>For <b>American Express</b>, the Security Code (CID) can be found on the front of the card.</p>
			<img src="/images/cid_Image.gif" />
		</div>
		<div class="modal-footer">
			<a href="#" class="btn btn-primary" data-dismiss="modal">Close</a>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="cvv2">CVV2</label>
		<div class="controls">
				<input class="span1" id="cvv2" name="cvv2" size="16" type="text" value="<?php echo $cvv2; ?>">
				<a class="btn btn-small btn-info" data-toggle="modal" href="#cvv2Help" ><i class="icon-info-sign icon-white"></i> Help</a>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="cardExpMonth">Expiration Month</label>
		<div class="controls">
			<select name="cardExpMonth" class="span2" id="cardExpMonth" name="cardExpMonth">
				<option></option>
				<script type="text/javascript">
				//<![CDATA[
				writeMonths(<?php echo $cardExpMonth; ?>);
				//]]>
				</script>
			</select>
			<p class="help-block error hidden"></p>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="cardExpYear">Expiration Year</label>
		<div class="controls">
			<select name="cardExpYear" class="span2" id="cardExpYear" name="cardExpYear">
				<option></option>
				<script type="text/javascript">
				//<![CDATA[
				writeYears(-1, 10<?php echo $cardExpYear ? "," . $cardExpYear : null; ?>);
				//]]>
				</script>
			</select>
		</div>
	</div>

	<script type="text/javascript">
	 var RecaptchaOptions = {
	    theme : 'white'
	 };
	 </script>

	<div class="control-group"<?php if($recapFail) {  echo ' style="background-color: #B94A48;"'; } ?>>
	<div class="controls">
	<script type="text/javascript" src="https://www.google.com/recaptcha/api/challenge?k=<?php echo $this->configuration->environment["API"]["key"]["recap"]["public"]; ?>"></script>
	<noscript>
		<iframe src="https://www.google.com/recaptcha/api/noscript?k=<?php echo $this->configuration->environment["API"]["key"]["recap"]["public"]; ?>" height="300" width="500" frameborder="0"></iframe><br>
		<textarea name="recaptcha_challenge_field" rows="3" cols="40"></textarea>
		<input type="hidden" name="recaptcha_response_field" value="manual_challenge">
	</noscript>
	</div>
	</div>

	<div class="form-actions">
		<?php
			if($apiFault) {
		?>
		<p><div class="alert alert-error">
		  <?php echo $apiFault; ?>
		</div></p>

		<?php
			}
		?>


		<?php
			if($recapFail) {
		?>
		<p><div class="alert alert-error">
		  <b>Security Check Failed</b>: Please re-type the words in the box above. If you would like a new set of challenge words, try clicking the small refresh icon in the box above. If you have questions please email <a href="mailto:igive@influencechurch.org">igive@influencechurch.org</a>.
		</div></p>

		<?php
			}
		?>

		<button type="submit" id="submitDonationBtn" class="btn btn-primary">Submit Donation</button>
	</div>

</fieldset>

</form>

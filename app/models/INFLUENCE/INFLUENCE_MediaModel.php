<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/

/** 
 * INFLUENCE_MediaModel.php
 * 
 * Contains the {INFLUENCE_MediaModel} class.
 * (this file may be modified safely)
 * 
 * @version $Rev: $ 
 * @package INFLUENCE 
 * @subpackage models
 */ 

/**
 * Requires structure
 */
require_once('../app/models/INFLUENCE/generated/INFLUENCE_MediaStructure.php');

/**
 * INFLUENCE_MediaModel class
 *
 * @package INFLUENCE 
 * @subpackage models
 */
class INFLUENCE_MediaModel extends INFLUENCE_MediaStructure {
  
}

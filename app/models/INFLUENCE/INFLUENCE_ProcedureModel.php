<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/

/** 
 * INFLUENCE_ProcedureModel.php
 * 
 * Contains the {INFLUENCE_ProcedureModel} class.
 * (this file may be modified safely)
 * 
 * @version $Rev: $ 
 * @package INFLUENCE 
 * @subpackage models
 */ 

/**
 * Requires INFLUENCE_Procedure
 */
require_once('../app/models/INFLUENCE/generated/INFLUENCE_Procedure.php');

/**
 * INFLUENCE_ProcedureModel class
 *
 * @package INFLUENCE 
 * @subpackage models
 */
class INFLUENCE_ProcedureModel extends INFLUENCE_Procedure {
  
}

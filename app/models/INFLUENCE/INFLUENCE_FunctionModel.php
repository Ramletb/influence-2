<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/

/** 
 * INFLUENCE_FunctionModel.php
 * 
 * Contains the {INFLUENCE_FunctionModel} class.
 * (this file may be modified safely)
 * 
 * @version $Rev: $ 
 * @package INFLUENCE 
 * @subpackage models
 */ 

/**
 * Requires INFLUENCE_Function
 */
require_once('../app/models/INFLUENCE/generated/INFLUENCE_Function.php');

/**
 * INFLUENCE_FunctionModel class
 *
 * @package INFLUENCE 
 * @subpackage models
 */
class INFLUENCE_FunctionModel extends INFLUENCE_Function {
  
}

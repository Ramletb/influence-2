<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/

 /* 
 / Generated By ModelBuilderPlugin.php: $Rev: 2 $ 
 /
 / THIS FILE MAY BE OVERWRITTEN
 / THIS IS A GENERATED SCRIPT
 / DO NOT EDIT THIS FILE
*/ 

/** 
 * INFLUENCE_Function.php (Generated)
 * 
 * Contains the {INFLUENCE_Function} class.
 * 
 * @version Generated By ModelBuilderPlugin.php: $Rev: 2 $ 
 * @package INFLUENCE 
 * @subpackage models
 */ 

/**
 * Requires appropriate model subclass
 */
require_once('../core/models/DatabaseModel.php');

/** 
 * INFLUENCE_Function Class
 * 
 * @version GENERATED by ModelBuilderPlugin.php 
 * @package INFLUENCE 
 * @subpackage models
 */ 
abstract class INFLUENCE_Function extends DatabaseModel { 

  /**
   * Specify package name.
   *
   * @var string $PACKAGE
   * @access protected
   */
  protected $PACKAGE='INFLUENCE';

  
}

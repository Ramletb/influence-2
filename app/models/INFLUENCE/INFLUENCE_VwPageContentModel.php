<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/

/** 
 * INFLUENCE_VwPageContentModel.php
 * 
 * Contains the {INFLUENCE_VwPageContentModel} class.
 * (this file may be modified safely)
 * 
 * @version $Rev: $ 
 * @package INFLUENCE 
 * @subpackage models
 */ 

/**
 * Requires structure
 */
require_once('../app/models/INFLUENCE/generated/INFLUENCE_VwPageContentStructure.php');

/**
 * INFLUENCE_VwPageContentModel class
 *
 * @package INFLUENCE 
 * @subpackage models
 */
class INFLUENCE_VwPageContentModel extends INFLUENCE_VwPageContentStructure {
  
}

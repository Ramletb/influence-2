<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/

/** 
 * INFLUENCE_RefCacheModel.php
 * 
 * Contains the {INFLUENCE_RefCacheModel} class.
 * (this file may be modified safely)
 * 
 * @version $Rev: $ 
 * @package INFLUENCE 
 * @subpackage models
 */ 

/**
 * Requires structure
 */
require_once('../app/models/INFLUENCE/generated/INFLUENCE_RefCacheStructure.php');

/**
 * INFLUENCE_RefCacheModel class
 *
 * @package INFLUENCE 
 * @subpackage models
 */
class INFLUENCE_RefCacheModel extends INFLUENCE_RefCacheStructure {

	/**
	* Upsert
	*
	* @param string $source
	* @param string $ref
	* @param string $content
	* @return array
	* @access public
	*/
	public function upsert($source, $ref, $content) {
		$this->db->execute("INSERT INTO " . $this->TABLE_NAME . "(`ref`, `source`, `content`) VALUES (" . $this->db->prepareString($ref) . "," . $this->db->prepareString($source) . "," . $this->db->prepareString($content) . ") ON DUPLICATE KEY UPDATE `content` = VALUES(content)");
	}

}

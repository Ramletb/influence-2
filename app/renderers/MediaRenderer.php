<?php 
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/

/**
 * MediaRenderer.php
 *
 * Contains the {@link HTMLRenderer} class.
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @package INFLUENCE
 * @subpackage renderer
 */

/**
 * The MediaRenderer Class
 *
 *
 * @package INFLUENCE
 * @subpackage renderer
 */
class MediaRenderer extends PresentationRenderer {

	/**
	* Content Type
	*
	* @access public 
	*/
	public function contentType() {
		return "content-type: application/rss+xml";
	}

	/**
	* Render
	*
	* Render an RSS Feed
	*
	* @access public 
	* @param string $view an optional view parameter.
	*/
	public function render($view=null) {
		$this->loadView($view);
	}

	/**
	* SC Make Release Date
	*
	* @access public 
	* @param object $track a soundcloud track
	*/
	public function scMakeReleaseDate($track) {
		$date = $track->release_year . "-" . $track->release_month . "-" . $track->release_day;
		return date('D, d M Y H:i:s O', strtotime($date));
	}


	/**
	* Milliseconds to HH:MM:SS
	*
	* @access public 
	* @param object $track a soundcloud track
	*/
	public function millsToHHMMSS($mills) {

		$msec_hh = 1000 * 60 * 60; // millisecs per hour
		$msec_mm = 1000 * 60; // millisecs per minute
		$msec_ss = 1000; // millisecs per second

		return sprintf("%02s:%02s:%02s", (int)($mills / $msec_hh), 
										 (int)(($mills % $msec_hh) / $msec_mm), 
										 (int)((($mills % $msec_hh) % $msec_mm) / $msec_ss));
	}

}

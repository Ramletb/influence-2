<?php 
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/

/**
 * HTMLRenderer.php
 *
 * Contains the {@link HTMLRenderer} class.
 *
 * @author Craig Johnston <craig.johnston@sudjam.com>
 * @package INFLUENCE
 * @subpackage renderer
 */

/**
 * The HTMLRenderer Class
 *
 *
 * @package INFLUENCE
 * @subpackage renderer
 */
class HTMLRenderer extends PresentationRenderer {

  /**
   * Render
   *
   * Render an HTML webpage.
   *
   * @access public 
   * @param string $view an optional view parameter.
   */
  public function render($view=null) {
    $this->loadView($view);
  }

  /**
   * nl2p
   *
   * newline to paragraph
   *
   * @access public 
   * @param string $string
   */
  public function nl2p($string) { 
        return '<p>' . preg_replace('#(<br\s*?/?>\s*?){2,}#', '</p>'."\n".'<p>', nl2br($string)) .'</p>'; 
    }

}

<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/

/** 
 * ToolController.php
 * 
 * Contains the {@link ToolController} class.
 * 
 * @package INFLUENCE 
 * @subpackage controllers
 */ 

/**
 * ContentController class
 *
 * @package INFLUENCE 
 * @subpackage controllers
 */
class ToolController extends Controller {


	public function cli_mediafix() {
		$model = $this->modelFactory->load("Media","INFLUENCE");
		$recs = $model->smartSelectAll();

		foreach($recs as $rec) {
			if($rec["vimeo_id"] > 0) {
				$data = file_get_contents("http://vimeo.com/api/v2/video/" . $rec["vimeo_id"] . ".php");
				$vimeoRec = unserialize($data);

				$mp3Size = 0;

				if($rec["mp3"]) {
					$mp3File = "http://" . $this->configuration->environment["INFLUENCE"]["media"]["server"] . "/" . $rec["type"] . "/" . $rec["mp3"];

					$ch = curl_init($mp3File);
					curl_setopt($ch, CURLOPT_NOBODY, true);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_HEADER, true);
					curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
					$data = curl_exec($ch);
					curl_close($ch);
					if ($data === false) {
					  echo 'cURL failed';
					  exit;
					}

					$contentLength = 'unknown';
					$status = 'unknown';
					if (preg_match('/^HTTP\/1\.[01] (\d\d\d)/', $data, $matches)) {
					  $status = (int)$matches[1];
					}
					if (preg_match('/Content-Length: (\d+)/', $data, $matches)) {
					  $contentLength = (int)$matches[1];
					  $mp3Size = $contentLength;
					}

				}

				$model->smartUpdate(array("duration"           => $this->sec2hms($vimeoRec[0]["duration"], true),
										  "mp3_size"           => $mp3Size,
										  "tags"               => $vimeoRec[0]["tags"],
										  "thumb"              => $vimeoRec[0]["thumbnail_medium"],
										  "vimeo_thumb_small"  => $vimeoRec[0]["thumbnail_small"],
										  "vimeo_thumb_medium" => $vimeoRec[0]["thumbnail_medium"],
										  "vimeo_thumb_large"  => $vimeoRec[0]["thumbnail_large"]),
										  array("vimeo_id" => $rec["vimeo_id"]));

			}
		}

		$this->setData($recs);
		$this->render();
	}


	public function cli_testMail() {
		require_once "Mail.php";


		$from = "\"" . $this->configuration->environment["EMAIL"]["igive"]["from"] . "\" <" . $this->configuration->environment["EMAIL"]["igive"]["username"] . ">";

		$to = "cjimti@gmail.com";
		$subject = "Test";
		$body = "Hi... ,\n\nThis is a test";

		$host = $this->configuration->environment["EMAIL"]["igive"]["host"];
		$port = $this->configuration->environment["EMAIL"]["igive"]["port"];
		$username = $this->configuration->environment["EMAIL"]["igive"]["username"];
		$password = $this->configuration->environment["EMAIL"]["igive"]["password"];

		$headers = array ('From' => $from,
						  'To' => $to,
						  'Subject' => $subject);


		$smtp = Mail::factory('smtp', array ('host' => $this->configuration->environment["EMAIL"]["igive"]["host"],
											 'port' => $this->configuration->environment["EMAIL"]["igive"]["port"],
											 'auth' => true,
											 'username' => $username,
											 'password' => $password));

		$mail = $smtp->send($to, $headers, $body);

		if (PEAR::isError($mail)) {
			$this->setData($mail->getMessage());
		} else {
			$this->setData("Message successfully sent!");
		}

		$this->render();
	}


	private function sec2hms($sec, $padHours = false) {
		// start with a blank string
		$hms = "";

		// do the hours first: there are 3600 seconds in an hour, so if we divide
		// the total number of seconds by 3600 and throw away the remainder, we're
		// left with the number of hours in those seconds
		$hours = intval(intval($sec) / 3600); 

		// add hours to $hms (with a leading 0 if asked for)
		$hms .= ($padHours)  ? str_pad($hours, 2, "0", STR_PAD_LEFT). ":" : $hours. ":";

		// dividing the total seconds by 60 will give us the number of minutes
		// in total, but we're interested in *minutes past the hour* and to get
		// this, we have to divide by 60 again and then use the remainder
		$minutes = intval(($sec / 60) % 60); 

		// add minutes to $hms (with a leading 0 if needed)
		$hms .= str_pad($minutes, 2, "0", STR_PAD_LEFT). ":";

		// seconds past the minute are found by dividing the total number of seconds
		// by 60 and using the remainder
		$seconds = intval($sec % 60); 

		// add seconds to $hms (with a leading 0 if needed)
		$hms .= str_pad($seconds, 2, "0", STR_PAD_LEFT);

		// done!
		return $hms;
	}

}

<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/

/** 
 * AdminController.php
 * 
 * Contains the {@link AdminController} class.
 * 
 * @package INFLUENCE 
 * @subpackage controllers
 */ 

/**
 * AdminController class
 *
 * @package VTUNNEL 
 * @subpackage controllers
 */
class AdminController extends Controller {

	/**
	* Package
	*
	*/
	protected $package = "INFLUENCE";

	/**
	* Public Models
	*
	*/
	protected $publicModels = array("content",
									"page",
									"pagecontent",
									"vwpagecontent"
									);

	/**
	* Pre Action
	*
	* preAction is called by the Controller before
	* dispatching to an action.
	*
	* @access public
	*/
	public function preAction() {
		parent::preAction();
	}

	/**
	* Cfg
	*
	* Returns the application or login html
	*
	* @access public
	*/
	public function web_cfg(ClientDataPkg $auth) {

		$user = $auth->getData();

		if(is_array($user) && array_key_exists("active", $user)) {
			if($user["active"] > 0) {
				$user["password"] = "********";
				$this->setData(array("user" => $user));
			}
		}

		$this->render();
	}

	/**
	* Home
	*
	*
	* @access public
	*/
	public function web_home(PayloadPkg $logout,
							PayloadPkg $username,
							PayloadPkg $password,
							ClientDataPkg $auth) {

		$this->web_app($logout, $username, $password, $auth);
	}


	/**
	* App
	*
	* Returns the application or login html
	*
	* @access public
	*/
	public function web_app(PayloadPkg $logout,
							PayloadPkg $username,
							PayloadPkg $password,
							ClientDataPkg $auth) {

		$u = $username->getString();
		$p = $password->getString();
		$a = $auth->getData();

		$this->logger->debug("Auth    : " . print_r($a,1));
		$this->logger->debug("Username: " . $u);
		$this->logger->debug("Password: " . $p);

		if($logout->getInt(0) > 0) {
			$this->setClientData('auth', null);
			$a = null;
		}

		// check username and password
		//
		if($u != "" && $p != "") {
			// go authenticate
			$m = $this->modelFactory->load("User", $this->package);
			$ret = $m->smartSelectOne(null, array("username" => $u, "password" => $p));
			if($ret["active"] > 0) {
				$this->setClientData('auth', $ret);
				$this->render("admin/app");
			} else {
				$this->render("admin/login");
			}
			return;
		}

		if(is_array($a) && array_key_exists("active", $a)) {
			if($a["active"] > 0) {
				$this->render("admin/app");
				return;
			}
		}

		$this->render("admin/login");

	}


	/**
	* Setup Node
	*
	* @param PayloadPkg $node string POST node UID
	* @access public
	*/
	public function web_setupNode(PayloadPkg $node) {

		$this->logger->debug("SETUP NODE: " . $node->getString());

		//$this->setData();
		$this->render();
	}


	/**
	* JSON Data Post
	*
	*
	*
	* @param PayloadPkg $model string GET model name
	* @param PayloadPkg $json  string GET json
	* @access public
	*/
	public function web_jsonDataPost(PayloadPkg $model,
									 PayloadPkg $json,
									 PayloadPkg $remove,
									 ClientDataPkg $auth) {

		$a = $auth->getData();
		if($a["active"] == 1 && $model->getString("None") != "None") {

			$m = $this->pluginFactory->load($model->getString(), "ICMAP");
			$m->process(json_decode($json->getData()), $remove->getInt(0));

		} else {

			$this->logger->error("-- USER IS NOT AUTHENTICATED OR NO MODEL SPECIFIED --");
			$this->setData(array("response" => "unauthorized"));

		}

		$this->setData(array("response" => $ret));
		$this->render();
	}

	/**
	* Get Models
	*
	* @access public
	*/
	public function web_getModels() {

		$mf = $this->pluginFactory->load("ModelFinder");

		$models = $mf->getModels($this->getPackage());

		for($i = 0; $i < count($models); $i++) {
			$model = $models[$i]["model"];
			if($model) {
				$m = $this->modelFactory->load($models[$i]["model"], $this->getPackage());
				$models[$i]["type"] = $m->getTableType();
			}
		}

		$this->setData($models);
		$this->render();
	}

	/**
	* Get Model Types
	*
	* @param PayloadPkg $model string GET model name
	* @access public
	*/
	public function web_getModelTypes(PayloadPkg $model) {

		$mf = $this->pluginFactory->load("ModelFinder");
		$modelTypes    = $mf->getModelTypes($this->getPackage(), $model->getString());

		$m = $this->modelFactory->load($model->getString(), $this->getPackage());

		$modelDefaults = $m->getDefaults();
		$autoIncrement = $m->getAutoIncrement();
		$modelType     = $m->getTableType();

		$this->setData(array("modelParamTypes"    => $modelTypes,
							 "modelParams"        => array_keys($modelTypes),
							 "modelDefaults"      => $modelDefaults,
							 "modelType"          => $modelType,
							 "modelAutoIncrement" => $autoIncrement));
		$this->render();
	}

	/**
	* Get Model Data
	*
	* @param PayloadPkg $model string GET model name
	* @param ApiBundle $api The ApiBundle Class
	* @access public
	*/
	public function web_getModelData(PayloadPkg $model,
									 ApiBundle $api,
									 ClientDataPkg $auth) {

		if(!$this->publicModelTypeCheck($model->getString())) {
			$this->render();
			return;
		}

		$a = $auth->getData();
		if($a["active"] == 1) {

			$api->where["userId"] = array($a["id"]);

			$api->load($model->getString(), $this->getPackage());
			$data = $api->getData();

			$this->setData($data);
		} else {
			$this->logger->error("-- USER IS NOT AUTHENTICATED --");
			$this->setData(array("message" => "unauthorized"));
		}
		$this->render();
	}

	/**
	* Check Public Service Models
	*
	* @param PayloadPkg $model string GET model name
	* @access protected
	* @return bool
	*/
	protected function publicModelTypeCheck($model) {

		if(in_array(strtolower($model), $this->publicModels)) {
			return true;
		}

		$this->logger->error(" WARNING: Unauthorized attempt to access a non-public model: " . strtolower($model));
		return false;
	}

	/**
	* Get Package
	*
	*
	* @access protected
	*/
	protected function getPackage() {
		if(isset($this->package)) {
			return $this->package;
		} else {
			return $this->configuration->framework["CORE"]["framework"]["package"];
		}
	}

}

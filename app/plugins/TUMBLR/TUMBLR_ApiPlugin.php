<?php
 /*

  _         _               _      
 | |_ _   _| |__  _ __ ___ | |_ __ 
 | __| | | | '_ \| '_ ` _ \| | '__|
 | |_| |_| | |_) | | | | | | | |   
  \__|\__,_|_.__/|_| |_| |_|_|_|   

 tumblr api utils

 cjimti@gmail.com

*/

/** 
 * TUMBLR_ApiPlugin.php
 * 
 * Contains the {@link TUMBLR_ApiPlugin} class.
 * 
 * @package TUMBLR 
 * @subpackage plugins
 */ 

/**
 * ApiPlugin class
 *
 * @package TUMBLR 
 * @subpackage plugins
 */
class TUMBLR_ApiPlugin extends Plugin {


	/**
	* Get Blocks
	*
	* 
	*/
	public function getBlocks($feed) {
		return $this->getCarousel($feed);
	}

	/**
	* Get Article
	*
	* 
	*/
	public function getArticle($feed, $offset=0) {
		$resp = $this->getPosts($feed, $offset, 1);
		$post = $resp->posts[0];

		return $this->parsePost($post);
	}

	/**
	* Get Carousel
	*
	* 
	*/
	public function getCarousel($feed) {
		$resp = $this->getPosts($feed);
		$carousel = array();

		foreach($resp->posts as $post) {

			$caption = explode("\n", strip_tags($post->caption), 2);

			$a = array("photo" => $post->photos[0]->original_size->url,
					   "title" => $caption[0] ? $caption[0] : "",
					   "blurb" => $caption[1] ? $caption[1] : "",
					   "url"   => $post->link_url);

			$carousel[$post->id] = $a;
		}

		return $carousel;
	}

	/**
	* Get Posts
	*
	* 
	*/
	public function getPosts($feed, $offset=0, $limit=20) {
		$url  = "http://api.tumblr.com/v2/blog/" . $feed . "/posts?offset=" . $offset . "&limit=" . $limit . "&id=" . $ref . $this->getKey();
		$data = file_get_contents($url);
		$resp = json_decode($data);

		// something went wrong! let's try to get the data from the database
		// 
		$tumblrCache = $this->modelFactory->load("TumblrCache", "INFLUENCE");

		// Did get get a valid response?
		// 
		if($resp && $resp->response) {
			// store data
			// 
			$tumblrCache->upsert($feed, $offset, $limit, $data);

			// store in db
			// return data
			return $resp->response;
		}

		// Someting went wrong. Let's see if we have a copy in the database
		//
		$this->logger->error("CONNECTION TO TUMBLR FAILED: " . $url);
		$row = $tumblrCache->getData($feed, $offset, $limit);
		$resp = json_decode($row['data']);
		return $resp->response;
		
	}

	/**
	* Parse Post
	*
	* 
	*/
	protected function parsePost($post) {
		$article = array("id"        => $post->id,
						 "slug"      => $post->slug,
						 "timestamp" => $post->timestamp,
						 "title"     => "");

		switch ($post->type) {

			case "text":
				$article = $this->parseTextPost($post, $article);
			break;

			case "photo":
				$article = $this->parsePhotoPost($post, $article);
			break;

			case "quote":
				$article = $this->parseQuotePost($post, $article);
			break;

			case "link":
				$article = $this->parseLinkPost($post, $article);
			break;

			case "chat":
				$article = $this->parseChatPost($post, $article);
			break;

			case "audio":
				$article = $this->parseAudioPost($post, $article);
			break;

			case "video":
				$article = $this->parseVideoPost($post, $article);
			break;

			default:

		}

		$article["body"] = $this->parseFilter($article["body"]);

		return $article;
	}

	/**
	* Parse Filter
	*
	* 
	*/
	public function parseFilter($articleBody) {

		$match = array();
		$replace = array();

		$match[0] = '/\<a\shref\=\"http\:\/\/vimeo\.com\/(\d+)\"\>http\:\/\/vimeo\.com\/\d+\<\/a\>/';

		$replace[0] = '<iframe src="http://player.vimeo.com/video/${1}?title=0&amp;portrait=0&amp;byline=0" style="padding-top: 10px; padding-bottom: 10px; width: 100%; height: 450px" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';

		$articleBody = preg_replace($match, $replace, $articleBody);

		return $articleBody;
	}

	/**
	* Parse Photo Post
	*
	* 
	*/
	protected function parsePhotoPost($post, $article) {
		$a = "";

		if($post->link_url && $post->link_url != "") {
			$a = '<a href="' . $post->link_url . '">';
		}
		$img = '<p>' . $a . '<img src="' . $post->photos[0]->original_size->url . '" /></p>';

		if($a != "") {
			$img .= "</a>";
		}

		$article["body"] = $img . "<p>" . $post->caption . "</p>";

		return $article;
	}

	/**
	* Parse Text Post
	*
	* 
	*/
	protected function parseTextPost($post, $article) {
		$article["title"] = $post->title;
		$article["body"]  = $post->body;

		return $article;
	}

	/**
	* Parse Quote Post
	*
	* 
	*/
	protected function parseQuotePost($post, $article) {
		$article["body"] = '<div class="hero-unit"><p>' . $post->text . '<p><p><small>' . $post->source . '</small></p></div>';
		return $article;
	}

	/**
	* Parse Link Post
	*
	* 
	*/
	protected function parseLinkPost($post, $article) {
		$article["body"] = '<div class="hero-unit"><p><a href="' . $post->url . '">' . $post->title . '</a></p></div>';
		return $article;
	}

	/**
	* Parse Chat Post
	*
	* 
	*/
	protected function parseChatPost($post, $article) {
		$article["title"] = $post->title;
		$article["body"] = '<dl class="dl-horizontal">';

		foreach($post->dialogue as $d) {
			$article["body"] .= '<dt>' . $d->name . '</dt>';
			$article["body"] .= '<dd>' . $d->phrase . '</dd>';
		}

		$article["body"] .= "</dl>";

		return $article;
	}

	/**
	* Parse Chat Post
	*
	* 
	*/
	protected function parseAudioPost($post, $article) {
		$article["body"] = '<p>' . $post->player . '</p><p>' . $post->caption . '</p>';
		return $article;
	}

	/**
	* Parse Video Post
	*
	* 
	*/
	protected function parseVideoPost($post, $article) {
		$article["body"]  = '<p>' . $post->player[1]->embed_code . '</p>';
		$article["body"] .= $post->caption;
		return $article;
	}

	/**
	* Get Key
	*
	* 
	*/
	protected function getKey() {
		return "&api_key=" . $this->configuration->environment["API"]["tumblr"]["key"];
	}

}

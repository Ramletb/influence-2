<?php
 /*
            _                 _ 
   ___  ___| | ___  _   _  __| |
  / __|/ __| |/ _ \| | | |/ _` |
  \__ \ (__| | (_) | |_| | (_| |
  |___/\___|_|\___/ \__,_|\__,_|

  influencechurch.org

*/

/** 
 * SCLOUD_ApiPlugin.php
 * 
 * Contains the {@link SCLOUD_ApiPlugin} class.
 * 
 * @package RECAP 
 * @subpackage plugins
 */ 

require_once(C_PATH_PLUGINS . 'SCLOUD/Services/Soundcloud.php');

/**
 * SCLOUD_ApiPlugin class
 *
 * @package SCLOUD 
 * @subpackage plugins
 */
class SCLOUD_ApiPlugin extends Plugin {

	/**
	* Get Tracks
	*
	* 
	*/
	public function getTracks() {

		$client = $this->getClient();

		$page_size = 100;

		// get first page of tracks
		$tracks = json_decode($client->get('users/' . $this->getUserId() . '/tracks', array(
			"filter" => "public",
			"limit"  => $page_size
		)));

		return $tracks;
	}

	/**
	* Get User Id
	*
	* 
	*/
	protected function getUserId() {
		return $this->configuration->application["SCLOUD"]["key"]["userId"];
	}


	/**
	* Request
	*
	* 
	*/
	protected function getClient() {

		$clientId     = $this->configuration->application["SCLOUD"]["key"]["clientId"];
		$clientSecret = $this->configuration->application["SCLOUD"]["key"]["clientSecret"];
		$userId       = $this->configuration->application["SCLOUD"]["key"]["userId"];

		$client = new Services_Soundcloud($clientId, $clientSecret);

		return $client;
	}

}

<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/

/** 
 * ICMAP_ContentPlugin.php
 * 
 * Contains the {@link ICMAP_ContentPlugin} class.
 * 
 * @package ICMAP 
 * @subpackage plugins
 */ 

require_once("ICMAP_BaseMapPlugin.php");

/**
 * ICMAP_ContentPlugin class
 *
 * @package ICMAP 
 * @subpackage plugins
 */
class ICMAP_ContentPlugin extends ICMAP_BaseMapPlugin {

	/**
	* Model
	*
	*/
	protected $model = "Content";

}

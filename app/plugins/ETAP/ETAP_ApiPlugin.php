<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/

/** 
 * ETAP_ApiPlugin.php
 * 
 * Contains the {@link ETAP_ApiPlugin} class.
 * 
 * @package ETAP 
 * @subpackage plugins
 */ 

require_once(C_PATH_PLUGINS . "nusoap/lib/nusoap.php");

/**
 * ICMAP_PagePlugin class
 *
 * @package ICMAP 
 * @subpackage plugins
 */
class ETAP_ApiPlugin extends Plugin {
	private $nsc = null;

	public $error = null;

	public $errorMessage = 'Critical System Error.<br /> Contact <a href="mailto:igive@influencechurch.org">igive@influencechurch.org</a> for assistance.';

	/**
	* Process Account
	*
	* 
	* @access public
	*/
	public function processAccount($props) {
		if(!$this->beginSession()) {
			return false;
		}

	}

	/**
	* Process Donation
	*
	* 
	* @access public
	*/
	public function processDonation($props) {
		if(!$this->beginSession()) {
			return false;
		}

		// Define account
		$account = array();
		$account["name"]        = trim($props["firstName"]) . " " . trim($props["lastName"]);
		$account["sortName"]    = trim($props["lastName"]) . ", " . trim($props["firstName"]);
		$account["personaType"] = "Personal";
		$account["address"]     = trim($props["address"]);
		$account["city"]        = trim($props["city"]);
		$account["state"]       = $props["state"];
		$account["postalCode"]  = trim($props["postalCode"]);
		$account["email"]       = trim($props["email"]);
		$account["shortSalutation"] = trim($props["firstName"]);
		$account["longSalutation"]  = $props["salutation"] . " " . trim($props["firstName"]) . " " . trim($props["lastName"]);

		// Define Phone
		$phone = array();
		$phone["type"] = "Mobile";
		$phone["number"] = $props["phone"];

		// Add Phone to Account
		$account["phones"] = array($phone);

		// CHECK FOR DUPLICATE ACCOUNT
		// 
		$existingAccount = $this->findDuplicate($account);

		if(is_array($existingAccount) && array_key_exists("ref", $existingAccount)) {
			$accountRef = $existingAccount["ref"];
		} else {
			// CREATE ACCOUNT
			//
			$accountRef = $this->addAccount($account);
			if(!$accountRef) {
				return false;
			}
		}

		// Define Transaction
		$trans = array();
		$trans["accountRef"] = $accountRef; // example: 1234.0.567812 (FROM: CREATE ACCOUNT)
		$trans["fund"]       = "General";
		$trans["amount"]     = $props["giftAmount"];;

		// Define CreditCard
		$cc = array();
		$cc["cardType"]        = $props["cardType"];
		$cc["number"]          = $props["cardNumber"];
		$cc["expirationMonth"] = $props["cardExpMonth"];
		$cc["expirationYear"]  = $props["cardExpYear"];
		$cc["cvv2"]            = $props["cvv2"];

		// Define Valuable
		$valuable = array();
		$valuable["type"] = 3; // 3 = Credit Card
		$valuable["creditCard"] = $cc;


		// ONE TIME
		//
		if($props["recurring"] < 1) {
			// Add Valuable to Gift
			$trans["valuable"] = $valuable;

			$response = $this->nsc->call("addAndProcessGift", array($trans, false));
			if($this->hasFault($response)) {
				return false;
			}

			//$this->logger->debug(print_r($trans,1));
			// If It's a ONE TIME gift we are done and can return
			return true;

		}


		// RECURRING
		//

		// Add scheduledValuable to transaction
		$trans["scheduledValuable"] = $valuable;

		// Define StandardPaymentSchedule
		$sps = array();
		$sps["processType"] = 1; // 1 - Automatic / 0 - Manual
		$sps["frequency"] = $props["recurring"];
		$sps["firstInstallmentDate"] = $this->formatDateAsDateTimeString(date("n/j/Y"), time());
		$sps["installmentAmount"] = $trans["amount"];

		// Add StandardPaymentSchedule to transaction
		$trans["schedule"] = $sps;

		//$this->logger->debug(print_r($trans,1));

		$response = $this->addRecurringGiftSchedule($trans);
		if(!$response || $this->hasFault($response)) {
			return false;
		}

		return true;
	}

	/**
	* Add & Process Gift
	*
	* @access public
	*/
	public function addRecurringGiftSchedule($gift) {
		$response = $this->nsc->call("addRecurringGiftSchedule", array($gift, false));
		$this->logger->debug("Recurring Gift Schedule: " . print_r($response,1));
		return $response;
	}

	/**
	* Add & Process Gift
	*
	* @access public
	*/
	public function addAndProcessGift($gift) {
		$response = $this->nsc->call("addAndProcessGift", array($gift, false));
		$this->logger->debug("Processed Gift: " . print_r($response,1));
		return $response;
	}

	/**
	* Add Account
	*
	* @access public
	*/
	public function addAccount($account) {
		$response = $this->nsc->call("addAccount", array($account, false));

		if($this->hasFault($response)) {
			return false;
		}

		$this->logger->debug("Added account: " . print_r($response,1));
		return $response;
	}

	/**
	* Find Duplicate Account
	*
	* @access public
	*/
	public function findDuplicate($account) {

		$das = array();
		$das["name"] = $account["name"];
		$das["address"] = $account["address"];
		$das["email"] = $account["email"];
		$das["accountRoleTypes"] = 1;
		$das["allowEmailOnlyMatch"] = true;

		$response = $this->nsc->call("getDuplicateAccount", array($das));

		if($this->hasFault($response)) {
			return false;
		}

		$this->logger->debug("Found account: " . print_r($response,1));
		return $response;
	}

	/**
	* Get Account By Id
	*
	* @access public
	*/
	public function getAccountById($id) {
		$response = $this->nsc->call("getAccountById", array($id));
		return $response;
	}

	/**
	* Begin Session
	*
	* @access public
	*/
	public function beginSession() {
		if(!$this->nsc) {
			return $this->startEtapestrySession();
		} else {
			return $this->nsc;
		}
	}

	/**
	* End Session
	*
	* @access public
	*/
	public function endSession() {
		if($this->nsc) {
			$this->stopEtapestrySession();
			$this->nsc = null;
		}
		return true;
	}


	/**
	 * Format Date
	 *
	 * Take a United States formatted date (mm/dd/yyyy) and
	 * convert it into a date/time string that NuSoap requires.
	 *
	 * @param string $dateStr Date String
	 * @access public
	 */
	public function formatDateAsDateTimeString($dateStr) {
		if ($dateStr == null || $dateStr == "") return "";
		if (substr_count($dateStr, "/") != 2) return "[Invalid Date: $dateStr]";

		$separator1 = stripos($dateStr, "/");
		$separator2 = stripos($dateStr, "/", $separator1 + 1);

		$month = substr($dateStr, 0, $separator1);
		$day   = substr($dateStr, $separator1 + 1, $separator2 - $separator1 - 1);
		$year  = substr($dateStr, $separator2 + 1);

		return ($month > 0 && $day > 0 && $year > 0) ? date(DATE_ATOM, mktime(0, 0, 0, $month, $day, $year)) : "[Invalid Date: $dateStr]";
	}

	/**
	* Check Response
	*
	* @access private
	*/
	private function hasFault($resp) {
		if(is_array($resp) && array_key_exists("faultcode", $resp)) {
			$this->logger->debug("ETAP API FAULT: " . print_r($resp,1));
			if($resp["faultcode"] == "13") {
				$this->errorMessage = $resp["faultstring"];
			}
			// TODO: write a reasonable error message
			return true;
		}

		return false;
	}

	/**
	* Start an eTapestry API session by instantiating a
	* nusoap_client instance and calling the login method.
	*
	* @access private
	*/
	private function startEtapestrySession() {
		$this->logger->debug(" ->>>>>>>>>>>>>>>>>>>>>> START SESSION <<<<<<<<<<<<<- ");

		$loginId = $this->configuration->environment["API"]["key"]["etap"]["user"];
		$password =  $this->configuration->environment["API"]["key"]["etap"]["pass"];
		$endpoint = "https://sna.etapestry.com/v2messaging/service?WSDL";

		$nsc = new nusoap_client($endpoint, true);
		$newEndpoint = $nsc->call("login", array($loginId, $password));

		$this->checkStatus($nsc);

		// Determine if the login method returned a value...this will occur
		// when the database you are trying to access is located at a different
		// environment that can only be accessed using the provided endpoint
		if ($newEndpoint != "" && !is_array($newEndpoint)) {
			$this->logger->debug("NEW END POINT: " . $newEndpoint);

			// Instantiate nusoap_client with different endpoint
			$this->logger->debug("Establishing NuSoap Client with new endpoint...");
			$nsc = new nusoap_client($newEndpoint, true);
			$this->logger->debug("Done");

			// Did an error occur?
			$this->checkStatus($nsc);

			// Invoke login method
			$this->logger->debug("Calling login method...");
			try {
				$nsc->call("login", array($loginId, $password));
			} catch (Exception $e) {
				$this->logger->debug(print_r(e,1));
			}
			$this->logger->debug("Done");

			// Did a soap fault occur?
			$this->checkStatus($nsc);
		} else {
			$this->error = $newEndpoint;
			return false;
		}

		$this->nsc = $nsc;
		return true;
	}

	/**
	* Stop an eTapestry API session by calling the logout
	* method given a nusoap_client instance.
	*
	* @access private
	*/
	private function stopEtapestrySession() {
		// Invoke logout method
		$this->logger->debug("Calling logout method...");
		$this->nsc->call("logout");
		$this->logger->debug(" ->>>>>>>>>>>>>>>>>>>>>> END SESSION <<<<<<<<<<<<<- ");

	}


	/**
	 * Utility method to determine if a NuSoap fault or error occurred.
	 * If so, output any relevant info and stop the code from executing. 
	 *
	 * @access private
	 */
	private function checkStatus($nsc) {
		if ($nsc->fault || $nsc->getError()) {
			if (!$nsc->fault) {
				$this->logger->debug("Error: " . $nsc->getError());
			} else {
				$this->logger->debug("Fault Code: ".$nsc->faultcode);
				$this->logger->debug("Fault String: ".$nsc->faultstring);
			}
		}
	}

}

<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/

/** 
 * RECAP_RecapPlugin.php
 * 
 * Contains the {@link RECAP_RecapPlugin} class.
 * 
 * @package RECAP 
 * @subpackage plugins
 */ 

require_once(C_PATH_PLUGINS . "RECAP/recaptchalib.php");

/**
 * RECAP_RecapPlugin class
 *
 * @package RECAP 
 * @subpackage plugins
 */
class RECAP_RecapPlugin extends Plugin {

	/**
	* Check Answer
	*
	* @param string $privkey
	* @param string $remoteip
	* @param string $challenge
	* @param string $response
	* @param array $extra_params an array of extra variables to post to the server
	* @return ReCaptchaResponse
	* @access public
	*/
	public function checkAnswer($privkey, $remoteip, $challenge, $response, $extra_params = array()) {
		return $resp = recaptcha_check_answer($privkey, $remoteip, $challenge, $response, $extra_params);
	}


}

<?php
 /*
  _        __ _                           
 (_)_ __  / _| |_   _  ___ _ __   ___ ___ 
 | | '_ \| |_| | | | |/ _ \ '_ \ / __/ _ \
 | | | | |  _| | |_| |  __/ | | | (_|  __/
 |_|_| |_|_| |_|\__,_|\___|_| |_|\___\___|

 influencechurch.org

*/

/** 
 * REF_RefDataPlugin.php
 * 
 * Contains the {@link REF_RefDataPlugin} class.
 * 
 * @package REF 
 * @subpackage plugins
 */ 

/**
 * REF_RefDataPlugin class
 *
 * @package REF 
 * @subpackage plugins
 */
class REF_RefDataPlugin extends Plugin {


	/**
	* Get Data
	*
	* @param string $view
	* @param string $ref
	* @return array
	* @access public
	*/
	public function getData($view, $ref) {

		if($view == "watch") {

			$mPageMedia = $this->modelFactory->load("VwPageMedia","INFLUENCE");
			$data       = $mPageMedia->smartSelectOne(null, array("media_vimeo_id" => $ref));
			$baseUrl    = "http://" . $this->configuration->environment["INFLUENCE"]["media"]["server"];

			$data['media_base_url'] = $baseUrl . "/" . $data["media_type"] . "/";

			return $data;
		}


	}


	/**
	* Get Content
	*
	* @param string $feed
	* @param string $ref
	* @return array
	* @access public
	*/
	public function getContent($feed, $ref) {
	  if(!$feed || $feed =="") {
	    $feed = "content.feed.influencemedia.org";
	  }

		$url  = "http://api.tumblr.com/v2/blog/" . $feed . "/posts?id=" . $ref;
		$url .= "&api_key=" . $this->configuration->environment["API"]["tumblr"]["key"];

		$contents = file_get_contents($url);
		$contentsLength = strlen($contents);

		// RETRY
		if(!$contents || $contentsLength < 2) {
		  $url  = "http://api.tumblr.com/v2/blog/content.feed.influencemedia.org/posts?id=" . $ref;
		  $url .= "&api_key=" . $this->configuration->environment["API"]["tumblr"]["key"];
		  // $this->logger->error("RETRY: " . $url);
		  $contents = file_get_contents($url);
		  $contentsLength = strlen($contents);
		}

		$refCache = $this->modelFactory->load("RefCache", "INFLUENCE");

		// STORE DATA
		if($contentsLength > 2) {
			$refCache->upsert($feed, $ref, $contents);
		}

		// CHECK LOCAL DB
		if(!$contents || $contentsLength < 2) {
			$rec = $refCache->smartSelectOne(array('content'), array('ref' => $ref));
			if($rec) {
				$contents = $rec["content"];
			}
		}

		$data = json_decode($contents);

		$tumblr = $this->pluginFactory->load("Api", "TUMBLR");
		for($i=0; $i< count($data->response->posts); $i++) {
			$data->response->posts[$i]->body = $tumblr->parseFilter($data->response->posts[$i]->body);
		}

		return $data;
	}

}
